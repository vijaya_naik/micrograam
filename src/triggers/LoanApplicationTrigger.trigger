trigger LoanApplicationTrigger on Loan_Application__c (after insert, after update) {
    if(Trigger.isAfter && Trigger.isInsert){
        LoanApplicationTriggerHanlder.onAfterInsert(Trigger.new);
    }
    
    
    if(Trigger.isAfter && Trigger.isUpdate){
        LoanApplicationTriggerHanlder.onAfterUpdate(Trigger.new, Trigger.oldMap);
    }
}