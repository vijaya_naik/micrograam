trigger IntegrationMessageTrigger on Integration_Message__c (before insert, before update, after insert) {
    if(Trigger.isBefore && Trigger.isInsert){
        IntegrationMessageTriggerHandler.onBeforeInsert(Trigger.new);
    }
    
    
    if(Trigger.isBefore && Trigger.isUpdate){
        IntegrationMessageTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
    }
    
    
    if(Trigger.isAfter && Trigger.isInsert){
        IntegrationMessageTriggerHandler.onAfterInsert(Trigger.new);
    }
}