trigger ApplicantTrigger on Applicant__c (after insert,after update) {
     if(Trigger.isAfter && Trigger.isInsert){
        ApplicantTriggerHandler.onAfterInsert(Trigger.new);
    }
    
    
    if(Trigger.isAfter && Trigger.isUpdate){
        ApplicantTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
    }

}