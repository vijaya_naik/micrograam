@isTest(seeAllData=false)
public class CreditCategoriesMasterResourceTest  {
    static Credit_Categories_Master__c creditMstr;
    private static final String ORG_URL = URL.getSalesforceBaseUrl().toExternalForm() ;
     
    static void init() {
        creditMstr = new Credit_Categories_Master__c();
        creditMstr.Category_Name__c='xyz';
        creditMstr.Category_Order__c=4;
        creditMstr.Category_Weight__c=3;
        insert creditMstr;
     }
     static testMethod void testDoPost() {
      Map<String, Object> paramMap = new Map<String, Object>();
      paramMap.put('categoryName','123');
      paramMap.put('categoryWeight','3');
      paramMap.put('categoryOrder','345');
      String JsonMsg=JSON.serialize(paramMap);
      RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/v1/creditCategoriesMaster';  //Request URL
      req.httpMethod = 'POST';
      req.requestBody = Blob.valueof(JsonMsg);
      RestContext.request = req;
      RestContext.response= res;
    
      Map<String,String> results = CreditCategoriesMasterResource.cCreditCategoriesMaster();

  }
  
   static testMethod void testDoGet() {
        init();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = ORG_URL+'/services/apexrest/v1/creditCategoriesMaster'; 
        req.httpMethod = 'GET';
        req.addParameter('id', creditMstr.Id);      
        RestContext.request = req;
        RestContext.response = res;
        Map<String,String> results=new Map<String,String>();
        results = CreditCategoriesMasterResource.getCreditCategoriesMaster();
  }
  
    static testMethod void testDoDelete() {
    init();
    RestRequest req = new RestRequest(); 

    req.requestURI = ORG_URL+'/services/apexrest/v1/creditCategoriesMaster';  
    req.httpMethod = 'DELETE';
    req.addParameter('id', creditMstr.Id);    
    RestContext.request = req;
    CreditCategoriesMasterResource.deleteCreditCategoriesMaster();  
    
  } 
  
   static testMethod void testDoPatch() {
      init();
      Map<String, Object> paramMap = new Map<String, Object>();
      paramMap.put('categoryName','123');
      paramMap.put('categoryWeight','3');
      paramMap.put('categoryOrder','345');
      String JsonMsg=JSON.serialize(paramMap);
      RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/v1/creditCategoriesMaster';  
      req.httpMethod = 'PATCH';
      req.addParameter('id', creditMstr.Id);      
      req.requestBody = Blob.valueof(JsonMsg);
      RestContext.request = req;
      RestContext.response= res;
    
      Map<String,String> results=new Map<String,String>();
      results = CreditCategoriesMasterResource.updateCreditCategoriesMaster();

  }
}