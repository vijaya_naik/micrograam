public class PsychometricService {
  public static Application_Psychometric__c getPsychometric(String Id) {
        Application_Psychometric__c psychometric = [
            SELECT Id,Loan_Application_Id__c,
            Psychometric_Test_Score__c,Source__c, Ability_To_Repay__c, Willing_To_Repay__c
            FROM Application_Psychometric__c 
            WHERE Id = :Id
        ];
        return psychometric;

    }
      public static Application_Psychometric__c getPsychometric(String loanApplicationId,String source) {
        Application_Psychometric__c psychometric = [
            SELECT Id,Loan_Application_Id__c,
            Psychometric_Test_Score__c,Source__c, Ability_To_Repay__c, Willing_To_Repay__c
            FROM Application_Psychometric__c 
            WHERE Loan_Application_Id__c = :loanApplicationId and Source__c=:source
        ];
        return psychometric;

    }

    public static Map<String, Map<String,String>> getPsychometrics(String loanApplicationId) {
        Map<String, Map<String,String>> psychometricssMap = new Map<String, Map<String,String>>();
        // Create query string to get Social Media records
 
        
        List<Application_Psychometric__c> psychometricss = [
            SELECT Id,Loan_Application_Id__c,
            Psychometric_Test_Score__c,Source__c, Ability_To_Repay__c, Willing_To_Repay__c
            FROM Application_Psychometric__c 
            WHERE Loan_Application_Id__c = :loanApplicationId
        ];        
        System.debug('getLoanApplications ResultSize=' + psychometricss.size());
        
        // Create Result Map
        for ( Application_Psychometric__c psychometrics : psychometricss ) {
            Map<String,String> psychometricsMap = PsychometricService.getPsychometricDenormalized(psychometrics);
            // Get additional data for Borrower
            psychometricssMap.put(psychometrics.Source__c, psychometricsMap);
        }    
        return psychometricssMap;
         }
            public static Map<String,String> createPsychometric(Map<String, Object> params) {
        //Create Application_KYC instance & set fields
        Application_Psychometric__c psychometric = createPsychometricInstance(params, null);
       
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
      
            insert psychometric;
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
        
        return getPsychometricDenormalized(psychometric);
    }
    
    public static Application_Psychometric__c createPsychometricInstance(Map<String, Object> params, Application_Psychometric__c psychometric) {
        if (psychometric == null)    psychometric = new Application_Psychometric__c(); //For new record creation
        boolean hasValue = false;
        if (params.containsKey('psychometricTestScore')){
            psychometric.Psychometric_Test_Score__c=String.valueOf(params.get('psychometricTestScore'));
            hasValue=true;
        }
        if (params.containsKey('abilityToRepay')){
            psychometric.Ability_To_Repay__c=String.valueOf(params.get('abilityToRepay'));
            hasValue=true;
        }
        if (params.containsKey('willingnessToRepay')){
            psychometric.Willing_To_Repay__c=String.valueOf(params.get('willingnessToRepay'));
            hasValue=true;
        }
        if (params.containsKey('source')) psychometric.Source__c=String.valueOf(params.get('source'));
        if (params.containsKey('loanApplicationId')) psychometric.Loan_Application_Id__c=String.valueOf(params.get('loanApplicationId'));
            
        if(hasValue)
            return psychometric;
        else 
            return null;
    }
    
    public static Map<String,String> getPsychometricDenormalized(Application_Psychometric__c psychometric) {
        Map<String,String> psychometricMap = new Map<String,String>();
        
        if (psychometric.Source__c != null) psychometricMap.put('source', psychometric.Source__c);
        if (psychometric.Psychometric_Test_Score__c != null) psychometricMap.put('psychometricTestScore', psychometric.Psychometric_Test_Score__c);
        if (psychometric.Ability_To_Repay__c != null) psychometricMap.put('abilityToRepay', psychometric.Ability_To_Repay__c);
        if (psychometric.Willing_To_Repay__c != null) psychometricMap.put('willingnessToRepay', psychometric.Willing_To_Repay__c);
        if (psychometric.Loan_Application_Id__c != null) psychometricMap.put('loanApplicationId', String.valueOf(psychometric.Loan_Application_Id__c));
		psychometricMap.put('id', String.valueOf(psychometric.Id));

        return psychometricMap;
    }
    
    public static void deletePsychometric(String psychometricId) {
        //Get account record
        Application_Psychometric__c psychometric = [SELECT Id FROM Application_Psychometric__c WHERE Application_Psychometric__c.Id = :psychometricId];
        //Get permanent address record
        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
          
            delete psychometric; //Delete applicant record
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
    
    public static Map<String,String> updatePsychometric(String psychometricId, Map<String, Object> params) {
                Application_Psychometric__c psychometric =null;
       	if(psychometricId!=null)  psychometric=getPsychometric(psychometricId);
       	        psychometric = createPsychometricInstance(params, psychometric);
       	
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
              update psychometric;
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
        
        return getPsychometricDenormalized(psychometric);
    }
    
    public static void deletePsychometricData(String loanApplicationId) {
        List<Application_Psychometric__c> psychometrics = [
            SELECT Id FROM Application_Psychometric__c 
            WHERE Loan_Application_Id__c = :loanApplicationId
        ];        
        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
            delete psychometrics;
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
}