public class CompanyManagerService {
public static Company_Manager__c getCompanyManagerById(String companyManagerId) {
    Company_Manager__c companyManagerDetail = [
        SELECT id,name,DIN__c,Loan_Application_Id__c,Manager_Address__C,
        Company_Manager_Designation__c, Work_Experience__c
        from Company_Manager__c
        WHERE Id = :companyManagerId
    ];
    return companyManagerDetail;
}
    public static List<Company_Manager__c> getCompanyManagerByLoanId(String loanApplicationId) {
    List<Company_Manager__c> companyManagerDetail = [
        SELECT id,name,DIN__c,Loan_Application_Id__c,Manager_Address__C,
        Company_Manager_Designation__c, Work_Experience__c
        from Company_Manager__c
        WHERE Loan_Application_Id__c = :loanApplicationId
    ];
    return companyManagerDetail;
}
       public static Company_Manager__c createCompanyManager(Map<String, Object> params,Company_Manager__c  companyManager) {
    if(companyManager==null)  companyManager= new Company_Manager__c();

    if(params.containsKey('din'))
        companyManager.DIN__c = String.valueOf(params.get('din'));
        
    if(params.containsKey('loanApplicationId'))
    	companyManager.Loan_Application_Id__c= String.valueOf(params.get('loanApplicationId'));
    
    if(params.containsKey('workExperience'))
    	companyManager.Work_Experience__c=Double.valueOf(params.get('workExperience'));
        
    if(params.containsKey('companyManagerName'))
    	companyManager.Name=String.valueOf(params.get('companyManagerName'));
        

    if(params.containsKey('companyManagerDesignation'))
    	companyManager.Company_Manager_Designation__c=String.valueOf(params.get('companyManagerDesignation'));
        
    if(companyManager.Company_Manager_Designation__c ==null && companyManager.Name==null && companyManager.Work_Experience__c ==null
      && companyManager.DIN__c==null)
        return null;
     else
		return companyManager;

}
       public static Map<String,String> getCompanyManagerDenormalized(Company_Manager__c companyManager) {
    Map<String,String> companyManagerMap = new Map<String,String>();
    if(companyManager!=null){
        if (companyManager.DIN__c != null) companyManagerMap.put('companyManagerPAN', String.valueOf(companyManager.DIN__c));
        if (companyManager.Loan_Application_Id__c !=null)companyManagerMap.put('loanApplicationId',String.valueOf(companyManager.Loan_Application_Id__c));
        if (   companyManager.Work_Experience__c!= null)companyManagerMap.put('workExperience',String.valueOf(companyManager.Work_Experience__c));
        if (   companyManager.Name!=null)companyManagerMap.put('companyManagerName',String.valueOf(companyManager.Name));
        if (   companyManager.Company_Manager_Designation__c!=null)companyManagerMap.put('companyManagerDesignation',String.valueOf(companyManager.Company_Manager_Designation__c));

        if (   companyManager.Manager_Address__c != null) {
            Address__c currentOfficeAddress = AddressService.getAddressById(companyManager.Manager_Address__c);
            companyManagerMap.putAll(AddressService.getManagerAddressDenormalized(currentOfficeAddress));
        }
    }
    return companyManagerMap;
}
           public static List<Map<String,String>> getCompanyManagerDenormalized(String loanApplicationId) {
               List<Map<String,String>> retval = new List<Map<String,String>>();
               List<Company_Manager__c> managers=getCompanyManagerByLoanId(loanApplicationId);
               for (Integer i =0;i< managers.size();i++){
                   Map<String,String> companyManagerMap = getCompanyManagerDenormalized(managers.get(i));
                   retval.add(companyManagerMap);
               }
 
    	return retval;
}
    
    public static void deleteWithLoanApplicationId(String loanApplicationId) {
		List<Company_Manager__c> companyManager=getCompanyManagerByLoanId(loanApplicationId);
        //List<Company_Owner__c> companyOwner=dto.companyOwner;
        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
 
            if(companyManager!=null) {
                for(integer i=0;i<companyManager.size();i++){
                    if ( companyManager.get(i).Manager_Address__c != null ) {
                    	AddressService.deleteAddress(companyManager.get(i).Manager_Address__c);
                    }
                    delete companyManager.get(i);
                }
            }
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
            public static void createCompanyManager(List<Map<String, Object>> params) {
            for (integer i=0;i<params.size();i++){
                 createCompanyManager(params.get(i),null);
            }
            
        }
}