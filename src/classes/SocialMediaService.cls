public class SocialMediaService {
    public static Application_Social_Media__c getSocialMedia(String Id) {
        Application_Social_Media__c socialMedia = [
            SELECT Id, Loan_Application_Id__c,
            Social_Media_Score__c, Social_Media_Score_Percentage__c, Source__c
            FROM Application_Social_Media__c 
            WHERE Id = :Id
        ];
        return socialMedia;
    }
    
    public static Application_Social_Media__c getSocialMedia(String loanApplicationId,String source) {
        Application_Social_Media__c socialMedia = [
            SELECT Id, Loan_Application_Id__c,
            Social_Media_Score__c, Social_Media_Score_Percentage__c, Source__c
            FROM Application_Social_Media__c 
            WHERE Loan_Application_Id__c = :loanApplicationId and Source__c=:source
        ];
        return socialMedia;
    }
    
    public static Map<String, Map<String,String>> getSocialMedias(String loanApplicationId) {
        Map<String, Map<String,String>> socialMediasMap = new Map<String, Map<String,String>>();
        // Create query string to get Social Media records
        List<Application_Social_Media__c> socialMedias = [
            SELECT Id, Loan_Application_Id__c,
            Social_Media_Score__c, Social_Media_Score_Percentage__c, Source__c
            FROM Application_Social_Media__c 
            WHERE Loan_Application_Id__c = :loanApplicationId 
        ];        
        System.debug('getLoanApplications ResultSize=' + socialMedias.size());
        
        // Create Result Map
        for ( Application_Social_Media__c socialMedia : socialMedias ) {
            Map<String,String> socialMediaMap = SocialMediaService.getSocialMediaDenormalized(socialMedia);
            // Get additional data for Borrower
            socialMediasMap.put(socialMedia.Source__c, socialMediaMap);
        }
        
        return socialMediasMap;
    }
    
    public static Map<String,String> createSocialMedia(Map<String, Object> params) {
        //Create Application_KYC instance & set fields
        Application_Social_Media__c socialMedia = createSocialMediaInstance(params, null);
       
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
            insert socialMedia;
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
        
        return getSocialMediaDenormalized(socialMedia);
    }
    
    public static Application_Social_Media__c createSocialMediaInstance(Map<String, Object> params, Application_Social_Media__c socialMedia) {
        if (socialMedia == null)    socialMedia = new Application_Social_Media__c(); //For new record creation
        boolean hasValue = false;
        if (params.containsKey('socialMediaScore')){
            socialMedia.Social_Media_Score__c=String.valueOf(params.get('socialMediaScore'));
            hasValue=true;
        }
        if (params.containsKey('socialMediaScorePercentage')){
            socialMedia.Social_Media_Score_Percentage__c=Decimal.valueOf(String.valueOf(params.get('socialMediaScorePercentage')));
            hasValue=true;
        }
        if (params.containsKey('source'))
            socialMedia.Source__c=String.valueOf(params.get('source'));
        if (params.containsKey('loanApplicationId'))
            socialMedia.Loan_Application_Id__c=String.valueOf(params.get('loanApplicationId'));
        
        if(hasValue)
            return socialMedia;
        else 
            return null;
    }
    
    public static Map<String,String> getSocialMediaDenormalized(Application_Social_Media__c socialMedia) {
        Map<String,String> socialMediaMap = new Map<String,String>();
        
        if (socialMedia.Source__c != null) socialMediaMap.put('source', socialMedia.Source__c);
        if (socialMedia.Social_Media_Score__c != null) socialMediaMap.put('socialMediaScore', socialMedia.Social_Media_Score__c);
        if (socialMedia.Social_Media_Score_Percentage__c != null) socialMediaMap.put('socialMediaScorePercentage', String.valueOf(socialMedia.Social_Media_Score_Percentage__c));
        if (socialMedia.Loan_Application_Id__c != null) socialMediaMap.put('loanApplicationId', String.valueOf(socialMedia.Loan_Application_Id__c));
        socialMediaMap.put('id', String.valueOf(socialMedia.Id));

        return socialMediaMap;
    }
    
    public static Map<String,String> updateSocialMedia(String socialMediaId, Map<String, Object> params) {
                Application_Social_Media__c socialMedia =null;
        if(socialMediaId!=null)  socialMedia=getSocialMedia(socialMediaId);
                socialMedia = createSocialMediaInstance(params, socialMedia);
        
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
              update socialMedia;
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
        
        return getSocialMediaDenormalized(socialMedia);
    }
    
    public static void deleteSocialMedia(String socialMediaId) {
        //Get applicant record
        Application_Social_Media__c dto=SocialMediaService.getSocialMedia(socialMediaId);

        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
             delete dto; //Delete applicant record
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
    
    public static void deleteSocialMediaData(String loanApplicationId) {
        List<Application_Social_Media__c> socialMedias = [
            SELECT Id FROM Application_Social_Media__c 
            WHERE Loan_Application_Id__c = :loanApplicationId 
        ];        
        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
            delete socialMedias;
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
}