global class IntegrationAaadhaarBridgeTestMock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        HTTPResponse res = new HTTPResponse();
        res.setBody('{"kyc":{"photo": "","poi":{"name": "Test Test","dob":"05-06-1991","gender":"M","phone":"1234567890","email": "test.test@test.com.test"},"poa":{"co": "Test","vtc":"A","dist":"B","state":"C","pc":"123456","po":"D"},"local-data":{},"raw-CmpResp":""},"aadhaar-id": "772267219669","success": true,"aadhaar-reference-code":"aaa"}');
        res.setStatus('OK');
        return res;
    }
}