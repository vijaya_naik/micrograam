public class AttachmentService {
    public static Map<String,String> uploadApplicantAttachment(String accountId, String attachmentType, Map<String, Object> params) {
        //Get applicant record
        Applicant__c applicant = ApplicantService.getApplicantForAccountId(accountId);

        //Get Applicant_Attachment record (if exists)
        Applicant_Attachment__c applicantAttachment = getAttachmentByAccountIdAndType(accountId, attachmentType);
        if ( applicantAttachment == null ) { //Otherwise create new
            applicantAttachment = new Applicant_Attachment__c();
            applicantAttachment.Applicant_Id__c = applicant.Id;
            applicantAttachment.Attachment_Type__c = attachmentType;
            if (params.containsKey('attachmentComments')) 
                applicantAttachment.Attachment_Comments__c = String.valueOf(params.get('attachmentComments'));
        }

        //Get attachment record (if exists)
        Attachment attachment = null;
        Attachment[] attachments = [SELECT Id FROM Attachment WHERE Id = :applicantAttachment.Attachment_Id__c];
        if ( attachments.size() == 0 ) { //Otherwise create new
            attachment = new Attachment();
        	attachment.ParentId = applicant.Id;
        } else {
            attachment = attachments[0];
        }
        attachment.Name = String.valueOf(params.get('Name'));
        attachment.Description = attachmentType;
        attachment.Body = EncodingUtil.base64Decode(String.valueOf(params.get('Body'))); //Convert the string to blob
        
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
            upsert attachment; //Create/update the actual Attachment record
            applicantAttachment.Attachment_Id__c = attachment.Id;
            upsert applicantAttachment; //Create/update the Attachment_Id in Applicant_Attachment record
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
        
        return getApplicantAttachmentDenormalized(accountId, applicantAttachment);
    }

    public static Map<String,String> uploadLoanApplicationAttachment(String loanApplicationId, String attachmentType, Map<String, Object> params) {
        //Get Loan_Application_Attachment record (if exists)
        Loan_Application_Attachment__c loanApplicationAttachment = getAttachmentByLoanApplicationIdAndType(loanApplicationId, attachmentType);
        if ( loanApplicationAttachment == null ) { //Otherwise create new
            loanApplicationAttachment = new Loan_Application_Attachment__c();
            loanApplicationAttachment.Loan_Application_Id__c = loanApplicationId;
            loanApplicationAttachment.Attachment_Type__c = attachmentType;
            if (params.containsKey('attachmentComments')) 
                loanApplicationAttachment.Attachment_Comments__c = String.valueOf(params.get('attachmentComments'));
        }

        //Get attachment record (if exists)
        Attachment attachment = null;
        Attachment[] attachments = [SELECT Id FROM Attachment WHERE Id = :loanApplicationAttachment.Attachment_Id__c];
        if ( attachments.size() == 0 ) { //Otherwise create new
            attachment = new Attachment();
        	attachment.ParentId = loanApplicationId;
        } else {
            attachment = attachments[0];
        }
        attachment.Name = String.valueOf(params.get('Name'));
        attachment.Description = attachmentType;
        attachment.Body = EncodingUtil.base64Decode(String.valueOf(params.get('Body'))); //Convert the string to blob
        
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
            upsert attachment; //Create/update the actual Attachment record
            loanApplicationAttachment.Attachment_Id__c = attachment.Id;
            upsert loanApplicationAttachment; //Create/update the Attachment_Id in Loan_Application_Attachment record
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
        
        return getLoanApplicationAttachmentDenormalized(loanApplicationId, loanApplicationAttachment);
    }
    
    public static String downloadApplicantAttachment(String accountId, String attachmentType) {
        //Get Applicant_Attachment record
        Applicant_Attachment__c applicantAttachment = getAttachmentByAccountIdAndType(accountId, attachmentType);
        if ( applicantAttachment == null ) {
            throw new MyCustomException('Attachment not found for accountId=' + accountId + ', attachmentType=' + attachmentType);
        }
        
        //Get attachment record
        Attachment attachment = [SELECT Id, Body FROM Attachment WHERE Id = :applicantAttachment.Attachment_Id__c];
		//Return the blob content as string
        return EncodingUtil.base64Encode(attachment.Body);
    }

    public static String downloadLoanApplicationAttachment(String loanApplicationId, String attachmentType) {
        //Get Loan_Application_Attachment record
        Loan_Application_Attachment__c loanApplicationAttachment = getAttachmentByLoanApplicationIdAndType(loanApplicationId, attachmentType);
        if ( loanApplicationAttachment == null ) {
            throw new MyCustomException('Attachment not found for loanApplicationId=' + loanApplicationId + ', attachmentType=' + attachmentType);
        }
        
        //Get attachment record
        Attachment attachment = [SELECT Id, Body FROM Attachment WHERE Id = :loanApplicationAttachment.Attachment_Id__c];
		//Return the blob content as string
        return EncodingUtil.base64Encode(attachment.Body);
    }
    
    public static void deleteApplicantAttachment(String accountId, String attachmentType) {
        //Get Applicant_Attachment record
        Applicant_Attachment__c applicantAttachment = getAttachmentByAccountIdAndType(accountId, attachmentType);
        if ( applicantAttachment == null ) {
            throw new MyCustomException('Attachment not found for accountId=' + accountId + ', attachmentType=' + attachmentType);
        }
        
        //Get attachment record
        Attachment attachment = [SELECT Id FROM Attachment WHERE Id = :applicantAttachment.Attachment_Id__c];

        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
            delete attachment; //Delete attachment record
            delete applicantAttachment; //Delete Applicant_Attachment record
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }

    public static void deleteLoanApplicationAttachment(String loanApplicationId, String attachmentType) {
        //Get Loan_Application_Attachment record
        Loan_Application_Attachment__c loanApplicationAttachment = getAttachmentByLoanApplicationIdAndType(loanApplicationId, attachmentType);
        if ( loanApplicationAttachment == null ) {
            throw new MyCustomException('Attachment not found for loanApplicationId=' + loanApplicationId + ', attachmentType=' + attachmentType);
        }
        
        //Get attachment record
        Attachment attachment = [SELECT Id FROM Attachment WHERE Id = :loanApplicationAttachment.Attachment_Id__c];

        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
            delete attachment; //Delete attachment record
            delete loanApplicationAttachment; //Delete Loan_Application_Attachment record
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
    
    public static Applicant_Attachment__c getAttachmentByAccountIdAndType(String accountId, String attachmentType) {
        //Get applicant record
        Applicant__c applicant = ApplicantService.getApplicantForAccountId(accountId);
        //Get Applicant_Attachment record
        Applicant_Attachment__c[] applicantAttachments = [
            SELECT Id, Applicant_Id__c, Attachment_Type__c, Attachment_Id__c, Attachment_Comments__c
            FROM Applicant_Attachment__c
            WHERE Applicant_Id__c = :applicant.Id AND Attachment_Type__c = :attachmentType
        ];
        
        if ( applicantAttachments.size() > 0 )
        	return applicantAttachments[0];
        else
            return null;
    }

    public static Loan_Application_Attachment__c getAttachmentByLoanApplicationIdAndType(String loanApplicationId, String attachmentType) {
        //Get Loan_Application_Attachment record
        Loan_Application_Attachment__c[] loanApplicationAttachments = [
            SELECT Id, Loan_Application_Id__c, Attachment_Type__c, Attachment_Id__c, Attachment_Comments__c
            FROM Loan_Application_Attachment__c
            WHERE Loan_Application_Id__c = :loanApplicationId AND Attachment_Type__c = :attachmentType
        ];
        
        if ( loanApplicationAttachments.size() > 0 )
        	return loanApplicationAttachments[0];
        else
            return null;
    }
    
    public static Map<String,String> getApplicantAttachmentDenormalized(String accountId, Applicant_Attachment__c applicantAttachment) {
        Map<String,String> attachmentMap = new Map<String,String>();
        
		attachmentMap.put('accountId', accountId);
        if (applicantAttachment.Applicant_Id__c != null) attachmentMap.put('applicantId', applicantAttachment.Applicant_Id__c);
        if (applicantAttachment.Attachment_Type__c != null) attachmentMap.put('attachmentType', applicantAttachment.Attachment_Type__c);
        if (applicantAttachment.Attachment_Id__c != null) attachmentMap.put('attachmentId', applicantAttachment.Attachment_Id__c);
        if (applicantAttachment.Attachment_Comments__c != null) attachmentMap.put('attachmentComments', applicantAttachment.Attachment_Comments__c);
        
        return attachmentMap;
    }

    public static Map<String,String> getLoanApplicationAttachmentDenormalized(String loanApplicationId, Loan_Application_Attachment__c loanApplicationAttachment) {
        Map<String,String> attachmentMap = new Map<String,String>();
        
		attachmentMap.put('loanApplicationId', loanApplicationId);
        if (loanApplicationAttachment.Attachment_Type__c != null) attachmentMap.put('attachmentType', loanApplicationAttachment.Attachment_Type__c);
        if (loanApplicationAttachment.Attachment_Id__c != null) attachmentMap.put('attachmentId', loanApplicationAttachment.Attachment_Id__c);
        if (loanApplicationAttachment.Attachment_Comments__c != null) attachmentMap.put('attachmentComments', loanApplicationAttachment.Attachment_Comments__c);
        
        return attachmentMap;
    }
    
    public static List<Map<String,String>> getAttachmentsByAccountId(String accountId) {
        List<Map<String,String>> attachmentList = new List<Map<String,String>>();
        //Get applicant record
        Applicant__c applicant = ApplicantService.getApplicantForAccountId(accountId);
        if ( applicant == null ) //Invalid accountId
            return attachmentList;
        
        //Get Applicant_Attachment records
        Applicant_Attachment__c[] applicantAttachments = [
            SELECT Id, Applicant_Id__c, Attachment_Type__c, Attachment_Id__c, Attachment_Comments__c
            FROM Applicant_Attachment__c
            WHERE Applicant_Id__c = :applicant.Id
        ];
        
        for ( Applicant_Attachment__c applicantAttachment : applicantAttachments ) {
			Map<String,String> applicantAttachmentMap = getApplicantAttachmentDenormalized(accountId, applicantAttachment);
            //Also get Attachment details
            Attachment attachment = [SELECT Id, Name, LastModifiedDate FROM Attachment WHERE Id = :applicantAttachment.Attachment_Id__c];
            applicantAttachmentMap.put('attachmentName', attachment.Name);
            applicantAttachmentMap.put('uploadedDate', String.valueOf(attachment.LastModifiedDate));
            attachmentList.add(applicantAttachmentMap);
        }
        
        return attachmentList;
    }
    
    public static List<Map<String,String>> getAttachmentsByLoanApplicationId(String loanApplicationId) {
        List<Map<String,String>> attachmentList = new List<Map<String,String>>();
        
        //Get Loan_Application_Attachment records
        Loan_Application_Attachment__c[] loanApplicationAttachments = [
            SELECT Id, Loan_Application_Id__c, Attachment_Type__c, Attachment_Id__c, Attachment_Comments__c
            FROM Loan_Application_Attachment__c
            WHERE Loan_Application_Id__c = :loanApplicationId
        ];
        
        for ( Loan_Application_Attachment__c loanApplicationAttachment : loanApplicationAttachments ) {
			Map<String,String> applicantAttachmentMap = getLoanApplicationAttachmentDenormalized(loanApplicationId, loanApplicationAttachment);
            //Also get Attachment details
            Attachment attachment = [SELECT Id, Name, LastModifiedDate FROM Attachment WHERE Id = :loanApplicationAttachment.Attachment_Id__c];
            applicantAttachmentMap.put('attachmentName', attachment.Name);
            applicantAttachmentMap.put('uploadedDate', String.valueOf(attachment.LastModifiedDate));
            attachmentList.add(applicantAttachmentMap);
        }
        
        return attachmentList;
    }    
}