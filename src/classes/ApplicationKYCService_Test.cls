@isTest
private class ApplicationKYCService_Test {
    
    static testmethod void test_method_one() {
        // Implement test code
        Test.startTest();
            
            Address__c add= new Address__c();
            add.State__c='1';
            add.Address_Line_1__c='aa';
            add.Address_Line_2__c='aac';
            add.City__c='qq';
            add.Country__c='aa';
            add.Pincode__c=111111.10;
            add.House_Ownership__c='rented';
            add.Residing_Since__c=Date.today();
            insert add ;
            
            Account tempAccount= new Account();    
            tempAccount.Name = 'XXX';
            insert tempAccount;
            
            Loan_Application__c loanApp=new Loan_Application__c();
            loanApp.Account_Id__c=tempAccount.Id;
            
            insert loanApp;
        
           
            
            Application_KYC__c m= new Application_KYC__c();
            m.Source__c='User Input';
            m.Loan_Application_Id__c=loanApp.Id;
            m.Aadhar_Number__c=1234567;
            m.Date_Of_Birth__c=Date.today();
            m.First_Name__c='User Input';
            m.Last_Name__c='User Input';
            m.Fathers_First_Name__c='User Input';
            m.Fathers_Last_Name__c='User Input';
            m.PAN__c='User Input';
            m.Gender__c='User Input';
            m.Permanent_Address__c=add.Id;
            m.Voter_ID__c='User Input';
            m.Loan_Application_Id__c=loanApp.Id;
            m.Account_Id__c=tempAccount.Id;
            m.Account_Id__c=tempAccount.Id;
            insert m;
        
            
            
            Map<String,Object> map1=new Map<String,Object>();
            map1.put('aadharNumber','12345678');
            map1.put('dateOfBirth',Date.today());
            map1.put('firstName','asdfgh');
            map1.put('fathersFirstName','sdfgh');
            map1.put('lastName','sdfghj');
            map1.put('fathersLastName','werfgh');
            map1.put('aadharNumber','1234567');
            map1.put('pan','12345678df');
            map1.put('source','qwerty');
            map1.put('voterID','12345678');
            map1.put('loanApplicationId',loanApp.Id);
            map1.put('lenderId',tempAccount.Id);
            map1.put('permanentAddressLine1','12345678');
            map1.put('permanentAddressLine2','12345678');
            map1.put('permanentAddressCity','12345678');
            map1.put('permanentAddressPincode','123456');
            map1.put('permanentAddressState','12345678');
            map1.put('permanentAddressCountry','12345678');
            map1.put('permanentAddressResidingSince',Date.today());
            
            
            ApplicationKYCService.getApplicationKYC(m.Id);
            ApplicationKYCService.getApplicationKYC(loanApp.Id,'User Input');
            ApplicationKYCService.getApplicationKYCs(loanApp.Id);
            ApplicationKYCService.getApplicationKYCForLender(tempAccount.Id,'User Input');
            ApplicationKYCService.getApplicationKYCsForLender(tempAccount.Id);
            ApplicationKYCService.createApplicationKYC(map1);
            ApplicationKYCService.createApplicationKYCInstance(map1,m);
            ApplicationKYCService.getApplicationKYCDenormalized(m);
            ApplicationKYCService.deleteApplicationKYC(m.Id);
            try{
            ApplicationKYCService.updateApplicationKYC(m.Id,map1);
            }catch(Exception ex){}
            ApplicationKYCService.deleteApplicationKYCsForLoanApplication(loanApp.Id);
            ApplicationKYCService.deleteApplicationKYCsForLender(tempAccount.Id);
            

            
        Test.stopTest();

    }
    
    
    
}