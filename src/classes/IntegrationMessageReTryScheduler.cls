public class IntegrationMessageReTryScheduler implements Schedulable {
    public void execute(SchedulableContext sc){
        Database.executeBatch(new IntegrationMessageReTryBatch(), 1);
    }
}