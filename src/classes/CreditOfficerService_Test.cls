@isTest
public class CreditOfficerService_Test {
    public static testmethod void crdtMethods(){
        Account acct = TestUtility.createAccount('testacc','true','Lender' );
        insert acct;
        
        Applicant__c aplcnt = TestUtility.objmgp2pApplicant();
        aplcnt.Account_Id__c = acct.Id;
        insert aplcnt;
        
        Loan_Application__c lnaplcnt = TestUtility.createApplication(acct.Id);
        insert lnaplcnt;
        
        Credit_Categories_Master__c ccm = TestUtility.objeccmst();
        insert ccm;
        
        Credit_Category_Criteria_Master__c cccm = TestUtility.objcccmst(ccm.Id);
        insert cccm;
        
        CreditOfficerService crdt = new CreditOfficerService();
        CreditOfficerService.getLoanApplicationList('Pass','Pass');
        CreditOfficerService.getLoanApplications('Pass','Pass');
        CreditOfficerService.updateKYCStatus(lnaplcnt.Id,'Pass');
        CreditOfficerService.updateKYCStatusForLender(acct.Id,'Pass');
        CreditOfficerService.updateCreditCheckStatus(lnaplcnt.Id,'Pass','Pass','test decision taker');
        CreditOfficerService.getCriteriaScore(ccm.Id, lnaplcnt.Id);
        CreditOfficerService.getLendersList('Pass');
        
    }
}