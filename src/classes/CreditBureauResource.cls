@RestResource(urlMapping='/v1/creditBureau')
global with sharing class CreditBureauResource {
    @HttpPost
    global static Map<String,String> cCreditBureau() {
        RestRequest req = RestContext.request;
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received CreditBureau Registration request:: ' + params);
        // Invoke service
        return CreditBureauService.createCreditBureau(params);
    }
    
    @HttpGet
    global static Map<String,String> getCreditBureau() {
        // Get id/accountId from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
        		String loanApplicationId = req.params.get('loanApplicationId');
        String source=req.params.get('source');
        if ( loanApplicationId != null && source!=null) {
            // Invoke service to get by loanApplicationId 
            System.debug('Received  Get request for loanApplicationId=' + loanApplicationId);
            id= CreditBureauService.getCreditBureau(loanApplicationId, source).Id;
                }
            // Invoke service to get by Loan Application id
            System.debug('Received CreditBureau Get request for id=' + id);
            return CreditBureauService.getCreditBureauDenormalized(CreditBureauService.getCreditBureau(id));
        
    }
    
    @HttpDelete
    global static void deleteCreditBureau() {
        // Get id from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
        System.debug('Received CreditBureau Delete request for id=' + id);
        // Invoke service
        CreditBureauService.deleteCreditBureau(id);
    }
    
    @HttpPatch
    global static Map<String,String> updateCreditBureau() {
        RestRequest req = RestContext.request;
        // Get id from request parameter
        String id = req.params.get('id');
                		String loanApplicationId = req.params.get('loanApplicationId');
        String source=req.params.get('source');
        if ( loanApplicationId != null && source!=null) {
            // Invoke service to get by loanApplicationId 
            System.debug('Received  Get request for loanApplicationId=' + loanApplicationId);
            id= CreditBureauService.getCreditBureau(loanApplicationId, source).Id;
                }
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received CreditBureau Update request for id=' + id + '::' + params);
        // Invoke service
        return CreditBureauService.updateCreditBureau(id, params);
    }

}