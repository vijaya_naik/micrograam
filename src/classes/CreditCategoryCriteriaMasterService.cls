public class CreditCategoryCriteriaMasterService {
    public static Credit_Category_Criteria_Master__c getCreditCategoryCriteriaMaster(String Id) {
        Credit_Category_Criteria_Master__c applicationCreditCategoryCriteriaMaster = [
            SELECT Id, Criterion_Name__c,Criterion_Display_Order__c,Criterion_Weight__c,
            Category_Id__c,Criterion_Description__c,Is_Leaf__c
            FROM Credit_Category_Criteria_Master__c 
            WHERE Id = :Id
        ];
        return applicationCreditCategoryCriteriaMaster;
    }
    
    public static List<Credit_Category_Criteria_Master__c> getCriterionsforCategory(String Id) {
        List<Credit_Category_Criteria_Master__c> applicationCreditCategoryCriteriaMaster = [
            SELECT Id, Criterion_Name__c,Criterion_Display_Order__c,Criterion_Weight__c,
            Category_Id__c,Criterion_Description__c,Is_Leaf__c
            FROM Credit_Category_Criteria_Master__c 
            WHERE Category_Id__c = :Id
        ];
        return applicationCreditCategoryCriteriaMaster;
    }

    public static Map<String,String> getCriteriaMasterMap(String categoryId) {
        Map<String,String> criteriaMasterMap = new Map<String,String>();
        
        List<Credit_Category_Criteria_Master__c> creditCategoryCriteria = [
            SELECT Id, Criterion_Name__c
            FROM Credit_Category_Criteria_Master__c 
            WHERE Category_Id__c = :categoryId
        ];
        for(integer i=0;i<creditCategoryCriteria.size();i++){
            Credit_Category_Criteria_Master__c row = creditCategoryCriteria.get(i);
            criteriaMasterMap.put(row.Criterion_Name__c, row.Id);
        }
        
        return criteriaMasterMap;
    }
    
    public static Credit_Category_Criteria_Master__c createCreditCategoryCriteriaMasterInstance(Map<String, Object> params, Credit_Category_Criteria_Master__c applicationCreditCategoryCriteriaMaster) {
        if (applicationCreditCategoryCriteriaMaster == null)    applicationCreditCategoryCriteriaMaster = new Credit_Category_Criteria_Master__c(); //For new record creation
        boolean hasValue = false;
        if (params.containsKey('criterionName')){
            applicationCreditCategoryCriteriaMaster.Criterion_Name__c=String.valueOf(params.get('criterionName'));
            hasValue=true;
        }
        if (params.containsKey('criterionWeight')){ 
            applicationCreditCategoryCriteriaMaster.Criterion_Weight__c=Integer.valueOf(params.get('criterionWeight'));
            hasValue=true;
        }
        if (params.containsKey('criterionDisplayOrder'))  {
            applicationCreditCategoryCriteriaMaster.Criterion_Display_Order__c=Integer.valueOf(params.get('criterionDisplayOrder'));
            hasValue=true;
        }
        if (params.containsKey('isLeaf')){
            applicationCreditCategoryCriteriaMaster.Is_Leaf__c=Boolean.valueOf(params.get('isLeaf'));
            hasValue=true;
        }
        if (params.containsKey('criterionDescription')){ 
            applicationCreditCategoryCriteriaMaster.Criterion_Description__c=String.valueOf(params.get('criterionDescription'));
            hasValue=true;
        }
        if (params.containsKey('categoryId')){
            applicationCreditCategoryCriteriaMaster.Category_Id__c=String.valueOf(params.get('categoryId'));
            hasValue=true;
        }
        
        if(hasValue)
            return applicationCreditCategoryCriteriaMaster;
        else 
            return null;
    }
    
    public static Map<String,String> getCreditCategoryCriteriaMasterDenormalized(Credit_Category_Criteria_Master__c applicationCreditCategoryCriteriaMaster) {
        Map<String,String> applicationCreditCategoryCriteriaMasterMap = new Map<String,String>();
        
        applicationCreditCategoryCriteriaMasterMap.put('criterionId', applicationCreditCategoryCriteriaMaster.Id);
        if (applicationCreditCategoryCriteriaMaster.Criterion_Weight__c != null) applicationCreditCategoryCriteriaMasterMap.put('criterionWeight', String.valueOf(applicationCreditCategoryCriteriaMaster.Criterion_Weight__c));
        if (applicationCreditCategoryCriteriaMaster.Criterion_Display_Order__c != null) applicationCreditCategoryCriteriaMasterMap.put('criterionDisplayOrder', String.valueOf(applicationCreditCategoryCriteriaMaster.Criterion_Display_Order__c));
        if (applicationCreditCategoryCriteriaMaster.Criterion_Name__c != null) applicationCreditCategoryCriteriaMasterMap.put('criterionName', String.valueOf(applicationCreditCategoryCriteriaMaster.Criterion_Name__c));
        if (applicationCreditCategoryCriteriaMaster.Criterion_Description__c != null) applicationCreditCategoryCriteriaMasterMap.put('criterionDescription', String.valueOf(applicationCreditCategoryCriteriaMaster.Criterion_Description__c));
        if (applicationCreditCategoryCriteriaMaster.Category_Id__c != null) applicationCreditCategoryCriteriaMasterMap.put('categoryId', String.valueOf(applicationCreditCategoryCriteriaMaster.Category_Id__c));
        if (applicationCreditCategoryCriteriaMaster.Is_Leaf__c != null) applicationCreditCategoryCriteriaMasterMap.put('isLeaf', String.valueOf(applicationCreditCategoryCriteriaMaster.Is_Leaf__c));
        
        return applicationCreditCategoryCriteriaMasterMap;
    }
    
    public static Map<String,String> createCreditCategoryCriteriaMaster(Map<String, Object> params) {
        //Create Application_KYC instance & set fields
        Credit_Category_Criteria_Master__c applicationCreditCategoryCriteriaMaster = createCreditCategoryCriteriaMasterInstance(params, null);
       
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
            insert applicationCreditCategoryCriteriaMaster;
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
        
        return getCreditCategoryCriteriaMasterDenormalized(applicationCreditCategoryCriteriaMaster);
    }
    
    public static Map<String,String> updateCreditCategoryCriteriaMaster(String applicationCreditCategoryCriteriaMasterId, Map<String, Object> params) {
        Credit_Category_Criteria_Master__c applicationCreditCategoryCriteriaMaster =null;
        if(applicationCreditCategoryCriteriaMasterId!=null)  applicationCreditCategoryCriteriaMaster=getCreditCategoryCriteriaMaster(applicationCreditCategoryCriteriaMasterId);
                applicationCreditCategoryCriteriaMaster = createCreditCategoryCriteriaMasterInstance(params, applicationCreditCategoryCriteriaMaster);
        
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
            update applicationCreditCategoryCriteriaMaster;
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
        
        return getCreditCategoryCriteriaMasterDenormalized(applicationCreditCategoryCriteriaMaster);
    }
    
    public static void deleteCreditCategoryCriteriaMaster(String creditCategoryCriteriaMasterId) {
        //Get applicant record
        Credit_Category_Criteria_Master__c dto=CreditCategoryCriteriaMasterService.getCreditCategoryCriteriaMaster(creditCategoryCriteriaMasterId);

        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
             delete dto; //Delete applicant record
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
}