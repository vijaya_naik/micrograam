public class WebserviceCallSoftcell {
    public static HTTPResponse AckCall(String acknowledgementId,String loanApplicationId){
                        HttpResponse res=null;

        String url='http://ua1.multibureau.in/CODExS/saas/saasRequest.action';
        if(acknowledgementId==null) acknowledgementId='1898576';
        Map<String,Object> header=new Map<String,Object>();
            Map<String,Object> kycrequest=new Map<String,Object>();
            List<String> responselist=new List<String>();
            header.put('APPLICATION-ID','1000');
            header.put('CUST-ID',loanApplicationId);
            header.put('REQUEST-TYPE', 'ISSUE');
            header.put('REQUEST-RECEIVED-TIME',DateTime.now().getTime() );
            Map<String,Object> values=new Map<String,Object>();
            kycrequest.put('HEADER',header);
            kycrequest.put('ACKNOWLEDGEMENT-ID',acknowledgementId);
        responselist.add('04');
            kycrequest.put('RESPONSE-FORMAT',responselist);

        
        res= mbcall(url,JSON.serialize(kycrequest));
        
                 System.debug(res.toString());
      System.debug('STATUS:'+res.getStatus());
      System.debug('STATUS_CODE:'+res.getStatusCode());
     System.debug('Body'+res.getBody());
    
    return res;
    }
    public static HTTPResponse CIBILCall(String loanApplicationId){
                HttpResponse res=null;

                String url='http://ua1.multibureau.in/CODExS/saas/saasRequest.action';
if ( loanApplicationId != null ) {
            // Invoke service to get by Borrower Account id
            System.debug('Received LoanApplication Get request for borrowerAccountId=' + loanApplicationId);
            Map<String,Object> loanapp=LoanApplicationService.getLoanApplicationDenormalized(loanApplicationId);
            Map<String,String> applicant=ApplicantService.getApplicantDenormalized(ApplicantService.getApplicantForAccountId(String.valueOf(loanapp.get('borrowerId'))));
            Map<String,Map<String,Object>> kycrequest=new Map<String,Map<String,Object>>();
            Map<String,String> name=new Map<String,String>();
            List<Map<String,String>> addresses= new List<Map<String,String>>();
            Map<String,String> ids=new Map<String,String>();

            Map<String,Object> header=new Map<String,Object>();
            header.put('APPLICATION-ID','1000');
            header.put('CUST-ID',loanApplicationId);
            header.put('REQUEST-TYPE', 'REQUEST');
            header.put('REQUEST-TIME',DateTime.now().getTime() );
            Map<String,Object> values=new Map<String,Object>();
            kycrequest.put('HEADER',header);
            values.put('01','LOW_PRIORITY');
            values.put( '02', 'CIR');
            if(loanapp.get('loanPurposeCategory').equals('Education'))
                values.put('03',String.valueOf(loanapp.get('loanPurposeCategory')).toUpperCase()+' LOAN');
            else if(loanapp.get('loanPurposeCategory').equals('Others'))
                values.put('03','OTHER');
            else values.put('03','PERSONAL LOAN');
            System.debug('gettting loan purpose'+loanapp.get('loanPurposeCategory'));
            values.put('04',Integer.valueOf(loanapp.get('loanAmount')));
            values.put('05','Individual'); //how to get individual or joint info from ourDB ?
            values.put('06','info@micrograam.com');
            values.put('07','TEST'); //given by softcell
            values.put('11','QA/UAT');
            name.put('01',applicant.get('firstName'));
            if(applicant.containsKey('middleName')) name.put('02',applicant.get('middleName'));
            if(applicant.containsKey('lastName')) name.put('03',applicant.get('lastName'));
         
            values.put('21',name);
            values.put('22',String.valueOf(applicant.get('gender')).toUpperCase());
            Date dob=Date.valueOf(applicant.get('dateOfBirth'));
            String d=String.valueOf(dob.day());
            String dobstr='';

            if(d.length()==1) dobstr='0'+d;
            else dobstr=d;

            d=String.valueOf(dob.month());

            if(d.length()==1) dobstr+='0'+d;
            else dobstr+=d;

            dobstr+=String.valueOf(dob.year());

            //dob.day()+dob.month()+dob.year()+'';
            values.put('27',dobstr);
            System.debug('Date of birth value.. '+dobstr+'check if it mmddyyyy');
    for(String key : applicant.keySet()){
        System.debug('applicant value.. '+ key + '  '+applicant.get(key));
    }
            if(applicant.containsKey('noOfDependents')) values.put('28',applicant.get('noOfDependents'));
        if(applicant.containsKey('currentAddressLine1')){
            Map<String,String> address=new Map<String,String>();

            address.put('01','RESIDENCE');
            address.put('03',applicant.get('currentAddressLine1'));
            address.put('04',applicant.get('currentAddressLine2'));
            address.put('05',applicant.get('currentAddressPincode'));
            if(applicant.containsKey('currentAddressState') && applicant.get('currentAddressState').equals('1')) address.put('06','KARNATAKA');
            if(address!=null && !address.isEmpty()) addresses.add(address);

        }else if(loanapp.containsKey('currentOfficeAddressLine1')){
                        Map<String,String> address=new Map<String,String>();

            address.put('01','RESIDENCE');
            address.put('03',String.valueOf(loanapp.get('currentOfficeAddressLine1')));
            address.put('04',String.valueOf(loanapp.get('currentOfficeAddressLine2')));
            address.put('05',String.valueOf(loanapp.get('currentOfficeAddressPincode')));
            if(loanapp.containsKey('currentOfficeAddressState')&& loanapp.get('currentOfficeAddressState').equals('1')) address.put('06','KARNATAKA');
                        if(address!=null && !address.isEmpty()) addresses.add(address);

        }
            values.put('29',addresses);//address
            values.put('32',applicant.get('eMail'));
            ids.put('01',applicant.get('pan'));
            ids.put('13',applicant.get('aadharNumber'));
            ids.put('07',applicant.get('voterID'));
            values.put('30',ids);
                kycrequest.put('REQUEST',values);
            System.debug('Value of request.. '+JSON.serialize(kycrequest));

        res=mbCall(url,JSON.serialize(kycrequest));
                 System.debug(res.toString());
      System.debug('STATUS:'+res.getStatus());
      System.debug('STATUS_CODE:'+res.getStatusCode());
     System.debug('Body'+res.getBody());
    
    return res;
}else return null;
    }
    public static HTTPResponse panCall(String loanApplicationId){
        HttpResponse res=null;
        if ( loanApplicationId != null ) {
            // Invoke service to get by Borrower Account id
            System.debug('Received LoanApplication Get request for borrowerAccountId=' + loanApplicationId);
            Map<String,Object> loanapp=LoanApplicationService.getLoanApplicationDenormalized(loanApplicationId);
            String accountId=String.valueOf(loanapp.get('borrowerId'));
            System.debug('got accountid to Get borrowerAccountId=' + accountId);
            Map<String,String> applicant=ApplicantService.getApplicantDenormalized(ApplicantService.getApplicantForAccountId(accountId));
                System.debug('got accountid to Get borrowerAccountId=' + applicant.get('pan'));
            Map<String,Map<String,Object>> kycrequest=new Map<String,Map<String,Object>>();
            Map<String,Object> header=new Map<String,Object>();

            Map<String,Object> values=new Map<String,Object>();
            Map<String,Object> pan=new Map<String,Object>();

            header.put('APPLICATION-ID','1000');
            header.put('REQUEST-TYPE', 'REQUEST');
            header.put('REQUEST-TIME',DateTime.now().getTime() );
            pan.put('PAN-NUMBER',applicant.get('pan'));
            values.put('PAN-DETAILS',pan);
            kycrequest.put('HEADER',header);
            kycrequest.put('KYC-REQUEST',values);
        String url='http://ua1.multibureau.in/SoftcellEKYC/kyc/kycPanRequest.action';
            System.debug('Test input string'+JSON.serialize(kycrequest));
            res=mbCall(url,JSON.serialize(kycrequest));
                 System.debug(res.toString());
      System.debug('STATUS:'+res.getStatus());
      System.debug('STATUS_CODE:'+res.getStatusCode());
     System.debug('Body'+res.getBody());
     Map<String, Object> root = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
     if(root.get('TXN-STATUS').equals('SUCCESS')){
    Map<String,Object> kycresponse = (Map<String,Object>)root.get('KYC-RESPONSE');
     Map<String,Object> panresponse=(Map<String,Object>)kycresponse.get('PAN-RESPONSE-DETAILS');
         if(panresponse.get('NSDL-STATUS').equals('SUCCESS')){
     Map<String,String> pandetails=(Map<String,String>)panresponse.get('PAN-RESPONSE');
              pandetails.put('source','PAN');
              ApplicationKYCService.createApplicationKYC(pandetails);
         }
     }
        }
       return res;
    }





        public static HTTPResponse AadharCall(String loanApplicationId){
        HttpResponse res=null;
        if ( loanApplicationId != null ) {
            // Invoke service to get by Borrower Account id
            System.debug('Received LoanApplication Get request for borrowerAccountId=' + loanApplicationId);
            Map<String,Object> loanapp=LoanApplicationService.getLoanApplicationDenormalized(loanApplicationId);
            String accountId=String.valueOf(loanapp.get('borrowerId'));
            System.debug('got accountid to Get borrowerAccountId=' + accountId);
            Map<String,String> applicant=ApplicantService.getApplicantDenormalized(ApplicantService.getApplicantForAccountId(accountId));
                System.debug('got accountid to Get borrowerAccountId=' + applicant.get('aadhar'));
            Map<String,Map<String,Object>> kycrequest=new Map<String,Map<String,Object>>();
            Map<String,Object> header=new Map<String,Object>();

            Map<String,Object> values=new Map<String,Object>();
            Map<String,Object> aadhar=new Map<String,Object>();
            Map<String,Object> metadata=new Map<String,Object>();
            Map<String,Object> uses=new Map<String,Object>();
            Map<String,Object> pid=new Map<String,Object>();
            Map<String,Object> pi=new Map<String,Object>();
            Map<String,Object> pi_details=new Map<String,Object>();
            header.put('APPLICATION-ID','1000');
            header.put('REQUEST-TYPE', 'REQUEST');
            header.put('REQUEST-TIME',DateTime.now().getTime() );
            aadhar.put('TERMINAL-ID','public');
            aadhar.put('KYC-TYPE','auth');
            aadhar.put('AUA-CODE','public');
            aadhar.put('SUB-AUA-CODE','public');
            aadhar.put('VERSION','1.6');
            aadhar.put('TRANSACTION-IDENTIFIER','UKC:public:'+DateTime.now().getTime());
            aadhar.put('LICENSE-KEY','MBFWjkJHNF-fLidl8oOHtUwgL5p1ZjDbWrqsMEVEJLVEDpnlNj_CZTg');
            aadhar.put('AADHAR-NUMBER',applicant.get('aadhar'));
            values.put('AADHAR-HOLDER-DETAILS',aadhar);
            kycrequest.put('HEADER',header);
            metadata.put('FINGERPRINT-DEVICE-CODE','NC');
            metadata.put('IRIS-DEVICE-CODE','NA');
            metadata.put('UNIQUE-HOST/TERMINAL-DEVICE-CODE','0201061508');
            metadata.put('PUBLIC-IP-ADDRESS','NA');
            metadata.put('LOCATION-TYPE','P');
            metadata.put('LOCATION-VALUE','411005');
            values.put('METADATA',metadata);
            uses.put('PI','y');
            uses.put('PA','n');
            uses.put('PFA','n');
            uses.put('BIO','n');
            uses.put('PIN','n');
            uses.put('OTP','n');
            values.put('USES',uses);
            pid.put('TIMESTAMP',DateTime.now().getTime());
            pid.put('VER','1.0');
                             
            pi_details.put('GENDER',applicant.get('gender'))    ;
            pi.put('PERSONAL-IDENTITY',pi_details);
            pid.put('DEMO',pi);
            values.put('PERSONAL-IDENTITY-DATA',pid);
            
            kycrequest.put('KYC-REQUEST',values);
        String url='http://ua1.multibureau.in/SoftcellEKYC/kyc/kycPanRequest.action';
            System.debug('Test input string'+JSON.serialize(kycrequest));
            res=mbCall(url,JSON.serialize(kycrequest));
                 System.debug(res.toString());
      System.debug('STATUS:'+res.getStatus());
      System.debug('STATUS_CODE:'+res.getStatusCode());
     System.debug('Body'+res.getBody());
     Map<String, Object> root = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
     if(root.get('TXN-STATUS').equals('SUCCESS')){
    Map<String,Object> kycresponse = (Map<String,Object>)root.get('KYC-RESPONSE');
     Map<String,Object> panresponse=(Map<String,Object>)kycresponse.get('PAN-RESPONSE-DETAILS');
         if(panresponse.get('NSDL-STATUS').equals('SUCCESS')){
     Map<String,String> pandetails=(Map<String,String>)panresponse.get('PAN-RESPONSE');
              pandetails.put('source','PAN');
              ApplicationKYCService.createApplicationKYC(pandetails);
         }
     }
        }
       return res;
    }
    public static HTTPResponse mbCall(String endpoint,String jsonstr){
        System.debug('endpoint........'+endpoint);
        System.debug('jsonstr.........'+jsonstr);
    List<NameValuePair> postParameters = new List<NameValuePair>();
    postParameters.add(new NameValuePair('INSTITUTION_ID', '4025'));
    postParameters.add(new NameValuePair('AGGREGATOR_ID', '559'));
    postParameters.add(new NameValuePair('MEMBER_ID', 'cpu@micr.com'));
    postParameters.add(new NameValuePair('PASSWORD', 'JHQ3c05uRHA='));
    postParameters.add(new NameValuePair('inputJson_', jsonstr));
        System.debug('making softcell call with parameters..'+endpoint+JSON.serialize(postParameters));
        return makeCall(postParameters,endpoint);
   }
    public static HTTPResponse makeCall(List<NameValuePair> postParameters,String endpoint){
        HttpRequest req = new HttpRequest();
        System.debug('postParameters.........'+postParameters);
        System.debug('endpoint.........'+endpoint);
        
                //Set HTTPRequest Method
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json'); 
        req.setHeader('Accept', 'application/json');
       //req.setHeader('Content-Type', 'multipart/form-data');
        String str='?';

        for(Integer i=0;i<postParameters.size();i++){
                str+= postParameters.get(i).getString()+'&';
            
        }
        System.debug('value of post parameters'+endpoint+str);
        
        req.setEndpoint(endpoint+str);


        Http http = new Http();
        HTTPResponse res =null;
 try {

      //Execute web service call here      

       res = http.send(req);
 
      //Helpful debug messages

 
 } catch(System.CalloutException e) {

        //Exception handling goes here....

 System.debug('Exception occoured during callout.. '+e);


 }     System.debug('res..........'+res);
            return res;

    }
}