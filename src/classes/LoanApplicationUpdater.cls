//Utility class to do mass updates for Loan Application records when needed
public class LoanApplicationUpdater {
    public static void updateCreditScoringRuleSet(String oldValue, String newValue) {
        //Get records by existing value
        List<Loan_Application__c> loanApplications = [
            SELECT Credit_Scoring_Ruleset__c
            FROM Loan_Application__c 
            WHERE Loan_Application__c.Credit_Scoring_Ruleset__c = :oldValue
        ];
        //Update to new value
        for ( Loan_Application__c loanApplication : loanApplications ) {
            loanApplication.Credit_Scoring_Ruleset__c = newValue;
        }
        update loanApplications;
        System.debug('Updated '+loanApplications.size()+' records.');
    }
}