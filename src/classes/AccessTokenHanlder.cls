public abstract class AccessTokenHanlder{
    public abstract String generateAccessToken();
    
    public void storeAccessToken(String accessToken, String apiName){
        Cache.Org.put(apiName,accessToken);
    }
    
    
    public String getAccessToken(String apiName){
        return (String) Cache.Org.get(apiName);
    }
}