@isTest(seeAllData=false)
public class CreditBureauResourceTest  {

    static Address__c addres;
    static Account acc;
    static Income_Detail__c incomeDetail;
    static Loan_Application__c loanApp ;
    static Application_Credit_Bureau__c  appBureau;
    
    private static final String ORG_URL = URL.getSalesforceBaseUrl().toExternalForm() ;
      
     static void init() {
    // setup test data 
    //create dummy Address Record
       addres =new  Address__c();
       addres.Address_Line_1__c='Street 1';
       addres.Address_Line_2__c='Street 2';
       addres.City__c='Bangalore ';
       addres.Pincode__c= 12345;
       addres.State__c='KA';
       addres.Country__c='India'; 
       addres.Residing_Since__c=System.today();
       addres.House_Ownership__c='Owned';
       insert addres;
          
       //create dummy account record
       acc=new Account();
       acc.Name='Microgram';
       acc.Account_Type__c='Borrower'; 
       insert acc;
       
       //create dummy Income Detail Record
       incomeDetail = new Income_Detail__c();
       incomeDetail.Designation__c='Manager';
       incomeDetail.Office_Address__c=addres.id;
       incomeDetail.Annual_Income__c=30000;
       insert incomeDetail;
       
       //create dummmy Loan Application Record.
       loanApp = new Loan_Application__c();
       loanApp.Account_Id__c=acc.id;
       loanApp.Reference_1_Address__c=addres.id;
       loanApp.Reference_2_Address__c=addres.id;
       loanApp.Income_Detail_Current_Job__c=incomeDetail.id;
       loanApp.Income_Detail_Previous_Job__c=incomeDetail.id;
       insert loanApp; 
       
       //create dummy application bureau record
       appBureau=new Application_Credit_Bureau__c();
       appBureau.Loan_Application_Id__c=loanApp.id; 
       appBureau.Source__c='Vendor';
       insert appBureau;
}
 static testMethod void testDoPost() {
      Map<String,  object> paramMap=new Map<String,  object>();
      paramMap.put('source','Cibil');
      paramMap.put('bureauScore','Experian');
      String JsonMsg=JSON.serialize(paramMap);
      RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/v1/creditBureau';  //Request URL
      req.httpMethod = 'POST';
      req.requestBody = Blob.valueof(JsonMsg);
      RestContext.request = req;
      RestContext.response= res;
    
      Map<String,String> results = CreditBureauResource.cCreditBureau();

  }
  
 static testMethod void testDoGet() {
    init();
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();

    req.requestURI = ORG_URL+'/services/apexrest/v1/creditBureau';  
    req.addParameter('id', appBureau.Id);      
    req.addParameter('loanApplicationId', loanApp.Id);      
    req.addParameter('source', 'Vendor');        
    req.httpMethod = 'GET';
    
    RestContext.request = req;
    RestContext.response = res;
    Map<String,String> results=new Map<String,String>();
    results = CreditBureauResource.getCreditBureau();
  }
  
  static testMethod void testDoDelete() {
    init();
    RestRequest req = new RestRequest(); 

    req.requestURI = ORG_URL+'/services/apexrest/v1/creditBureau';  
    req.httpMethod = 'DELETE';
    req.addParameter('id', appBureau.Id);    
    RestContext.request = req;
    CreditBureauResource.deleteCreditBureau();  
  }
  
  static testMethod void testDoPatch() {
      init();
      Map<String,  object> paramMap=new Map<String,  object>();
      paramMap.put('source','Cibil');
      paramMap.put('bureauScore','Experian');
      String JsonMsg=JSON.serialize(paramMap);
      RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/socialMedia';  //Request URL
      req.httpMethod = 'PATCH';
      paramMap.put('id',appBureau.Id);
      req.addParameter('loanApplicationId', loanApp.Id);      
      req.addParameter('source', 'Vendor');       
      req.requestBody = Blob.valueof(JsonMsg);
      RestContext.request = req;
      RestContext.response= res;
    Map<String,String> results=new Map<String,String>();
    results = CreditBureauResource.updateCreditBureau();

  }
}