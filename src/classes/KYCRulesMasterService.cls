public class KYCRulesMasterService {
    public static KYC_Rules_Master__c getKYCRulesMaster(String Id) {
        KYC_Rules_Master__c applicationKYCRulesMaster = [
            SELECT Id, KYC_Scoring_Ruleset__c, Field_Name__c,Field_Order__c,Min_Score__c
            
            FROM KYC_Rules_Master__c 
            WHERE Id = :Id
        ];
        return applicationKYCRulesMaster;

    }
    
    public static Map<String,KYC_Rules_Master__c> getKYCRules() {
         Map<String,KYC_Rules_Master__c> retValue=new Map<String,KYC_Rules_Master__c>();
        List<KYC_Rules_Master__c> applicationKYCRules = [
            SELECT Id, KYC_Scoring_Ruleset__c, Field_Name__c,Field_Order__c,Min_Score__c
            FROM KYC_Rules_Master__c 
        ];
         for(integer i=0;i<applicationKYCRules.size();i++){
             KYC_Rules_Master__c row=applicationKYCRules.get(i);
             retValue.put(row.Id,row);
         }
        return retValue;
    }
    
     public static Map<String,String> getKYCRulesMasterMap() {
         Map<String,String> fieldsMasterMap = new Map<String,String>();
         List<KYC_Rules_Master__c> applicationKYCRules = [
             SELECT Id, Field_Name__c
             FROM KYC_Rules_Master__c 
         ];
         for(integer i=0;i<applicationKYCRules.size();i++){
             KYC_Rules_Master__c row=applicationKYCRules.get(i);
             fieldsMasterMap.put(row.Field_Name__c, row.Id);
         }
         return fieldsMasterMap;
    }
    
    public static Map<String,String> createKYCRulesMaster(Map<String, Object> params) {
        //Create Application_KYC instance & set fields
        KYC_Rules_Master__c applicationKYCRulesMaster = createKYCRulesMasterInstance(params, null);
       
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
      
            insert applicationKYCRulesMaster;
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
        
        return getKYCRulesMasterDenormalized(applicationKYCRulesMaster);
    }
    
    public static KYC_Rules_Master__c createKYCRulesMasterInstance(Map<String, Object> params, KYC_Rules_Master__c applicationKYCRulesMaster) {
        if (applicationKYCRulesMaster == null)    applicationKYCRulesMaster = new KYC_Rules_Master__c(); //For new record creation
        boolean hasValue = false;
        if (params.containsKey('kycScoringRuleset')){
            applicationKYCRulesMaster.KYC_Scoring_Ruleset__c=String.valueOf(params.get('kycScoringRuleset'));
            hasValue=true;
        }
        if (params.containsKey('fieldName')){
            applicationKYCRulesMaster.Field_Name__c=String.valueOf(params.get('fieldName'));
            hasValue=true;
        }
        if (params.containsKey('minScore')){ 
            applicationKYCRulesMaster.Min_Score__c=Integer.valueOf(params.get('minScore'));
            hasValue=true;
        }
        if (params.containsKey('fieldOrder'))  {
            applicationKYCRulesMaster.Field_Order__c=Integer.valueOf(params.get('fieldOrder'));
            hasValue=true;
        }
        
        if(hasValue)
            return applicationKYCRulesMaster;
        else 
            return null;
    }
    
    public static Map<String,String> getKYCRulesMasterDenormalized(KYC_Rules_Master__c applicationKYCRulesMaster) {
        Map<String,String> applicationKYCRulesMasterMap = new Map<String,String>();
        
        if (applicationKYCRulesMaster.KYC_Scoring_Ruleset__c != null) applicationKYCRulesMasterMap.put('kycScoringRuleset', String.valueOf(applicationKYCRulesMaster.KYC_Scoring_Ruleset__c));
        if (applicationKYCRulesMaster.Min_Score__c != null) applicationKYCRulesMasterMap.put('minScore', String.valueOf(applicationKYCRulesMaster.Min_Score__c));
        if (applicationKYCRulesMaster.Field_Order__c != null) applicationKYCRulesMasterMap.put('fieldOrder', String.valueOf(applicationKYCRulesMaster.Field_Order__c));
        if (applicationKYCRulesMaster.Field_Name__c != null) applicationKYCRulesMasterMap.put('fieldName', String.valueOf(applicationKYCRulesMaster.Field_Name__c));
        
        return applicationKYCRulesMasterMap;
    }
    
    public static Map<String,String> updateKYCRulesMaster(String applicationKYCRulesMasterId, Map<String, Object> params) {
        KYC_Rules_Master__c applicationKYCRulesMaster =null;
        if(applicationKYCRulesMasterId!=null)  applicationKYCRulesMaster=getKYCRulesMaster(applicationKYCRulesMasterId);
        applicationKYCRulesMaster = createKYCRulesMasterInstance(params, applicationKYCRulesMaster);
        
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
            update applicationKYCRulesMaster;
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
        
        return getKYCRulesMasterDenormalized(applicationKYCRulesMaster);
    }
    
    public static void deleteKYCRulesMaster(String creditCategoriesMasterId) {
        //Get applicant record
        KYC_Rules_Master__c dto=KYCRulesMasterService.getKYCRulesMaster(creditCategoriesMasterId);


        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
             delete dto; //Delete applicant record
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
}