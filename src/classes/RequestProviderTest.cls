public class RequestProviderTest implements IRequestProvider{
    public Object execute(Integration_Message__c message){
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndPoint('http://api.fixer.io/latest?base=USD&symbols=INR');
        
        return (new Http()).send(req);
    }
}