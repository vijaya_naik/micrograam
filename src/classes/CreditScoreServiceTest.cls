@isTest(seealldata = false)
public class CreditScoreServiceTest {
    public static testmethod void creditMethods(){
        
      Credit_Categories_Master__c credMstr= new Credit_Categories_Master__c();
        credMstr.Category_Name__c='xyz';
        credMstr.Category_Order__c=4;
        credMstr.Category_Weight__c=3;
        insert credMstr;
        
    Credit_Category_Criteria_Master__c crdCriteria=new Credit_Category_Criteria_Master__c();
       crdCriteria.Category_Id__c=credMstr.id;
       crdCriteria.Criterion_Description__c='Microgram';
       crdCriteria.Criterion_Name__c='xyz'; 
       crdCriteria.Criterion_Display_Order__c=5;
           crdCriteria.Is_Leaf__c=true;
        insert  crdCriteria;
          Address__c mgp=new  Address__c();
          mgp.Address_Line_1__c='BDA Complex';
          mgp.Address_Line_2__c='permanentAddressLine2';
          mgp.City__c='permanentAddressCity';
          mgp.Pincode__c=123;
          mgp.State__c='Karnataka';
          mgp.Country__c='India'; 
          mgp.Residing_Since__c=Date.today();
          mgp.House_Ownership__c='Owned';
          insert mgp;
          
               
      Account acc=new Account();
      acc.Name='Microgram';
      acc.Account_Type__c='Borrower'; 
      insert acc;
      
      Income_Detail__c income=new Income_Detail__c();
      income.Designation__c='Manager';
      income.Office_Address__c=mgp.id;
      income.Annual_Income__c=200000;
      insert income;
      
      Loan_Application__c loan=new Loan_Application__c();
      loan.Account_Id__c=acc.id;
      loan.Reference_1_Address__c=mgp.id;
      loan.Reference_2_Address__c=mgp.id;
      loan.Income_Detail_Current_Job__c=income.id;
      loan.Income_Detail_Previous_Job__c=income.id;
      insert loan; 
      
        Application_Credit_Score__c appScore=new Application_Credit_Score__c();
        appScore.Category_Id__c=credMstr.id;
        appScore.Criterion_Id__c=crdCriteria.id;
        appScore.Loan_Application_Id__c=loan.id;
        appScore.Score__c=6;
        insert appScore;
        
          Map<String,Object> addressMap=new Map<String,Object>();
          addressMap.put('score',6.0);
          addressMap.put('categoryId',credMstr.id);
          addressMap.put('criterionId',crdCriteria.id);
           addressMap.put('loanApplicationId',loan.id);
        
       List< Application_Credit_Score__c> parmsList=new List< Application_Credit_Score__c>();
       parmsList.add(appScore);
        //parmsList.add('loanApplicationId');
       // parmsList.add(crdCriteria.id);
       //parms.add(appScore);
        CreditScoreService Crd=new CreditScoreService();
        CreditScoreService.getCreditScore(appScore.id);
        CreditScoreService.deleteCreditScore(appScore.id);
        CreditScoreService.getCreditScoreDenormalized(appScore);
        CreditScoreService.createCreditScoreInstance(addressMap,appScore);
        CreditScoreService.getCreditScore(appScore.id,'category',appScore.id);
        CreditScoreService.getCreditScore(appScore.id,'criteria',appScore.id);
        try{
        CreditScoreService.createCreditScore(parmsList); 
        }catch(Exception ex){}
    }

}