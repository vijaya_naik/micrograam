public class IntegrationMessageTriggerHandler{

    public static String messageCode = '';
    
    
    public static void onBeforeUpdate(List<Integration_Message__c> messageList, Map<Id, Integration_Message__c> messageMap){
        IntegrationMessageTriggerHandler.setStopRetryFlag(messageList);
    }
    
    
    
    public static void onAfterInsert(List<Integration_Message__c> messageList){
        IntegrationMessageTriggerHandler.makeCallout(messageList);
    }
    
    
    public static void onBeforeInsert(List<Integration_Message__c> messageList){
        IntegrationMessageTriggerHandler.setMessageCode(messageList);
        IntegrationMessageTriggerHandler.setRetryCount(messageList);
    }
    
    
    public static void setMessageCode(List<Integration_Message__c> messageList){
        Integer tyear = System.now().year();
        Integer tmonth = System.now().month();
        Integer tday = System.now().day();
        Integer thour = System.now().minute();
        Integer tsecond = System.now().second();
        Double tranddom = math.random();
        
        for(Integration_Message__c message :messageList){
            message.Message_Code__c = tyear+'_'+tmonth+'_'+tday+'_'+thour+'_'+tsecond+'_'+tranddom;
            IntegrationMessageTriggerHandler.messageCode = message.Message_Code__c;
        }
    }
    
    
    public static void makeCallout(List<Integration_Message__c> messageList){
        if(messageList.size() < (Limits.getLimitFutureCalls()-Limits.getFutureCalls())){
            for(Integration_Message__c message :messageList){
                IntegrationMessageTriggerHandler.makeFutureCallout(message.Id);
            }
        }
        else{
            Database.executeBatch(new IntegrationMessageProessBatch(IntegrationMessageTriggerHandler.messageCode),1);
        }
    }
    
    
    @Future(callout=true)
    public static void makeFutureCallout(Id messageId){
        try{
            IntegrationMessageHandler.execute(IntegrationMessageTriggerHandler.getMessageById(messageId));
        }
        catch(Exception e){
            ExceptionHandler.saveExceptionLog(e, 'Callout Exception', 'IntegrationMessageHandler', 'execute');
        }        
    }
    
    
    public static void setRetryCount(List<Integration_Message__c> messageList){
        for(Integration_Message__c message :messageList){
            message.Max_No_of_Retries__c = message.Max_No_of_Retries__c <> null ? message.Max_No_of_Retries__c : 0;
            message.No_of_Retries__c = 0;
        }
    }
    
    
    public static Integration_Message__c getMessageById(Id messageId){
        return [SELECT Id, Error_Message__c, Input_String__c, Request_Provider__c, Response_Handler__c, Status__c, 
             Status_Code__c, No_of_Retries__c
             FROM Integration_Message__c 
             WHERE Id = :messageId];
    }
    
    
    public static void setStopRetryFlag(List<Integration_Message__c> messageList){
        for(Integration_Message__c message :messageList){
            if(message.No_of_Retries__c >= message.Max_No_of_Retries__c){
                message.Stop_Retry__c = TRUE;
            }
            else{
                message.Stop_Retry__c = FALSE;
            }
        }
    }
}