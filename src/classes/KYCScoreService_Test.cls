@isTest
private class KYCScoreService_Test {
    
    @isTest static void test_method_one() {
        // Implement test code
        Account acct = TestUtility.createAccount('testacc','true','Lender' );
        insert acct;

        Applicant__c aplcnt = TestUtility.objmgp2pApplicant();
        aplcnt.Account_Id__c = acct.Id;
        insert aplcnt;

        Loan_Application__c lnaplcnt = TestUtility.createApplication(acct.Id);
        insert lnaplcnt;

        KYC_Rules_Master__c kycRM = TestUtility.objKYCrm();
        insert kycRM;

        Application_KYC_Score__c applKYC = TestUtility.inserApplKYCscore(acct.Id, kycRM.Id, lnaplcnt.Id);
        insert applKYC;

        List<Application_KYC_Score__c> kycScoreList = new List<Application_KYC_Score__c>();
        kycScoreList.add(applKYC);

        Application_KYC_Score__c kycObj1 = TestUtility.inserApplKYCscore(acct.Id, kycRM.Id, lnaplcnt.Id);
        Map<String, Object> kycMap = new Map<String,Object>();
        kycMap.put('fieldId',kycObj1.Field_Id__c);
        kycMap.put('loanApplicationId', kycObj1.Loan_Application_Id__c);
        kycMap.put('lenderId', kycObj1.Account_Id__c);
        kycMap.put('score', kycObj1.Score__c);

        Map<String,String> fieldsMasterMap = KYCRulesMasterService.getKYCRulesMasterMap();
        
        KYCScoreService kycSS = new KYCScoreService();

        //KYCScoreService.getKYCScore(lnaplcnt.Id);
        //KYCScoreService.getKYCScoreForLender
        //KYCScoreService.createKYCScore(kycScoreList);
        KYCScoreService.createKYCScoreInstance(fieldsMasterMap , kycMap, applKYC);
        KYCScoreService.getKYCScoreDenormalized(applKYC.Id);
        KYCScoreService.deleteKYCScore(lnaplcnt.Id);
        KYCScoreService.deleteKYCScoreForLender(acct.Id);
    }
}