@RestResource(urlMapping='/v1/kycScore')
global with sharing class KYCScoreResource {
    @HttpPost
    global static void cKYCScore() {
        RestRequest req = RestContext.request;
        // Deserialize the JSON string into name-value pairs
        System.debug('======> '+ req.requestBody.tostring());
        String loanApplicationId = req.params.get('loanApplicationId');
        String lenderId = req.params.get('lenderId');

        //Delete records with this loan application id if exists.
        if (loanApplicationId != null) { 
        	System.debug('Received KYCScore Get request for loanApplicationId=' + loanApplicationId);
            KYCScoreService.deleteKYCScore(loanApplicationId);
        } else if (lenderId != null) { 
        	System.debug('Received KYCScore Get request for lenderId=' + lenderId);
            KYCScoreService.deleteKYCScoreForLender(lenderId);
        }
        
        List<Object> params = (List<Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received KYCScore Registration request:: ' + params);
        // Invoke service
        KYCScoreService.createKYCScore(params);
    }
  
    @HttpGet
    global static List<Map<String,String>> getKYCScore() {
        List<Map<String,String>> retlist=new List<Map<String,String>>();
        // Get id/accountId from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
           // Invoke service to get by Loan Application id
            System.debug('Received KYCScore Get request for id=' + id);
        	List<Application_KYC_Score__c> rows=KYCScoreService.getKYCScore(id);
        for(Integer i=0;i<rows.size();i++){
            retlist.add(KYCScoreService.getKYCScoreDenormalized(rows.get(i)));
    
        }
        return retlist;
    }
   
    @HttpDelete
    global static void deleteKYCScore() {
        // Get id from request parameter
        RestRequest req = RestContext.request;
        String loanApplicationId = req.params.get('loanApplicationId');
        System.debug('Received KYCScore Delete request for loanApplicationid=' + loanApplicationId);
        // Invoke service
        KYCScoreService.deleteKYCScore(loanApplicationId);
    }
 }