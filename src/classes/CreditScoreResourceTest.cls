@isTest(seeAllData=false)
public class CreditScoreResourceTest  {
    static Address__c addres;
    static Account acc;
    static Income_Detail__c incomeDetail;
    static Loan_Application__c loanApp ;
    static Application_Credit_Score__c appScore;
    static Credit_Category_Criteria_Master__c crdCriteria;
    static Credit_Categories_Master__c credMstr;
    
    private static final String ORG_URL = URL.getSalesforceBaseUrl().toExternalForm() ;
    static void init() {
        //create dummy credit categories master record
        credMstr= new Credit_Categories_Master__c();
        credMstr.Category_Name__c='xyz';
        credMstr.Category_Order__c=4;
        credMstr.Category_Weight__c=3;
        insert credMstr;
        
        //create dummy credit category criteria master record
       crdCriteria=new Credit_Category_Criteria_Master__c();
       crdCriteria.Category_Id__c=credMstr.id;
       crdCriteria.Criterion_Description__c='Microgram';
       crdCriteria.Criterion_Name__c='xyz'; 
       crdCriteria.Criterion_Display_Order__c=5;
       crdCriteria.Is_Leaf__c=true;
       insert  crdCriteria;
       
       //create dummy Address Record
       addres =new  Address__c();
       addres.Address_Line_1__c='Street 1';
       addres.Address_Line_2__c='Street 2';
       addres.City__c='Bangalore ';
       addres.Pincode__c= 12345;
       addres.State__c='KA';
       addres.Country__c='India'; 
       addres.Residing_Since__c=System.today();
       addres.House_Ownership__c='Owned';
       insert addres;
          
       //create dummy account record
       acc=new Account();
       acc.Name='Microgram';
       acc.Account_Type__c='Borrower'; 
       insert acc;
       
       //create dummy Income Detail Record
       incomeDetail = new Income_Detail__c();
       incomeDetail.Designation__c='Manager';
       incomeDetail.Office_Address__c=addres.id;
       incomeDetail.Annual_Income__c=30000;
       insert incomeDetail;
       
       //create dummmy Loan Application Record.
       loanApp = new Loan_Application__c();
       loanApp.Account_Id__c=acc.id;
       loanApp.Reference_1_Address__c=addres.id;
       loanApp.Reference_2_Address__c=addres.id;
       loanApp.Income_Detail_Current_Job__c=incomeDetail.id;
       loanApp.Income_Detail_Previous_Job__c=incomeDetail.id;
       insert loanApp; 
        //create dummy Application_Social_Media__c  record.
       appScore=new Application_Credit_Score__c();
       appScore.Category_Id__c=credMstr.id;
       appScore.Criterion_Id__c=crdCriteria.id;
       appScore.Loan_Application_Id__c=loanApp.id;
       appScore.Score__c=6;
       insert appScore;
  }
    
    static testMethod void testDoGet() {
        init();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = ORG_URL+'/services/apexrest/v1/creditScore';  
        req.httpMethod = 'GET';
        req.addParameter('id', loanApp.Id);      
        RestContext.request = req;
        RestContext.response = res;
        List<Application_Credit_Score__c> results=new List<Application_Credit_Score__c>();
        results = CreditScoreResource.getCreditScore();
    }
    
     static testMethod void testDoPost() {
      init();
      Map<String,Object> paramMap=new Map<String,Object>();
      paramMap.put('categoryId',credMstr.id);
      paramMap.put('criterionId',crdCriteria.id);
      paramMap.put('loanApplicationId',loanApp.id);
      List<Object> paramsMapList = paramMap.Values();
      String JsonMsg=JSON.serialize(paramsMapList);
      RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/v1/creditScore';  //Request URL
      req.httpMethod = 'POST';
      req.requestBody = Blob.valueof(JsonMsg);
      RestContext.request = req;
      RestContext.response= res;
      try{
      CreditScoreResource.cCreditScore();
      }
      catch(Exception e){
      }

  }
  
  static testMethod void testDoDelete() {
    init();
    RestRequest req = new RestRequest(); 

    req.requestURI = ORG_URL+'/services/apexrest/v1/creditScore';  
    req.httpMethod = 'DELETE';
    req.addParameter('id', appScore.Id);    
    RestContext.request = req;
    CreditScoreResource.deleteCreditScore();  
  }
}