@RestResource(urlMapping='/v1/creditCategoryCriteriaMaster')
global with sharing class CreditCategoryCriteriaMasterResource {
 @HttpPost
    global static Map<String,String> cCreditCategoryCriteriaMaster() {
        RestRequest req = RestContext.request;
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received CreditCategoryCriteriaMaster Registration request:: ' + params);
        // Invoke service
        return CreditCategoryCriteriaMasterService.createCreditCategoryCriteriaMaster(params);
    }
    
    @HttpGet
    global static Map<String,String> getCreditCategoryCriteriaMaster() {
        // Get id/accountId from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
        if(id==null){
            //get all id name pair for join later
            return null;
        }else{
           // Invoke service to get by Loan Application id
           
            System.debug('Received CreditCategoryCriteriaMaster Get request for id=' + id);
            return CreditCategoryCriteriaMasterService.getCreditCategoryCriteriaMasterDenormalized(CreditCategoryCriteriaMasterService.getCreditCategoryCriteriaMaster(id));
        }
        
    }
    
    @HttpDelete
    global static void deleteCreditCategoryCriteriaMaster() {
        // Get id from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
        System.debug('Received CreditCategoryCriteriaMaster Delete request for id=' + id);
        // Invoke service
        CreditCategoryCriteriaMasterService.deleteCreditCategoryCriteriaMaster(id);
    }
    
    @HttpPatch
    global static Map<String,String> updateCreditCategoryCriteriaMaster() {
        RestRequest req = RestContext.request;
        // Get id from request parameter
        String id = req.params.get('id');

        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received CreditCategoryCriteriaMaster Update request for id=' + id + '::' + params);
        // Invoke service
        return CreditCategoryCriteriaMasterService.updateCreditCategoryCriteriaMaster(id, params);
    }

}