public class TestUtility {
    public static account createAccount(String name, String IsActive, String AccountType){
        
         Account acc = new Account(Name=name,AccountNumber='12456',Account_Type__c=AccountType,
                                  Active__c =IsActive);
         return acc;
    }
    public static Loan_Application__c createApplication(String Accid){
        Loan_Application__c app = new Loan_Application__c();
        app.Account_Id__c = Accid;
        app.Credit_Check_Status__c = 'Pass';
        app.KYC_Status__c = 'Pass';
        app.Total_Work_Experience__c = 2;
        app.Rental_Income__c=1000;
        app.Other_Income__c =1000;
        app.Auto_Loan__c =1000;
        return app;
    }
    
    public static Applicant__c  objmgp2pApplicant(){
        Applicant__c  objApplicant = new Applicant__c();
        objApplicant.Aadhar_Number__c = 12;
        objApplicant.Applicant_Nature__c = 'Primary';
        objApplicant.Bank_Account_Number__c = 12;
        objApplicant.Bank_IFSC_Code__c = 'ISDC001';
        objApplicant.Bank_Name__c = 'Test Bank';
        objApplicant.Completion_Year__c = 2014;
        objApplicant.Date_Of_Birth__c = date.today();
        objApplicant.Date_of_Incorporation__c = date.today();
        objApplicant.E_Mail__c = 'test@test.com';
        objApplicant.Fathers_First_Name__c = 'Test';
        objApplicant.Fathers_Last_Name__c  = 'Test1';
        objApplicant.Fathers_Middle_Name__c = 'Test2';
        objApplicant.First_Name__c = 'TestData1';
        objApplicant.Gender__c = 'Male';
        objApplicant.Highest_Qualification_Desc__c = 'Graduate';
        objApplicant.Highest_Qualification_Type__c = 'Graduate';
        objApplicant.KYC_Status__c = 'Pass';
        objApplicant.Landline_Number__c = 123;
        objApplicant.Landline_STD_Code__c = 1234;
        objApplicant.Last_Education_Institute__c = 'test';
        objApplicant.Last_Name__c = 'Test';
        objApplicant.Marital_Status__c = 'Single';
        objApplicant.Middle_Name__c = 'Test Data';
        objApplicant.Mobile_Number__c = '1234';
        objApplicant.No_Of_Dependent_Children__c = 1;
        objApplicant.No_Of_Dependents__c = 3;
        objApplicant.Office_Extension__c = 123;
        objApplicant.Office_Number__c = '1234567890';
        objApplicant.Office_STD_Code__c = 123;
        objApplicant.PAN__c = 'ka1234567';
        objApplicant.Primary_Contact_Name__c = 'TestData';
        objApplicant.Profile_Facebook__c = 'www.Facebook.com';
        objApplicant.Profile_LinkedIn__c = 'www.linkedin.com';
        objApplicant.Profile_Twitter__c = 'www.twitter.com';
        objApplicant.Registration_Number__c = '123';
        objApplicant.Voter_ID__c = '123456789';
        return objApplicant; 
    }

    //create credit category master
    public static Credit_Categories_Master__c objeccmst(){
        Credit_Categories_Master__c ccmObj = new Credit_Categories_Master__c();
            //ccmObj.Name = 'test credit categories master';
            ccmObj.Category_Name__c = 'test category name';
            ccmObj.Category_Order__c = 100;
            ccmObj.Category_Weight__c = 25;
            //ccmObj.Credit_Scoring_Ruleset__c = 'Individual - Standard';
            return ccmObj;
    }
    
    //create credit criteria master
    public static Credit_Category_Criteria_Master__c objcccmst(String Ccmid){
        Credit_Category_Criteria_Master__c cccmObj = new Credit_Category_Criteria_Master__c();
            //cccmObj.Name = '';
            cccmObj.Category_Id__c = Ccmid;
            cccmObj.Criterion_Description__c = 'test description credit category master';
            cccmObj.Criterion_Display_Order__c = 100;
            cccmObj.Criterion_Name__c = 'test name';
            cccmObj.Criterion_Weight__c = 25;
            cccmObj.Is_Leaf__c = true;
            return cccmObj;
    }

    //create KYC master object for test class
    public static KYC_Rules_Master__c objKYCrm(){
        KYC_Rules_Master__c kycObj = new KYC_Rules_Master__c();
            //kycObj.Name = 'test kyc';
            kycObj.Field_Name__c = 'test field';
            kycObj.Field_Order__c = 23;
            kycObj.Min_Score__c = 15;
            return kycObj;
    }

    public static Application_KYC_Score__c inserApplKYCscore(String acctId,String kycId,String loanId){
        Application_KYC_Score__c applKYCsc = new Application_KYC_Score__c();
            applKYCsc.Account_Id__c = acctId;
            applKYCsc.Field_Id__c = kycId;
            applKYCsc.Loan_Application_Id__c = loanId;
            applKYCsc.Score__c = 50;

        return applKYCsc;
    }
}