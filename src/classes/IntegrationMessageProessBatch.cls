global class IntegrationMessageProessBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    global String Query;
    global String messageCode;
    
    global IntegrationMessageProessBatch(String messageCode){
        this.messageCode = messageCode;
        query = 'SELECT Id FROM Integration_Message__c WHERE Message_Code__c = :messageCode';
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<Integration_Message__c> messages){
        try{
            for(Integration_Message__c message :messages){
                IntegrationMessageHandler.execute(IntegrationMessageTriggerHandler.getMessageById(message.Id));
            }
        }
        catch(Exception e){
            ExceptionHandler.saveExceptionLog(e, 'Callout Exception', 'IntegrationMessageHandler', 'execute');
        }
    }
    
    
    global void finish(Database.BatchableContext BC){
    }
}