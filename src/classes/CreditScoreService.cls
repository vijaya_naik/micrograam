public class CreditScoreService {
 public static List<Application_Credit_Score__c> getCreditScore(String Id) {
        List<Application_Credit_Score__c> applicationCreditScore = [
            SELECT Id,Loan_Application_Id__c,
            Score__c,Category_Id__c,    Criterion_Id__c
            FROM Application_Credit_Score__c 
            WHERE Loan_Application_Id__c = :Id
        ];
        return applicationCreditScore;

    }
    public static Integer getCreditScore(String id,String type,String loanApplicationId) {
        Integer score=null;
        System.debug('Id'+id+':type'+type+':loanApplicationId'+loanApplicationId);
        if('category'.equalsIgnoreCase(type) ){
         List<Object> scorelist = [
            SELECT Score__c
            FROM Application_Credit_Score__c 
            WHERE Loan_Application_Id__c = :loanApplicationId and
             Category_Id__c=:id and Criterion_Id__c =null
        ];
            if(scorelist!=null && scorelist.size()==1){
                score=Integer.valueOf(((Application_Credit_Score__c)scorelist.get(0)).Score__c);
            }
    } else if('criteria'.equalsIgnoreCase(type) ){
         List<Object> scorelist  = [
            SELECT Score__c
            FROM Application_Credit_Score__c 
            WHERE Loan_Application_Id__c = :loanApplicationId and
             Criterion_Id__c=:Id limit 1
        ];
        if(scorelist!=null && scorelist.size()==1)
                score=Integer.valueOf(((Application_Credit_Score__c)scorelist.get(0)).Score__c);
    }
        return score;

    }
    public static void createCreditScore(List<Object> paramsMap) {
        for(Integer i=0;i<paramsMap.size();i++){
            Map<String, Object> params=(Map<String, Object>)paramsMap.get(i);
            //Create Application_KYC instance & set fields
            Application_Credit_Score__c applicationCreditScore = createCreditScoreInstance(params, null);
           
            //Update in database
            Savepoint sp = Database.setSavePoint();
            try {
                insert applicationCreditScore;
            } catch (Exception e) {
                Database.rollback(sp); //Rollback in case of error
                throw e;
            }
        }    
    }
    
    public static Application_Credit_Score__c createCreditScoreInstance(Map<String, Object> params, Application_Credit_Score__c applicationCreditScore) {
        if (applicationCreditScore == null)    applicationCreditScore = new Application_Credit_Score__c(); //For new record creation
        boolean hasValue = false;
        if (params.containsKey('score')){
            applicationCreditScore.Score__c=Double.valueOf(params.get('score'));
            hasValue=true;
        }
        if (params.containsKey('criterionId') && !''.equals(String.valueOf(params.get('criterionId'))) )        applicationCreditScore.Criterion_Id__c=String.valueOf(params.get('criterionId'));
        if (params.containsKey('categoryId'))           applicationCreditScore.Category_Id__c=String.valueOf(params.get('categoryId'));
        
        if (params.containsKey('loanApplicationId'))            applicationCreditScore.Loan_Application_Id__c=String.valueOf(params.get('loanApplicationId'));
        //if (params.containsKey('ratingPoints'))           applicationCreditScore.Rating_Points__c=Double.valueOf(params.get('ratingPoints'));
        
        if(hasValue)
            return applicationCreditScore;
        else 
            return null;
    }
    
    public static Map<String,String> getCreditScoreDenormalized(Application_Credit_Score__c applicationCreditScore) {
        Map<String,String> applicationCreditScoreMap = new Map<String,String>();
        
        if (applicationCreditScore.Criterion_Id__c != null) applicationCreditScoreMap.put('criterionId', applicationCreditScore.Criterion_Id__c);
                if (applicationCreditScore.Category_Id__c != null) applicationCreditScoreMap.put('categoryId', applicationCreditScore.Category_Id__c);
        
        if (applicationCreditScore.Score__c != null) applicationCreditScoreMap.put('score', String.valueOf(applicationCreditScore.Score__c));
        if (applicationCreditScore.Loan_Application_Id__c != null) applicationCreditScoreMap.put('loanApplicationId', String.valueOf(applicationCreditScore.Loan_Application_Id__c));
        //if (applicationCreditScore.Rating_Points__c != null) applicationCreditScoreMap.put('ratingPoints', String.valueOf(applicationCreditScore.Rating_Points__c));
        
        return applicationCreditScoreMap;
    }

    public static void deleteCreditScore(String loanApplicationId) {
        //Get applicant record
        List<Application_Credit_Score__c> dto=CreditScoreService.getCreditScore(loanApplicationId);


        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
             delete dto; //Delete applicant record
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
}