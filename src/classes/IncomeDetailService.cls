public class IncomeDetailService {
        public static Income_Detail__c getIncomeDetailById(String incomeId) {
        Income_Detail__c incomedetail = [
            SELECT Id, Annual_Income__c,Designation__c,Experience_in_Job__c,Industry_Profression__c,
            Occupation__c,Office_Address__c,Office_Contact_Number__c,Organization__c
            FROM Income_Detail__c 
            WHERE Id = :incomeId
        ];
        return incomedetail;
    }

    public static Map<String,String> getCurrentIncomeDetailDenormalized(Income_Detail__c currentIncomeDetail) {
        Map<String,String> currentIncomeMap = new Map<String,String>();
        if(currentIncomeDetail!=null){
            if (currentIncomeDetail.Annual_Income__c != null) currentIncomeMap.put('currentAnnualIncome', String.valueOf(currentIncomeDetail.Annual_Income__c));
            if (currentIncomeDetail.Designation__c !=null)currentIncomeMap.put('currentDesignation',String.valueOf(currentIncomeDetail.Designation__c));
            if (   currentIncomeDetail.Experience_in_Job__c!= null)currentIncomeMap.put('currentExperienceInJob',String.valueOf(currentIncomeDetail.Experience_in_Job__c));
            if (   currentIncomeDetail.Industry_Profression__c!=null)currentIncomeMap.put('currentIndustryProfession',String.valueOf(currentIncomeDetail.Industry_Profression__c));
            if (   currentIncomeDetail.Occupation__c != null)currentIncomeMap.put('currentOccupation',String.valueOf(currentIncomeDetail.Occupation__c));
            if (   currentIncomeDetail.Office_Address__c != null) {
                Address__c currentOfficeAddress = AddressService.getAddressById(currentIncomeDetail.Office_Address__c);
                currentIncomeMap.putAll(AddressService.getCurrentOfficeAddressDenormalized(currentOfficeAddress));
            }
            if (   currentIncomeDetail.Office_Contact_Number__c != null)currentIncomeMap.put('currentOfficeContactNumber',String.valueOf(currentIncomeDetail.Office_Contact_Number__c));
            if (   currentIncomeDetail.Organization__c != null)currentIncomeMap.put('currentOrganization',String.valueOf(currentIncomeDetail.Organization__c));
            //if (   currentIncomeDetail.Total_Work_Experience__c != null)currentIncomeMap.put('totalWorkExperience',String.valueOf(currentIncomeDetail.Total_Work_Experience__c));
        }
        return currentIncomeMap;
    }

  public static Map<String,String> getPreviousIncomeDetailDenormalized(Income_Detail__c previousIncomeDetail) {
        Map<String,String> previousIncomeMap = new Map<String,String>();
      	if(previousIncomeDetail!=null){
          if (previousIncomeDetail.Annual_Income__c != null) previousIncomeMap.put('previousAnnualIncome', String.valueOf(previousIncomeDetail.Annual_Income__c));
          if (previousIncomeDetail.Designation__c !=null)previousIncomeMap.put('previousDesignation',String.valueOf(previousIncomeDetail.Designation__c));
          if (   previousIncomeDetail.Experience_in_Job__c!= null)previousIncomeMap.put('previousExperienceInJob',String.valueOf(previousIncomeDetail.Experience_in_Job__c));
          if (   previousIncomeDetail.Industry_Profression__c!=null)previousIncomeMap.put('previousIndustryProfession',String.valueOf(previousIncomeDetail.Industry_Profression__c));
          if (   previousIncomeDetail.Occupation__c != null)previousIncomeMap.put('previousOccupation',String.valueOf(previousIncomeDetail.Occupation__c));
          if (   previousIncomeDetail.Office_Address__c != null) {
              Address__c previousOfficeAddress = AddressService.getAddressById(previousIncomeDetail.Office_Address__c);
              previousIncomeMap.putAll(AddressService.getPreviousOfficeAddressDenormalized(previousOfficeAddress));         
          }
          if (   previousIncomeDetail.Office_Contact_Number__c != null)previousIncomeMap.put('previousOfficeContactNumber',String.valueOf(previousIncomeDetail.Office_Contact_Number__c));
          if (   previousIncomeDetail.Organization__c != null)previousIncomeMap.put('previousOrganization',String.valueOf(previousIncomeDetail.Organization__c));
      	}
        return previousIncomeMap;
    }
    
    public static Income_Detail__c createCurrentIncomeDetail(Map<String, Object> params,Income_Detail__c  currentIncomeDetail) {
        if(currentIncomeDetail==null)  currentIncomeDetail= new Income_Detail__c();
        boolean hasValue=false;

        if(params.containsKey('currentAnnualIncome')){
            currentIncomeDetail.Annual_Income__c = Double.valueOf(params.get('currentAnnualIncome'));
            hasValue=true;
        }
        if(params.containsKey('currentDesignation')){
			currentIncomeDetail.Designation__c= String.valueOf(params.get('currentDesignation'));
            hasValue=true;
        }
        if(params.containsKey('currentExperienceInJob')){
        	currentIncomeDetail.Experience_in_Job__c=Double.valueOf(params.get('currentExperienceInJob'));
            hasValue=true;
        }
        if(params.containsKey('currentIndustryProfession')){
			currentIncomeDetail.Industry_Profression__c=String.valueOf(params.get('currentIndustryProfession'));
            hasValue=true;
        }
		if(params.containsKey('currentOccupation')){            
        	currentIncomeDetail.Occupation__c=String.valueOf(params.get('currentOccupation'));
            hasValue=true;
        }
		if(params.containsKey('currentOfficeContactNumber')){            
        	currentIncomeDetail.Office_Contact_Number__c=Double.valueOf(params.get('currentOfficeContactNumber'));
            hasValue=true;
        }
        if(params.containsKey('currentOrganization')){
        	currentIncomeDetail.Organization__c=String.valueOf(params.get('currentOrganization'));
            hasValue=true;
        }
		/*if(params.containsKey('totalWorkExperience')){            
        	currentIncomeDetail.Total_Work_Experience__c = Double.valueOf(params.get('totalWorkExperience'));
            hasValue=true;
        }*/
        
        if(hasValue)
			return currentIncomeDetail;
        else
            return null;
    }
    
    public static Income_Detail__c createPreviousIncomeDetail(Map<String, Object> params,Income_Detail__c  previousIncomeDetail) {
        if(previousIncomeDetail==null) previousIncomeDetail= new Income_Detail__c();
        boolean hasValue=false;
        
        if(params.containsKey('previousAnnualIncome')){
        	previousIncomeDetail.Annual_Income__c = Double.valueOf(params.get('previousAnnualIncome'));
            hasValue=true;
        }
		if(params.containsKey('previousDesignation')){
  			previousIncomeDetail.Designation__c= String.valueOf(params.get('previousDesignation'));
            hasValue=true;
        }
		if(params.containsKey('previousExperienceInJob')){
        	previousIncomeDetail.Experience_in_Job__c=Double.valueOf(params.get('previousExperienceInJob'));
            hasValue=true;
        }
		if(params.containsKey('previousIndustryProfession')){
			previousIncomeDetail.Industry_Profression__c=String.valueOf(params.get('previousIndustryProfession'));
            hasValue=true;
        }
		if(params.containsKey('previousOccupation')){
        	previousIncomeDetail.Occupation__c=String.valueOf(params.get('previousOccupation'));
            hasValue=true;
        }
		if(params.containsKey('previousOfficeContactNumber')){
        	previousIncomeDetail.Office_Contact_Number__c=Double.valueOf(params.get('previousOfficeContactNumber'));
            hasValue=true;
        }
		if(params.containsKey('previousOrganization')){
        	previousIncomeDetail.Organization__c=String.valueOf(params.get('previousOrganization'));	
            hasValue=true;
        }
            
        if(hasValue){
            System.debug('Test values of annual income when not given'+previousIncomeDetail.Annual_Income__c);
            if(previousIncomeDetail.Annual_Income__c==null)
                previousIncomeDetail.Annual_Income__c=0;
            System.debug('Test values of annual income set to 0'+previousIncomeDetail.Annual_Income__c);

        	return previousIncomeDetail;
        }else
            return null;
    }
}