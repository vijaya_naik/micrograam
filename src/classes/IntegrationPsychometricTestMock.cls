global class IntegrationPsychometricTestMock implements HttpCalloutMock {
     global HTTPResponse respond(HTTPRequest req) {
        HTTPResponse res = new HTTPResponse();
        res.setBody('{"code": 1,"message": "Results","callbackObj": {"willing to Repay": "HIGH","ability to Repay": "HIGH", "filename" : "Test.pdf","file":"aaa"}}');
        res.setStatus('OK');
        return res;
    }

}