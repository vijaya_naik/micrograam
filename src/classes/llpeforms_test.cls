/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class llpeforms_test {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        llpeforms.BodyCorporateLLPForm11ReturnDDto ll1=new llpeforms.BodyCorporateLLPForm11ReturnDDto();
        llpeforms.ChargeIdDetailsReturnDDto ll2=new llpeforms.ChargeIdDetailsReturnDDto();
        llpeforms.DetailsOfBodyCorporateLLPReturnDDto ll3=new llpeforms.DetailsOfBodyCorporateLLPReturnDDto();
        llpeforms.DetailsOfPartnersReturnDDto ll4=new llpeforms.DetailsOfPartnersReturnDDto();
        llpeforms.FrgnLLPApplicantDtlsReturnDDto ll5=new llpeforms.FrgnLLPApplicantDtlsReturnDDto();
        llpeforms.LLPCINLLPINDetailsReturnDDto ll6=new llpeforms.LLPCINLLPINDetailsReturnDDto();
        llpeforms.LLPCINLLPINDetailsReturnSCDDto ll7=new llpeforms.LLPCINLLPINDetailsReturnSCDDto();
        llpeforms.LLPForm11IndividualsDetailsReturnDDto ll8=new llpeforms.LLPForm11IndividualsDetailsReturnDDto();
        llpeforms.LLPForm11ReturnDDto ll9 =new llpeforms.LLPForm11ReturnDDto();
        llpeforms.LLPForm1ApplicantDetailsReturnDDto ll10=new llpeforms.LLPForm1ApplicantDetailsReturnDDto();
        llpeforms.LLPForm1SrnReturnDDto ll11=new llpeforms.LLPForm1SrnReturnDDto();
        llpeforms.LLPForm25SRNDetailsReturnDDto ll12=new llpeforms.LLPForm25SRNDetailsReturnDDto();
        llpeforms.LLPForm3DetailsReturnDDto ll13=new llpeforms.LLPForm3DetailsReturnDDto();
        llpeforms.LLPForm8ApplicantDetailsReturnDDto ll14=new llpeforms.LLPForm8ApplicantDetailsReturnDDto();
        llpeforms.LLPForm8ContriDetailsReturnDDto ll15=new llpeforms.LLPForm8ContriDetailsReturnDDto();
    }
}