global class IntegrationMessageReTryBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    global String Query;
    
    final String STATUS = 'Failure';
    final boolean STOP_RETRY = FALSE;
    
    global IntegrationMessageReTryBatch(){
        query = 'SELECT Id FROM Integration_Message__c WHERE Status__c = :STATUS AND Stop_Retry__c = :STOP_RETRY';
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<Integration_Message__c> messages){
        try{
            for(Integration_Message__c message :messages){
                IntegrationMessageHandler.retry(IntegrationMessageTriggerHandler.getMessageById(message.Id));
            }
        }
        catch(Exception e){
            ExceptionHandler.saveExceptionLog(e, 'Callout Exception', 'IntegrationMessageHandler', 'execute');
        }
    }
    
    
    global void finish(Database.BatchableContext BC){
    }
}