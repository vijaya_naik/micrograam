public class BorrowerService {
    public static Map<String,String> registerBorrower(Map<String, Object> params) {
        String firstName = String.valueOf(params.get('firstName'));
        String lastName = String.valueOf(params.get('lastName'));
        String applicantType = String.valueOf(params.get('applicantType'));

        //1. Create Account instance & set fields
        Account borrowerAccount = new Account();
        borrowerAccount.Account_Type__c = String.valueOf(params.get('accountType'));
        borrowerAccount.Applicant_Type__c=applicantType;
        if('Individual'.equals(applicantType))
            borrowerAccount.Name = firstName + ' ' + lastName;
        else if('Non-Individual'.equals(applicantType))
            borrowerAccount.Name=String.valueOf(params.get('companyFirmName'));
        Address__c registeredAddress= AddressService.createRegisteredAddress(params, null);
        Address__c correspondenseAddress= AddressService.createCorrespondenceAddress(params, null);

        //2. Create Applicant instance & set fields
        Applicant__c applicant = ApplicantService.createApplicant(params, null);
 
        //3. Create Permanant Address instance & set fields
        Address__c permanentAddress = AddressService.createPermanentAddress(params, null);

        //4. Create Current Address instance & set fields
        Address__c currentAddress = AddressService.createCurrentAddress(params, null);

        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
            insert borrowerAccount;
            applicant.Account_Id__c = borrowerAccount.Id; //Update parent Account Id
            if (permanentAddress != null) {
                insert permanentAddress;
                applicant.Permanent_Address__c = permanentAddress.Id; //Update related Address Id
            }
            if (currentAddress != null) {
                insert currentAddress;
                applicant.Current_Address__c = currentAddress.Id; //Update related Address Id
            }
            if (registeredAddress != null) {
                insert registeredAddress;
                applicant.Registered_Address__c = registeredAddress.Id; //Update related Address Id
            }
             if (correspondenseAddress != null) {
                insert correspondenseAddress;
                applicant.Correspondence_Address__c = correspondenseAddress.Id; //Update related Address Id
            }
            insert applicant;
            
            //In case of Submit - additionally create Loan Application record
            //                    and Application_KYC record for "User Input" source
            String isSubmit = String.valueOf(params.get('submit'));
            if ( isSubmit != null && 'true'.equalsIgnoreCase(isSubmit) ) {
                LoanApplicationDTO loanApplicationMap = createLoanApplicationOnSubmit(borrowerAccount.Id, params);
                if('Individual'.equals(applicantType)) {
                    createApplicationKYCOnSubmitForIndividual(loanApplicationMap.loanApplication.Id, applicant);
                } else if('Non-Individual'.equals(applicantType)) {
                    createApplicationKYCOnSubmitForNonIndividual(loanApplicationMap.loanApplication.Id, applicant, borrowerAccount.Name);
                }
                //Update the KYC_Status to "Submitted"
                applicant.KYC_Status__c = 'Submitted';
                update applicant;
            }
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
        
        return getBorrowerDenormalized(borrowerAccount.Id);
    }

    public static BorrowerDTO getBorrower(String borrowerAccountId) {
        BorrowerDTO borrower = new BorrowerDTO();

        // Get account details
        Account account = [
            SELECT Id, Name, Account_Type__c,Applicant_Type__c
            FROM Account 
            WHERE Account.Id = :borrowerAccountId
        ];
        borrower.borrowerAccount = account;
        
        //Get applicant details
        Applicant__c applicant = ApplicantService.getApplicantForAccountId(borrowerAccountId);
        borrower.applicant = applicant;
        
        //Get current address details
        borrower.currentAddress = AddressService.getAddressById(applicant.Current_Address__c);
        
        //Get permanent address details
        borrower.permanentAddress = AddressService.getAddressById(applicant.Permanent_Address__c);
        //Get registered address details
        borrower.registeredAddress = AddressService.getAddressById(applicant.Registered_Address__c);
        //Get correspondense address details
        borrower.correspondenseAddress = AddressService.getAddressById(applicant.Correspondence_Address__c);        
        return borrower;
    }
    
    public static Map<String,String> getBorrowerDenormalized(String borrowerAccountId) {
        Map<String,String> borrowerMap = new Map<String,String>();
        // Get account details
        Account account = [
            SELECT Id, Name, Account_Type__c,Applicant_Type__c
            FROM Account 
            WHERE Account.Id = :borrowerAccountId
        ];
        
        //Get applicant details
        Applicant__c applicant = ApplicantService.getApplicantForAccountId(borrowerAccountId);

        //Get current address details
        if (applicant.Current_Address__c != null) {
            Address__c currentAddress = AddressService.getAddressById(applicant.Current_Address__c);
            borrowerMap.putAll(AddressService.getCurrentAddressDenormalized(currentAddress));
        }
        
        //Get permanent address details
        if (applicant.Permanent_Address__c != null) {
            Address__c permanentAddress = AddressService.getAddressById(applicant.Permanent_Address__c);
            borrowerMap.putAll(AddressService.getPermanentAddressDenormalized(permanentAddress));
        }
        if (applicant.Registered_Address__c != null) {
            Address__c RegisteredAddress = AddressService.getAddressById(applicant.Registered_Address__c);
            borrowerMap.putAll(AddressService.getRegisteredAddressDenormalized(RegisteredAddress));
        }
        if (applicant.Correspondence_Address__c != null) {
            Address__c CorrespondenseAddress = AddressService.getAddressById(applicant.Correspondence_Address__c);
            borrowerMap.putAll(AddressService.getCorrespondenceAddressDenormalized(CorrespondenseAddress));
        }

        borrowerMap.putAll(ApplicantService.getApplicantDenormalized(applicant));
        if (account.Account_Type__c != null) borrowerMap.put('accountType', account.Account_Type__c);
        if ('Non-Individual'.equals(account.Applicant_Type__c)) borrowerMap.put('companyFirmName', account.Name);
        
        borrowerMap.put('applicantType', account.Applicant_Type__c);
        borrowerMap.put('id', account.Id);
        
        return borrowerMap;
    }
    
    public static void deleteBorrower(String borrowerAccountId) {
        //Get applicant record
        Applicant__c applicant = ApplicantService.getApplicantForAccountId(borrowerAccountId);
        //Get account record
        Account borrowerAccount = [SELECT Id FROM Account WHERE Account.Id = :borrowerAccountId];
        //Get current address record
        Address__c currentAddress = null;
        if (applicant.Current_Address__c != null) {
            currentAddress = AddressService.getAddressById(applicant.Current_Address__c);
        }
        //Get permanent address record
        Address__c permanentAddress = null;
        if (applicant.Permanent_Address__c != null) {
            permanentAddress = AddressService.getAddressById(applicant.Permanent_Address__c);
        }
        Address__c RegisteredAddress =null;
       if (applicant.Registered_Address__c != null) 
             RegisteredAddress = AddressService.getAddressById(applicant.Registered_Address__c);
        Address__c CorrespondenseAddress =null;
        if (applicant.Correspondence_Address__c != null) 
             CorrespondenseAddress = AddressService.getAddressById(applicant.Correspondence_Address__c);
            
        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
            if (currentAddress != null)
                delete currentAddress; //Delete current address record
            if (permanentAddress != null)
                delete permanentAddress; //Delete permanent address record
            if (RegisteredAddress != null)
                delete RegisteredAddress; //Delete registered address record
            if (CorrespondenseAddress != null)
                delete CorrespondenseAddress; //Delete correspondence address record 
            
            delete applicant; //Delete applicant record
            delete borrowerAccount; //Delete account record
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
    
    public static Map<String,String> updateBorrower(String borrowerAccountId, Map<String, Object> params) {
        String firstName = params.get('firstName') == null ? null : String.valueOf(params.get('firstName'));
        String lastName = params.get('lastName') == null ? null : String.valueOf(params.get('lastName'));
        String applicantType = params.get('applicantType') == null ? null : String.valueOf(params.get('applicantType'));
        System.debug('in update borrower.. ');
        //Get & update applicant record
        Applicant__c applicant = ApplicantService.getApplicantForAccountId(borrowerAccountId);
        applicant = ApplicantService.createApplicant(params, applicant);

        //Get & update account record
        Account borrowerAccount = [SELECT Id FROM Account WHERE Account.Id = :borrowerAccountId];
        if (params.get('accountType') != null)
            borrowerAccount.Account_Type__c = String.valueOf(params.get('accountType'));
        if (applicantType != null)
            borrowerAccount.Applicant_Type__c=applicantType;
        if(applicantType != null && 'Individual'.equals(applicantType) && firstName != null && lastName != null)
            borrowerAccount.Name = firstName + ' ' + lastName;
        else if(applicantType != null && 'Non-Individual'.equals(applicantType))
            borrowerAccount.Name=String.valueOf(params.get('companyFirmName'));

        
        //Get & update current address record
        Address__c currentAddress = null;
        if (applicant.Current_Address__c != null) {
            currentAddress = AddressService.getAddressById(applicant.Current_Address__c);
        }
        currentAddress = AddressService.createCurrentAddress(params, currentAddress);
        //Get & update permanent address record
        Address__c permanentAddress = null;
        if (applicant.Permanent_Address__c != null) {
            permanentAddress = AddressService.getAddressById(applicant.Permanent_Address__c);
            System.debug('got permanadentn address from applicant'+permanentAddress.Id);
        }
        Address__c addresscurr = AddressService.createPermanentAddress(params, null);
        if(addresscurr!=null){
            System.debug('got new values for permanent address');
            permanentAddress=addresscurr;
        }else System.debug('didnt get new values from json');
        
        Address__c RegisteredAddress =null;
        if (applicant.Registered_Address__c != null) {
             RegisteredAddress = AddressService.getAddressById(applicant.Registered_Address__c);
        }
        addresscurr = AddressService.createRegisteredAddress(params, null);
        if(addresscurr!=null){
            RegisteredAddress=addresscurr;
        }
        
        Address__c CorrespondenseAddress =null;
        if (applicant.Correspondence_Address__c != null) {
             CorrespondenseAddress = AddressService.getAddressById(applicant.Correspondence_Address__c);
        }
         addresscurr = AddressService.createCorrespondenceAddress(params, null);
        if(addresscurr!=null){
            CorrespondenseAddress=addresscurr;
        }
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
            if (permanentAddress != null) {
                upsert permanentAddress;
                applicant.Permanent_Address__c = permanentAddress.Id; //Update related Address Id
            }
            if (currentAddress != null) {
                upsert currentAddress;
                applicant.Current_Address__c = currentAddress.Id; //Update related Address Id
            }
            if(CorrespondenseAddress!=null){
                upsert CorrespondenseAddress;
                applicant.Correspondence_Address__c=CorrespondenseAddress.Id;
            }
            if(RegisteredAddress!=null){
                upsert RegisteredAddress;
                applicant.Registered_Address__c=RegisteredAddress.Id;
            }
            applicant=ApplicantService.createApplicant(params,applicant);
            update applicant;
            update borrowerAccount;
            
/*            //In case of Submit - additionally create Loan Application record
            //                    and Application_KYC record for "User Input" source
            String isSubmit = String.valueOf(params.get('submit'));
            if ( isSubmit != null && 'true'.equalsIgnoreCase(isSubmit) ) {
                LoanApplicationDTO loanApplicationMap = createLoanApplicationOnSubmit(borrowerAccount.Id, params);
                if('Individual'.equals(applicantType)) {
                    createApplicationKYCOnSubmit(loanApplicationMap.loanApplication.Id, applicant);
                }
                 //Update the KYC_Status to "Submitted"
                applicant.KYC_Status__c = 'Submitted';
                update applicant;
           } */
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
        
        return getBorrowerDenormalized(borrowerAccountId);
    }
    
    private static LoanApplicationDTO createLoanApplicationOnSubmit(String borrowerId, Map<String, Object> params) {
        params.put('borrowerId', borrowerId);
        params.put('kycStatus', 'Submitted');
        params.remove('submit'); //Important to remove this, otherwise Loan Application will be incorrectly created as Credit_Check_Status='Submitted'
        return LoanApplicationService.createLoanApplication(params);
    }
    
    private static void createApplicationKYCOnSubmitForIndividual(String loanApplicationId, Applicant__c applicant) {
        Map<String, Object> params = new Map<String, Object>();
        //Create "User Input" source record
        params.put('source', 'User Input');
        params.put('loanApplicationId', loanApplicationId);
        params.put('firstName', applicant.First_Name__c);
        params.put('lastName', applicant.Last_Name__c);
        params.put('dateOfBirth', applicant.Date_Of_Birth__c);
        params.put('gender', applicant.Gender__c);
        params.put('fathersFirstName', applicant.Fathers_First_Name__c);
        params.put('fathersLastName', applicant.Fathers_Last_Name__c);
        params.put('aadharNumber', applicant.Aadhar_Number__c);
        params.put('pan', applicant.PAN__c);
        params.put('voterID', applicant.Voter_ID__c);
        
        //Copy Permanent Address to "User Input" source record
        Address__c permanentAddress = AddressService.getAddressById(applicant.Permanent_Address__c);
        params.put('permanentAddressLine1', permanentAddress.Address_Line_1__c);
        params.put('permanentAddressLine2', permanentAddress.Address_Line_2__c);
        params.put('permanentAddressCity', permanentAddress.City__c);
        params.put('permanentAddressPincode', permanentAddress.Pincode__c);
        params.put('permanentAddressState', permanentAddress.State__c);
        params.put('permanentAddressCountry', permanentAddress.Country__c);
        params.put('currentAddressResidingSince', permanentAddress.Residing_Since__c);
        
        ApplicationKYCService.createApplicationKYC(params);
    }

    private static void createApplicationKYCOnSubmitForNonIndividual(
        String loanApplicationId, Applicant__c applicant, String companyFirmName) {
        Map<String, Object> params = new Map<String, Object>();
        //Create "User Input" source record
        params.put('source', 'User Input');
        params.put('loanApplicationId', loanApplicationId);
        params.put('companyFirmName', companyFirmName);
        params.put('dateOfIncorporation', applicant.Date_of_Incorporation__c);
        params.put('pan', applicant.PAN__c);
        
        //Copy Registered Address to "User Input" source record
        Address__c registeredAddress = AddressService.getAddressById(applicant.Registered_Address__c);
        params.put('registeredAddressLine1', registeredAddress.Address_Line_1__c);
        params.put('registeredAddressLine2', registeredAddress.Address_Line_2__c);
        params.put('registeredAddressCity', registeredAddress.City__c);
        params.put('registeredAddressPincode', registeredAddress.Pincode__c);
        params.put('registeredAddressState', registeredAddress.State__c);
        params.put('registeredAddressCountry', registeredAddress.Country__c);
        
        ApplicationKYCService.createApplicationKYC(params);
    }
}