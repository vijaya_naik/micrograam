public class CreditScoreCreateService {
    public static void createCreditScore(String creditScoringRuleSet, List<Object> paramsMap) {
        //Get Credit_Categories_Master Ids
        Map<String,String> categoriesMasterMap = CreditCategoriesMasterService.getCreditCategoriesMasterMap(creditScoringRuleSet);
        List<String> categoryIdsMasterList = categoriesMasterMap.values();
        //Get Credit_Category_Criteria_Master Ids for each Category
        Map<String,Map<String,String>> categoriesCriteriaMasterMap = new Map<String,Map<String,String>>();
        for(integer i=0;i<categoryIdsMasterList.size();i++){
            String categoryId = categoryIdsMasterList.get(i);
            Map<String,String> criteriaMasterMap = CreditCategoryCriteriaMasterService.getCriteriaMasterMap(categoryId);
            categoriesCriteriaMasterMap.put(categoryId, criteriaMasterMap);
        }
        
        //Create the Credit_Score record    
        for(Integer i=0;i<paramsMap.size();i++){
            Map<String, Object> params=(Map<String, Object>)paramsMap.get(i);
            //Create Application_KYC instance & set fields
            Application_Credit_Score__c applicationCreditScore = 
                createCreditScoreInstance(categoriesMasterMap, categoriesCriteriaMasterMap, params, null);
           
            //Update in database
            Savepoint sp = Database.setSavePoint();
            try {
                insert applicationCreditScore;
            } catch (Exception e) {
                Database.rollback(sp); //Rollback in case of error
                throw e;
            }
        }    
    }
    
    public static Application_Credit_Score__c createCreditScoreInstance(
        Map<String,String> categoriesMasterMap,
        Map<String,Map<String,String>> categoriesCriteriaMasterMap,
        Map<String, Object> params, Application_Credit_Score__c applicationCreditScore) {
        
        if (applicationCreditScore == null)    applicationCreditScore = new Application_Credit_Score__c(); //For new record creation
        boolean hasValue = false;
        if (params.containsKey('score')){
            applicationCreditScore.Score__c = Double.valueOf(params.get('score'));
            hasValue=true;
        }
        if (params.containsKey('loanApplicationId')) applicationCreditScore.Loan_Application_Id__c = String.valueOf(params.get('loanApplicationId'));

        //Get Category_Id from Category Name
        String categoryName = String.valueOf(params.get('categoryName'));
        String categoryId = categoriesMasterMap.get(categoryName);
        applicationCreditScore.Category_Id__c = categoryId;

        //Get Criterion_Id from Criterion Name
        String criterionName = String.valueOf(params.get('criterionName'));
        Map<String,String> criteriaMasterMap = categoriesCriteriaMasterMap.get(categoryId);
        String criterionId = criteriaMasterMap.get(criterionName);
        applicationCreditScore.Criterion_Id__c = criterionId;

        //if (params.containsKey('criterionId') && !''.equals(String.valueOf(params.get('criterionId'))) )        applicationCreditScore.Criterion_Id__c=String.valueOf(params.get('criterionId'));
        //if (params.containsKey('categoryId'))           applicationCreditScore.Category_Id__c=String.valueOf(params.get('categoryId'));
        
        if(hasValue)
            return applicationCreditScore;
        else 
            return null;
    }
}