@RestResource(urlMapping='/v1/loanapplication')
global with sharing class LoanApplicationResource {
    @HttpPost
    global static LoanApplicationDTO cLoanApplication() {
        RestRequest req = RestContext.request;
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received LoanApplication Registration request:: ' + params);
        // Invoke service
        return LoanApplicationService.createLoanApplication(params);
    }
    
    @HttpGet
    global static void getLoanApplication() {
        // Get id/accountId from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
        RestResponse res = RestContext.response;
    	if (res == null) {
        	res = new RestResponse();
        	RestContext.response = res;
    	}
        String borrowerAccountId = req.params.get('borrowerId');
        if ( borrowerAccountId != null ) {
            // Invoke service to get by Borrower Account id
            System.debug('Received LoanApplication Get request for borrowerAccountId=' + borrowerAccountId);
            res.responseBody = Blob.valueOf(JSON.serialize(LoanApplicationService.getLoanApplicationDenormalized(LoanApplicationService.getActiveApplicationForAccountId(borrowerAccountId))));
        res.statusCode = 200;
        }
        else {
            // Invoke service to get by Loan Application id
            System.debug('Received LoanApplication Get request for id=' + id);
                        res.responseBody = Blob.valueOf(JSON.serialize(LoanApplicationService.getLoanApplicationDenormalized(id)));
        res.statusCode = 200;
        }
    }
    
    @HttpDelete
    global static void deleteLoanApplication() {
        // Get id from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
        System.debug('Received LoanApplication Delete request for id=' + id);
        // Invoke service
        LoanApplicationService.deleteLoanApplication(id);
    }
    
    @HttpPatch
    global static LoanApplicationDTO updateLoanApplication() {
        RestRequest req = RestContext.request;
        // Get id from request parameter
        String id = req.params.get('id');
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received LoanApplication Update request for id=' + id + '::' + params);
        // Invoke service
        return LoanApplicationService.updateLoanApplication(id, params);
    }
}