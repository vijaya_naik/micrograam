@isTest(seealldata = false)
public class CreditCategoryCriteriaMasterServiceTest{
 
 public static  testmethod void criteriaTestMethods(){
 
   Credit_Categories_Master__c credMstr= new Credit_Categories_Master__c();
        credMstr.Category_Name__c='xyz';
        credMstr.Category_Order__c=4;
        credMstr.Category_Weight__c=3;
        insert credMstr;
        
    Credit_Category_Criteria_Master__c crdCriteria=new Credit_Category_Criteria_Master__c();
       crdCriteria.Category_Id__c=credMstr.id;
       crdCriteria.Criterion_Description__c='Microgram';
       crdCriteria.Criterion_Name__c='xyz'; 
       crdCriteria.Criterion_Display_Order__c=5;
           crdCriteria.Is_Leaf__c=true;
        insert  crdCriteria;
        
         Credit_Category_Criteria_Master__c crdCriteria1=new Credit_Category_Criteria_Master__c();
       crdCriteria1.Category_Id__c=credMstr.id;
       crdCriteria1.Criterion_Description__c='Microgram1';
       crdCriteria1.Criterion_Name__c='xydz'; 
       crdCriteria1.Criterion_Display_Order__c=7;
        insert  crdCriteria1;
        
     Map<String, Object> mapaddress=new Map<String, Object>();
        mapaddress.put('criterionName','pqr');
        mapaddress.put('criterionWeight',42);
        mapaddress.put('criterionDisplayOrder',3);
        mapaddress.put('criterionDescription','Microfram');   
        mapaddress.put('isLeaf','true');   
        CreditCategoryCriteriaMasterService mstr=new CreditCategoryCriteriaMasterService();
        CreditCategoryCriteriaMasterService.getCreditCategoryCriteriaMaster(crdCriteria.id);
        CreditCategoryCriteriaMasterService.getCriterionsforCategory(crdCriteria.id);
        CreditCategoryCriteriaMasterService.createCreditCategoryCriteriaMasterInstance(mapaddress,crdCriteria);
        CreditCategoryCriteriaMasterService.getCreditCategoryCriteriaMasterDenormalized(crdCriteria);
        CreditCategoryCriteriaMasterService.getCriteriaMasterMap(credMstr.id);
        CreditCategoryCriteriaMasterService.createCreditCategoryCriteriaMaster(mapaddress);
        CreditCategoryCriteriaMasterService.updateCreditCategoryCriteriaMaster(crdCriteria1.id,mapaddress);
        CreditCategoryCriteriaMasterService.deleteCreditCategoryCriteriaMaster(crdCriteria.id);
 
 }

}