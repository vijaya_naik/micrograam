@RestResource(urlMapping='/v1/borrower')
global with sharing class BorrowerResource {
    @HttpPost
    global static Map<String,String> registerBorrower() {
        RestRequest req = RestContext.request;
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received Borrower Registration request:: ' + params);
        // Invoke service
        return BorrowerService.registerBorrower(params);
    }
    
    @HttpGet
    global static Map<String,String> getBorrower() {
        // Get id from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
        System.debug('Received Borrower Get request for id=' + id);
        // Invoke service
        return BorrowerService.getBorrowerDenormalized(id);
    }
    
    @HttpDelete
    global static void deleteBorrower() {
        // Get id from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
        System.debug('Received Borrower Delete request for id=' + id);
        // Invoke service
        BorrowerService.deleteBorrower(id);
    }
    
    @HttpPatch
    global static Map<String,String> updateBorrower() {
        RestRequest req = RestContext.request;
        // Get id from request parameter
        String id = req.params.get('id');
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received Borrower Update request for id=' + id + '::' + params);
        // Invoke service
        return BorrowerService.updateBorrower(id, params);
    }
}