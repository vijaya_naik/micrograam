public interface IRequestProvider{
    Object execute(Integration_Message__c message);
}