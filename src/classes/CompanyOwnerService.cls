public class CompanyOwnerService {
public static Company_Owner__c getCompanyOwnerById(String companyOwnerId) {
    Company_Owner__c companyOwnerDetail = [
        SELECT id,name,Company_Owner_PAN__c,Loan_Application_Id__c,Owner_Address__C,
        Ownership_Percentage__C 
        from Company_Owner__c
        WHERE Id = :companyOwnerId
    ];
    return companyOwnerDetail;
}
    public static List<Company_Owner__c> getCompanyOwnerByLoanId(String loanApplicationId) {
    List<Company_Owner__c> companyOwnerDetail = [
        SELECT id,name,Company_Owner_PAN__c,Loan_Application_Id__c,Owner_Address__C,
        Ownership_Percentage__C 
        from Company_Owner__c
        WHERE Loan_Application_Id__c = :loanApplicationId
    ];
    return companyOwnerDetail;
}
    public static Map<String,String> getCompanyOwnerDenormalized(Company_Owner__c companyOwner) {
    Map<String,String> companyOwnerMap = new Map<String,String>();
    if(companyOwner!=null){
        if (companyOwner.Company_Owner_PAN__c != null) companyOwnerMap.put('companyOwnerPAN', String.valueOf(companyOwner.Company_Owner_PAN__c));
        if (companyOwner.Loan_Application_Id__c !=null)companyOwnerMap.put('loanApplicationId',String.valueOf(companyOwner.Loan_Application_Id__c));
        if (   companyOwner.Ownership_Percentage__c!= null)companyOwnerMap.put('ownershipPercentage',String.valueOf(companyOwner.Ownership_Percentage__c));
        if (   companyOwner.Name!=null)companyOwnerMap.put('companyOwnerName',String.valueOf(companyOwner.Name));
        if (   companyOwner.Owner_Address__c != null) {
            Address__c currentOfficeAddress = AddressService.getAddressById(companyOwner.Owner_Address__c);
            companyOwnerMap.putAll(AddressService.getOwnerAddressDenormalized(currentOfficeAddress));
        }
    }
    return companyOwnerMap;
}
        public static void createCompanyOwner(List<Map<String, Object>> params) {
            for (integer i=0;i<params.size();i++){
                 createCompanyOwner(params.get(i),null);
            }
            
        }
    public static Company_Owner__c createCompanyOwner(Map<String, Object> params,Company_Owner__c  companyOwner) {
    if(companyOwner==null)  companyOwner= new Company_Owner__c();

    if(params.containsKey('companyOwnerPAN'))
        companyOwner.Company_Owner_PAN__c = String.valueOf(params.get('companyOwnerPAN'));
        
    if(params.containsKey('loanApplicationId'))
    	companyOwner.Loan_Application_Id__c= String.valueOf(params.get('loanApplicationId'));
        
    if(params.containsKey('ownershipPercentage'))
    	companyOwner.Ownership_Percentage__c=String.valueOf(params.get('ownershipPercentage'));
        
    if(params.containsKey('companyOwnerName'))
    	companyOwner.Name=String.valueOf(params.get('companyOwnerName'));
   
    if(companyOwner.Name==null && companyOwner.Ownership_Percentage__c==null && companyOwner.Company_Owner_PAN__c== null)
        return null;
        else
		return companyOwner;

}
           public static List<Map<String,String>> getCompanyOwnerDenormalized(String loanApplicationId) {
               List<Map<String,String>> retval = new List<Map<String,String>>();
               List<Company_Owner__c> owners=getCompanyOwnerByLoanId(loanApplicationId);
               for (Integer i =0;i< owners.size();i++){
                   Map<String,String> companyOwnerMap = getCompanyOwnerDenormalized(owners.get(i));
                   retval.add(companyOWnerMap);
               }
 
    	return retval;
}
        public static void deleteWithLoanApplicationId(String loanApplicationId) {
            System.debug('deleted with loan application id'+loanApplicationId);
        List<Company_Owner__c> companyOwner=getCompanyOwnerByLoanId(loanApplicationId);
        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
 
            if(companyOwner!=null) {
                for(integer i=0;i<companyOwner.size();i++){
                    if ( companyOwner.get(i).Owner_Address__c != null ) {
                    	AddressService.deleteAddress(companyOwner.get(i).Owner_Address__c);
                    }
                    delete companyOwner.get(i);
                }
            }
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
}