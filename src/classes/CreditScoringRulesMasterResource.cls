@RestResource(urlMapping='/v1/creditScoringRulesMaster')
global with sharing class CreditScoringRulesMasterResource {
    
    @HttpGet
    global static List<CreditScoringRulesDTO> getAllCreditScoringRules() {
        List<CreditScoringRulesDTO> rulesList = new List<CreditScoringRulesDTO>();
        
        /* Ruleset #1 */
        String creditScoringRuleset1 = 'Individual - Personal Loan';
        rulesList.add(getRulesForRuleset(creditScoringRuleset1));

        /* Ruleset #2 */
        String creditScoringRuleset2 = 'Non-Individual - Standard';
        rulesList.add(getRulesForRuleset(creditScoringRuleset2));

        /* Ruleset #3 */
        String creditScoringRuleset3 = 'Individual - Housing Loan';
        rulesList.add(getRulesForRuleset(creditScoringRuleset3));

        return rulesList;
    }
    
    private static CreditScoringRulesDTO getRulesForRuleset(String creditScoringRuleset) {
        CreditScoringRulesDTO rulesDTO = new CreditScoringRulesDTO();
        //Get Categories for this ruleset
        List<Credit_Categories_Master__c> creditCategories = [
            SELECT Id, Category_Name__c,Category_Order__c,Category_Weight__c,Credit_Scoring_Ruleset__c
            FROM Credit_Categories_Master__c 
            WHERE Credit_Scoring_Ruleset__c = :creditScoringRuleset
        ];
        rulesDTO.creditScoringRuleSet = creditScoringRuleset;
        //Get Criteria for each category
        Map<String, List<Map<String,String>>> creditCategoryCriteriaMap = new Map<String, List<Map<String,String>>>();
        for( integer i=0; i < creditCategories.size(); i++) {
            //Get denormalized Category attributes
            Credit_Categories_Master__c category = creditCategories.get(i);
            List<Map<String,String>> valuesMap = new List<Map<String,String>>();
            valuesMap.add(CreditCategoriesMasterService.getCreditCategoriesMasterDenormalized(category));
            
            //Get denormalized Criteria attributes
            List<Credit_Category_Criteria_Master__c> criteria = CreditCategoryCriteriaMasterService.getCriterionsforCategory(category.Id);
            for( integer j=0; j < criteria.size(); j++ ) {
                Credit_Category_Criteria_Master__c criterion = criteria.get(j);
                valuesMap.add(CreditCategoryCriteriaMasterService.getCreditCategoryCriteriaMasterDenormalized(criterion));
            }

            creditCategoryCriteriaMap.put(category.Category_Name__c, valuesMap);
        }
        rulesDTO.creditCategoryCriteria = creditCategoryCriteriaMap;
        
        return rulesDTO;        
    }
}