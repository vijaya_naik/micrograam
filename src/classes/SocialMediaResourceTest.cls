@isTest(seeAllData=false)
public class SocialMediaResourceTest  {
    static Address__c addres;
    static Account acc;
    static Income_Detail__c incomeDetail;
    static Loan_Application__c loanApp ;
    static Application_Social_Media__c  appSocialMedia;
    
    private static final String ORG_URL = URL.getSalesforceBaseUrl().toExternalForm() ;
      
     static void init() {
    // setup test data 
    //create dummy Address Record
       addres =new  Address__c();
       addres.Address_Line_1__c='Street 1';
       addres.Address_Line_2__c='Street 2';
       addres.City__c='Bangalore ';
       addres.Pincode__c= 12345;
       addres.State__c='KA';
       addres.Country__c='India'; 
       addres.Residing_Since__c=System.today();
       addres.House_Ownership__c='Owned';
       insert addres;
          
       //create dummy account record
       acc=new Account();
       acc.Name='Microgram';
       acc.Account_Type__c='Borrower'; 
       insert acc;
       
       //create dummy Income Detail Record
       incomeDetail = new Income_Detail__c();
       incomeDetail.Designation__c='Manager';
       incomeDetail.Office_Address__c=addres.id;
       incomeDetail.Annual_Income__c=30000;
       insert incomeDetail;
       
       //create dummmy Loan Application Record.
       loanApp = new Loan_Application__c();
       loanApp.Account_Id__c=acc.id;
       loanApp.Reference_1_Address__c=addres.id;
       loanApp.Reference_2_Address__c=addres.id;
       loanApp.Income_Detail_Current_Job__c=incomeDetail.id;
       loanApp.Income_Detail_Previous_Job__c=incomeDetail.id;
       insert loanApp; 
        //create dummy Application_Social_Media__c  record.
       appSocialMedia = new Application_Social_Media__c ();
       appSocialMedia.Loan_Application_Id__c = loanApp.Id;
       appSocialMedia.Social_Media_Score__c = 'test';
       appSocialMedia.Source__c = 'Vendor';
       insert appSocialMedia;  
  }
      
  static testMethod void testDoPost() {
      init();
      //String JsonMsg = '{"totalResults": 1, "startIndex":0, "Application_Social_Media__c":[{"socialMediaScore":socialMediaScore,"source":Vendor,"loanApplicationId":loanApp.Id}]}';
      Map<String, Object> paramMap = new Map<String, Object>();
      paramMap.put('socialMediaScore','socialMediaScore');
      paramMap.put('source','Vendor');
      paramMap.put('loanApplicationId',loanApp.id);
      String JsonMsg=JSON.serialize(paramMap);
      RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/socialMedia';  //Request URL
      req.httpMethod = 'POST';
      req.requestBody = Blob.valueof(JsonMsg);
      RestContext.request = req;
      RestContext.response= res;
    
      Map<String,String> stas=SocialMediaResource.cSocialMedia();

  }
  
   static testMethod void testDoGet() {
    init();
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();

    req.requestURI = ORG_URL+'/services/apexrest/v.37.0/v1/socialMedia';  
    req.httpMethod = 'GET';
    req.addParameter('loanApplicationId', loanApp.Id);      
    req.addParameter('source', 'Vendor');        
    RestContext.request = req;
    RestContext.response = res;
    Map<String,String> results=new Map<String,String>();
    results = SocialMediaResource.getSocialMedia();
  }
  
  static testMethod void testDoDelete() {
    init();
    RestRequest req = new RestRequest(); 

    req.requestURI = ORG_URL+'/services/apexrest/v.37.0/v1/socialMedia';  
    req.httpMethod = 'DELETE';
    req.addParameter('id', appSocialMedia.Id);    
    RestContext.request = req;
    SocialMediaResource.deleteSocialMedia();  
    //RestContext.request = req;
    //RestContext.response = res;
     //SocialMediaService.deleteSocialMedia(appSocialMedia.Id);
  }
  
  static testMethod void testDoPatch() {
      init();
      //String JsonMsg = '{"totalResults": 1, "startIndex":0, "Application_Social_Media__c":[{"socialMediaScore":socialMediaScore,"source":Vendor,"loanApplicationId":loanApp.Id}]}';
      Map<String, Object> paramMap = new Map<String, Object>();
      paramMap.put('socialMediaScore','socialMediaScore');
      paramMap.put('source','Vendor');
      paramMap.put('loanApplicationId',loanApp.id);
      String JsonMsg=JSON.serialize(paramMap);
      RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/socialMedia';  //Request URL
      req.httpMethod = 'PATCH';
      req.addParameter('loanApplicationId', loanApp.Id);      
      req.addParameter('source', 'Vendor');       
      req.requestBody = Blob.valueof(JsonMsg);
      RestContext.request = req;
      RestContext.response= res;
    
      SocialMediaResource.updateSocialMedia();

  }
}