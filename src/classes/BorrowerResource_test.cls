/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class BorrowerResource_test {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        try{
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();

    // pass the req and resp objects to the method     
    req.requestURI = 'https://cs9.salesforce.com/services/apexrest/v1/borrower';  
    req.httpMethod = 'POST';
    
      Map<String, Object> params = new  Map<String, Object>();
        params.put('Body','test');
        params.put('id','Shambhavi');
       // req.requestBody();
       
      String JsonMsg=JSON.serialize(params);
      req.requestBody = Blob.valueof(JsonMsg);
      RestContext.request = req;
      RestContext.response= res;
      BorrowerResource.registerBorrower();

     
      BorrowerResource.updateBorrower();
      }Catch(Exception e){}
    }
     static testMethod void myUnitTestDelete() {
     try{
       RestRequest req = new RestRequest(); 
       RestResponse res = new RestResponse();
       req.requestURI = 'https://cs9.salesforce.com/services/apexrest/v1/borrower';  
       req.httpMethod = 'Delete';
       
        
      Map<String, Object> params = new  Map<String, Object>();
        params.put('Body','test');
        params.put('id','Shambhavi');
        
        String JsonMsg=JSON.serialize(params);
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        BorrowerResource.deleteBorrower();
        }Catch(Exception e){}
     }
      static testMethod void myUnitTestGet() {
      try{
       RestRequest req = new RestRequest(); 
       RestResponse res = new RestResponse();
       req.requestURI = 'https://cs9.salesforce.com/services/apexrest/v1/borrower';  
       req.httpMethod = 'Get';
       
        
      Map<String, Object> params = new  Map<String, Object>();
        params.put('Body','test');
        params.put('id','Shambhavi');
        
        String JsonMsg=JSON.serialize(params);
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        
        BorrowerResource.getBorrower();   
        }Catch(Exception e){}
          }
          
       static testMethod void myUnitTestUpdate() {
       try{
         RestRequest req = new RestRequest(); 
       RestResponse res = new RestResponse();
       req.requestURI = 'https://cs9.salesforce.com/services/apexrest/v1/borrower';  
       req.httpMethod = 'Patch';
       
        
       Map<String, Object> params = new  Map<String, Object>();
        params.put('Body','test');
        params.put('id','Shambhavi');
        BorrowerResource.updateBorrower (); 
        }Catch(Exception e){}
        }
     
}