public class ResponseHandlerPsychometric implements IResponseHandler {
    public void execute(Object response, Integration_Message__c message)
    {
         HttpResponse resp = (HttpResponse) response;
         System.debug('Psy Response data'+resp);
         String jsonString = resp.getBody();
         
         Map<String, Object> obj = (Map<String, Object>) JSON.deserializeUntyped(jsonString);
         
         Map<String, Object> callbackObj = (Map<String, Object>) obj.get('callbackObj');
         Application_Psychometric__c appPsyDetails = new Application_Psychometric__c();
         appPsyDetails.Loan_Application_Id__c=message.Input_String__c;
         appPsyDetails.Willing_To_Repay__c=(String)callbackObj.get('willing to Repay');
         appPsyDetails.Ability_To_Repay__c=(String)callbackObj.get('ability to Repay');
         appPsyDetails.Custom_External_Id__c=message.Input_String__c + 'Psychometric Integration';
         upsert appPsyDetails Custom_External_Id__c;
    }

}