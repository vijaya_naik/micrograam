@RestResource(urlMapping='/v1/lender')
global with sharing class LenderResource {
    @HttpPost
    global static Map<String,String> registerLender() {
        RestRequest req = RestContext.request;
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received Lender Registration request:: ' + params);
        // Invoke service
        return LenderService.registerLender(params);
    }
    
    @HttpGet
    global static Map<String,String> getLender() {
        // Get id from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
        System.debug('Received Lender Get request for id=' + id);
        // Invoke service
        return LenderService.getLenderDenormalized(id);
    }
    
    @HttpDelete
    global static void deleteLender() {
        // Get id from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
        System.debug('Received Lender Delete request for id=' + id);
        // Invoke service
        LenderService.deleteLender(id);
    }
    
    @HttpPatch
    global static Map<String,String> updateLender() {
        RestRequest req = RestContext.request;
        // Get id from request parameter
        String id = req.params.get('id');
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received Lender Update request for id=' + id + '::' + params);
        // Invoke service
        return LenderService.updateLender(id, params);
    }
}