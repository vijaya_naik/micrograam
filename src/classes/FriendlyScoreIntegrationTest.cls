@isTest (SeeAllData=false)
private class FriendlyScoreIntegrationTest{
    static testMethod void testOne(){
        IntegrationMessageProvider__c messageProvider = new IntegrationMessageProvider__c(
            Name = Label.FriendlyScore,
            Request_Provider__c = 'RequestProviderFriendlyScore',
            Response_Handler__c = 'ResponseHandlerFriendlyScore',
            No_of_Retries__c = 5
        );
        insert messageProvider;
        
        Account acc = new Account(
            Name = 'Test'
        );
        insert acc;
        
        Loan_Application__c loanApp = new Loan_Application__c(
            Account_Id__c = acc.Id,
            FriendlyScore_Submitted__c = TRUE
        );
        Test.startTest();
        insert loanApp;
        Test.stopTest();
    }
    
    
    static testMethod void testTwo(){
        IntegrationMessageProvider__c messageProvider = new IntegrationMessageProvider__c(
            Name = Label.FriendlyScore,
            Request_Provider__c = 'RequestProviderFriendlyScore',
            Response_Handler__c = 'ResponseHandlerFriendlyScore',
            No_of_Retries__c = 5
        );
        insert messageProvider;
        
        Account acc = new Account(
            Name = 'Test'
        );
        insert acc;
        
        Loan_Application__c loanApp = new Loan_Application__c(
            Account_Id__c = acc.Id,
            FriendlyScore_Submitted__c = TRUE
        );
        
        AccessTokenHandlerFriendlyScore tokenHanlder = new AccessTokenHandlerFriendlyScore();
        tokenHanlder.storeAccessToken(Label.FriendlyScore,'aaaaaa');
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new IntegrationFriendlyScoreTestMock());
        insert loanApp;
        Test.stopTest();
    }
    
    
    static testMethod void testThree(){
        IntegrationMessageProvider__c messageProvider = new IntegrationMessageProvider__c(
            Name = Label.FriendlyScore,
            Request_Provider__c = 'RequestProviderFriendlyScore',
            Response_Handler__c = 'ResponseHandlerFriendlyScore',
            No_of_Retries__c = 5
        );
        insert messageProvider;
        
        Account acc = new Account(
            Name = 'Test'
        );
        insert acc;
        
        Loan_Application__c loanApp = new Loan_Application__c(
            Account_Id__c = acc.Id,
            FriendlyScore_Submitted__c = FALSE
        );
        insert loanApp;
        
        AccessTokenHandlerFriendlyScore tokenHanlder = new AccessTokenHandlerFriendlyScore();
        tokenHanlder.storeAccessToken('aaaaaa', Label.FriendlyScore);
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new IntegrationFriendlyScoreTestMock());
        loanApp.FriendlyScore_Submitted__c = TRUE;
        update loanApp;
        Test.stopTest();
    }
}