public class ApplicantService {
    public static Applicant__c getApplicantForAccountId(String accountId) {
        Applicant__c[] applicant = [
            SELECT Id, Account_Id__c, Applicant_Nature__c,
                   First_Name__c, Middle_Name__c, Last_Name__c,
                   Current_Address__c, Permanent_Address__c,pan__c,aadhar_Number__c,Voter_ID__c,
                   Bank_Account_Number__c,bank_IFSC_Code__c,bank_Name__c,completion_Year__c,
                   date_Of_Birth__c,e_Mail__c,fathers_First_Name__c,fathers_Last_Name__c,
                   fathers_Middle_Name__c,gender__c,highest_Qualification_Desc__c,highest_Qualification_Type__c,
                   landline_Number__c,landline_STD_Code__c,Last_Education_Institute__c,marital_Status__c,mobile_Number__c,
                   no_Of_Dependent_Children__c,no_Of_Dependents__c,office_Number__c,office_Extension__c,
                   office_STD_Code__c,profile_facebook__c,profile_linkedIn__c,profile_twitter__c,KYC_Status__c,
                   Registered_Address__c,Correspondence_Address__c,
                   Registration_Number__c,Date_of_Incorporation__c,
                   Primary_Contact_Name__c,Name,Aadhaar_Bridge_Otp__c
            FROM Applicant__c
            WHERE Account_Id__c = :accountId LIMIT 1
        ];
        
        if ( applicant.size() > 0 )
            return applicant[0];
        else
            return null;
    }

    public static Applicant__c createApplicant(Map<String, Object> params, Applicant__c applicant) {
        if (applicant == null)
            applicant = new Applicant__c(); //For new record creation
         if (params.containsKey('dateOfIncorporation') && params.get('dateOfIncorporation') != null)
            applicant.Date_of_Incorporation__c=Date.valueOf(String.valueOf(params.get('dateOfIncorporation')));
          if (params.containsKey('registrationNumber'))
            applicant.Registration_Number__c = String.valueOf(params.get('registrationNumber'));
         if (params.containsKey('primaryContactName'))
            applicant.Primary_Contact_Name__c = String.valueOf(params.get('primaryContactName'));
        if (params.containsKey('applicantNature'))
            applicant.Applicant_Nature__c = String.valueOf(params.get('applicantNature'));
        if (params.containsKey('firstName'))
            applicant.First_Name__c =  String.valueOf(params.get('firstName'));
        if (params.containsKey('middleName'))
            applicant.Middle_Name__c = String.valueOf(params.get('middleName'));
        if (params.containsKey('lastName'))
            applicant.Last_Name__c =String.valueOf(params.get('lastName'));
        if (params.containsKey('aadharNumber'))
            applicant.Aadhar_Number__c=Double.valueOf(params.get('aadharNumber'));
        if (params.containsKey('voterID'))
            applicant.Voter_ID__c=String.valueOf(params.get('voterID'));
        if (params.containsKey('bankAccountNumber'))
            applicant.Bank_Account_Number__c=Double.valueOf(params.get('bankAccountNumber'));
        if (params.containsKey('bankIFSCCode'))
            applicant.Bank_IFSC_Code__c=String.valueOf(params.get('bankIFSCCode'));
        if (params.containsKey('bankName'))
            applicant.Bank_Name__c=String.valueOf(params.get('bankName'));
        if (params.containsKey('completionYear'))
            applicant.Completion_Year__c=Integer.valueOf(params.get('completionYear'));
        if (params.containsKey('dateOfBirth') && params.get('dateOfBirth') != null)
            applicant.Date_Of_Birth__c=Date.valueOf(String.valueOf(params.get('dateOfBirth')));
        if (params.containsKey('eMail'))
            applicant.E_Mail__c=String.valueOf(params.get('eMail'));
        if (params.containsKey('fathersFirstName'))
            applicant.Fathers_First_Name__c=String.valueOf(params.get('fathersFirstName'));
        if (params.containsKey('fathersLastName'))
            applicant.Fathers_Last_Name__c=String.valueOf(params.get('fathersLastName'));
        if (params.containsKey('fathersMiddleName'))
            applicant.Fathers_Middle_Name__c=String.valueOf(params.get('fathersMiddleName'));
        if (params.containsKey('gender'))
            applicant.Gender__c=String.valueOf(params.get('gender'));
        if (params.containsKey('highestQualificationDesc'))
            applicant.Highest_Qualification_Desc__c=String.valueOf(params.get('highestQualificationDesc'));
        if (params.containsKey('highestQualificationType'))
            applicant.Highest_Qualification_Type__c=String.valueOf(params.get('highestQualificationType'));
        if (params.containsKey('landlineNumber'))
            applicant.Landline_Number__c=Integer.valueOf(params.get('landlineNumber'));
        if (params.containsKey('landlineSTDCode'))
            applicant.Landline_STD_Code__c=Integer.valueOf(params.get('landlineSTDCode'));
        if (params.containsKey('lastEducationInstitute'))
            applicant.Last_Education_Institute__c=String.valueOf(params.get('lastEducationInstitute'));
        if (params.containsKey('maritalStatus'))
            applicant.Marital_Status__c=String.valueOf(params.get('maritalStatus'));
        if (params.containsKey('mobileNumber'))
            applicant.Mobile_Number__c=String.valueOf(params.get('mobileNumber'));
        if (params.containsKey('noOfDependentChildren'))
            applicant.No_Of_Dependent_Children__c=Integer.valueOf(params.get('noOfDependentChildren'));
        if (params.containsKey('noOfDependents'))
            applicant.No_Of_Dependents__c=Integer.valueOf(params.get('noOfDependents'));
        if (params.containsKey('officeNumber'))
            applicant.Office_Number__c=String.valueOf(params.get('officeNumber'));
        if (params.containsKey('officeExtension'))
            applicant.Office_Extension__c=Integer.valueOf(params.get('officeExtension'));
        if (params.containsKey('officeSTDCode'))
            applicant.Office_STD_Code__c=Integer.valueOf(params.get('officeSTDCode'));
        if (params.containsKey('pan'))
            applicant.PAN__c=String.valueOf(params.get('pan'));
        if (params.containsKey('profileFacebook'))
            applicant.Profile_Facebook__c=String.valueOf(params.get('profileFacebook'));
        if (params.containsKey('profileLinkedIn'))
            applicant.Profile_LinkedIn__c=String.valueOf(params.get('profileLinkedIn'));
        if (params.containsKey('profileTwitter'))
            applicant.Profile_Twitter__c=String.valueOf(params.get('profileTwitter'));
        if(params.containsKey('aadhaarBridgeOTP'))
            applicant.Aadhaar_Bridge_Otp__c=String.valueOf(params.get('aadhaarBridgeOTP'));
        return applicant;
    }

    public static Map<String,String> getApplicantDenormalized(Applicant__c applicant) {
        Map<String,String> applicantMap = new Map<String,String>();

        if (applicant.Applicant_Nature__c != null) applicantMap.put('applicantNature', applicant.Applicant_Nature__c);
        if (applicant.First_Name__c != null) applicantMap.put('firstName', applicant.First_Name__c);
        if (applicant.Middle_Name__c != null) applicantMap.put('middleName', applicant.Middle_Name__c);
        if (applicant.Last_Name__c != null) applicantMap.put('lastName', applicant.Last_Name__c);
        if (applicant.Aadhar_Number__c != null) applicantMap.put('aadharNumber', String.valueOf(applicant.Aadhar_Number__c));
        if (applicant.Voter_ID__c != null) applicantMap.put('voterID', String.valueOf(applicant.Voter_ID__c));
        if (applicant.Bank_Account_Number__c != null) applicantMap.put('bankAccountNumber', String.valueOf(applicant.Bank_Account_Number__c));
        if (applicant.Bank_IFSC_Code__c != null) applicantMap.put('bankIFSCCode', applicant.Bank_IFSC_Code__c);
        if (applicant.Bank_Name__c != null) applicantMap.put('bankName', applicant.Bank_Name__c);
        if (applicant.Completion_Year__c != null) applicantMap.put('completionYear', String.valueOf(applicant.Completion_Year__c));
        if (applicant.Date_Of_Birth__c != null) applicantMap.put('dateOfBirth', String.valueOf(applicant.Date_Of_Birth__c));
        if (applicant.E_Mail__c != null) applicantMap.put('eMail', applicant.E_Mail__c);
        if (applicant.Fathers_First_Name__c != null) applicantMap.put('fathersFirstName', applicant.Fathers_First_Name__c);
        if (applicant.Fathers_Middle_Name__c != null) applicantMap.put('fathersMiddleName', applicant.Fathers_Middle_Name__c);
        if (applicant.Fathers_Last_Name__c != null) applicantMap.put('fathersLastName', applicant.Fathers_Last_Name__c);
        if (applicant.Gender__c != null) applicantMap.put('gender', applicant.Gender__c);
        if (applicant.Highest_Qualification_Desc__c != null) applicantMap.put('highestQualificationDesc', applicant.Highest_Qualification_Desc__c);
        if (applicant.Highest_Qualification_Type__c != null) applicantMap.put('highestQualificationType', applicant.Highest_Qualification_Type__c);
        if (applicant.KYC_Status__c != null) applicantMap.put('kycStatus', applicant.KYC_Status__c);
        if (applicant.Landline_Number__c != null) applicantMap.put('landlineNumber', String.valueOf(applicant.Landline_Number__c));
        if (applicant.Landline_STD_Code__c != null) applicantMap.put('landlineSTDCode', String.valueOf(applicant.Landline_STD_Code__c));
        if (applicant.Last_Education_Institute__c != null) applicantMap.put('lastEducationInstitute', applicant.Last_Education_Institute__c);
        if (applicant.Marital_Status__c != null) applicantMap.put('maritalStatus', applicant.Marital_Status__c);
        if (applicant.Mobile_Number__c != null) applicantMap.put('mobileNumber', applicant.Mobile_Number__c);
        if (applicant.No_Of_Dependent_Children__c != null) applicantMap.put('noOfDependentChildren', String.valueOf(applicant.No_Of_Dependent_Children__c));
        if (applicant.No_Of_Dependents__c != null) applicantMap.put('noOfDependents', String.valueOf(applicant.No_Of_Dependents__c));
        if (applicant.Office_Number__c != null) applicantMap.put('officeNumber', applicant.Office_Number__c);
        if (applicant.Office_Extension__c != null) applicantMap.put('officeExtension', String.valueOf(applicant.Office_Extension__c));
        if (applicant.Office_STD_Code__c != null) applicantMap.put('officeSTDCode', String.valueOf(applicant.Office_STD_Code__c));
        if (applicant.PAN__c != null) applicantMap.put('pan', applicant.PAN__c);
        if (applicant.Profile_Facebook__c != null) applicantMap.put('profileFacebook', applicant.Profile_Facebook__c);
        if (applicant.Profile_LinkedIn__c != null) applicantMap.put('profileLinkedIn', applicant.Profile_LinkedIn__c);
        if (applicant.Profile_Twitter__c != null) applicantMap.put('profileTwitter', applicant.Profile_Twitter__c);
        if (applicant.Primary_Contact_Name__c != null) applicantMap.put('primaryContactName', applicant.Primary_Contact_Name__c);
        if (applicant.Date_of_Incorporation__c != null) applicantMap.put('dateOfIncorporation', String.valueOf(applicant.Date_of_Incorporation__c));
        if (applicant.Registration_Number__c != null) applicantMap.put('registrationNumber', applicant.Registration_Number__c);
        if (applicant.Registered_Address__c != null) applicantMap.putAll(AddressService.getRegisteredAddressDenormalized(AddressService.getAddressById(applicant.Registered_Address__c)));
        if (applicant.Correspondence_Address__c != null) applicantMap.putAll(AddressService.getCorrespondenceAddressDenormalized(AddressService.getAddressById(applicant.Correspondence_Address__c)));
        if (applicant.Profile_Twitter__c != null) applicantMap.put('profileTwitter', applicant.Profile_Twitter__c);
        if (applicant.Aadhaar_Bridge_Otp__c != null) applicantMap.put('aadhaarBridgeOTP',applicant.Aadhaar_Bridge_Otp__c);
        
        return applicantMap;
    }
}