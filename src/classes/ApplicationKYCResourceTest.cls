@isTest(seeAllData=false)
public class ApplicationKYCResourceTest {

    static Address__c addres;
    static Address__c add;
    static Account acc;
    static Income_Detail__c incomeDetail;
    static Loan_Application__c loanApp ;
    static Application_KYC__c m;
    static Application_Social_Media__c  appSocialMedia;
    
    private static final String ORG_URL = URL.getSalesforceBaseUrl().toExternalForm() ;
      
     static void init() {
       // setup test data 
       //create dummy Address Record
       addres =new  Address__c();
       addres.Address_Line_1__c='Street 1';
       addres.Address_Line_2__c='Street 2';
       addres.City__c='Bangalore ';
       addres.Pincode__c= 12345;
       addres.State__c='KA';
       addres.Country__c='India'; 
       addres.Residing_Since__c=System.today();
       addres.House_Ownership__c='Owned';
       insert addres;
       
       add= new Address__c();
        add.State__c='1';
        add.Address_Line_1__c='aa';
        add.Address_Line_2__c='aac';
        add.City__c='qq';
        add.Country__c='aa';
        add.Pincode__c=111111.10;
        add.House_Ownership__c='rented';
        add.Residing_Since__c=Date.today();
        insert add ;
          
       //create dummy account record
       acc=new Account();
       acc.Name = 'XXX';
       acc.Account_Type__c='Borrower'; 
       insert acc;
       
       //create dummy Income Detail Record
       incomeDetail = new Income_Detail__c();
       incomeDetail.Designation__c='Manager';
       incomeDetail.Office_Address__c=addres.id;
       incomeDetail.Annual_Income__c=30000;
       insert incomeDetail;
       
       //create dummmy Loan Application Record.
       loanApp = new Loan_Application__c();
       loanApp.Account_Id__c=acc.id;
       loanApp.Reference_1_Address__c=addres.id;
       loanApp.Reference_2_Address__c=addres.id;
       loanApp.Income_Detail_Current_Job__c=incomeDetail.id;
       loanApp.Income_Detail_Previous_Job__c=incomeDetail.id;
       insert loanApp; 
       
       //create dummy application KYC
        m= new Application_KYC__c();
        m.Source__c='User Input';
        m.Loan_Application_Id__c=loanApp.Id;
        m.Aadhar_Number__c=1234567;
        m.Date_Of_Birth__c=Date.today();
        m.First_Name__c='User Input';
        m.Last_Name__c='User Input';
        m.Fathers_First_Name__c='User Input';
        m.Fathers_Last_Name__c='User Input';
        m.PAN__c='User Input';
        m.Gender__c='User Input';
        m.Permanent_Address__c=add.Id;
        m.Voter_ID__c='User Input';
        m.Loan_Application_Id__c=loanApp.Id;
        m.Account_Id__c=acc.Id;
        m.Account_Id__c=acc.Id;
        insert m;
      
  }
  
    static testMethod void testDoPost() {
      init();
      Map<String,Object> paramMap = new Map<String,Object>();
        paramMap.put('aadharNumber','12345678');
        paramMap.put('dateOfBirth',Date.today());
        paramMap.put('firstName','asdfgh');
        paramMap.put('fathersFirstName','sdfgh');
        paramMap.put('lastName','sdfghj');
        paramMap.put('fathersLastName','werfgh');
        paramMap.put('aadharNumber','1234567');
        paramMap.put('pan','12345678df');
        paramMap.put('source','qwerty');
        paramMap.put('voterID','12345678');
        paramMap.put('loanApplicationId',loanApp.Id);
        paramMap.put('lenderId',acc.Id);
        paramMap.put('permanentAddressLine1','12345678');
        paramMap.put('permanentAddressLine2','12345678');
        paramMap.put('permanentAddressCity','12345678');
        paramMap.put('permanentAddressPincode','123456');
        paramMap.put('permanentAddressState','12345678');
        paramMap.put('permanentAddressCountry','12345678');
        paramMap.put('permanentAddressResidingSince',Date.today());
      String JsonMsg=JSON.serialize(paramMap);
      RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/v1/applicationKYC';  //Request URL
      req.httpMethod = 'POST';
      req.requestBody = Blob.valueof(JsonMsg);
      RestContext.request = req;
      RestContext.response= res;
    
      Map<String,String> results = ApplicationKYCResource.cApplicationKYC();

  }
  static testMethod void testDoGet() {
    init();
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();

    req.requestURI = ORG_URL+'/services/apexrest/v1/applicationKYC';
    req.httpMethod = 'GET';
    req.addParameter('id', m.Id);      
    req.addParameter('loanApplicationId', loanApp.Id);      
    req.addParameter('lenderId',acc.Id);      
    req.addParameter('source', 'User Input');        
    RestContext.request = req;
    RestContext.response = res;
    Map<String,String> results=new Map<String,String>();
    results = ApplicationKYCResource.getApplicationKYC();
  }
  
  static testMethod void testDoDelete() {
    init();
    RestRequest req = new RestRequest(); 

    req.requestURI = ORG_URL+'/services/apexrest/v1/applicationKYC'; 
    req.httpMethod = 'DELETE';
    req.addParameter('id', m.Id);    
    RestContext.request = req;
    ApplicationKYCResource.deleteApplicationKYC();  
   
  }
  
  static testMethod void testDoPatch() {
      init();
      //String JsonMsg = '{"totalResults": 1, "startIndex":0, "Application_Social_Media__c":[{"socialMediaScore":socialMediaScore,"source":Vendor,"loanApplicationId":loanApp.Id}]}';
      Map<String, Object> paramMap = new Map<String, Object>();
      paramMap.put('socialMediaScore','socialMediaScore');
      paramMap.put('source','Vendor');
      paramMap.put('loanApplicationId',loanApp.id);
      String JsonMsg=JSON.serialize(paramMap);
      RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/v1/applicationKYC';  //Request URL
      req.httpMethod = 'PATCH';
      req.addParameter('id', m.Id);   
      req.addParameter('loanApplicationId', loanApp.Id);   
      req.addParameter('lenderId', acc.Id);      
      req.addParameter('source', 'User Input');       
      req.requestBody = Blob.valueof(JsonMsg);
      RestContext.request = req;
      RestContext.response= res;
    
      Map<String,String> results=new Map<String,String>();
      results = ApplicationKYCResource.updateApplicationKYC();

  }
  
}