/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest
private class CreditOfficerResource_test {
    
    static testMethod void myUnitTest() {
    try{
        // TO DO: implement unit test
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        // pass the req and resp objects to the method     
        req.requestURI = 'https://cs9.salesforce.com/services/apexrest/v1/creditofficer/loanapplication';  
        req.httpMethod = 'GET';
        
        Account acct = TestUtility.createAccount('testacc','true','Lender' );
        insert acct;
        
        Applicant__c aplcnt = TestUtility.objmgp2pApplicant();
        aplcnt.Account_Id__c = acct.Id;
        insert aplcnt;
        
        Loan_Application__c lnaplcnt = TestUtility.createApplication(acct.Id);
        insert lnaplcnt;
        
        Credit_Categories_Master__c ccm = TestUtility.objeccmst();
        insert ccm;
        
        Credit_Category_Criteria_Master__c cccm = TestUtility.objcccmst(ccm.Id);
        insert cccm;
        
        //Map<String, Object> params = new  Map<String, Object>();
        req.params.put('loanApplicationId','test');
        req.params.put('lenderId','Shambhavi');
        req.params.put('approvalType','Shambhavi');
        req.params.put('applicantType','Individual');
        req.params.put('kycStatus','Pass');
        req.params.put('creditCheckStatus','Pass');
        // req.requestBody();
        
        
       // String JsonMsg=JSON.serialize(params);
      //  req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
        
        CreditOfficerResource.getData();
        }Catch(Exception e){}
    }
    static testMethod void myUnitTestTwo() {
        try{
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        CreditOfficerResource.getData();
        }Catch(Exception e){}
    }
    static testMethod void myUnitTestApplicationKYC() {
    try{
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'https://cs9.salesforce.com/services/apexrest/v1/creditofficer/applicationKYC';  
        req.httpMethod = 'GET'; 
        
        Map<String, Object> params = new  Map<String, Object>();
         req.params.put('loanApplicationId','test');
         req.params.put('lenderId','Shambhavi');
         req.params.put('approvalType','Shambhavi');
        // req.requestBody();
        
        String JsonMsg=JSON.serialize(params);
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        RestContext.response= res;
        
        CreditOfficerResource.getData();   
        }Catch(Exception e){} 
    }
    static testMethod void myUnitTestCreditBureau() {
        try{
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'https://cs9.salesforce.com/services/apexrest/v1/creditofficer/creditBureau';  
        req.httpMethod = 'GET'; 
        
        Map<String, Object> params = new  Map<String, Object>();
        req.params.put('loanApplicationId','test');
        req.params.put('lenderId','Shambhavi');
        req.params.put('approvalType','Shambhavi');
        // req.requestBody();
        
        String JsonMsg=JSON.serialize(params);
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        RestContext.response= res;
        
        CreditOfficerResource.getData();
        }Catch(Exception e){}
    }
    static testMethod void myUnitTestreditScore() {
        try{
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'https://cs9.salesforce.com/services/apexrest/v1/creditofficer/creditScore';  
        req.httpMethod = 'GET'; 
        
        Map<String, Object> params = new  Map<String, Object>();
        req.params.put('loanApplicationId','test');
        req.params.put('lenderId','Shambhavi');
        req.params.put('approvalType','kyc');
        // req.requestBody();
        
        String JsonMsg=JSON.serialize(params);
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        RestContext.response= res;
        
        CreditOfficerResource.getData();
        }Catch(Exception e){}
    }
    static testMethod void myUnitTestPsychometric() {
        try{
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'https://cs9.salesforce.com/services/apexrest/v1/creditofficer/psychometric';  
        req.httpMethod = 'GET'; 
        
        RestContext.request = req;
        RestContext.response= res;
        
        CreditOfficerResource.getData();    
        }Catch(Exception e){}
    }
    static testMethod void myUnitTestSocialMedia() {
        try{
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'https://cs9.salesforce.com/services/apexrest/v1/creditofficer/socialMedia';  
        req.httpMethod = 'GET'; 
        
        Map<String, Object> params = new  Map<String, Object>();
        req.params.put('loanApplicationId','test');
        req.params.put('lenderId','Shambhavi');
        req.params.put('approvalType','Shambhavi');
        // req.requestBody();
        
        String JsonMsg=JSON.serialize(params);
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        RestContext.response= res;
        
        CreditOfficerResource.getData();    
        }Catch(Exception e){}
    }
    static testMethod void myUnitTestApprove() {
    try{
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'https://cs9.salesforce.com/services/apexrest/v1/creditofficer/approve';  
        req.httpMethod = 'GET'; 
        
        Map<String, Object> params = new  Map<String, Object>();
        req.params.put('loanApplicationId','test');
        req.params.put('lenderId','Shambhavi');
        req.params.put('approvalType','creditcheck');
        // req.requestBody();
        
        String JsonMsg=JSON.serialize(params);
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        RestContext.response= res;
        
        CreditOfficerResource.getData(); 
        }Catch(Exception e){}   
    }
    static testMethod void myUnitTestApprove2() {
    try{
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'https://cs9.salesforce.com/services/apexrest/v1/creditofficer/approve';  
        req.httpMethod = 'GET'; 
        
        Map<String, Object> params = new  Map<String, Object>();
       // req.params.put('loanApplicationId','test');
        req.params.put('lenderId','Shambhavi');
        req.params.put('approvalType','kyc');
        // req.requestBody();
        
        String JsonMsg=JSON.serialize(params);
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        RestContext.response= res;
        
        CreditOfficerResource.getData();    
        }Catch(Exception e){}
    }
    static testMethod void myUnitTestReject() {
    try{
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'https://cs9.salesforce.com/services/apexrest/v1/creditofficer/reject';  
        req.httpMethod = 'GET'; 
        
        Map<String, Object> params = new  Map<String, Object>();
        req.params.put('loanApplicationId','test');
        req.params.put('lenderId','Shambhavi');
        req.params.put('approvalType','kyc');
        // req.requestBody();
        
        String JsonMsg=JSON.serialize(params);
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        RestContext.response= res;
        
        CreditOfficerResource.getData();    
        }Catch(Exception e){}
    }
    static testMethod void myUnitTestReject2() {
        try{
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'https://cs9.salesforce.com/services/apexrest/v1/creditofficer/reject';  
        req.httpMethod = 'GET'; 
        
        Map<String, Object> params = new  Map<String, Object>();
       // req.params.put('loanApplicationId','test');
        req.params.put('lenderId','Shambhavi');
        req.params.put('approvalType','creditcheck');
        // req.requestBody();
        
        String JsonMsg=JSON.serialize(params);
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        RestContext.response= res;
        
        CreditOfficerResource.getData();    
        }Catch(Exception e){}
    }
    static testMethod void myUnitTestCategoryScore() {
       try{
       RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'https://cs9.salesforce.com/services/apexrest/v1/creditofficer/categoryScore';  
        req.httpMethod = 'GET'; 
        
        Credit_Categories_Master__c credMstr1= new Credit_Categories_Master__c();
        credMstr1.Category_Name__c='pqrs';
        credMstr1.Category_Order__c=4;
        credMstr1.Category_Weight__c=3;
        insert credMstr1;
        
       // Map<String, Object> params = new  Map<String, Object>();
        req.params.put('loanApplicationId',credMstr1.id);
        req.params.put('lenderId','Shambhavi');
        req.params.put('approvalType','Shambhavi');
        // req.requestBody();
        
       // String JsonMsg=JSON.serialize(params);
      //  req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        RestContext.response= res;
        
        CreditOfficerResource.getData();    
        }Catch(Exception e){}
    } 
    static testMethod void myUnitTestKycScore() {
       try{
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'https://cs9.salesforce.com/services/apexrest/v1/creditofficer/kycScore';  
        req.httpMethod = 'GET'; 
        
        Map<String, Object> params = new  Map<String, Object>();
        req.params.put('loanApplicationId','test');
        req.params.put('lenderId','Shambhavi');
        req.params.put('approvalType','Shambhavi');
        // req.requestBody();
        
        String JsonMsg=JSON.serialize(params);
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        RestContext.response= res;
        
        CreditOfficerResource.getData();   
        }Catch(Exception e){} 
    }
     static testMethod void myUnitTestKycScore2() {
        try{
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'https://cs9.salesforce.com/services/apexrest/v1/creditofficer/kycScore';  
        req.httpMethod = 'GET'; 
        
        Map<String, Object> params = new  Map<String, Object>();
       // req.params.put('loanApplicationId','test');
        req.params.put('lenderId','Shambhavi');
        req.params.put('approvalType','Shambhavi');
        // req.requestBody();
        
        String JsonMsg=JSON.serialize(params);
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        RestContext.response= res;
        
        CreditOfficerResource.getData();    
        }Catch(Exception e){}
    }
    static testMethod void myUnitTestEightCriteriaScore() {
        try{
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'https://cs9.salesforce.com/services/apexrest/v1/creditofficer/criteriaScore';  
        req.httpMethod = 'GET'; 
        
        Map<String, Object> params = new  Map<String, Object>();
        req.params.put('loanApplicationId','test');
        req.params.put('lenderId','Shambhavi');
        req.params.put('approvalType','Shambhavi');
        // req.requestBody();
        
        String JsonMsg=JSON.serialize(params);
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        RestContext.response= res;
        
        CreditOfficerResource.getData();    
        }Catch(Exception e){}
    }
    
    static testMethod void myUnitTestLender() {
        try{
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = 'https://cs9.salesforce.com/services/apexrest/v1/creditofficer/lender';  
        req.httpMethod = 'GET'; 
        
        Map<String, Object> params = new  Map<String, Object>();
        req.params.put('loanApplicationId','test');
        req.params.put('lenderId','Shambhavi');
        req.params.put('approvalType','Shambhavi');
        // req.requestBody();
        
        String JsonMsg=JSON.serialize(params);
        req.requestBody = Blob.valueof(JsonMsg);
        
        RestContext.request = req;
        RestContext.response= res;
        
        CreditOfficerResource.getData();    
        }Catch(Exception e){}
    }
}