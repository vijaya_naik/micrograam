@RestResource(urlMapping='/v1/attachment')
global with sharing class AttachmentResource {
    @HttpPost
    global static Map<String,String> uploadAttachment() {
        Map<String,String> attachmentMap = new Map<String,String>();
        RestRequest req = RestContext.request;
        // Get accountId from request parameter
        String accountId = req.params.get('accountId');
        // Get loanApplicationId from request parameter
        String loanApplicationId = req.params.get('loanApplicationId');
        // Get attachmentType from request parameter
        String attachmentType = req.params.get('type');
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        if ( accountId != null ) {
            System.debug('Received Upload Attachment request for accountId=' + accountId + ' type=' + attachmentType);
            attachmentMap = AttachmentService.uploadApplicantAttachment(accountId, attachmentType, params);
        } else if ( loanApplicationId != null ) {
            System.debug('Received Upload Attachment request for loanApplicationId=' + loanApplicationId + ' type=' + attachmentType);
            attachmentMap = AttachmentService.uploadLoanApplicationAttachment(loanApplicationId, attachmentType, params);
        }
        return attachmentMap;
    }
    
    @HttpGet
    global static String downloadAttachment() {
        String returnStr = '';
        RestRequest req = RestContext.request;
        // Get accountId from request parameter
        String accountId = req.params.get('accountId');
        // Get loanApplicationId from request parameter
        String loanApplicationId = req.params.get('loanApplicationId');
        // Get attachmentType from request parameter
        String attachmentType = req.params.get('type');
        if ( accountId != null ) {
			System.debug('Received Download Attachment request for accountId=' + accountId + ' type=' + attachmentType);
            returnStr = AttachmentService.downloadApplicantAttachment(accountId, attachmentType);
        } else if ( loanApplicationId != null ) {
            System.debug('Received Download Attachment request for loanApplicationId=' + loanApplicationId + ' type=' + attachmentType);
            returnStr = AttachmentService.downloadLoanApplicationAttachment(loanApplicationId, attachmentType);
        }
        return returnStr;
    }

    @HttpDelete
    global static void deleteAttachment() {
        RestRequest req = RestContext.request;
        // Get accountId from request parameter
        String accountId = req.params.get('accountId');
        // Get loanApplicationId from request parameter
        String loanApplicationId = req.params.get('loanApplicationId');
        // Get attachmentType from request parameter
        String attachmentType = req.params.get('type');
        if ( accountId != null ) {
            System.debug('Received Delete Attachment request for accountId=' + accountId + ' type=' + attachmentType);
            AttachmentService.deleteApplicantAttachment(accountId, attachmentType);
        } else if ( loanApplicationId != null ) {
            System.debug('Received Delete Attachment request for loanApplicationId=' + loanApplicationId + ' type=' + attachmentType);
            AttachmentService.deleteLoanApplicationAttachment(loanApplicationId, attachmentType);
        }
    }
}