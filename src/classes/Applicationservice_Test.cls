@isTest
public class Applicationservice_Test {
   
    public static testMethod void TestData(){
         Account objAccc = TestUtility.createAccount('Krishna','true','TestData');
         insert objAccc;
         Loan_Application__c objLoanApplication = TestUtility.createApplication(objAccc.id);
         insert objLoanApplication ;
         Applicant__c  objApplicant =  TestUtility.objmgp2pApplicant();
         objApplicant.Account_Id__c =  objAccc.id;
         insert objApplicant ;
         
         Test.startTest();
         //mgp2p.ApplicantService.getApplicantForAccountId(objAccc.id); 
         //mgp2p.ApplicantService.getApplicantDenormalized(objApplicant);
         Test.stopTest();
    }
}