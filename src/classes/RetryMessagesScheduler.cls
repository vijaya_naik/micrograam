public class RetryMessagesScheduler implements Schedulable {

    public void execute(SchedulableContext sc)
    {
        IntegrationMessageReTryBatch retryBatch=new IntegrationMessageReTryBatch();
        Database.executeBatch(retryBatch);
    }
}