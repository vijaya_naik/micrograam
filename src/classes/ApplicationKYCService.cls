public class ApplicationKYCService {
    public static Application_KYC__c getApplicationKYC(String Id) {
        Application_KYC__c applicationKYC = [
            SELECT Id, Account_Id__c, Aadhar_Number__c,Date_Of_Birth__c,
            Fathers_First_Name__c,Fathers_Last_Name__c,First_Name__c,
            Last_Name__c,Gender__c,PAN__c,
            Loan_Application_Id__c,
            Permanent_Address__c,Source__c,Voter_ID__c,
            Company_Name__c, Date_of_Incorporation__c, Registered_Address__c
            FROM Application_KYC__c 
            WHERE Id = :Id
        ];
        return applicationKYC;

    }
       public static Application_KYC__c getApplicationKYC(String loanApplicationId,String source) {
        Application_KYC__c applicationKYC = [
            SELECT Id, Account_Id__c, Aadhar_Number__c,Date_Of_Birth__c,
            Fathers_First_Name__c,Fathers_Last_Name__c,First_Name__c,
            Last_Name__c,Gender__c,PAN__c,
            Loan_Application_Id__c,
            Permanent_Address__c,Source__c,Voter_ID__c,
            Company_Name__c, Date_of_Incorporation__c, Registered_Address__c
            FROM Application_KYC__c 
            WHERE Loan_Application_Id__c = :loanApplicationId and Source__c=:source
        ];
        return applicationKYC;

    }

    public static Map<String, Map<String,String>> getApplicationKYCs(String loanApplicationId) {
        Map<String, Map<String,String>> applicationKYCsMap = new Map<String, Map<String,String>>();
        // Create query string
        List<Application_KYC__c> applicationKYCs = [
            SELECT Id, Account_Id__c, Aadhar_Number__c,Date_Of_Birth__c,
            Fathers_First_Name__c,Fathers_Last_Name__c,First_Name__c,
            Last_Name__c,Gender__c,PAN__c,
            Loan_Application_Id__c,
            Permanent_Address__c,Source__c,Voter_ID__c,
            Company_Name__c, Date_of_Incorporation__c, Registered_Address__c
            FROM Application_KYC__c 
            WHERE Loan_Application_Id__c = :loanApplicationId 
        ];        
        
        // Create Result Map
        for ( Application_KYC__c applicationKYC : applicationKYCs ) {
            Map<String,String> applicationKYCMap = ApplicationKYCService.getApplicationKYCDenormalized(applicationKYC);
            applicationKYCsMap.put(applicationKYC.Source__c, applicationKYCMap);
        }    
        return applicationKYCsMap;
    }

    public static Application_KYC__c getApplicationKYCForLender(String lenderId,String source) {
        Application_KYC__c applicationKYC = [
            SELECT Id, Account_Id__c, Aadhar_Number__c,Date_Of_Birth__c,
            Fathers_First_Name__c,Fathers_Last_Name__c,First_Name__c,
            Last_Name__c,Gender__c,PAN__c,
            Loan_Application_Id__c,
            Permanent_Address__c,Source__c,Voter_ID__c,
            Company_Name__c, Date_of_Incorporation__c, Registered_Address__c
            FROM Application_KYC__c 
            WHERE Account_Id__c = :lenderId and Source__c=:source
        ];
        return applicationKYC;
}
    
    public static Map<String, Map<String,String>> getApplicationKYCsForLender(String lenderId) {
        Map<String, Map<String,String>> applicationKYCsMap = new Map<String, Map<String,String>>();
        // Create query string
        List<Application_KYC__c> applicationKYCs = [
            SELECT Id, Account_Id__c, Aadhar_Number__c,Date_Of_Birth__c,
            Fathers_First_Name__c,Fathers_Last_Name__c,First_Name__c,
            Last_Name__c,Gender__c,PAN__c,
            Loan_Application_Id__c,
            Permanent_Address__c,Source__c,Voter_ID__c,
            Company_Name__c, Date_of_Incorporation__c, Registered_Address__c
            FROM Application_KYC__c 
            WHERE Account_Id__c = :lenderId 
        ];        
        
        // Create Result Map
        for ( Application_KYC__c applicationKYC : applicationKYCs ) {
            Map<String,String> applicationKYCMap = ApplicationKYCService.getApplicationKYCDenormalized(applicationKYC);
            applicationKYCsMap.put(applicationKYC.Source__c, applicationKYCMap);
        }    
        return applicationKYCsMap;
    }
    
    public static Map<String,String> createApplicationKYC(Map<String, Object> params) {
        //Create Application_KYC instance & set fields
        Application_KYC__c applicationKYC = createApplicationKYCInstance(params, null);
        //Create Permanent Address instance & set fields
        Address__c permanentAddress = AddressService.createPermanentAddress(params, null);
        //Create Registered Address instance & set fields
        Address__c registeredAddress = AddressService.createRegisteredAddress(params, null);
        
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
            if (permanentAddress != null) {
                insert permanentAddress;
                applicationKYC.Permanent_Address__c = permanentAddress.Id; //Update related Address Id
            }
            if (registeredAddress != null) {
                insert registeredAddress;
                applicationKYC.Registered_Address__c = registeredAddress.Id; //Update related Address Id
            }
        
            insert applicationKYC;
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
        
        return getApplicationKYCDenormalized(applicationKYC);
    }
    
    public static Application_KYC__c createApplicationKYCInstance(Map<String, Object> params, Application_KYC__c applicationKYC) {
        if (applicationKYC == null)
            applicationKYC = new Application_KYC__c(); //For new record creation
        
        boolean hasValue = false;
        if (params.containsKey('aadharNumber')){
            applicationKYC.Aadhar_Number__c = Double.valueOf(params.get('aadharNumber'));
            hasValue = true;
        }
        if (params.containsKey('dateOfBirth') && params.get('dateOfBirth') != null) {
            applicationKYC.Date_Of_Birth__c=Date.valueOf(String.valueOf(params.get('dateOfBirth')));
            hasValue = true;
        }
        if (params.containsKey('firstName')) {
            applicationKYC.First_Name__c =  String.valueOf(params.get('firstName'));
            hasValue = true;
        }
        if (params.containsKey('fathersFirstName')) {
            applicationKYC.Fathers_First_Name__c = String.valueOf(params.get('fathersFirstName'));
            hasValue = true;
        }
        if (params.containsKey('lastName')) {
            applicationKYC.Last_Name__c =String.valueOf(params.get('lastName'));
            hasValue = true;
        }
        if (params.containsKey('fathersLastName')) {
            applicationKYC.Fathers_Last_Name__c =String.valueOf(params.get('fathersLastName'));
            hasValue = true;
        }
        if (params.containsKey('gender')) {
            applicationKYC.Gender__c=String.valueOf(params.get('gender'));
            hasValue = true;
        }
        if (params.containsKey('pan')) {
            applicationKYC.PAN__c=String.valueOf(params.get('pan'));
            hasValue = true;
        }
        if (params.containsKey('source')) {
            applicationKYC.Source__c=String.valueOf(params.get('source'));
            hasValue = true;
        }
        if (params.containsKey('voterID')) {
            applicationKYC.Voter_ID__c=String.valueOf(params.get('voterID'));
            hasValue = true;
        }
        if (params.containsKey('loanApplicationId')) {
            applicationKYC.Loan_Application_Id__c=String.valueOf(params.get('loanApplicationId'));
            hasValue = true;
        }
        if (params.containsKey('lenderId')) {
            applicationKYC.Account_Id__c=String.valueOf(params.get('lenderId'));
            hasValue = true;
        }
        if (params.containsKey('companyFirmName')) {
            applicationKYC.Company_Name__c=String.valueOf(params.get('companyFirmName'));
            hasValue = true;
        }
        if (params.containsKey('dateOfIncorporation') && params.get('dateOfIncorporation') != null) {
            applicationKYC.Date_of_Incorporation__c=Date.valueOf(String.valueOf(params.get('dateOfIncorporation')));
            hasValue = true;
        }
        
        if (hasValue)
            return applicationKYC;
        else
            return null;
    }
    
    public static Map<String,String> getApplicationKYCDenormalized(Application_KYC__c applicationKYC) {
        Map<String,String> applicationKYCMap = new Map<String,String>();
        
        if (applicationKYC.Aadhar_Number__c != null) applicationKYCMap.put('aadharNumber', String.valueOf(applicationKYC.Aadhar_Number__c));
        if (applicationKYC.Date_Of_Birth__c != null) applicationKYCMap.put('dateOfBirth', String.valueOf(applicationKYC.Date_Of_Birth__c));
        if (applicationKYC.First_Name__c != null) applicationKYCMap.put('firstName', applicationKYC.First_Name__c);
        if (applicationKYC.Last_Name__c != null) applicationKYCMap.put('lastName', applicationKYC.Last_Name__c);
        if (applicationKYC.Fathers_First_Name__c != null) applicationKYCMap.put('fathersFirstName', applicationKYC.Fathers_First_Name__c);
        if (applicationKYC.Fathers_Last_Name__c != null) applicationKYCMap.put('fathersLastName', applicationKYC.Fathers_Last_Name__c);
        if (applicationKYC.PAN__c != null) applicationKYCMap.put('pan', applicationKYC.PAN__c);
        if (applicationKYC.Gender__c != null) applicationKYCMap.put('gender', applicationKYC.Gender__c);
        if (applicationKYC.Permanent_Address__c != null)    
                    applicationKYCMap.putAll(AddressService.getPermanentAddressDenormalized(AddressService.getAddressById(applicationKYC.Permanent_Address__c)));
        if (applicationKYC.Source__c != null) applicationKYCMap.put('source', applicationKYC.Source__c);
        if (applicationKYC.Voter_ID__c != null) applicationKYCMap.put('voterID', applicationKYC.Voter_ID__c);
        if (applicationKYC.Loan_Application_Id__c != null) applicationKYCMap.put('loanApplicationId', String.valueOf(applicationKYC.Loan_Application_Id__c));
        if (applicationKYC.Account_Id__c != null) applicationKYCMap.put('lenderId', String.valueOf(applicationKYC.Account_Id__c));
        if (applicationKYC.Company_Name__c != null) applicationKYCMap.put('companyFirmName', applicationKYC.Company_Name__c);
        if (applicationKYC.Date_of_Incorporation__c != null) applicationKYCMap.put('dateOfIncorporation', String.valueOf(applicationKYC.Date_of_Incorporation__c));
        if (applicationKYC.Registered_Address__c != null)
                    applicationKYCMap.putAll(AddressService.getRegisteredAddressDenormalized(AddressService.getAddressById(applicationKYC.Registered_Address__c)));
        
        applicationKYCMap.put('id', String.valueOf(applicationKYC.Id));

        return applicationKYCMap;
    }
    
    public static void deleteApplicationKYC(String applicationKYCId) {
        //Get account record
        Application_KYC__c applicationKYC = 
            [SELECT Id, Permanent_Address__c, Registered_Address__c 
             FROM Application_KYC__c WHERE Application_KYC__c.Id = :applicationKYCId];
        //Get permanent address record
        Address__c permanentAddress = null;
        if (applicationKYC.Permanent_Address__c != null) {
            permanentAddress = AddressService.getAddressById(applicationKYC.Permanent_Address__c);
        }
        //Get registered address record
        Address__c registeredAddress = null;
        if (applicationKYC.Registered_Address__c != null) {
            registeredAddress = AddressService.getAddressById(applicationKYC.Registered_Address__c);
        }

        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
             if (permanentAddress != null)
                delete permanentAddress; //Delete permanent address record
             if (registeredAddress != null)
                 delete registeredAddress; //Delete registered address record
            
            delete applicationKYC; //Delete Application_KYC record
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
    
    public static Map<String,String> updateApplicationKYC(String applicationKYCId, Map<String, Object> params) {
        //Get & update Application_KYC record
        Application_KYC__c applicationKYC = getApplicationKYC(applicationKYCId);
        applicationKYC = createApplicationKYCInstance(params, applicationKYC);
        
        //Get & update permanent address record
        Address__c permanentAddress = null;
        if (applicationKYC.Permanent_Address__c != null) {
            permanentAddress = AddressService.getAddressById(applicationKYC.Permanent_Address__c);
        }
        permanentAddress = AddressService.createPermanentAddress(params, permanentAddress);

        //Get & update registered address record
        Address__c registeredAddress = null;
        if (applicationKYC.Registered_Address__c != null) {
            registeredAddress = AddressService.getAddressById(applicationKYC.Registered_Address__c);
        }
        registeredAddress = AddressService.createRegisteredAddress(params, registeredAddress);
        
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
            if(permanentAddress != null) {
                upsert permanentAddress;
                applicationKYC.Permanent_Address__c = permanentAddress.Id; //Update related Address Id
            }
            if(registeredAddress != null) {
                upsert registeredAddress;
                applicationKYC.Registered_Address__c = registeredAddress.Id; //Update related Address Id
            }
            
            update applicationKYC;
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
        
        return getApplicationKYCDenormalized(applicationKYC);
    }
    
    public static void deleteApplicationKYCsForLoanApplication(String loanApplicationId) {
         List<Application_KYC__c> applicationKYCs = [
            SELECT Id, Permanent_Address__c, Registered_Address__c FROM Application_KYC__c 
            WHERE Loan_Application_Id__c = :loanApplicationId 
        ];
        deleteApplicationKYCs(applicationKYCs);
    }

    public static void deleteApplicationKYCsForLender(String lenderId) {
         List<Application_KYC__c> applicationKYCs = [
            SELECT Id, Permanent_Address__c, Registered_Address__c FROM Application_KYC__c 
            WHERE Account_Id__c = :lenderId 
        ];
        deleteApplicationKYCs(applicationKYCs);
    }
    
    private static void deleteApplicationKYCs(List<Application_KYC__c> applicationKYCs) {
        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
            for ( Application_KYC__c applicationKYC : applicationKYCs ) {
                //Get permanent address record
                if (applicationKYC.Permanent_Address__c != null) {
                    Address__c permanentAddress = AddressService.getAddressById(applicationKYC.Permanent_Address__c);
                    if (permanentAddress != null)
                        delete permanentAddress; //Delete permanent address record
                }
                //Get registered address record
                if (applicationKYC.Registered_Address__c != null) {
                    Address__c registeredAddress = AddressService.getAddressById(applicationKYC.Registered_Address__c);
                    if (registeredAddress != null)
                        delete registeredAddress; //Delete registered address record
                }
                
                delete applicationKYC; //Delete Application_KYC record
            }
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
}