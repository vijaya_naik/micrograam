public class CreditCategoriesMasterService {
 public static Credit_Categories_Master__c getCreditCategoriesMaster(String Id) {
        Credit_Categories_Master__c applicationCreditCategoriesMaster = [
            SELECT Id, Category_Name__c,Category_Order__c,Category_Weight__c,Credit_Scoring_Ruleset__c
            FROM Credit_Categories_Master__c 
            WHERE Id = :Id
        ];
        return applicationCreditCategoriesMaster;

    }
    
    public static Map<String,Map<String,String>> getCreditCategories(String id) {
         Loan_application__C loan_application=null;

         if(id!=null)  loan_application=LoanApplicationService.getLoanApplicationById(id).loanApplication;
         String val=loan_application.Credit_Scoring_Ruleset__c;
         String queryString = 'SELECT Id, Category_Name__c,Category_Order__c,Category_Weight__c,Credit_Scoring_Ruleset__c '+
            'FROM Credit_Categories_Master__c';
         if(loan_application!=null && val!=null){
             queryString += ' WHERE Credit_Scoring_Ruleset__c=:val';
         }
        
         Map<String,Map<String,String>> retValue=new Map<String,Map<String,String>>();
         List<Credit_Categories_Master__c> applicationCreditCategories = Database.query(queryString);
         for(integer i=0;i<applicationCreditCategories.size();i++){
             Credit_Categories_Master__c row=applicationCreditCategories.get(i);
             retValue.put(row.Id,getCreditCategoriesMasterDenormalized(applicationCreditCategories.get(i)));
         }
        
         return retValue;
    }
    
    public static Map<String,String> getCreditCategoriesMasterMap(String creditScoringRuleSet) {
        Map<String,String> categoriesMasterMap = new Map<String,String>();
        
        List<Credit_Categories_Master__c> creditCategories = [
            SELECT Id, Category_Name__c, Credit_Scoring_Ruleset__c
            FROM Credit_Categories_Master__c 
            WHERE Credit_Scoring_Ruleset__c = :creditScoringRuleSet
        ];
        for(integer i=0;i<creditCategories.size();i++){
            Credit_Categories_Master__c row = creditCategories.get(i);
            categoriesMasterMap.put(row.Category_Name__c, row.Id);
        }
        
        return categoriesMasterMap;
    }
    
    public static Map<String,Map<String,String>> getCreditCategories() {
         String queryString = 'SELECT Id, Category_Name__c,Category_Order__c,Category_Weight__c,Credit_Scoring_Ruleset__c '+
            'FROM Credit_Categories_Master__c';

         Map<String,Map<String,String>> retValue=new Map<String,Map<String,String>>();
         List<Credit_Categories_Master__c> applicationCreditCategories = Database.query(queryString);
         for(integer i=0;i<applicationCreditCategories.size();i++){
             Credit_Categories_Master__c row=applicationCreditCategories.get(i);
             retValue.put(row.Id,getCreditCategoriesMasterDenormalized(applicationCreditCategories.get(i)));
         }
         return retValue;
    }
    
    public static Map<String,String> createCreditCategoriesMaster(Map<String, Object> params) {
        //Create Application_KYC instance & set fields
        Credit_Categories_Master__c applicationCreditCategoriesMaster = createCreditCategoriesMasterInstance(params, null);
       
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
      
            insert applicationCreditCategoriesMaster;
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
        
        return getCreditCategoriesMasterDenormalized(applicationCreditCategoriesMaster);
    }
    
    public static Credit_Categories_Master__c createCreditCategoriesMasterInstance(Map<String, Object> params, Credit_Categories_Master__c applicationCreditCategoriesMaster) {
        if (applicationCreditCategoriesMaster == null)    applicationCreditCategoriesMaster = new Credit_Categories_Master__c(); //For new record creation
        boolean hasValue = false;
        if (params.containsKey('categoryName')){
            applicationCreditCategoriesMaster.Category_Name__c=String.valueOf(params.get('categoryName'));
            hasValue=true;
        }
        if (params.containsKey('categoryWeight')){ 
            applicationCreditCategoriesMaster.Category_Weight__c=Integer.valueOf(params.get('categoryWeight'));
            hasValue=true;
        }
        if (params.containsKey('categoryOrder'))  {
            applicationCreditCategoriesMaster.Category_Order__c=Integer.valueOf(params.get('categoryOrder'));
            hasValue=true;
        }
        if (params.containsKey('creditScoringRuleset')){
            applicationCreditCategoriesMaster.Credit_Scoring_Ruleset__c=String.valueOf(params.get('creditScoringRuleset'));
            hasValue=true;
        }
        
        if(hasValue)
            return applicationCreditCategoriesMaster;
        else 
            return null;
    }
    
    public static Map<String,String> getCreditCategoriesMasterDenormalized(Credit_Categories_Master__c applicationCreditCategoriesMaster) {
        Map<String,String> applicationCreditCategoriesMasterMap = new Map<String,String>();
        
        if (applicationCreditCategoriesMaster.Category_Weight__c != null) applicationCreditCategoriesMasterMap.put('categoryWeight', String.valueOf(applicationCreditCategoriesMaster.Category_Weight__c));
        if (applicationCreditCategoriesMaster.Category_Order__c != null) applicationCreditCategoriesMasterMap.put('categoryOrder', String.valueOf(applicationCreditCategoriesMaster.Category_Order__c));
        
        if (applicationCreditCategoriesMaster.Category_Name__c != null) applicationCreditCategoriesMasterMap.put('categoryName', String.valueOf(applicationCreditCategoriesMaster.Category_Name__c));
        if (applicationCreditCategoriesMaster.Credit_Scoring_Ruleset__c != null) applicationCreditCategoriesMasterMap.put('creditScoringRuleset', String.valueOf(applicationCreditCategoriesMaster.Credit_Scoring_Ruleset__c));
        applicationCreditCategoriesMasterMap.put('categoryId', String.valueOf(applicationCreditCategoriesMaster.Id));

        return applicationCreditCategoriesMasterMap;
    }
    
    public static Map<String,String> updateCreditCategoriesMaster(String applicationCreditCategoriesMasterId, Map<String, Object> params) {
        Credit_Categories_Master__c applicationCreditCategoriesMaster =null;
        if(applicationCreditCategoriesMasterId!=null)  applicationCreditCategoriesMaster=getCreditCategoriesMaster(applicationCreditCategoriesMasterId);
                applicationCreditCategoriesMaster = createCreditCategoriesMasterInstance(params, applicationCreditCategoriesMaster);
        
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
            update applicationCreditCategoriesMaster;
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
        
        return getCreditCategoriesMasterDenormalized(applicationCreditCategoriesMaster);
    }
    
    public static void deleteCreditCategoriesMaster(String creditCategoriesMasterId) {
        //Get applicant record
        Credit_Categories_Master__c dto=CreditCategoriesMasterService.getCreditCategoriesMaster(creditCategoriesMasterId);

        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
             delete dto; //Delete applicant record
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
}