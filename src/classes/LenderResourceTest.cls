@isTest(seeAllData=false)
public class LenderResourceTest  {

    static Address__c addres;
    static Account acc;
    static Applicant__c app;
    static  Applicant__c app1;
    static Account acc1;
    private static final String ORG_URL = URL.getSalesforceBaseUrl().toExternalForm() ;
      
     static void init() {
      // setup test data 
      //create dummy Address Record
       addres =new  Address__c();
       addres.Address_Line_1__c='Street 1';
       addres.Address_Line_2__c='Street 2';
       addres.City__c='Bangalore ';
       addres.Pincode__c= 12345;
       addres.State__c='KA';
       addres.Country__c='India'; 
       addres.Residing_Since__c=System.today();
       addres.House_Ownership__c='Owned';
       insert addres;
       
       //create dummy account record
       acc=new Account();
       acc.Name='Microgram';
       acc.Account_Type__c='Borrower'; 
       insert acc;
       
       acc1=new Account();
       acc1.Name='Microgram1';
       acc1.Account_Type__c='Borrower1';
       acc1.Applicant_Type__c='Individual';
       insert acc1;    
       //create dummy applicant record
       app= new Applicant__c();
       app.Aadhar_Number__c=12;
       app.Account_Id__c=acc1.id;
       app.Registered_Address__c=addres.id;
       app.Applicant_Nature__c='Primary';
       app.Correspondence_Address__c=addres.id;
       app.Current_Address__c=addres.id;
       app.Permanent_Address__c=addres.id;
       app.E_Mail__c='Microgram@gmail.com';
       insert app;
       
       app1= new Applicant__c();
       app1.Aadhar_Number__c=12;
       app1.Account_Id__c=acc.id;
       app1.Applicant_Nature__c='Primary';
       app1.E_Mail__c='Microgram@gmail.com';
       insert app1;
        
     }
    static testMethod void testDoPost() {
      init();
      Map<String,Object> lenderMap=new Map<String,Object>();
        lenderMap.put('firstName', 'Etmarlabs');
        lenderMap.put('lastName', 'Tquila');
        lenderMap.put('Name', 'Etmarlabs Tquila');
        lenderMap.put('applicantType', 'Individual');
        lenderMap.put('accountType', 'Borrower');
        lenderMap.put('eMail', 'Borreer@gmail.com');
        lendermap.put('registeredAddressLine1', addres.id);
        lendermap.put('registeredAddressLine2', addres.id);
        lendermap.put('registeredAddressCity', 'banglore');
        lendermap.put('registeredAddressPincode', 524137);
        lendermap.put('registeredAddressState','karnataka');
        lendermap.put('registeredAddressCountry','India');
        lendermap.put('currentAddressLine1', addres.id);
        lendermap.put('currentAddressLine2', addres.id);
        lendermap.put('currentAddressCity', 'banglore');
        lendermap.put('currentAddressPincode', 524137);
        lendermap.put('currentAddressState','karnataka');
        lendermap.put('currentAddressCountry','India');
        lendermap.put('correspondenceAddressLine1', addres.id);
        lendermap.put('correspondenceAddressLine2', addres.id);
        lendermap.put('correspondenceAddressCity', 'banglore');
        lendermap.put('correspondenceAddressPincode', 524137);
        lendermap.put('correspondenceAddressState','karnataka');
        lendermap.put('correspondenceAddressCountry','India');
        lendermap.put('permanentAddressLine1', addres.id);
        lendermap.put('permanentAddressLine2', addres.id);
        lendermap.put('permanentAddressCity', 'banglore');
        lendermap.put('permanentAddressPincode', 524137);
        lendermap.put('permanentAddressState','karnataka');
        lendermap.put('permanentAddressCountry','India');
        String JsonMsg=JSON.serialize(lendermap);
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
    
        req.requestURI = '/services/apexrest/v1/lender';  //Request URL
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.request = req;
        RestContext.response= res;
    
      Map<String,String> results = LenderResource.registerLender();

  }
  
      static testMethod void testDoGet() {
        init();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = ORG_URL+'/services/apexrest/v1/lender';  //Request URL
        req.httpMethod = 'GET';
        req.addParameter('id', acc.Id);      
        RestContext.request = req;
        RestContext.response = res;
        Map<String,String> results=new Map<String,String>();
        results = LenderResource.getLender();
      }
      
      static testMethod void testDoDelete() {
        init();
        RestRequest req = new RestRequest(); 
    
        req.requestURI = ORG_URL+'/services/apexrest/v1/lender';  //Request URL
        req.httpMethod = 'DELETE';
        req.addParameter('id', acc.Id);    
        RestContext.request = req;
        LenderResource.deleteLender();  
        
      }
      
      static testMethod void testDoPatch() {
          init();
          Map<String, Object> appMap=new Map<String,Object>();
          appMap.put('firstName', 'Microgram');
          appMap.put('Name', 'xyz');
          appMap.put('accountType', 'Borrower');
          appMap.put('applicantType', 'Individual');
          appMap.put('registeredAddressLine1', addres.id);
          appMap.put('registeredAddressLine2', addres.id);
          appMap.put('registeredAddressCity', 'banglore');
          appMap.put('registeredAddressPincode', 524137);
          appMap.put('registeredAddressState','karnataka');
          appMap.put('registeredAddressCountry','India');
          appMap.put('currentAddressLine1', addres.id);
          appMap.put('currentAddressLine2', addres.id);
          appMap.put('currentAddressCity', 'banglore');
          appMap.put('currentAddressPincode', 524137);
          appMap.put('currentAddressState','karnataka');
          appMap.put('currentAddressCountry','India');
          appMap.put('correspondenceAddressLine1', addres.id);
          appMap.put('correspondenceAddressLine2', addres.id);
          appMap.put('correspondenceAddressCity', 'banglore');
          appMap.put('correspondenceAddressPincode', 524137);
          appMap.put('correspondenceAddressState','karnataka');
          appMap.put('correspondenceAddressCountry','India');
          appMap.put('permanentAddressLine1', addres.id);
          appMap.put('permanentAddressLine2', addres.id);
          appMap.put('permanentAddressCity', 'banglore');
          appMap.put('permanentAddressPincode', 524137);
          appMap.put('permanentAddressState','karnataka');
          appMap.put('permanentAddressCountry','India');
          String JsonMsg=JSON.serialize(appMap);
          RestRequest req = new RestRequest();
          RestResponse res = new RestResponse();
    
          req.requestURI = '/services/apexrest/v1/lender';  //Request URL
          req.httpMethod = 'PATCH';
          req.addParameter('id', acc.Id);      
          req.requestBody = Blob.valueof(JsonMsg);
          RestContext.request = req;
          RestContext.response= res;
        
        Map<String,String> results=new Map<String,String>();
        results = LenderResource.updateLender();
      }
}