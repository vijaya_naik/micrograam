public class RequestProviderAadhaarDetails implements IRequestProvider {

    
    public Object execute(Integration_Message__c message){
      HttpResponse res=RequestProviderAadhaarDetails.doCallOut(message);
      String responseBody = res.getBody();
      Map<String, Object> resParsed = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
      return res;
    }
    
    
     public static HttpResponse doCallout(Integration_Message__c message){
         
         Applicant__c applicant=[SELECT Id, Aadhaar_Bridge_Otp__c, Aadhar_Number__c 
             from Applicant__c where Id=:message.Input_String__c];
         HttpRequest req = new HttpRequest();
         req.setMethod('POST');
         req.setEndPoint('http://34.194.186.69:9090/kyc/raw');
         req.setHeader('Content-Type','application/x-www-form-urlencoded');
         req.setBody('{"consent": "Y","mec": "Y","auth-capture-request": {"aadhaar-id": "'+applicant.Aadhar_Number__c+'","location": {"type": "pincode","pincode": "'+Label.AadhaarPinCode+'"},"modality": "otp","certificate-type": "'+Label.AadhaarReqType+'","otp":"'+applicant.Aadhaar_Bridge_Otp__c+'"}}');
         return (new Http()).send(req);
     }
}