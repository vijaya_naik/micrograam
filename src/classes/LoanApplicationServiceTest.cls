@isTest(seealldata = false)
public class  LoanApplicationServiceTest{

  public static testmethod void loanSrvcTestdata(){
  
     Address__c mgp=new  Address__c();
          mgp.Address_Line_1__c='BDA Complex';
          mgp.Address_Line_2__c='permanentAddressLine2';
          mgp.City__c='permanentAddressCity';
          mgp.Pincode__c=123;
          mgp.State__c='Karnataka';
          mgp.Country__c='India'; 
          mgp.Residing_Since__c=Date.today();
          mgp.House_Ownership__c='Owned';
          insert mgp;
      
      Address__c mgp1=new  Address__c();
          mgp1.Address_Line_1__c='BDA Complex';
          mgp1.Address_Line_2__c='permanentAddressLine2';
          mgp1.City__c='permanentAddressCity';
          mgp1.Pincode__c=123;
          mgp1.State__c='Karnataka';
          mgp1.Country__c='India'; 
          mgp1.Residing_Since__c=Date.today();
          mgp1.House_Ownership__c='Owned';
          insert mgp1;
          
               
      Account acc=new Account();
      acc.Name='Microgram';
      acc.Account_Type__c='Borrower'; 
      insert acc;
      
      Account acc1=new Account();
      acc1.Name='Microgram';
      acc1.Account_Type__c='Borrower'; 
      insert acc1;
      
      
      Income_Detail__c income=new Income_Detail__c();
      income.Designation__c='Manager';
      income.Office_Address__c=mgp.id;
      income.Annual_Income__c=200000;
      insert income;
      
      Loan_Application__c loan=new Loan_Application__c();
      loan.Account_Id__c=acc.id;
      loan.Reference_1_Address__c=mgp.id;
      loan.Reference_2_Address__c=mgp.id;
      loan.Income_Detail_Current_Job__c=income.id;
      loan.Income_Detail_Previous_Job__c=income.id;
      loan.Loan_Status__c='In Progress';
      insert loan;
      
      Loan_Application__c loan1=new Loan_Application__c();
      loan1.Account_Id__c=acc1.id;
      loan1.Reference_1_Address__c=mgp1.id;
      loan1.Reference_2_Address__c=mgp1.id;
      loan1.Income_Detail_Current_Job__c=income.id;
      loan1.Income_Detail_Previous_Job__c=income.id;
      loan1.Loan_Status__c='In Progress';
      insert loan1;
      
      
      
      Map<String, Object> loanMap=new Map<String, Object>();
         loanMap.put('firstName', 'Microgram');
         loanMap.put('Name', 'xyz');
         loanMap.put('accountType', 'Borrower');
         loanMap.put('applicantType', 'Individual');
         loanMap.put('currentAddressLine1', mgp.id);
         loanMap.put('currentAddressLine2', mgp.id);
         loanMap.put('currentAddressCity', 'banglore');
         loanMap.put('currentAddressPincode', 524137);
         loanMap.put('currentAddressState','karnataka');
         loanMap.put('currentAddressCountry','India');
         loanMap.put('borrowerId',acc1.id);
         loanMap.put('kycStatus','Not Submitted');
         loanMap.put('creditCheckStatus','Not Submitted');
         loanMap.put('applicantType','Individual');
         loanMap.put('creditScore',6);
         loanMap.put('loanAmount',2000);
         loanMap.put('loanMaximumEMI',2);
         loanMap.put('loanPurposeCategory','health');
         loanMap.put('loanPurposeDescription','health issue');
         loanMap.put('loanStatus','In Progress');
         loanMap.put('loanTenor',2);
         loanMap.put('decisionDate',Date.today());
         loanMap.put('decisionTakenBy','Manager');
         loanMap.put('creditOfficerActionComments','health issue');
         loanMap.put('reference2MobileNumber',94);
         loanMap.put('reference2LastName','xyz');
         loanMap.put('creditOfficerJudgementScore',23);
         loanMap.put('reference2FirstName','Milligram');
         loanMap.put('reference1MobileNumber',93);
         loanMap.put('reference1LastName','et');
         loanMap.put('reference1FirstName','Marlabs');
         loanMap.put('loanNeededBy',Date.today());
         loanMap.put('otherLoansDetail','noLoan');
          loanMap.put('otherLoans',32);
         loanMap.put('otherAssetsDetail','Land');
         loanMap.put('otherIncomeDetail','Job');
         loanMap.put('otherBorrowingDescription','borrow');
         loanMap.put('otherLoansDetail','noLoan');
         loanMap.put('otherAssetsDetail','Land');
         loanMap.put('otherIncomeDetail','Job');
         loanMap.put('totalWorkExperience',22);
         loanMap.put('interestOtherInvestmentIncome',21);
         loanMap.put('rentalIncome',12);
         loanMap.put('bankDeposits',31);
         loanMap.put('lifeInsurance',12);
         loanMap.put('otherAssets',21);
         loanMap.put('housingLoan',23);
         loanMap.put('autoLoan',13);
         loanMap.put('netProfitLatestYear',11);
         loanMap.put('netProfitPrior2Year',22);
         loanMap.put('netWorthLatestYear',21);
         loanMap.put('netWorthPriorYear',12);
         loanMap.put('netProfitPriorYear',16);
         loanMap.put('netWorthPrior2Year',31);
         loanMap.put('cashCreditSanctioned',12);
         loanMap.put('cashCreditOutstanding',11);
         loanMap.put('termLoanSanctioned',22);
         loanMap.put('creditCardOutstanding',21);
         loanMap.put('persoanalLoan',12);
         loanMap.put('termLoanOutstanding',31);
         loanMap.put('lCGuaranteeSanctioned',12);
         loanMap.put('billsDiscountingOutstanding',21);
         loanMap.put('lCGuaranteeOutstanding',12);
         loanMap.put('otherBorrowingOutstanding',31);
         loanMap.put('businessType','private');
         loanMap.put('turnoverLatestYear',24);
         loanMap.put('turnoverPriorYear',27);
         loanMap.put('turnoverPrior2Years',21);
         loanMap.put('mutualFunds',12);
         loanMap.put('shares',31);
         loanMap.put('vehicles',1);
         loanMap.put('housePropertyLand',24);
         loanMap.put('otherIncome',27);
      
       Map<String, Object> appMap=new Map<String,Object>();
          appMap.put('firstName', 'Microgram');
          appMap.put('Name', 'xyz');
          appMap.put('accountType', 'Borrower');
          appMap.put('applicantType', 'Individual');
          appMap.put('currentOfficeAddressLine1', mgp.id);
          appMap.put('currentOfficeAddressLine2', mgp.id);
          appMap.put('currentOfficeAddressCity', 'banglore');
         appMap.put('currentOfficeAddressPincode', 524137);
         appMap.put('currentOfficeAddressState','karnataka');
         appMap.put('currentOfficeAddressCountry','India');
        appMap.put('previousOfficeAddressLine1', mgp.id);
         appMap.put('previousOfficeAddressLine2', mgp.id);
         appMap.put('previousOfficeAddressCity', 'banglore');
         appMap.put('previousOfficeAddressPincode', 524137);
         appMap.put('previousOfficeAddressState','karnataka');
         appMap.put('previousOfficeAddressCountry','India');
          appMap.put('reference1AddressLine1', mgp.id);
         appMap.put('reference1AddressLine2', mgp.id);
         appMap.put('reference1AddressCity', 'banglore');
         appMap.put('reference1AddressPincode', 524137);
         appMap.put('reference1AddressState','karnataka');
         appMap.put('reference1AddressCountry','India');
         appMap.put('reference2AddressLine1', mgp.id);
         appMap.put('reference2AddressLine2', mgp.id);
         appMap.put('reference2AddressCity', 'banglore');
         appMap.put('reference2AddressPincode', 524137);
         appMap.put('reference2AddressState','karnataka');
         appMap.put('reference2AddressCountry','India');
      
      
       LoanApplicationService  loanService=new  LoanApplicationService ();
       LoanApplicationService.getActiveApplicationForAccountId(loan.id);
       LoanApplicationService.getApplicationsForAccountId(loan.id);
       LoanApplicationService.getLoanApplicationDenormalized(loan);
       LoanApplicationService.getLoanApplicationDenormalized(loan.id);
       LoanApplicationService.getLoanApplicationById(loan.id);
       LoanApplicationService.createLoanApplication(loanMap);
       LoanApplicationService.updateLoanApplication(loan.id,appMap);
       try{
       LoanApplicationService.deleteLoanApplication(loan1.id);
       }catch(Exception ex){
       
       }
       
  
  }

}