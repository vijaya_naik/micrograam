global class LoanapplicationDTO {
    public Loan_Application__c loanApplication { get; set; }
    public Income_Detail__c currentIncomeDetail { get; set; }
    public Income_Detail__c previousIncomeDetail { get; set; }
    public List<Company_Owner__c> companyOwners { get; set; }
    public List<Company_Manager__c> companyManagers { get; set; }
}