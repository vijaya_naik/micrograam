@isTest
private class KYCScoreResource_Test {
	
	static Credit_Categories_Master__c creditMstr;
    private static final String ORG_URL = URL.getSalesforceBaseUrl().toExternalForm() ;
     
    @isTest static void test_method_one() {

    	Account acct = TestUtility.createAccount('testacc','true','Lender' );
        insert acct;

		Applicant__c aplcnt = TestUtility.objmgp2pApplicant();
        aplcnt.Account_Id__c = acct.Id;
        insert aplcnt;

		Loan_Application__c lnaplcnt = TestUtility.createApplication(acct.Id);
        insert lnaplcnt;

        KYC_Rules_Master__c kycRM = TestUtility.objKYCrm();
        insert kycRM;

        Application_KYC_Score__c applKYC = TestUtility.inserApplKYCscore(acct.Id, kycRM.Id, lnaplcnt.Id);
        insert applKYC;
	
		// Implement test code
		Map<String, Object> paramMap = new Map<String, Object>();
	      Application_KYC_Score__c kycscore = new Application_KYC_Score__c();
	      paramMap.put('loanApplicationId',applKYC.Id);
	      paramMap.put('lenderId',acct.Id);
	      List<Object> paramlist = paramMap.Values();
	      String JsonMsg=JSON.serialize(paramlist);
	      RestRequest req = new RestRequest();
	      RestResponse res = new RestResponse();

	      req.requestURI = '/services/apexrest/v1/kycScore';  //Request URL
	      req.httpMethod = 'POST';
	      req.requestBody = Blob.valueof(JsonMsg);
	      RestContext.request = req;
	      RestContext.response= res;
		try {
			KYCScoreResource.cKYCScore();
		} catch(Exception e) {
			System.debug(e.getMessage());
		}
		
	}
	
	@isTest static void test_method_two() {
		// Implement test code

		Account acct = TestUtility.createAccount('testacc','true','Lender' );
        insert acct;

		Applicant__c aplcnt = TestUtility.objmgp2pApplicant();
        aplcnt.Account_Id__c = acct.Id;
        insert aplcnt;

		Loan_Application__c lnaplcnt = TestUtility.createApplication(acct.Id);
        insert lnaplcnt;

        KYC_Rules_Master__c kycRM = TestUtility.objKYCrm();
        insert kycRM;

        Application_KYC_Score__c applKYC = TestUtility.inserApplKYCscore(acct.Id, kycRM.Id, lnaplcnt.Id);
        insert applKYC;

	    RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();

	    req.requestURI = ORG_URL+'/services/apexrest/v.37.0/v1/socialMedia';  
	    req.httpMethod = 'GET';
	    req.addParameter('loanApplicationId', lnaplcnt.Id);      
	    req.addParameter('lenderId', acct.Id);        
	    RestContext.request = req;
	    RestContext.response = res;
	    Map<String,String> results=new Map<String,String>();

	    try {
	    	KYCScoreResource.getKYCScore();
	    } catch(Exception e) {
	    	System.debug(e.getMessage());
	    }
	}

	@isTest static void test_method_three() {

		Account acct = TestUtility.createAccount('testacc','true','Lender' );
        insert acct;

		Applicant__c aplcnt = TestUtility.objmgp2pApplicant();
        aplcnt.Account_Id__c = acct.Id;
        insert aplcnt;

		Loan_Application__c lnaplcnt = TestUtility.createApplication(acct.Id);
        insert lnaplcnt;

        KYC_Rules_Master__c kycRM = TestUtility.objKYCrm();
        insert kycRM;

        Application_KYC_Score__c applKYC = TestUtility.inserApplKYCscore(acct.Id, kycRM.Id, lnaplcnt.Id);
        insert applKYC;

		RestRequest req = new RestRequest(); 
	    req.requestURI = ORG_URL+'/services/apexrest/v1/creditCategoriesMaster';  
	    req.httpMethod = 'DELETE';
	    req.addParameter('loanApplicationId', lnaplcnt.Id);    
	    RestContext.request = req;
	    KYCScoreResource.deleteKYCScore();
	}
}