public class CreditOfficerService {
    
    public static Map<String, Map<String,Object>> getLoanApplicationList(String kycStatus, String creditCheckStatus) {
        Map<String, Map<String,Object>> loanApplicationsMap = new Map<String, Map<String,Object>>();
        // Create query string to get Loan Application records
        String queryString = 'SELECT Id, Name, Account_Id__c, Loan_Amount__c, ' +
            'Loan_Purpose_Category__c, LastModifiedDate, Decision_Taken_By__c, Decision_Date__c ' +
            'FROM Loan_Application__c ';
        //Add filter clauses
        String kycFilter = kycStatus != null ? 'KYC_Status__c = :kycStatus' : '';
        String creditCheckFilter = creditCheckStatus != null ? 'Credit_Check_Status__c = :creditCheckStatus' : '';
        if ( kycStatus != null || creditCheckStatus != null ) {
            queryString += ' WHERE ';
            queryString += kycFilter;
            if ( kycStatus != null && creditCheckStatus != null ) queryString += ' AND ';
            queryString += creditCheckFilter;
            
        }
        
        List<Loan_Application__c> loanApplications = Database.query(queryString);        
        System.debug('getLoanApplications ResultSize=' + loanApplications.size());
        
        List<String> borrowerIds = new List<String>();
        // Create Result Map
        for ( Loan_Application__c loanApplication : loanApplications ) {
            Map<String,Object> loanApplicationMap = new Map<String,Object>();
            loanApplicationMap.put('loanApplicationId',loanApplication.Id);
            loanApplicationMap.put('borrowerId',loanApplication.Account_Id__c);
            loanApplicationMap.put('loanApplicationNumber',loanApplication.Name);
            loanApplicationMap.put('loanDate', String.valueOf(loanApplication.LastModifiedDate));
            if(loanApplication.Loan_Purpose_Category__c!=null)loanApplicationMap.put('loanPurposeCategory',loanApplication.Loan_Purpose_Category__c);
            if(loanApplication.Loan_Amount__c != null) loanApplicationMap.put('loanAmount', String.valueOf(loanApplication.Loan_Amount__c));
            if('Pass'.equalsIgnoreCase(creditCheckStatus) || 'Fail'.equalsIgnoreCase(creditCheckStatus)) {
                if(loanApplication.Decision_Taken_By__c != null) loanApplicationMap.put('decisionTakenBy', loanApplication.Decision_Taken_By__c);
                if(loanApplication.Decision_Date__c != null) loanApplicationMap.put('decisionDate', String.valueOf(loanApplication.Decision_Date__c));
            }
            
            //Need to save borrowerIds to get the Name separately
            borrowerIds.add(loanApplication.Account_Id__c);
            
            loanApplicationsMap.put(loanApplication.Id, loanApplicationMap);
        }
        
        // Create query string to get Account records
        String queryString2 = 'SELECT Id, Name,Applicant_Type__c FROM Account ';
        //Add filter clauses
        if ( !borrowerIds.isEmpty() ) {
            queryString2 += ' WHERE Id = :borrowerIds';
            List<Account> borrowers = Database.query(queryString2);
            Map<String,Account> borrowerMap = new Map<String,Account>();
            for ( Account borrower : borrowers ) {
                borrowerMap.put(borrower.Id, borrower);
            }
            
            //Add to return map
            List<Map<String,Object>> loanApplicationsList = loanApplicationsMap.values();
            for ( Map<String,Object> loanApplicationMap : loanApplicationsList ) {
                String borrowerId = String.valueOf(loanApplicationMap.get('borrowerId'));
                System.debug('Getting borrower infor'+borrowerId);
                Account borrower = borrowerMap.get(borrowerId);
                loanApplicationMap.put('name', borrower.Name);
                loanApplicationMap.put('applicantType',borrower.Applicant_Type__c);
                System.debug('borrower'+borrower);
            }
        }
        
        return loanApplicationsMap;
    }
    
    
    public static Map<String, Map<String,Object>> getLoanApplications(String kycStatus, String creditCheckStatus) {
        Map<String, Map<String,Object>> loanApplicationsMap = new Map<String, Map<String,Object>>();
        // Create query string to get Loan Application records
        String queryString = 'SELECT Id,  Account_Id__c,Credit_Check_Status__c,Loan_Amount__c,Loan_Maximum_EMI__c, ' +
            'Loan_Needed_by__c,Loan_Purpose_Category__c,Loan_Purpose_Description__c,Loan_Status__c, ' +
            'Loan_Tenor__c,Income_Detail_Current_Job__c,Income_Detail_Previous_Job__c, KYC_Status__c, LastModifiedDate ,Credit_Score__c ,Total_Work_Experience__c ,Interest_Other_Investment_Income__c,'+
            'Rental_Income__c,Other_Income__c,House_Property_Land__c,Vehicles__c,Bank_Deposits__c,'+
            'Shares__c,Mutual_Funds__c,Life_Insurance__c,Other_Assets__c,Housing_Loan__c,Auto_Loan__c,'+
            'Credit_Card_Outstanding__c,Persoanal_Loan__c,Other_Loans__c,Turnover_Latest_Year__c,Turnover_Prior_Year__c,'+
            'Turnover_Prior_2_Years__c,Net_Profit_Latest_Year__c,Net_Profit_Prior_Year__c,Net_Profit_Prior_2_Years__c,Net_Worth_Prior_Year__c,'+
            'Net_Worth_Prior_2_Years__c,Net_Worth_Latest_Year__c,Cash_Credit_Sanctioned__c,Cash_Credit_Outstanding__c,'+
            'Term_Loan_Sanctioned__c,Term_Loan_Outstanding__c,Bills_Discounting_Outstanding__c,LC_Guarantee_Sanctioned__c,'+
            'LC_Guarantee_Outstanding__c,Other_Borrowing_Outstanding__c,Legal_Structure__c,Business_Type__c,Other_Borrowing_Description__c,'+
            'Bills_Discounting_Sanctioned__c,Other_Borrowing_Sanctioned__c,'+
            'Company_Manager__c,Company_Owner__c,Other_Income_Detail__c,Other_Assets_Detail__c,Other_Loans_Detail__c,Credit_Scoring_Ruleset__c,' +
            'Reference_1_First_Name__c ,Reference_1_Last_Name__c ,Reference_1_Mobile_Number__c,Reference_1_Address__c,'+
            'Reference_2_First_Name__c ,Reference_2_Last_Name__c ,Reference_2_Mobile_Number__c,Reference_2_Address__c,'+
            'Credit_Officer_Judgement_Score__c,Credit_Officer_Action_Comments__c,Decision_Taken_By__c,Decision_Date__c,System_Score__c, '+
            'FriendlyScore_Submitted__c, Psychometric_Survey_Submitted__c, Security_Value__c, Security_Description__c ' +
            ' FROM Loan_Application__c ';
        //Add filter clauses
        String kycFilter = kycStatus != null ? 'KYC_Status__c = :kycStatus' : '';
        String creditCheckFilter = creditCheckStatus != null ? 'Credit_Check_Status__c = :creditCheckStatus' : '';
        if ( kycStatus != null || creditCheckStatus != null ) {
            queryString += ' WHERE ';
            queryString += kycFilter;
            if ( kycStatus != null && creditCheckStatus != null ) queryString += ' AND ';
            queryString += creditCheckFilter;
        }
        
        List<Loan_Application__c> loanApplications = Database.query(queryString);        
        System.debug('getLoanApplications ResultSize=' + loanApplications.size());
        
        // Create Result Map
        for ( Loan_Application__c loanApplication : loanApplications ) {
            Map<String,Object> loanApplicationMap = LoanApplicationService.getLoanApplicationDenormalized(loanApplication);
            // Get additional data for Borrower
            loanApplicationMap.putAll((Map<String,Object>)BorrowerService.getBorrowerDenormalized(loanApplication.Account_Id__c));
            loanApplicationsMap.put(loanApplication.Id, loanApplicationMap);
        }
        
        return loanApplicationsMap;
    }

    public static void updateKYCStatus (String loanApplicationId,String status){
        Loan_Application__c loanApplication=LoanApplicationService.getLoanApplicationById(loanApplicationId).loanApplication;
        Applicant__c applicant= ApplicantService.getApplicantForAccountId(loanApplication.Account_Id__c);
       applicant.KYC_Status__c=status;
        loanApplication.KYC_Status__c=status;
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
            update applicant;
            update loanApplication;
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }

    public static void updateKYCStatusForLender(String lenderId, String status){
        Applicant__c applicant= ApplicantService.getApplicantForAccountId(lenderId);
        applicant.KYC_Status__c=status;
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
            update applicant;
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
    
    public static void updateCreditCheckStatus (String loanApplicationId,String ccstatus,String status,String decisionTakenBy){
        Loan_Application__c loanApplication=LoanApplicationService.getLoanApplicationById(loanApplicationId).loanApplication;
        loanApplication.Credit_Check_Status__c=ccstatus;
        loanApplication.Loan_Status__c=status;
        loanApplication.Decision_Date__c = Date.today();
        if (decisionTakenBy != null) loanApplication.Decision_Taken_By__c = decisionTakenBy;

        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
            update loanApplication;
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }

    public static List<Map<String,Object>> getCriteriaScore(String category,String loanApplicationId){
        List<Map<String,String>> allcategories= new List<Map<String,String>>();
               List<Credit_Category_Criteria_Master__c> criteriaList=CreditCategoryCriteriaMasterService.getCriterionsforCategory(category);
                System.debug('criterias for category'+criteriaList.size());
               Map<String,String> criterionMap=new Map<String,String>();
               for(Integer i=0;i<criteriaList.size();i++){
                   Credit_Category_Criteria_Master__c row=criteriaList.get(i);
                     Integer score=CreditScoreService.getCreditScore(row.Id, 'criteria', loanApplicationId);
                   System.debug('score for this criteria..'+score+ row.Id);
                   criterionMap=CreditCategoryCriteriaMasterService.getCreditCategoryCriteriaMasterDenormalized(row);
                    criterionMap.put('score',String.valueOf(score));
                    if(score!=null)allcategories.add(criterionMap);

               }
                   return allcategories;
    }  
    
    public static Map<String, Map<String,Object>> getLendersList(String kycStatus) {
        Map<String, Map<String,Object>> lendersMap = new Map<String, Map<String,Object>>();
        // Create query string to get Lender records
        String queryString = 'SELECT Id, Name, Applicant_Type__c, LastModifiedDate FROM Account ' +
            'WHERE Account_Type__c = \'Lender\' ' +
            'AND Id IN (SELECT Account_Id__c FROM Applicant__c WHERE KYC_Status__c = :kycStatus)';
        
        List<Account> lenders = Database.query(queryString);        
        
        Integer count = 0;
        // Create Result Map
        for ( Account lender : lenders ) {
            Map<String,Object> lenderMap = new Map<String,Object>();
            lenderMap.put('lenderId', lender.Id);
            lenderMap.put('name', lender.Name);
            lenderMap.put('applicantType', lender.Applicant_Type__c);
            lenderMap.put('lenderNumber', ++count);
            lenderMap.put('date', String.valueOf(lender.LastModifiedDate));
            
            lendersMap.put(lender.Id, lenderMap);
        }
        
        return lendersMap;
    }
}