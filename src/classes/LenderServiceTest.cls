@isTest(seealldata = false)
public class LenderServiceTest{
   
   public static  testmethod void lenderServiceMethods(){
   
   Account acc=new Account();
   acc.Name='Microgram';
   acc.Account_Type__c='Borrower';
   acc.Applicant_Type__c='Individual';
   insert acc;
    
   
   Account acc1=new Account();
   acc1.Name='Microgram1';
   acc1.Account_Type__c='Borrower1';
   acc1.Applicant_Type__c='Individual';
   insert acc1;    
   
          Address__c mgp=new  Address__c();
          mgp.Address_Line_1__c='BDA Complex';
          mgp.Address_Line_2__c='permanentAddressLine2';
          mgp.City__c='permanentAddressCity';
          mgp.Pincode__c=123;
          mgp.State__c='Karnataka';
          mgp.Country__c='India'; 
          mgp.Residing_Since__c=Date.today();
          mgp.House_Ownership__c='Owned';
          insert mgp;
          
          Applicant__c app= new Applicant__c();
          app.Aadhar_Number__c=12;
          app.Account_Id__c=acc1.id;
          app.Registered_Address__c=mgp.id;
          app.Applicant_Nature__c='Primary';
          app.Correspondence_Address__c=mgp.id;
          app.Current_Address__c=mgp.id;
          app.Permanent_Address__c=mgp.id;
          app.E_Mail__c='Microgram@gmail.com';
          insert app;
       
           Applicant__c app1= new Applicant__c();
          app1.Aadhar_Number__c=12;
          app1.Account_Id__c=acc.id;
          //app.Registered_Address__c=mgp.id;
          app1.Applicant_Nature__c='Primary';
          //app.Correspondence_Address__c=mgp.id;
          //app.Current_Address__c=mgp.id;
          //app.Permanent_Address__c=mgp.id;
          app1.E_Mail__c='Microgram@gmail.com';
          insert app1;
       
          Map<String, Object> appMap=new Map<String,Object>();
          appMap.put('firstName', 'Microgram');
          appMap.put('Name', 'xyz');
          appMap.put('accountType', 'Borrower');
          appMap.put('applicantType', 'Individual');
            appMap.put('registeredAddressLine1', mgp.id);
         appMap.put('registeredAddressLine2', mgp.id);
         appMap.put('registeredAddressCity', 'banglore');
         appMap.put('registeredAddressPincode', 524137);
         appMap.put('registeredAddressState','karnataka');
         appMap.put('registeredAddressCountry','India');
        appMap.put('currentAddressLine1', mgp.id);
         appMap.put('currentAddressLine2', mgp.id);
         appMap.put('currentAddressCity', 'banglore');
         appMap.put('currentAddressPincode', 524137);
         appMap.put('currentAddressState','karnataka');
         appMap.put('currentAddressCountry','India');
         appMap.put('correspondenceAddressLine1', mgp.id);
         appMap.put('correspondenceAddressLine2', mgp.id);
         appMap.put('correspondenceAddressCity', 'banglore');
         appMap.put('correspondenceAddressPincode', 524137);
         appMap.put('correspondenceAddressState','karnataka');
         appMap.put('correspondenceAddressCountry','India');
         appMap.put('permanentAddressLine1', mgp.id);
         appMap.put('permanentAddressLine2', mgp.id);
         appMap.put('permanentAddressCity', 'banglore');
         appMap.put('permanentAddressPincode', 524137);
         appMap.put('permanentAddressState','karnataka');
         appMap.put('permanentAddressCountry','India');
       
          
        Map<String,Object> lenderMap=new Map<String,Object>();
        lenderMap.put('firstName', 'Etmarlabs');
        lenderMap.put('lastName', 'Tquila');
        lenderMap.put('Name', 'Etmarlabs Tquila');
        lenderMap.put('applicantType', 'Individual');
        lenderMap.put('accountType', 'Borrower');
        lenderMap.put('eMail', 'Borreer@gmail.com');
        lendermap.put('registeredAddressLine1', mgp.id);
        lendermap.put('registeredAddressLine2', mgp.id);
        lendermap.put('registeredAddressCity', 'banglore');
        lendermap.put('registeredAddressPincode', 524137);
        lendermap.put('registeredAddressState','karnataka');
        lendermap.put('registeredAddressCountry','India');
        lendermap.put('currentAddressLine1', mgp.id);
        lendermap.put('currentAddressLine2', mgp.id);
        lendermap.put('currentAddressCity', 'banglore');
        lendermap.put('currentAddressPincode', 524137);
        lendermap.put('currentAddressState','karnataka');
        lendermap.put('currentAddressCountry','India');
        lendermap.put('correspondenceAddressLine1', mgp.id);
        lendermap.put('correspondenceAddressLine2', mgp.id);
        lendermap.put('correspondenceAddressCity', 'banglore');
        lendermap.put('correspondenceAddressPincode', 524137);
        lendermap.put('correspondenceAddressState','karnataka');
        lendermap.put('correspondenceAddressCountry','India');
        lendermap.put('permanentAddressLine1', mgp.id);
        lendermap.put('permanentAddressLine2', mgp.id);
        lendermap.put('permanentAddressCity', 'banglore');
        lendermap.put('permanentAddressPincode', 524137);
        lendermap.put('permanentAddressState','karnataka');
        lendermap.put('permanentAddressCountry','India');
        
         LenderService lenderSrvc=new LenderService();
          
           LenderService.getLenderDenormalized(acc.id);
           LenderService.updateLender(acc.id,appMap);
           LenderService.deleteLender(acc.id);
           LenderService.registerLender(lenderMap);
   
   }

}