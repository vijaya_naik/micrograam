public class IntegrationMessageHandler{
    public static void execute(Integration_Message__c message){
        Type t1 = Type.forName(message.Request_Provider__c);
        IRequestProvider requestProvider = (IRequestProvider) t1.newInstance();
        
        Type t2 = Type.forName(message.Response_Handler__c);
        IResponseHandler responseHandler = (IResponseHandler) t2.newInstance();
        
        HttpResponse res = (HttpResponse) requestProvider.execute(message);
        
        if(res.getStatus() == 'OK'){
            responseHandler.execute(res, message);
            message.Status__c = 'Success';
            message.Error_Message__c = '';
            message.Status_Code__c = res.getStatusCode();
        }
        else{
            message.Status__c = 'Failure';
            message.Error_Message__c = res.getBody();
            message.Status_Code__c = res.getStatusCode();
        }
        
        update message;
    }
    
    
    public static void retry(Integration_Message__c message){
        message.No_of_Retries__c = message.No_of_Retries__c <> null ? message.No_of_Retries__c + 1 : 1;
        IntegrationMessageHandler.execute(message);  
    }
    
    
    public static Integration_Message__c getIntegrationMessages(String inputString, IntegrationMessageProvider__c messageProvider){
        return new Integration_Message__c(
            Request_Provider__c = messageProvider.Request_Provider__c,
            Response_Handler__c = messageProvider.Response_Handler__c,
            Input_String__c = inputString,
            Max_No_of_Retries__c = (Integer) messageProvider.No_of_Retries__c
        );
    }
}