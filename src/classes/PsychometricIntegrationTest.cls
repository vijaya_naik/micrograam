@isTest (SeeAllData=false)
public class PsychometricIntegrationTest {
    static testMethod void testOne(){
         IntegrationMessageProvider__c messageProvider = new IntegrationMessageProvider__c(
            Name = Label.Psychometric,
            Request_Provider__c = 'RequestProviderPsychometric',
            Response_Handler__c = 'ResponseHandlerPsychometric',
            No_of_Retries__c = 5
        );
        insert messageProvider;
        
        IntegrationMessageProvider__c messageProviderPDF = new IntegrationMessageProvider__c(
            Name = Label.PsychometricPDF,
            Request_Provider__c = 'RequestProviderPsychometricPDF',
            Response_Handler__c = 'ResponseHandlerPsychometricPDF',
            No_of_Retries__c = 5
        );
        insert messageProviderPDF;
        
        Account acc = new Account(
            Name = 'Test'
        );
        insert acc;
        
        Loan_Application__c loanApp = new Loan_Application__c(
            Account_Id__c = acc.Id,
            Psychometric_Survey_Submitted__c = TRUE
        );
        Test.startTest();
        insert loanApp;
        Test.stopTest();
    }
    
    
    static testMethod void testTwo(){
        IntegrationMessageProvider__c messageProvider = new IntegrationMessageProvider__c(
            Name = Label.Psychometric,
            Request_Provider__c = 'RequestProviderPsychometric',
            Response_Handler__c = 'ResponseHandlerPsychometric',
            No_of_Retries__c = 5
        );
        insert messageProvider;
        
        IntegrationMessageProvider__c messageProviderPDF = new IntegrationMessageProvider__c(
            Name = Label.PsychometricPDF,
            Request_Provider__c = 'RequestProviderPsychometricPDF',
            Response_Handler__c = 'ResponseHandlerPsychometricPDF',
            No_of_Retries__c = 5
        );
        insert messageProviderPDF;
        
        Account acc = new Account(
            Name = 'Test'
        );
        insert acc;
        
        Loan_Application__c loanApp = new Loan_Application__c(
            Account_Id__c = acc.Id,
            Psychometric_Survey_Submitted__c = TRUE
        );
        
        AccessTokenHandlerPsychometric tokenHanlder = new AccessTokenHandlerPsychometric();
        tokenHanlder.storeAccessToken(Label.Psychometric,'aaaaaa');
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new IntegrationPsychometricTestMock());
        insert loanApp;
        Test.stopTest();
    }
    
    
    static testMethod void testThree(){
       IntegrationMessageProvider__c messageProvider = new IntegrationMessageProvider__c(
            Name = Label.Psychometric,
            Request_Provider__c = 'RequestProviderPsychometric',
            Response_Handler__c = 'ResponseHandlerPsychometric',
            No_of_Retries__c = 5
        );
        insert messageProvider;
        
        IntegrationMessageProvider__c messageProviderPDF = new IntegrationMessageProvider__c(
            Name = Label.PsychometricPDF,
            Request_Provider__c = 'RequestProviderPsychometricPDF',
            Response_Handler__c = 'ResponseHandlerPsychometricPDF',
            No_of_Retries__c = 5
        );
        insert messageProviderPDF;
        
        Account acc = new Account(
            Name = 'Test'
        );
        insert acc;
        
        Loan_Application__c loanApp = new Loan_Application__c(
            Account_Id__c = acc.Id,
            Psychometric_Survey_Submitted__c = FALSE
        );
        insert loanApp;
        
        AccessTokenHandlerPsychometric tokenHanlder = new AccessTokenHandlerPsychometric();
        tokenHanlder.storeAccessToken('aaaaaa', Label.Psychometric);
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new IntegrationPsychometricTestMock());
        loanApp.Psychometric_Survey_Submitted__c = TRUE;
        update loanApp;
        Test.stopTest();
    }

}