@RestResource(urlMapping='/v1/psychometric')
global with sharing class PsychometricResource {
    @HttpPost
    global static Map<String,String> cPsychometric() {
        RestRequest req = RestContext.request;
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received Psychometric Registration request:: ' + params);
        // Invoke service
        return PsychometricService.createPsychometric(params);
    }
    
    @HttpGet
    global static Map<String,String> getPsychometric() {
        // Get id/accountId from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
        		String loanApplicationId = req.params.get('loanApplicationId');
        String source=req.params.get('source');
        if ( loanApplicationId != null && source!=null) {
            // Invoke service to get by loanApplicationId 
            System.debug('Received  Get request for loanApplicationId=' + loanApplicationId);
            id= PsychometricService.getPsychometric(loanApplicationId, source).Id;
                }
            // Invoke service to get by Loan Application id
            System.debug('Received Psychometric Get request for id=' + id);
            return PsychometricService.getPsychometricDenormalized(PsychometricService.getPsychometric(id));
        
    }
    
    @HttpDelete
    global static void deletePsychometric() {
        // Get id from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
        System.debug('Received Psychometric Delete request for id=' + id);
        // Invoke service
        PsychometricService.deletePsychometric(id);
    }
    
    @HttpPatch
    global static Map<String,String> updatePsychometric() {
        RestRequest req = RestContext.request;
        // Get id from request parameter
        String id = req.params.get('id');
                		String loanApplicationId = req.params.get('loanApplicationId');
        String source=req.params.get('source');
        if ( loanApplicationId != null && source!=null) {
            // Invoke service to get by loanApplicationId 
            System.debug('Received  Get request for loanApplicationId=' + loanApplicationId);
            id= PsychometricService.getPsychometric(loanApplicationId, source).Id;
                }
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received Psychometric Update request for id=' + id + '::' + params);
        // Invoke service
        return PsychometricService.updatePsychometric(id, params);
    }

}