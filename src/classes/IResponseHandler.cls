public interface IResponseHandler{
    void execute(Object response, Integration_Message__c message);
}