@RestResource(urlMapping='/v2/creditScore')
global with sharing class CreditScoreResource2 {
    @HttpPost
    global static void cCreditScore() {
        RestRequest req = RestContext.request;
        // Deserialize the JSON string into name-value pairs
        List<Object> params = (List<Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received CreditScore Registration request:: ' + params);
        String loanApplicationId = req.params.get('loanApplicationId');
        String creditScoringRuleSet = req.params.get('creditScoringRuleSet');
        //Invoke service to get by Loan Application id
        System.debug('Received CreditScore Get request for loanApplicationId=' + loanApplicationId + ',creditScoringRuleSet=' + creditScoringRuleSet);
        //Delete records with this loan application id if exists.
        if (loanApplicationId!=null) CreditScoreService.deleteCreditScore(loanApplicationId);
        // Invoke service
        CreditScoreCreateService.createCreditScore(creditScoringRuleSet, params);
    }
    
    @HttpGet
    global static List<Application_Credit_Score__c> getCreditScore() {
        // Get id/accountId from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
           // Invoke service to get by Loan Application id
            System.debug('Received CreditScore Get request for id=' + id);
            return CreditScoreService.getCreditScore(id);
        
    }
    
    @HttpDelete
    global static void deleteCreditScore() {
        // Get id from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
        System.debug('Received CreditScore Delete request for id=' + id);
        // Invoke service
        CreditScoreService.deleteCreditScore(id);
    }
    
   
}