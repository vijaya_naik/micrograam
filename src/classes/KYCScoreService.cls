public class KYCScoreService {
    public static List<Application_KYC_Score__c> getKYCScore(String loanApplicationId) {
        List<Application_KYC_Score__c> applicationKYCScore = [
            SELECT Id, Account_Id__c,Loan_Application_Id__c,
           Field_Id__c,Score__c
            FROM Application_KYC_Score__c 
            WHERE Loan_Application_Id__c = :loanApplicationId
        ];
        return applicationKYCScore;

    }

    public static List<Application_KYC_Score__c> getKYCScoreForLender(String lenderId) {
        List<Application_KYC_Score__c> applicationKYCScore = [
            SELECT Id, Account_Id__c,Loan_Application_Id__c,
           Field_Id__c,Score__c
            FROM Application_KYC_Score__c 
            WHERE Account_Id__c = :lenderId
        ];
        return applicationKYCScore;

    }
    
    public static void createKYCScore(List<Object> mapParams) {
        //Get KYC_Rules_Master Ids
        Map<String,String> fieldsMasterMap = KYCRulesMasterService.getKYCRulesMasterMap();
        
        Savepoint sp = Database.setSavePoint();
        try {
            for(Integer i=0;i<mapParams.size();i++){
                Map<String, Object> params = (Map<String, Object>) mapParams.get(i);
                //Create Application_KYC instance & set fields
                Application_KYC_Score__c applicationKYCScore = createKYCScoreInstance(fieldsMasterMap, params, null);
           
                //Update in database
                insert applicationKYCScore;
            }
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
    
    public static Application_KYC_Score__c createKYCScoreInstance(
        Map<String,String> fieldsMasterMap,
        Map<String, Object> params, Application_KYC_Score__c applicationKYCScore) {
        
        if (applicationKYCScore == null)
            applicationKYCScore = new Application_KYC_Score__c(); //For new record creation
        
        boolean hasValue = false;
/*      if (params.containsKey('fieldId')){
            applicationKYCScore.Field_Id__c=String.valueOf(params.get('fieldId'));
            hasValue=true;
        }
*/      
        String fieldName = String.valueOf(params.get('fieldName'));
        applicationKYCScore.Field_Id__c = fieldsMasterMap.get(fieldName);
            
        if (params.containsKey('loanApplicationId')) {
            applicationKYCScore.Loan_Application_Id__c=String.valueOf(params.get('loanApplicationId'));
            hasValue=true;
        }
        if (params.containsKey('lenderId')) {
            applicationKYCScore.Account_Id__c=String.valueOf(params.get('lenderId'));
            hasValue=true;
        }
        if (params.containsKey('score'))  {
            hasValue=true;
            applicationKYCScore.Score__c=Double.valueOf(params.get('score'));
        }
        
        if(hasValue)
            return applicationKYCScore;
        else 
            return null;
    }
    
    public static Map<String,String> getKYCScoreDenormalized(Application_KYC_Score__c applicationKYCScore) {
        Map<String,String> applicationKYCScoreMap = new Map<String,String>();
        
        if (applicationKYCScore.Field_Id__c != null) applicationKYCScoreMap.put('fieldId', applicationKYCScore.Field_Id__c);
        if (applicationKYCScore.Loan_Application_Id__c != null) applicationKYCScoreMap.put('loanApplicationId', String.valueOf(applicationKYCScore.Loan_Application_Id__c));
        if (applicationKYCScore.Account_Id__c != null) applicationKYCScoreMap.put('lenderId', String.valueOf(applicationKYCScore.Account_Id__c));
        if (applicationKYCScore.Score__c != null) applicationKYCScoreMap.put('score', String.valueOf(applicationKYCScore.Score__c));
        
        return applicationKYCScoreMap;
    }
    
    public static Map<String,String> getKYCScoreDenormalized(String id) {
        Application_KYC_Score__c applicationKYCScore = [
            SELECT Id, Account_Id__c, Loan_Application_Id__c,
            Field_Id__c,Score__c
            FROM Application_KYC_Score__c 
            WHERE Id = :Id
        ];
        return getKYCScoreDenormalized(applicationKYCScore);
       }
    
    public static void deleteKYCScore(String loanApplicationId) {
        //Get applicant record
        List<Application_KYC_Score__c> dto=KYCScoreService.getKYCScore(loanApplicationId);


        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
             delete dto; //Delete KYC Score records
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }

    public static void deleteKYCScoreForLender(String lenderId) {
        //Get applicant record
        List<Application_KYC_Score__c> dto=KYCScoreService.getKYCScoreForLender(lenderId);


        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
             delete dto; //Delete KYC Score records
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
}