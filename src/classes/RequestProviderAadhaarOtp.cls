public class RequestProviderAadhaarOtp implements IRequestProvider {
    
    public Object execute(Integration_message__c message)
    {
         HttpResponse res=RequestProviderAadhaarOtp.doCallOut(message);
         return res;        
    }


     public static HttpResponse doCallout(Integration_Message__c message){
        
        Applicant__c application = [SELECT Id, Aadhar_Number__c FROM Applicant__c WHERE Id = :message.Input_String__c];
       
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndPoint('http://34.194.186.69:9090/otp');
        req.setHeader('Content-Type','application/json');
        req.setBody('{"aadhaar-id": "'+application.Aadhar_Number__c+'","certificate-type": "'+Label.AadhaarReqType+'","channel":"SMS","type":"A","location": {"type": "","latitude": "","longitude": "","altitude": "","pincode": ""}}');
        return (new Http()).send(req);
    }

}