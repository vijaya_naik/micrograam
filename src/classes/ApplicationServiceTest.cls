//class Created By:shambhavi
@isTest(seealldata = false)
public class ApplicationServiceTest {
 public static testmethod void testunitOne(){
  string i='first';
  Account a=UtilityClassTest.insertAccount(i);
  insert a;
   Applicant__c  Applicant=UtilityClassTest.applicant(a.id);
  insert Applicant;
  Applicant__c  mgApplicant=UtilityClassTest.createApplicant(a.id);
    Object obj='true';
   date d=Date.today();
   Map<String, Object> params=new  Map<String, Object>();
      params.put('registrationNumber','test');
      params.put('primaryContactName','Shambhavi');
      params.put('firstName','test');
      params.put('middleName','Shambhavi');
      params.put('lastName','obj');
      params.put('aadharNumber',121212);
      params.put('voterID','Shambhavi');
      params.put('bankAccountNumber',10101010);
      params.put('bankIFSCCode','test');
      params.put('bankName','Shambhavi');
      params.put('completionYear',2016);
      params.put('dateOfBirth',d);
      params.put('eMail','test');
      params.put('fathersFirstName','Shambhavi');
      params.put('fathersLastName','Shambhavi');
      params.put('fathersMiddleName',obj);
      params.put('gender','test');
      params.put('highestQualificationDesc','Shambhavi');
      params.put('highestQualificationType','Shambhavi');
      params.put('landlineNumber',1212121);
      params.put('landlineSTDCode',802);
      params.put('lastEducationInstitute','Shambhavi');
      params.put('mobileNumber','Shambhavi');
      params.put('noOfDependentChildren',4);
      params.put('noOfDependents',4);
      params.put('officeNumber','111111');
      params.put('officeExtension',12);
      params.put('officeSTDCode',080);
      params.put('profileFacebook','Shambhavi');
      params.put('profileLinkedIn','Shambhavi');
      params.put('profileTwitter','Shambhavi');
      
      ApplicantService.getApplicantForAccountId(a.id);
      ApplicantService.createApplicant(params,mgApplicant);
      ApplicantService.getApplicantDenormalized(mgApplicant);
 }
}