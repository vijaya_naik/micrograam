/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class eforms_test {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        eforms.ResolutionReturnDDto ef=new eforms.ResolutionReturnDDto();
        eforms.AmalgamatedCompanyBlockReturnDDto ef1=new eforms.AmalgamatedCompanyBlockReturnDDto();
        eforms.AuthorisedPersonsBlockReturnDDto  ef2=new  eforms.AuthorisedPersonsBlockReturnDDto();
        eforms.AuthorisedPersonsReturnDDto ef3=new eforms.AuthorisedPersonsReturnDDto();
        eforms.BankDetailsReturnDDto EF4=new eforms.BankDetailsReturnDDto();
        eforms.CAOBlockReturnDDto ef5=new eforms.CAOBlockReturnDDto();
        eforms.ChargeReturnDDto ef6=new eforms.ChargeReturnDDto();
        eforms.ChrgHolderReturnDDto ef7=new eforms.ChrgHolderReturnDDto();
        eforms.ClssSrnReturnDDto ef8=new eforms.ClssSrnReturnDDto();
        eforms.CmpnyDetailsForSrnReturnDDto ef9=new eforms.CmpnyDetailsForSrnReturnDDto();
        eforms.CompanyBlockReturnAddressDtls ef10=new eforms.CompanyBlockReturnAddressDtls();
        eforms.CompanyBlockReturnDDto ef11=new eforms.CompanyBlockReturnDDto();
        eforms.CompanyBlockReturnForForm1DDto ef12=new eforms.CompanyBlockReturnForForm1DDto();
        eforms.CompanyBlockReturnName1AtDDto ef13=new eforms.CompanyBlockReturnName1AtDDto();
        eforms.CompanyBlockReturnSCDDto ef14=new eforms.CompanyBlockReturnSCDDto();
        eforms.CompanyBlockReturnWithEmailDDto ef15=new  eforms.CompanyBlockReturnWithEmailDDto();
        eforms.CompanyBlockReturnWithNameAtIncorpDDto ef16=new eforms.CompanyBlockReturnWithNameAtIncorpDDto();
        eforms.CompanyBlockReturnWithEmailSCDDto ef17=new eforms.CompanyBlockReturnWithEmailSCDDto();
        eforms.CompanyWithFrm18FlagSCReturnDDto ef18=new eforms.CompanyWithFrm18FlagSCReturnDDto();
        eforms.CompanyWithFrm18FlagReturnDDto ef19=new eforms.CompanyWithFrm18FlagReturnDDto();
        eforms.CompanyReturnDDto ef20=new eforms.CompanyReturnDDto();
        eforms.CompanyNameChangeBlockReturnatIncorpDto ef21=new eforms.CompanyNameChangeBlockReturnatIncorpDto();
        eforms.CompanyDetailsForm2P6ForCINReturnDDTO ef22=new eforms.CompanyDetailsForm2P6ForCINReturnDDTO();
        eforms.CompanyDetailsForCINSCReturnDDTO ef23=new eforms.CompanyDetailsForCINSCReturnDDTO();
        eforms.CompanyDetailsForCINReturnDDTO ef24=new eforms.CompanyDetailsForCINReturnDDTO();
        eforms.CompanyBlockReturnWithNomineeNameDDto ef25 =new eforms.CompanyBlockReturnWithNomineeNameDDto();
        eforms.CompanyBlockReturnWithNameAtIncorpDDto ef26=new eforms.CompanyBlockReturnWithNameAtIncorpDDto();
        
        eforms.StmpWithDocReturnDDto ef27=new eforms.StmpWithDocReturnDDto();
        eforms.StampDutyReturnDDto ef28=new eforms.StampDutyReturnDDto();
        eforms.SRNdetailsforForm2p2ReturnDDto ef29=new eforms.SRNdetailsforForm2p2ReturnDDto();
        eforms.SRNDetails67ReturnDDto ef30=new eforms.SRNDetails67ReturnDDto();
        eforms.SRNdeatilsforForm1ReturnDDto ef31=new eforms.SRNdeatilsforForm1ReturnDDto();
        eforms.ResolutionReturnDDto ef32=new eforms.ResolutionReturnDDto();
        eforms.ReceiverManagerReturnDDto ef33=new eforms.ReceiverManagerReturnDDto();
        eforms.RefundSrnReturnDDto ef34=new eforms.RefundSrnReturnDDto();
        eforms.ReceiverManagerReturnDDto ef35=new eforms.ReceiverManagerReturnDDto();
        eforms.ReceiverManagerBlockReturnDDto ef36=new eforms.ReceiverManagerBlockReturnDDto();
        eforms.PymntDetailForSrnReturnDDto ef37=new eforms.PymntDetailForSrnReturnDDto();
        eforms.PrpsdCmpnyReturnGovPubDDto ef38=new eforms.PrpsdCmpnyReturnGovPubDDto();
        
        eforms.ProposedCompanyWithRocCodeReturnDDto ef39=new eforms.ProposedCompanyWithRocCodeReturnDDto();
        eforms.PrpsdCmpnyReturnGovPubDDto ef40=new eforms.PrpsdCmpnyReturnGovPubDDto();
        eforms.ProposedCompanyWithRocCodeReturnDDto ef41=new eforms.ProposedCompanyWithRocCodeReturnDDto();
        eforms.ProposedCompanySplitReturnDDto ef42=new eforms.ProposedCompanySplitReturnDDto();
        eforms.ProposedCompanyReturnDDto ef43=new eforms.ProposedCompanyReturnDDto();
        eforms.ProposedCINAddReturnDDto ef44=new eforms.ProposedCINAddReturnDDto();
        
        eforms.DIN3ReturnDto ef45=new eforms.DIN3ReturnDto();
        eforms.DINDtlsNewReturnDDto ef46=new eforms.DINDtlsNewReturnDDto();
        eforms.DirectorDetailsForDINReturnDDTO ef47=new eforms.DirectorDetailsForDINReturnDDTO();
        eforms.DirectorListReturnDDto ef48=new eforms.DirectorListReturnDDto();
        eforms.DirectorsAssociatedWithCINSCDDto ef49=new  eforms.DirectorsAssociatedWithCINSCDDto();
        eforms.DtlsOfAOC4NonXbrlReturnDDto ef50=new eforms.DtlsOfAOC4NonXbrlReturnDDto();
        
        eforms.FinancialParameterReturnDDto ef51=new eforms.FinancialParameterReturnDDto();
        eforms.Form1ADtlsReturnDDto ef52 =new eforms.Form1ADtlsReturnDDto();
        eforms.Form23CCRA2SRNDetailsDDto ef53 =new eforms.Form23CCRA2SRNDetailsDDto();
        eforms.Form23CSrnNewReturnDDto ef54 =new eforms.Form23CSrnNewReturnDDto();
        eforms.Form23CSrnReturnDDto ef55 =new eforms.Form23CSrnReturnDDto();
        eforms.Form2P1DtlsReturnDDto ef56 =new eforms.Form2P1DtlsReturnDDto();
        eforms.Form32DINDtlsReturnDDto ef57 =new eforms.Form32DINDtlsReturnDDto();
        
        eforms.NameApprovalReturnDDto ef58=new eforms.NameApprovalReturnDDto();
        eforms.DirectorReturnDDto ef59=new eforms.DirectorReturnDDto();
    }
}