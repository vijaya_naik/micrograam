public class AdminService {
    public static void deleteAllDataByAccountId(String accountId) {
        //Get applicant record
        Applicant__c applicant = ApplicantService.getApplicantForAccountId(accountId);
        //Get account record
        Account account = [SELECT Id FROM Account WHERE Account.Id = :accountId];
        //Get current address record
        Address__c currentAddress = null;
        if (applicant.Current_Address__c != null) {
        	currentAddress = AddressService.getAddressById(applicant.Current_Address__c);
        }
        //Get permanent address record
        Address__c permanentAddress = null;
        if (applicant.Permanent_Address__c != null) {
        	permanentAddress = AddressService.getAddressById(applicant.Permanent_Address__c);
        }
        Address__c RegisteredAddress =null;
       if (applicant.Registered_Address__c != null) 
             RegisteredAddress = AddressService.getAddressById(applicant.Registered_Address__c);
        Address__c CorrespondenseAddress =null;
        if (applicant.Correspondence_Address__c != null) 
             CorrespondenseAddress = AddressService.getAddressById(applicant.Correspondence_Address__c);

        //Get loan application records
        List<Loan_Application__c> loanApplications = LoanApplicationService.getApplicationsForAccountId(accountId);
        
        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
            if (currentAddress != null)
            	delete currentAddress; //Delete current address record
            if (permanentAddress != null)
            	delete permanentAddress; //Delete permanent address record
            if (RegisteredAddress != null)
            	delete RegisteredAddress; //Delete registered address record
            if (CorrespondenseAddress != null)
            	delete CorrespondenseAddress; //Delete correspondence address record 
            
            //Delete Loan application records
            for ( Loan_Application__c loanApplication : loanApplications ) {
                //Delete Credit Bureau records
                CreditBureauService.deleteCreditBureaus(loanApplication.Id);
                //Delete Credit Score records
                CreditScoreService.deleteCreditScore(loanApplication.Id);
                //Delete Application KYC records for Borrower
                ApplicationKYCService.deleteApplicationKYCsForLoanApplication(loanApplication.Id);
                //Delete Application KYC Score records for Borrower
                KYCScoreService.deleteKYCScore(loanApplication.Id);
                //Delete Pyschometric records
                PsychometricService.deletePsychometricData(loanApplication.Id);
                //Delete Social Media records
                SocialMediaService.deleteSocialMediaData(loanApplication.Id);
                
                LoanApplicationService.deleteLoanApplication(loanApplication.Id);
            }
            
            //Delete Application KYC records for Lender
            ApplicationKYCService.deleteApplicationKYCsForLender(accountId);
            //Delete Application KYC Score records for Lender
            KYCScoreService.deleteKYCScoreForLender(accountId);
            
            delete applicant; //Delete applicant record
            delete account; //Delete account record
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
}