/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Prefill_test {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        prefill.get23CSRNDetails_element pf1=new  prefill.get23CSRNDetails_element();
        prefill.get23CSRNDetailsResponse_element pf2=new prefill.get23CSRNDetailsResponse_element();
        prefill.get23CSRNNewDetails_element pf3=new prefill.get23CSRNNewDetails_element();
        prefill.get23CSRNNewDetailsResponse_element pf4=new prefill.get23CSRNNewDetailsResponse_element();
        prefill.getAmalgamatedCompanyDate_element  pf5=new prefill.getAmalgamatedCompanyDate_element();
        prefill.getAmalgamatedCompanyDateResponse_element pf6=new prefill.getAmalgamatedCompanyDateResponse_element();
        prefill.getAuthPersonBlockDetails_element pf7=new  prefill.getAuthPersonBlockDetails_element();
        prefill.getAuthPersonBlockDetailsResponse_element pf8=new prefill.getAuthPersonBlockDetailsResponse_element();
        prefill.getAuthPersonForeignDetails_element pf9=new prefill.getAuthPersonForeignDetails_element();
        prefill.getAuthPersonForeignDetailsForCompany_element pf10=new prefill.getAuthPersonForeignDetailsForCompany_element();
        prefill.getAuthPersonForeignDetailsForCompanyResponse_element pf11=new prefill.getAuthPersonForeignDetailsForCompanyResponse_element();
        prefill.getAuthPersonForeignDetailsResponse_element pf12=new prefill.getAuthPersonForeignDetailsResponse_element();
        prefill.getAuthPersonIndianDetails_element pf13=new prefill.getAuthPersonIndianDetails_element();
        
        prefill.getStmpWithDocDtlsResponse_element pf14=new prefill.getStmpWithDocDtlsResponse_element();
        prefill.PrefillService pf15=new  prefill.PrefillService();
        prefill.getStmpWithDocDtls_element pf16=new prefill.getStmpWithDocDtls_element();
        prefill.getStampDutyDtlsResponse_element pf17=new prefill.getStampDutyDtlsResponse_element();
        prefill.getStampDutyDtls_element pf18=new prefill.getStampDutyDtls_element();
        prefill.getSRNforForm2p2DetailsResponse_element pf19=new prefill.getSRNforForm2p2DetailsResponse_element();
        prefill.getSRNforForm2p2Details_element pf20=new prefill.getSRNforForm2p2Details_element();
        prefill.getSRNforForm1DetailsResponse_element pf21=new prefill.getSRNforForm1DetailsResponse_element();
        prefill.getSRNforForm1Details_element pf22=new prefill.getSRNforForm1Details_element();
        prefill.getSRNDetailsfor67Response_element pf23=new prefill.getSRNDetailsfor67Response_element();
        prefill.getSRNDetailsfor67_element pf24=new prefill.getSRNDetailsfor67_element();
        prefill.getSignatoryNameResponse_element pf25=new prefill.getSignatoryNameResponse_element();
        prefill.getSignatoryName_element pf26=new prefill.getSignatoryName_element();
        prefill.getSecretaryNameResponse_element pf27=new prefill.getSecretaryNameResponse_element();
        prefill.getSecretaryName_element pf28 =new prefill.getSecretaryName_element();
        
        prefill.getResolutionDetailsResponse_element pf29=new prefill.getResolutionDetailsResponse_element();
        prefill.getResolutionDetails_element pf30=new prefill.getResolutionDetails_element();
        prefill.getRefundSRNDetailsResponse_element pf31=new prefill.getRefundSRNDetailsResponse_element();
        prefill.getRefundSRNDetails_element pe32=new prefill.getRefundSRNDetails_element();
        prefill.getRecvrMangrDtlsForPANResponse_element pe33=new prefill.getRecvrMangrDtlsForPANResponse_element();
        prefill.getRecvrMangrDtlsForPAN_element pe34 = new prefill.getRecvrMangrDtlsForPAN_element();
        prefill.getRecvrMangrBlockDtlsForPANResponse_element pe35=new prefill.getRecvrMangrBlockDtlsForPANResponse_element();
        prefill.getRecvrMangrBlockDtlsForPAN_element pe36=new prefill.getRecvrMangrBlockDtlsForPAN_element();
        prefill.getReceiverManagerDetailsResponse_element pe37=new prefill.getReceiverManagerDetailsResponse_element();
        prefill.getRecvrMangrBlockDtlsForPAN_element pe38=new prefill.getRecvrMangrBlockDtlsForPAN_element();
         
        prefill.getReceiverManagerDetailsResponse_element pe39=new prefill.getReceiverManagerDetailsResponse_element();
        prefill.getReceiverManagerDetails_element pe40=new prefill.getReceiverManagerDetails_element();
        prefill.getReceiverManagerBlockDetailsResponse_element pe41=new prefill.getReceiverManagerBlockDetailsResponse_element();
        prefill.getReceiverManagerBlockDetails_element pe42=new prefill.getReceiverManagerBlockDetails_element();
        
        prefill.getPymntDetailsForSrn_element pe43=new prefill.getPymntDetailsForSrn_element();
        prefill.getPymntDetailsForSrnResponse_element pe44=new prefill.getPymntDetailsForSrnResponse_element();
        
        prefill.getProposedCompanySplitDetailsResponse_element pe45=new prefill.getProposedCompanySplitDetailsResponse_element();
        prefill.getProposedCompanyDetails_element pe46=new prefill.getProposedCompanyDetails_element();
        prefill.getProposedCompanyDetailsResponse_element pe47=new prefill.getProposedCompanyDetailsResponse_element();
        prefill.getProposedCompanyGovDetails_element pe48=new prefill.getProposedCompanyGovDetails_element();
        prefill.getProposedCompanyName_element pe49=new prefill.getProposedCompanyName_element();
        prefill.getProposedCompanyNameResponse_element pe50=new prefill.getProposedCompanyNameResponse_element();
        prefill.getProposedCompanyNameWthROCCode_element pe51=new prefill.getProposedCompanyNameWthROCCode_element();
        prefill.getProposedCompanyNameWthROCCodeResponse_element pe52=new prefill.getProposedCompanyNameWthROCCodeResponse_element();
        prefill.getProposedCompanySplitDetails_element pe53=new prefill.getProposedCompanySplitDetails_element();
        prefill.getProposedCompanySplitDetailsResponse_element pe54=new prefill.getProposedCompanySplitDetailsResponse_element();
        
        prefill.getNameApprovalDetailsResponse_element pe55=new prefill.getNameApprovalDetailsResponse_element();
        prefill.getNameApprovalDetails_element pe56=new prefill.getNameApprovalDetails_element();
        
        prefill.getLLPForm8ContriDetailsResponse_element pe562 =new prefill.getLLPForm8ContriDetailsResponse_element();
        prefill.getLLPForm8ApplicantDetails_element pe57 =new prefill.getLLPForm8ApplicantDetails_element();
        prefill.getLLPForm8ApplicantDetailsResponse_element pe58=new prefill.getLLPForm8ApplicantDetailsResponse_element();
        prefill.getLLPForm8ContriDetails_element pe59=new prefill.getLLPForm8ContriDetails_element();
        
        prefill.getLLPForm11Details_element pe592=new prefill.getLLPForm11Details_element();
        prefill.getLLPForm11DetailsResponse_element pe60=new prefill.getLLPForm11DetailsResponse_element();
        prefill.getLLPForm11IndividualsDetails_element pe61=new prefill.getLLPForm11IndividualsDetails_element();
        prefill.getLLPForm11IndividualsDetailsResponse_element pe62=new prefill.getLLPForm11IndividualsDetailsResponse_element();
        prefill.getLLPForm1ApplicantDetails_element pe63=new prefill.getLLPForm1ApplicantDetails_element();
        prefill.getLLPForm1ApplicantDetailsResponse_element pe64=new prefill.getLLPForm1ApplicantDetailsResponse_element();
        prefill.getLLPForm25SRNDetails_element pe65=new prefill.getLLPForm25SRNDetails_element();
        prefill.getLLPForm25SRNDetailsResponse_element pe66=new prefill.getLLPForm25SRNDetailsResponse_element();
        prefill.getLLPForm3Details_element pe67=new prefill.getLLPForm3Details_element();
        prefill.getLLPForm3DetailsResponse_element pe68=new prefill.getLLPForm3DetailsResponse_element();
        prefill.getLLPForm3DetailsResponse_element pe69=new prefill.getLLPForm3DetailsResponse_element();
        prefill.getLLPForm8ApplicantDetails_element pe70=new prefill.getLLPForm8ApplicantDetails_element();
        prefill.getLLPForm8ApplicantDetailsResponse_element pe71=new prefill.getLLPForm8ApplicantDetailsResponse_element();
       
        prefill.getForeignCompanyDetails_element pe72=new prefill.getForeignCompanyDetails_element();
        prefill.getForeignCompanyDetailsNew_element pe73=new prefill.getForeignCompanyDetailsNew_element();
        prefill.getForeignCompanyDetailsNewResponse_element pe74=new prefill.getForeignCompanyDetailsNewResponse_element();
        prefill.getForeignCompanyDetailsResponse_element pe75=new prefill.getForeignCompanyDetailsResponse_element();
        
        prefill.getBankDetails_element pe76=new prefill.getBankDetails_element();
        prefill.getBankDetailsResponse_element pe77=new prefill.getBankDetailsResponse_element();
        prefill.getBodyCorporatePartnersForm11_element pe78=new prefill.getBodyCorporatePartnersForm11_element();
        prefill.getBodyCorporatePartnersForm11Response_element pe79=new prefill.getBodyCorporatePartnersForm11Response_element();
        
        prefill.getCAODetails_element pe80=new prefill.getCAODetails_element();
        prefill.getCAODetailsResponse_element pe81=new prefill.getCAODetailsResponse_element();
        prefill.getChangedName_element pe82=new prefill.getChangedName_element();
        prefill.getChangedNameResponse_element pe83=new prefill.getChangedNameResponse_element();
        prefill.getChangedNameWithNameatIncorp_element pe84=new prefill.getChangedNameWithNameatIncorp_element();
        prefill.getChangedNameWithNameatIncorpResponse_element pe85=new prefill.getChangedNameWithNameatIncorpResponse_element();
        prefill.getChargeDetails_element pe86=new prefill.getChargeDetails_element();
        prefill.getChangedNameResponse_element pe87=new prefill.getChangedNameResponse_element();
        prefill.getChangedNameWithNameatIncorp_element pe88=new prefill.getChangedNameWithNameatIncorp_element();
        prefill.getChangedNameWithNameatIncorpResponse_element pe89=new prefill.getChangedNameWithNameatIncorpResponse_element();
        prefill.getChargeDetailsResponse_element pe90=new prefill.getChargeDetailsResponse_element();
        prefill.getChargeHolderBlockDtls_element pe91=new prefill.getChargeHolderBlockDtls_element();
        prefill.getChargeHolderBlockDtlsResponse_element pe92=new prefill.getChargeHolderBlockDtlsResponse_element();
        prefill.getChargeHolderDtls_element pe93=new  prefill.getChargeHolderDtls_element();
        prefill.getChargeHolderDtlsResponse_element pe94=new prefill.getChargeHolderDtlsResponse_element();
        prefill.getChargeHolderWithEmailDtls_element pe95=new prefill.getChargeHolderWithEmailDtls_element();
        prefill.getChargeHolderWithEmailDtlsResponse_element pe96=new prefill.getChargeHolderWithEmailDtlsResponse_element();
        prefill.getChargeIdDetails_element pe97=new prefill.getChargeIdDetails_element();
        prefill.getChargeIdDetailsResponse_element pe98=new prefill.getChargeIdDetailsResponse_element();
        prefill.getChargeHolderDtls_element pe99=new prefill.getChargeHolderDtls_element();
        prefill.getChrgHolderDetailsResponse_element pe100=new prefill.getChrgHolderDetailsResponse_element();
        prefill.getChrgHolderDetails_element pe101=new prefill.getChrgHolderDetails_element();
        prefill.getChrgHolderAddressWitCondResponse_element pe102=new prefill.getChrgHolderAddressWitCondResponse_element();
        prefill.getChrgHolderAddressWitCond_element pe103=new prefill.getChrgHolderAddressWitCond_element();
        prefill.getChargeIdDetailsResponse_element pe104=new prefill.getChargeIdDetailsResponse_element();
        prefill.getChargeIdDetails_element pe105=new prefill.getChargeIdDetails_element();
        
        prefill.getCompanyCinfor1A_element pe106=new prefill.getCompanyCinfor1A_element();
        prefill.getCompanyCinfor1AResponse_element pe107=new prefill.getCompanyCinfor1AResponse_element();
        prefill.getCompanyCinforForm1_element pe108=new prefill.getCompanyCinforForm1_element();
        prefill.getCompanyCinforForm1Response_element pe109=new prefill.getCompanyCinforForm1Response_element();
        prefill.getCompanyDetails_element pe110=new prefill.getCompanyDetails_element();
        prefill.getCompanyDetails_SC_element pe111=new prefill.getCompanyDetails_SC_element();
        prefill.getCompanyDetailsForCIN_element pe112=new prefill.getCompanyDetailsForCIN_element();
        prefill.getCompanyDetailsForCIN_SC_element pe113=new prefill.getCompanyDetailsForCIN_SC_element();
        prefill.getCompanyDetailsForCIN_SCResponse_element pe114=new prefill.getCompanyDetailsForCIN_SCResponse_element();
        prefill.getCompanyDetailsForCINResponse_element pe115=new prefill.getCompanyDetailsForCINResponse_element();
        prefill.getCompanyDetailsForm2P6ForCIN_element pe116=new prefill.getCompanyDetailsForm2P6ForCIN_element();
        prefill.getCompanyDetailsGovPublic_SC_element pe117=new prefill.getCompanyDetailsGovPublic_SC_element();
        prefill.getCompanySplitDetailsResponse_element pe118=new prefill.getCompanySplitDetailsResponse_element();
        prefill.getCompanySplitDetails_element pe119=new prefill.getCompanySplitDetails_element();
        prefill.getCompanyNameListResponse_element pe120=new prefill.getCompanyNameListResponse_element();
        prefill.getCompanyNameList_element pe121=new prefill.getCompanyNameList_element();
        prefill.getCompanyNameAsString_element pe122=new prefill.getCompanyNameAsString_element();
        prefill.getCompanyNameAsStringResponse_element pe123=new prefill.getCompanyNameAsStringResponse_element();
        prefill.getCompanyFullDetailsResponse_element pe124=new prefill.getCompanyFullDetailsResponse_element();
        prefill.getCompanyFullDetails_element pe125=new prefill.getCompanyFullDetails_element();
        prefill.getCompanyDirectorDetailsResponse_element pe126=new prefill.getCompanyDirectorDetailsResponse_element();
        prefill.getCompanyDirectorDetails_element pe127=new prefill.getCompanyDirectorDetails_element();
        prefill.getCompanyDetailsWithNameatIncorpResponse_element pe128=new prefill.getCompanyDetailsWithNameatIncorpResponse_element();
        prefill.getCompanyDetailsWithNameatIncorpResponse_element pe129=new prefill.getCompanyDetailsWithNameatIncorpResponse_element();
        prefill.getCompanyDetailsWithNameatIncorp_element pe130=new prefill.getCompanyDetailsWithNameatIncorp_element();
        prefill.getCompanyDetailsWithFrm18FlagResponse_element pe131=new  prefill.getCompanyDetailsWithFrm18FlagResponse_element();
        prefill.getCompanyDetailsWithFrm18Flag_SCResponse_element pe132=new prefill.getCompanyDetailsWithFrm18Flag_SCResponse_element();
        prefill.getCompanyDetailsWithFrm18Flag_element pe133=new prefill.getCompanyDetailsWithFrm18Flag_element();
        prefill.getCompanyDetailsWithFrm18Flag_SC_element pe134=new prefill.getCompanyDetailsWithFrm18Flag_SC_element();
        
        prefill.getCINLLPINDetails_element pe135 =new prefill.getCINLLPINDetails_element();
        prefill.getCINLLPINDetails_SC_element pe136 =new prefill.getCINLLPINDetails_SC_element();
        prefill.getCINLLPINDetails_SCResponse_element pe137 = new prefill.getCINLLPINDetails_SCResponse_element();
        prefill.getCINLLPINDetailsResponse_element pe138 = new prefill.getCINLLPINDetailsResponse_element();
     
        prefill.getCLSSSRNDetails_element pe139=new prefill.getCLSSSRNDetails_element();
        prefill.getCLSSSRNDetailsResponse_element pe140=new prefill.getCLSSSRNDetailsResponse_element();
        
        prefill.getCmpnyDtlsFor1ASharedCapital_element pe141=new prefill.getCmpnyDtlsFor1ASharedCapital_element();
        prefill.getCmpnyDtlsFor1ASharedCapitalResponse_element pe142=new prefill.getCmpnyDtlsFor1ASharedCapitalResponse_element();
        prefill.getCmpnyFinancialParameters_element pe143=new prefill.getCmpnyFinancialParameters_element();
        prefill.getCmpnyDtlsFor1ASharedCapitalResponse_element pe144=new prefill.getCmpnyDtlsFor1ASharedCapitalResponse_element();
        prefill.getCmpnyDtlsFor1ASharedCapital_element pe145=new prefill.getCmpnyDtlsFor1ASharedCapital_element();
        
        prefill.getDetailsAOC4NonXbrl_element pe146=new prefill.getDetailsAOC4NonXbrl_element();
        prefill.getDetailsAOC4NonXbrlResponse_element pe147=new prefill.getDetailsAOC4NonXbrlResponse_element();
        prefill.getDetailsOfBodyCorporateLLP_element pe148=new prefill.getDetailsOfBodyCorporateLLP_element();
        prefill.getDetailsOfBodyCorporateLLPResponse_element pe149=new prefill.getDetailsOfBodyCorporateLLPResponse_element();
        prefill.getDetailsOfPartnerDetails_element pe150=new prefill.getDetailsOfPartnerDetails_element();
        
        prefill.getDIN3DetailsResponse_element  pe151   =   new prefill.getDIN3DetailsResponse_element  ();
        prefill.getDIN3Details_element  pe152   =   new         prefill.getDIN3Details_element  ();
        prefill.getDINDetailsNew_element    pe153   =   new         prefill.getDINDetailsNew_element    ();
        prefill.getDINDetailsNewResponse_element    pe154   =   new         prefill.getDINDetailsNewResponse_element    ();
        
        prefill.getDIRDetailsForDIN11_element   pe156   =   new         prefill.getDIRDetailsForDIN11_element   ();
        prefill.getDIRDetailsForDIN11Response_element   pe157   =   new         prefill.getDIRDetailsForDIN11Response_element   ();
        prefill.getDIRDetailsForDIN_element pe158   =   new         prefill.getDIRDetailsForDIN_element ();
        prefill.getDIRDetailsForDINResponse_element pe159   =   new         prefill.getDIRDetailsForDINResponse_element ();
        prefill.getDirectorDetails_element  pe160   =   new         prefill.getDirectorDetails_element  ();
        prefill.getDirectorDetailsResponse_element  pe161   =   new         prefill.getDirectorDetailsResponse_element  ();
        prefill.getDirectorName_element pe162   =   new         prefill.getDirectorName_element ();
        prefill.getDirectorNameResponse_element pe163   =   new         prefill.getDirectorNameResponse_element ();
            
        prefill.getDisqualifiedDINforCompany_SC_element pe165   =   new         prefill.getDisqualifiedDINforCompany_SC_element ();
        prefill.getDisqualifiedDINforCompany_SCResponse_element pe166   =   new         prefill.getDisqualifiedDINforCompany_SCResponse_element ();
       
       
    }  
    static testMethod void myUnitTestTwo() {
    prefill.PrefillService preP = new prefill.PrefillService(); 
     try{       
        preP.get23CSRNDetails('true', 'true');
          }catch(Exception e){}
          
     try{       
        preP.getAmalgamatedCompanyDate('true');
          }catch(Exception e){}
          
    try{       
        //preP.getAuthPersonForeignDetail(null');
          }catch(Exception e){}
          
      try{       
        preP.getAuthPersonForeignDetailsForCompany('strPAN','strCompanyID');
          }catch(Exception e){}
      
      try{       
        preP.getAuthPersonIndianDetails('i');
        
          }catch(Exception e){}
          
     try{       
        preP.getBankDetails('strFormId');
          }catch(Exception e){}
          
     try{       
        preP.getBodyCorporatePartnersForm11( 'CIN',  'LLPIN',  'effectiveDate',  'typeOfBodyCorp',  'purpose');
        
          }catch(Exception e){}
     
     try{       
        preP.getCAODetails( 'strChar',  'strYear');
        
          }catch(Exception e){}
     try{       
        preP.getCINLLPINDetails( 'strCINLLPIN');
        
          }catch(Exception e){}
      
     try{       
        preP.getCLSSSRNDetails( 'strCinFcrn');
        
          }catch(Exception e){}    
      
     try{       
        preP.getCLSSSRNDetails( 'strCinFcrn');
          }catch(Exception e){} 
      
     try{       
        preP.getCINLLPINDetails_SC('strCINLLPIN');
          }catch(Exception e){}  
     
          try{       
        preP.getChangedName( 'strCIN');
          }catch(Exception e){}  
      
        try{       
        preP.getChangedNameWithNameatIncorp( 'strCIN');
          }catch(Exception e){}  
        
        try{       
        preP.getChargeDetails( 'strChargeID');
        
          }catch(Exception e){}  
        
        try{       
        preP.getChargeHolderBlockDtls( 'strChargeID');
        
          }catch(Exception e){}  
        
        try{       
        preP.getChargeHolderDtls(' strChargeID');
        
          }catch(Exception e){}  
        
         try{       
        preP.getChargeHolderWithEmailDtls( 'strChargeID');
        
          }catch(Exception e){}  
        
         try{       
        preP.getChargeIdDetails( 'strChargeID');
        
          }catch(Exception e){}  
        
        try{       
        preP.getChrgHolderAddressWitCond('strCompanyID');
        
          }catch(Exception e){}  
        
        try{       
        preP.getChrgHolderDetails('strCompanyID');
        
          }catch(Exception e){}  
        
        try{       
        preP.getChrgHolderDetails('strCompanyID');
        
          }catch(Exception e){}  
        
         try{       
        preP.getChrgHolderDetails('strCompanyID');
        
          }catch(Exception e){} 
          
        try{       
        preP.getCmpnyDtlsFor1ASharedCapital('strCompanyID');
        
          }catch(Exception e){}  
       try{       
        preP.getCmpnyFinancialParameters('strCmpnyId');
        
        
          }catch(Exception e){} 
       
        try{       
        preP.getCompanyCinfor1A('strCompanyID');
        
        
          }catch(Exception e){}  
         try{       
        preP.getCompanyCinforForm1( 'strCompanyID1', ' strCompanyID2', ' strCompanyID3');
        
        
          }catch(Exception e){}  
       
        try{       
        preP.getCompanyDetails( 'strCompanyID');
              
          }catch(Exception e){}  
        
         try{       
        preP.getCompanyDetailsForCIN('strCIN');
                
          }catch(Exception e){}  
        
         try{       
        preP.getCompanyDetailsForCIN_SC('strCIN');
        
        
          }catch(Exception e){}  
          
          try{       
        preP.getCompanyDetailsForm2P6ForCIN('strCIN');
        
        
          }catch(Exception e){}  
        
         try{       
        preP.getCompanyDetailsGovPublic('strCompanyID');
        
        
          }catch(Exception e){}  
          
          try{       
        preP.getCompanyDetailsGovPublic_SC('strCompanyID');
        
        
          }catch(Exception e){}  
          
          try{       
        preP.getCompanyDetailsNew('strCompanyID');
        
        
          }catch(Exception e){}  
          
           try{       
        preP.getCompanyDetailsNew_SC('strCompanyID');
        
        
          }catch(Exception e){}  
           try{       
        preP.getCompanyDetailsWithFrm18Flag('strCompanyID');
        
        
          }catch(Exception e){}  
           try{       
        preP.getCompanyDetailsWithFrm18Flag_SC('strCompanyID');
        
        
          }catch(Exception e){}  
           try{       
         preP.getCompanyDetailsWithNameatIncorp('strCompanyID');
        
          }catch(Exception e){}   
        try{       
         preP.getCompanyDetails_SC('strCompanyID');
           
          }catch(Exception e){} 
       
        try{       
         preP.getCompanyDirectorDetails('strCompanyID');
           
          }catch(Exception e){} 
        
         try{       
         preP.getCompanyFullDetails('strCompanyID');
           
          }catch(Exception e){} 
         
          try{       
          preP.getCompanyNameAsString(null);
        
          }catch(Exception e){} 
          
          try{       
         preP.getCompanyNameList(null);
           
          }catch(Exception e){} 
          
          try{       
         preP.getCompanySplitDetails('strCompanyID');
           
          }catch(Exception e){} 
         
          try{       
         preP.getDIN3Details('strDIN');       
          }catch(Exception e){}    
          
        try{
            preP.getDINDetailsNew('strDIN','strCIN', 'dateAGM','dueDateAGM');
                    
        }catch(Exception e){} 
          
      try{
        preP.getDIRDetailsForDIN('strDIN','strCIN');            
        }catch(Exception e){} 
      
      try{
        preP.getDIRDetailsForDIN11('strDIN','strCIN');          
        }catch(Exception e){} 
      
      try{
        preP.getDetailsAOC4NonXbrl('strSRN');           
        }catch(Exception e){} 
      
      try{
        preP.getDetailsOfBodyCorporateLLP('strTypeBodyCorporate', 'strLLPIN',  'strCINLLPIN');          
        }catch(Exception e){} 
      
      try{
        preP.getDetailsOfPartnerDetails('strDPINs', 'strLLPIN');            
        }catch(Exception e){} 
      
      try{
        preP.getDirectorDetails('strPANNo');            
        }catch(Exception e){}  
      
       try{
        preP.getDirectorName('strPAN');         
        }catch(Exception e){} 
        
        try{
        preP.getDisqualifiedDINforCompany_SC('strCIN');         
        }catch(Exception e){} 
        
         try{
        preP.getForeignCompanyDetails('strCompanyID');          
        }catch(Exception e){} 
        
       try{
       preP.getForm1ADetails('strDIN');     
        }catch(Exception e){} 
        
        try{
        preP.getForm1AddressDetails('strCompanyID');        
        }catch(Exception e){} 
        
         try{
       preP.getForm1LLPSRNDetails('strSRN');
                
        }catch(Exception e){}  
        
       try{
        preP.getForm23CCRA2SRNDetails('strSRN');            
        }catch(Exception e){}  
        
       try{
        preP.getForm2P1Details('strDIN');           
        }catch(Exception e){}  
        
        try{
        preP.getForm32DINDetails('strDIN');         
        }catch(Exception e){}  
        
        try{
        preP.getForm68CmpnyDetailsForSrn('strSRN', 'strFormId');            
        }catch(Exception e){}  
        
        try{
        preP.getFrgnLLPApplicantDetails('strFLLPIN', 'strAppDPINPAN');          
        }catch(Exception e){}  
        
        try{
        preP.getLLPForm11Details('strCINLLPIN',  'strAnnYear',  'FinStartDate');
                    
        }catch(Exception e){}  
        
        try{
        preP.getLLPForm11IndividualsDetails('strIndividualsID', 'strLLPin',  'eventDate');
                    
        }catch(Exception e){}  
        
        try{
        preP.getLLPForm1ApplicantDetails('strApplicantDPIN');       
        }catch(Exception e){}  
        
        try{
        preP.getLLPForm25SRNDetails('strSRN');                  
        }catch(Exception e){}  
    }  
    
}