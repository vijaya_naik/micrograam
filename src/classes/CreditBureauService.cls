public class CreditBureauService {
 public static Application_Credit_Bureau__c getCreditBureau(String Id) {
        Application_Credit_Bureau__c applicationCreditBureau = [
            SELECT Id,Loan_Application_Id__c,
            Bureau_Score__c,Source__c,Credit_Enquiries_Last_6_Months__c
            FROM Application_Credit_Bureau__c 
            WHERE Id = :Id
        ];
        return applicationCreditBureau;

    }
     public static Application_Credit_Bureau__c getCreditBureau(String loanApplicationId,String source) {
        Application_Credit_Bureau__c creditBureau = [
            SELECT Id,Loan_Application_Id__c,
            Bureau_Score__c,Source__c,Credit_Enquiries_Last_6_Months__c
            FROM Application_Credit_Bureau__c 
            WHERE Loan_Application_Id__c = :loanApplicationId and Source__c=:source
        ];
        return creditBureau;

    }
    
    public static Map<String, Map<String,String>> getCreditBureaus(String loanApplicationId) {
        Map<String, Map<String,String>> creditBureausMap = new Map<String, Map<String,String>>();
        // Create query string to get Social Media records
 
        
        List<Application_Credit_Bureau__c> creditBureaus = [
            SELECT Id,Loan_Application_Id__c,
            Bureau_Score__c,Source__c,Credit_Enquiries_Last_6_Months__c
            FROM Application_Credit_Bureau__c 
            WHERE Loan_Application_Id__c = :loanApplicationId 
        ];        
        System.debug('getLoanApplications ResultSize=' + creditBureaus.size());
        
        // Create Result Map
        for ( Application_Credit_Bureau__c creditBureau : creditBureaus ) {
            Map<String,String> creditBureauMap = creditBureauService.getCreditBureauDenormalized(creditBureau);
            // Get additional data for Borrower
            creditBureausMap.put(creditBureau.Source__c, creditBureauMap);
        }
        
        // Create dummy record for CIBIL if unavailable
        if ( creditBureausMap.get('CIBIL') == null ) {
            Map<String,String> creditBureauMap = new Map<String,String>();
            creditBureauMap.put('source', 'CIBIL');
            creditBureauMap.put('loanApplicationId', loanApplicationId);
            creditBureauMap.put('bureauScore', '-1');
            creditBureauMap.put('creditEnquiriesLast6Months', '-1');
            creditBureausMap.put(creditBureauMap.get('source'), creditBureauMap);
        }
        
        return creditBureausMap;
    }
    public static Map<String,String> createCreditBureau(Map<String, Object> params) {
        //Create Application_KYC instance & set fields
        Application_Credit_Bureau__c applicationCreditBureau = createCreditBureauInstance(params, null);
       
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
      
            insert applicationCreditBureau;
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
        
        return getCreditBureauDenormalized(applicationCreditBureau);
    }
            public static Application_Credit_Bureau__c createCreditBureauInstance(Map<String, Object> params, Application_Credit_Bureau__c applicationCreditBureau) {
        if (applicationCreditBureau == null)    applicationCreditBureau = new Application_Credit_Bureau__c(); //For new record creation
        boolean hasValue = false;
            if (params.containsKey('bureauScore')){
                  	applicationCreditBureau.Bureau_Score__c=String.valueOf(params.get('bureauScore'));
                hasValue=true;
            }
        if (params.containsKey('source'))       	applicationCreditBureau.Source__c=String.valueOf(params.get('source'));
        if (params.containsKey('loanApplicationId'))        	applicationCreditBureau.Loan_Application_Id__c=String.valueOf(params.get('loanApplicationId'));
        if (params.containsKey('creditEnquiriesLast6Months'))        	applicationCreditBureau.Credit_Enquiries_Last_6_Months__c=Double.valueOf(params.get('creditEnquiriesLast6Months'));
        
            if(hasValue)
				return applicationCreditBureau;
            else return null;
    }
            public static Map<String,String> getCreditBureauDenormalized(Application_Credit_Bureau__c applicationCreditBureau) {
        Map<String,String> applicationCreditBureauMap = new Map<String,String>();
        
        if (applicationCreditBureau.Source__c != null) applicationCreditBureauMap.put('source', applicationCreditBureau.Source__c);
        if (applicationCreditBureau.Bureau_Score__c != null) applicationCreditBureauMap.put('bureauScore', applicationCreditBureau.Bureau_Score__c);
        if (applicationCreditBureau.Loan_Application_Id__c != null) applicationCreditBureauMap.put('loanApplicationId', String.valueOf(applicationCreditBureau.Loan_Application_Id__c));
        if (applicationCreditBureau.Credit_Enquiries_Last_6_Months__c != null) applicationCreditBureauMap.put('creditEnquiriesLast6Months', String.valueOf(applicationCreditBureau.Credit_Enquiries_Last_6_Months__c));
        applicationCreditBureauMap.put('id', String.valueOf(applicationCreditBureau.Id));

        return applicationCreditBureauMap;
    }
                public static Map<String,String> updateCreditBureau(String applicationCreditBureauId, Map<String, Object> params) {
                Application_Credit_Bureau__c applicationCreditBureau =null;
       	if(applicationCreditBureauId!=null)  applicationCreditBureau=getCreditBureau(applicationCreditBureauId);
       	        applicationCreditBureau = createCreditBureauInstance(params, applicationCreditBureau);
       	
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
              update applicationCreditBureau;
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
        
        return getCreditBureauDenormalized(applicationCreditBureau);
    }
         public static void deleteCreditBureau(String creditBureauId) {
        //Get applicant record
        Application_Credit_Bureau__c dto=CreditBureauService.getCreditBureau(creditBureauId);


        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
             delete dto; //Delete applicant record
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
    
    public static void deleteCreditBureaus(String loanApplicationId) {
        //Get Credit Bureau records
        List<Application_Credit_Bureau__c> creditBureaus = [
            SELECT Id FROM Application_Credit_Bureau__c 
            WHERE Loan_Application_Id__c = :loanApplicationId 
        ];

        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
             delete creditBureaus;
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
}