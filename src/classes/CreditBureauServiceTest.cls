@isTest(seealldata = false)
public class CreditBureauServiceTest {

  public static testmethod void crdtMethods(){
  
      //inserting test data
       Address__c mgp=new  Address__c();
          mgp.Address_Line_1__c='BDA Complex';
          mgp.Address_Line_2__c='permanentAddressLine2';
          mgp.City__c='permanentAddressCity';
          mgp.Pincode__c=123;
          mgp.State__c='Karnataka';
          mgp.Country__c='India'; 
          mgp.Residing_Since__c=Date.today();
          mgp.House_Ownership__c='Owned';
          insert mgp;
          
  
      Account acc=new Account();
      acc.Name='Microgram';
      acc.Account_Type__c='Borrower'; 
      insert acc;
      
      Income_Detail__c income=new Income_Detail__c();
      income.Designation__c='Manager';
      income.Office_Address__c=mgp.id;
      income.Annual_Income__c=200000;
      insert income;
      
      Loan_Application__c loan=new Loan_Application__c();
      loan.Account_Id__c=acc.id;
      loan.Reference_1_Address__c=mgp.id;
      loan.Reference_2_Address__c=mgp.id;
      loan.Income_Detail_Current_Job__c=income.id;
      loan.Income_Detail_Previous_Job__c=income.id;
      insert loan;  
      
      Application_Credit_Bureau__c app=new Application_Credit_Bureau__c();
      app.Loan_Application_Id__c=loan.id; 
      app.Source__c='Override';
      insert app;
      
      Application_Credit_Bureau__c appbure=new Application_Credit_Bureau__c();
      appbure.Loan_Application_Id__c=loan.id; 
      appbure.Bureau_Score__c='Override';
      insert appbure;
      
      
        Map<String,  object> addressMap=new Map<String,  object>();
           Application_Credit_Bureau__c mgpAddress=new  Application_Credit_Bureau__c();
          addressMap.put('source','Cibil');
          addressMap.put('bureauScore','Experian');
          //addressMap.put('creditEnquiriesLast6Months',mgpAddress);
          
      //calling CreditBureauService methods
      CreditBureauService crdtsrvc=new CreditBureauService();
      CreditBureauService.getCreditBureau(app.id);
      CreditBureauService.getCreditBureau(loan.id,'Override');
      CreditBureauService.getCreditBureaus(loan.id);
       CreditBureauService.createCreditBureauInstance(addressMap,app);
       CreditBureauService.updateCreditBureau(app.id,addressMap);
        CreditBureauService.getCreditBureauDenormalized(app);
        CreditBureauService.deleteCreditBureau(app.id);
        CreditBureauService.deleteCreditBureaus(loan.id);
       CreditBureauService.createCreditBureau(addressMap);            
        
  }

}