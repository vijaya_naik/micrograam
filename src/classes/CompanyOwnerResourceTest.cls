@isTest(seeAllData=false)
public class CompanyOwnerResourceTest  {
static Address__c addres;
    static Account acc;
    static Income_Detail__c incomeDetail;
    static Loan_Application__c loanApp ;
    static Company_Owner__c  cmpowner ;
    
    private static final String ORG_URL = URL.getSalesforceBaseUrl().toExternalForm() ;
      
     static void init() {
    // setup test data 
    //create dummy Address Record
       addres =new  Address__c();
       addres.Address_Line_1__c='Street 1';
       addres.Address_Line_2__c='Street 2';
       addres.City__c='Bangalore ';
       addres.Pincode__c= 12345;
       addres.State__c='KA';
       addres.Country__c='India'; 
       addres.Residing_Since__c=System.today();
       addres.House_Ownership__c='Owned';
       insert addres;
          
       //create dummy account record
       acc=new Account();
       acc.Name='Microgram';
       acc.Account_Type__c='Borrower'; 
       insert acc;
       
       //create dummy Income Detail Record
       incomeDetail = new Income_Detail__c();
       incomeDetail.Designation__c='Manager';
       incomeDetail.Office_Address__c=addres.id;
       incomeDetail.Annual_Income__c=30000;
       insert incomeDetail;
       
       //create dummmy Loan Application Record.
       loanApp = new Loan_Application__c();
       loanApp.Account_Id__c=acc.id;
       loanApp.Reference_1_Address__c=addres.id;
       loanApp.Reference_2_Address__c=addres.id;
       loanApp.Income_Detail_Current_Job__c=incomeDetail.id;
       loanApp.Income_Detail_Previous_Job__c=incomeDetail.id;
       insert loanApp; 
       
       //create dummy company owner record
       cmpowner= new Company_Owner__c();
       cmpowner.Company_Owner_PAN__c='ABCDE1234Y';
       cmpowner.Loan_Application_Id__c=loanApp.id;
       cmpowner.Owner_Address__c=addres.id;
       cmpowner.Ownership_Percentage__c='3';
       cmpowner.Name='Microgram';
       insert cmpowner;
}
static testMethod void testDoPost() {
      Map<String, Company_Owner__c> paramMap = new Map<String, Company_Owner__c>();
      Company_Owner__c companyOwner = new Company_Owner__c();
      paramMap.put('companyOwnerPAN',companyOwner);
      paramMap.put('companyOwnerName',companyOwner);
      paramMap.put('ownershipPercentage',companyOwner);
      List<Object> paramlist = paramMap.Values();
      String JsonMsg=JSON.serialize(paramlist);
      RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/v1/companyOwner';  //Request URL
      req.httpMethod = 'POST';
      req.requestBody = Blob.valueof(JsonMsg);
      RestContext.request = req;
      RestContext.response= res;
    
      CompanyOwnerResource.cCompanyOwner();

}

static testMethod void testDoGet() {
    init();
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();

    req.requestURI = ORG_URL+'/services/apexrest/v.37.0/v1/socialMedia';  
    req.httpMethod = 'GET';
    req.addParameter('id',cmpowner.Id);  
    req.addParameter('loanApplicationId', loanApp.Id);      
    req.addParameter('source', 'Vendor');        
    RestContext.request = req;
    RestContext.response = res;
    Map<String,String> results=new Map<String,String>();
    results = CompanyOwnerResource.getCompanyOwner();
  }
  

}