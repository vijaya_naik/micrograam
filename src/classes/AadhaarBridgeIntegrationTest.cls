@isTest (SeeAllData=false)
private class AadhaarBridgeIntegrationTest{
    static testMethod void testOne(){
        IntegrationMessageProvider__c messageProvider = new IntegrationMessageProvider__c(
            Name = Label.AadhaarOtp,
            Request_Provider__c = 'RequestProviderAadhaarOtp',
            Response_Handler__c = 'ResponseHandlerAadhaarOtp',
            No_of_Retries__c = 5
        );
        insert messageProvider;
        
         Account acc = new Account(
            Name = 'Test'
        );
        insert acc;
        
        Test.startTest();
        
         Test.setMock(HttpCalloutMock.class, new IntegrationAaadhaarBridgeTestMock());
        
        Applicant__c applicant = new Applicant__c(
            Account_Id__c = acc.Id,
            Aadhar_Number__c = 1234567890,
            KYC_Status__c = 'Submitted',
            E_Mail__c = 'test.test@test.com.test'
        );  
        insert applicant; 
        Test.stopTest();     
    }    
    
    
    static testMethod void testTwo(){
        IntegrationMessageProvider__c messageProvider = new IntegrationMessageProvider__c(
            Name = Label.AadhaarOtp,
            Request_Provider__c = 'RequestProviderAadhaarOtp',
            Response_Handler__c = 'ResponseHandlerAadhaarOtp',
            No_of_Retries__c = 5
        );
        insert messageProvider;
        
         Account acc = new Account(
            Name = 'Test'
        );
        insert acc;
        
        Applicant__c applicant = new Applicant__c(
            Account_Id__c = acc.Id,
            Aadhar_Number__c = 1234567890,
            KYC_Status__c = 'Not Submitted',
            E_Mail__c = 'test.test@test.com.test'
        );  
        insert applicant;
        
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new IntegrationAaadhaarBridgeTestMock());
        applicant.KYC_Status__c = 'Submitted';
        update applicant;
         
        Test.stopTest();     
    } 
    
    
    static testMethod void testThree(){
        IntegrationMessageProvider__c messageProvider = new IntegrationMessageProvider__c(
            Name = Label.FetchAadhaarDetails,
            Request_Provider__c = 'RequestProviderAadhaarDetails',
            Response_Handler__c = 'ResponseHandlerAadhaarDetails',
            No_of_Retries__c = 5
        );
        insert messageProvider;
        
         Account acc = new Account(
            Name = 'Test'
        );
        insert acc;
        
        Applicant__c applicant = new Applicant__c(
            Account_Id__c = acc.Id,
            Aadhar_Number__c = 1234567890,
            KYC_Status__c = 'Not Submitted',
            E_Mail__c = 'test.test@test.com.test'
        );  
        insert applicant;
        
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new IntegrationAaadhaarBridgeTestMock());
        applicant.Aadhaar_Bridge_Otp__c = '123456';
        update applicant;
         
        Test.stopTest();     
    } 
}