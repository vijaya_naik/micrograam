@isTest(seeAllData=false)
global class ApexTestTest  {

 private static final String ORG_URL = URL.getSalesforceBaseUrl().toExternalForm() ;
 
static testMethod void testDoGet() {
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();

    req.requestURI = ORG_URL+'/services/apexrest/Check/*';  
    req.httpMethod = 'GET';
    req.addParameter('name','U74120TG2004PTC043472');      
    req.addParameter('source', 'Vendor');        
    RestContext.request = req;
    RestContext.response = res;
    //String result = ApexTest.getTestRest();
    test.startTest();
    Test.setMock(HttpcalloutMock.class, new mock_test());
    try{
    ApexTest.getTestRest();
    }
    catch(Exception e){}
    test.stopTest();
}

 global class mock_test implements HttpCalloutMock {
        global httpResponse respond(HTTPrequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            return res;
        }
  }
}