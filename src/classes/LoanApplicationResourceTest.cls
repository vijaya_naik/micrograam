@isTest(seeAllData=false)
public class LoanApplicationResourceTest  {
    static Address__c addres;
    static Account acc;
    static Income_Detail__c incomeDetail;
    static Loan_Application__c loanApp ;
    
    private static final String ORG_URL = URL.getSalesforceBaseUrl().toExternalForm() ;
      
     static void init() {
    // setup test data 
    //create dummy Address Record
       addres =new  Address__c();
       addres.Address_Line_1__c='Street 1';
       addres.Address_Line_2__c='Street 2';
       addres.City__c='Bangalore ';
       addres.Pincode__c= 12345;
       addres.State__c='KA';
       addres.Country__c='India'; 
       addres.Residing_Since__c=System.today();
       addres.House_Ownership__c='Owned';
       insert addres;
          
       //create dummy account record
       acc=new Account();
       acc.Name='Microgram';
       acc.Account_Type__c='Borrower'; 
       insert acc;
       
       //create dummy Income Detail Record
       incomeDetail = new Income_Detail__c();
       incomeDetail.Designation__c='Manager';
       incomeDetail.Office_Address__c=addres.id;
       incomeDetail.Annual_Income__c=30000;
       insert incomeDetail;
       
       //create dummmy Loan Application Record.
       loanApp = new Loan_Application__c();
       loanApp.Account_Id__c=acc.id;
       loanApp.Reference_1_Address__c=addres.id;
       loanApp.Reference_2_Address__c=addres.id;
       loanApp.Income_Detail_Current_Job__c=incomeDetail.id;
       loanApp.Income_Detail_Previous_Job__c=incomeDetail.id;
       insert loanApp; 
       
    }
    static testMethod void testDoPost() {
      init();
      Map<String, Object> loanMap=new Map<String, Object>();
      loanMap.put('firstName', 'Microgram');
      loanMap.put('Name', 'xyz');
      loanMap.put('accountType', 'Borrower');
      loanMap.put('applicantType', 'Individual');
      loanMap.put('currentAddressLine1', addres.id);
      loanMap.put('currentAddressLine2', addres.id);
      loanMap.put('currentAddressCity', 'banglore');
      loanMap.put('currentAddressPincode', 524137);
      loanMap.put('currentAddressState','karnataka');
      loanMap.put('currentAddressCountry','India');
      loanMap.put('borrowerId',acc.id);
      loanMap.put('kycStatus','Not Submitted');
      loanMap.put('creditCheckStatus','Not Submitted');
      loanMap.put('applicantType','Individual');
      loanMap.put('creditScore',6);
      loanMap.put('loanAmount',2000);
      loanMap.put('loanMaximumEMI',2);
      loanMap.put('loanPurposeCategory','health');
      loanMap.put('loanPurposeDescription','health issue');
      loanMap.put('loanStatus','In Progress');
      loanMap.put('loanTenor',2);
      loanMap.put('decisionDate',Date.today());
      loanMap.put('decisionTakenBy','Manager');
      loanMap.put('creditOfficerActionComments','health issue');
      loanMap.put('reference2MobileNumber',94);
      loanMap.put('reference2LastName','xyz');
      String JsonMsg=JSON.serialize(loanMap);
      RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/v1/loanapplication';  //Request URL
      req.httpMethod = 'POST';
      req.requestBody = Blob.valueof(JsonMsg);
      RestContext.request = req;
      RestContext.response= res;
    
      LoanApplicationResource.cLoanApplication();

  }
  
  static testMethod void testDoDelete() {
    init();
    RestRequest req = new RestRequest(); 

    req.requestURI = ORG_URL+'/services/apexrest/v1/loanapplication';  
    req.httpMethod = 'DELETE';
    req.addParameter('id', loanApp.Id);    
    RestContext.request = req;
    try{
        LoanApplicationResource.deleteLoanApplication();  
    }
    catch(Exception e){
    }
  }
  static testMethod void testDoPatch() {
      init();
      Map<String, Object> loanMap=new Map<String, Object>();
      loanMap.put('firstName', 'Microgram');
      loanMap.put('Name', 'xyz');
      loanMap.put('accountType', 'Borrower');
      loanMap.put('applicantType', 'Individual');
      loanMap.put('currentAddressLine1', addres.id);
      loanMap.put('currentAddressLine2', addres.id);
      loanMap.put('currentAddressCity', 'banglore');
      loanMap.put('currentAddressPincode', 524137);
      loanMap.put('currentAddressState','karnataka');
      loanMap.put('currentAddressCountry','India');
      loanMap.put('borrowerId',acc.id);
      loanMap.put('kycStatus','Not Submitted');
      loanMap.put('creditCheckStatus','Not Submitted');
      loanMap.put('applicantType','Individual');
      loanMap.put('creditScore',6);
      loanMap.put('loanAmount',2000);
      loanMap.put('loanMaximumEMI',2);
      loanMap.put('loanPurposeCategory','health');
      loanMap.put('loanPurposeDescription','health issue');
      loanMap.put('loanStatus','In Progress');
      loanMap.put('loanTenor',2);
      loanMap.put('decisionDate',Date.today());
      loanMap.put('decisionTakenBy','Manager');
      loanMap.put('creditOfficerActionComments','health issue');
      loanMap.put('reference2MobileNumber',94);
      loanMap.put('reference2LastName','xyz');
      String JsonMsg=JSON.serialize(loanMap);
      RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/v1/loanapplication';  //Request URL
      req.httpMethod = 'PATCH';
      req.addParameter('id', loanApp.Id);      
      req.requestBody = Blob.valueof(JsonMsg);
      RestContext.request = req;
      RestContext.response= res;
    
      LoanApplicationResource.updateLoanApplication();

  }
  
    static testMethod void testDoGet() {
        init();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = ORG_URL+'/services/apexrest/v1/loanapplication';  
        req.httpMethod = 'GET';
        req.addParameter('id', loanApp.Id);      
        req.addParameter('borrowerId', loanApp.Id);        
        RestContext.request = req;
        RestContext.response = res;
        try {
            LoanApplicationResource.getLoanApplication();
        }
        catch(Exception e){}
   }

}