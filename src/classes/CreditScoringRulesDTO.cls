global class CreditScoringRulesDTO {
    public String creditScoringRuleSet { get; set; }
    public Map<String, List<Map<String,String>>> creditCategoryCriteria { get; set; }
}