public class AccessTokenHandlerFriendlyScore extends AccessTokenHanlder{
    public override String generateAccessToken(){
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndPoint(Label.FriendlyScoreTokenEndPoint);
        req.setBody(Label.FriendlyScoreTokenBody);
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        
        HttpResponse res = (new Http()).send(req);
        
        System.debug('Response---'+res.getBody());
        
        Map<String, Object> resParsed = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
        return (String) resParsed.get('access_token');
    }
}