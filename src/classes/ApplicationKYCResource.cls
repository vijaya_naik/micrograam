@RestResource(urlMapping='/v1/applicationKYC')
global with sharing class ApplicationKYCResource {
    @HttpPost
    global static Map<String,String> cApplicationKYC() {
        RestRequest req = RestContext.request;
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received ApplicationKYC Registration request:: ' + params);
        // Invoke service
        return ApplicationKYCService.createApplicationKYC(params);
    }
    
    @HttpGet
    global static Map<String,String> getApplicationKYC() {
        // Get id/accountId from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
		String loanApplicationId = req.params.get('loanApplicationId');
        String lenderId = req.params.get('lenderId');
        String source=req.params.get('source');
        if ( loanApplicationId != null && source!=null) {
            // Invoke service to get by loanApplicationId 
            System.debug('Received  Get request for loanApplicationId=' + loanApplicationId);
            id= ApplicationKYCService.getApplicationKYC(loanApplicationId, source).Id;
        } else if ( lenderId != null && source!=null) {
            System.debug('Received  Get request for lenderId=' + lenderId);
            id= ApplicationKYCService.getApplicationKYCForLender(lenderId, source).Id;
        }
        System.debug('Received ApplicationKYC Get request for id=' + id);
        return ApplicationKYCService.getApplicationKYCDenormalized(ApplicationKYCService.getApplicationKYC(id));
    }
    
    @HttpDelete
    global static void deleteApplicationKYC() {
        // Get id from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
        System.debug('Received ApplicationKYC Delete request for id=' + id);
        // Invoke service
        ApplicationKYCService.deleteApplicationKYC(id);
    }
    
    @HttpPatch
    global static Map<String,String> updateApplicationKYC() {
        RestRequest req = RestContext.request;
        // Get id from request parameter
        String id = req.params.get('id');
        String loanApplicationId = req.params.get('loanApplicationId');
        String lenderId = req.params.get('lenderId');
        String source=req.params.get('source');
        if ( loanApplicationId != null && source!=null) {
            // Invoke service to get by Borrower Account id
            System.debug('Received  Update request for loanApplicationId=' + loanApplicationId);
            id= ApplicationKYCService.getApplicationKYC(loanApplicationId, source).Id;
        } else if ( lenderId != null && source!=null) {
            System.debug('Received  Update request for lenderId=' + lenderId);
            id= ApplicationKYCService.getApplicationKYCForLender(lenderId, source).Id;
        }
    
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received ApplicationKYC Update request for id=' + id + '::' + params);
        // Invoke service
        return ApplicationKYCService.updateApplicationKYC(id, params);
    }

}