public class RequestProviderPsychometric implements IRequestProvider {  
      public Object execute(Integration_Message__c message){
        AccessTokenHandlerPsychometric accessTokenHandler = new AccessTokenHandlerPsychometric();
        
        System.debug('Token before Callout--->'+(String) Cache.Org.get(Label.Psychometric));
    
        HttpResponse res = RequestProviderPsychometric.doCallout(message);
        
        String responseBody = res.getBody();
        Map<String, Object> resParsed = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
        
        System.debug('Status Code----'+res.getStatusCode());
        System.debug('Error Message----'+(String) resParsed.get('error_description'));
            
        if(res.getStatus() <> 'OK' && res.getStatusCode() == 401){
            accessTokenHandler.storeAccessToken('', Label.Psychometric);
            System.debug('Token after Callout1--->'+(String) Cache.Org.get(Label.Psychometric));
            res = RequestProviderPsychometric.doCallout(message);
            System.debug('Token after Callout2--->'+(String) Cache.Org.get(Label.Psychometric));
        }
        
        return res;
    }
    
    
    public static HttpResponse doCallout(Integration_Message__c message){
        AccessTokenHandlerPsychometric accessTokenHandler = new AccessTokenHandlerPsychometric();
        
        Loan_Application__c application = [SELECT Id, Partner_Id__c FROM Loan_Application__c WHERE Id = :message.Input_String__c];
        
        String token = '';
        if(accessTokenHandler.getAccessToken(Label.Psychometric) == null || accessTokenHandler.getAccessToken(Label.Psychometric) == ''){
            token = accessTokenHandler.generateAccessToken();
            accessTokenHandler.storeAccessToken(token, Label.Psychometric);
        }
        else{
            token = accessTokenHandler.getAccessToken(Label.Psychometric);
        }
        
        
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndPoint('http://micrograamapi.azurewebsites.net/api/V1/PDF/FetchUserResultRaw');
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setHeader('Authorization','Bearer '+token);
        req.setBody('sObject={"UniqueID":"'+application.partner_Id__c+'"}');
        
        return (new Http()).send(req);
    }

}