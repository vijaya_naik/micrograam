@RestResource(urlMapping='/v1/creditCategoriesMaster')
global with sharing class CreditCategoriesMasterResource {
 @HttpPost
    global static Map<String,String> cCreditCategoriesMaster() {
        RestRequest req = RestContext.request;
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received CreditCategoriesMaster Registration request:: ' + params);
        // Invoke service
        return CreditCategoriesMasterService.createCreditCategoriesMaster(params);
    }
    
    @HttpGet
    global static Map<String,String> getCreditCategoriesMaster() {
        // Get id/accountId from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');

        if(id!=null){
            
           
            System.debug('Received CreditCategoriesMaster Get request for id=' + id);
            return CreditCategoriesMasterService.getCreditCategoriesMasterDenormalized(CreditCategoriesMasterService.getCreditCategoriesMaster(id));
    }
        return null;
    }
    
    @HttpDelete
    global static void deleteCreditCategoriesMaster() {
        // Get id from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
        System.debug('Received CreditCategoriesMaster Delete request for id=' + id);
        // Invoke service
        CreditCategoriesMasterService.deleteCreditCategoriesMaster(id);
    }
    
    @HttpPatch
    global static Map<String,String> updateCreditCategoriesMaster() {
        RestRequest req = RestContext.request;
        // Get id from request parameter
        String id = req.params.get('id');

        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received CreditCategoriesMaster Update request for id=' + id + '::' + params);
        // Invoke service
        return CreditCategoriesMasterService.updateCreditCategoriesMaster(id, params);
    }

}