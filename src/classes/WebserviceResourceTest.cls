@isTest(seeAllData=false)
global class WebserviceResourceTest  {
    static Address__c addres;
    static Account acc;
    static Income_Detail__c incomeDetail;
    static Loan_Application__c loanApp ;
    static Loan_Application__c loanApp1 ;
    static Application_Social_Media__c  appSocialMedia;
    static Applicant__c aplcnt ;
    
    private static final String ORG_URL = URL.getSalesforceBaseUrl().toExternalForm() ;
      
     static void init() {
    // setup test data 
    //create dummy Address Record
       addres =new  Address__c();
       addres.Address_Line_1__c='Street 1';
       addres.Address_Line_2__c='Street 2';
       addres.City__c='Bangalore ';
       addres.Pincode__c= 12345;
       addres.State__c='KA';
       addres.Country__c='India'; 
       addres.Residing_Since__c=System.today();
       addres.House_Ownership__c='Owned';
       insert addres;
          
       //create dummy account record
       acc=new Account();
       acc.Name='Microgram';
       acc.Account_Type__c='Borrower'; 
       insert acc;
       
       aplcnt = TestUtility.objmgp2pApplicant();
       aplcnt.Account_Id__c = acc.Id;
       insert aplcnt;
        
       //create dummy Income Detail Record
       incomeDetail = new Income_Detail__c();
       incomeDetail.Designation__c='Manager';
       incomeDetail.Office_Address__c=addres.id;
       incomeDetail.Annual_Income__c=30000;
       insert incomeDetail;
       
       //create dummmy Loan Application Record.
       loanApp = new Loan_Application__c();
       loanApp.Account_Id__c=acc.id;
       loanApp.Loan_Purpose_Category__c = 'Education';
       loanApp.Reference_1_Address__c=addres.id;
       loanApp.Reference_2_Address__c=addres.id;
       loanApp.Income_Detail_Current_Job__c=incomeDetail.id;
       loanApp.Income_Detail_Previous_Job__c=incomeDetail.id;
       insert loanApp; 
       
       //create dummmy Loan Application Record.
       loanApp1 = new Loan_Application__c();
       loanApp1.Account_Id__c=acc.id;
       loanApp1.Loan_Purpose_Category__c = 'Others';
       loanApp1.Reference_1_Address__c=addres.id;
       loanApp1.Reference_2_Address__c=addres.id;
       loanApp1.Income_Detail_Current_Job__c=incomeDetail.id;
       loanApp1.Income_Detail_Previous_Job__c=incomeDetail.id;
       insert loanApp1; 
        //create dummy Application_Social_Media__c  record.
       appSocialMedia = new Application_Social_Media__c ();
       appSocialMedia.Loan_Application_Id__c = loanApp.Id;
       appSocialMedia.Social_Media_Score__c = 'test';
       appSocialMedia.Source__c = 'Vendor';
       insert appSocialMedia;  
  }
  
 static testMethod void testDoPanPost() {
      init();
      Map<String, Object> paramMap = new Map<String, Object>();
      paramMap.put('loanApplicationId',loanApp.id);
      String JsonMsg=JSON.serialize(paramMap);
      RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();

      req.requestURI = '/v1/ws/pan';  //Request URL
      req.httpMethod = 'POST';
      req.requestBody = Blob.valueof(JsonMsg);
      RestContext.request = req;
      RestContext.response= res;
      
      Test.startTest();
      Test.setMock(HttpcalloutMock.class, new mock_test());
      WebserviceResource.callWS();
      Test.stopTest();    
      

  }
  
   static testMethod void testDoAadharPost() {
      init();
      Map<String, Object> paramMap = new Map<String, Object>();
      paramMap.put('loanApplicationId',loanApp.id);
      String JsonMsg=JSON.serialize(paramMap);
      RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();

      req.requestURI = '/v1/ws/aadhar';  //Request URL
      req.httpMethod = 'POST';
      req.requestBody = Blob.valueof(JsonMsg);
      RestContext.request = req;
      RestContext.response= res;
      Test.startTest();
      Test.setMock(HttpcalloutMock.class, new mock_test());
      WebserviceResource.callWS();
      Test.stopTest();
      //WebserviceResource.callWS();

  }
  
  static testMethod void testDoCibilPost() {
      init();
      Map<String, Object> paramMap = new Map<String, Object>();
      paramMap.put('loanApplicationId',loanApp.id);
      String JsonMsg=JSON.serialize(paramMap);
      RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();

      req.requestURI = '/v1/ws/cibil';  //Request URL
      req.httpMethod = 'POST';
      req.requestBody = Blob.valueof(JsonMsg);
      RestContext.request = req;
      RestContext.response= res;
      
      Test.startTest();
      Test.setMock(HttpcalloutMock.class, new mock_test());
      WebserviceResource.callWS();
      Test.stopTest();
      
  }
  /*static testMethod void testCibilakPost() {
      init();
      Map<String, Object> paramMap = new Map<String, Object>();
      paramMap.put('acknowledgementId',appSocialMedia.id);
      paramMap.put('loanApplicationId',loanApp.id);
      String JsonMsg=JSON.serialize(paramMap);
      RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();

      req.requestURI = '/v1/ws/cibilack';  //Request URL
      req.httpMethod = 'POST';
      req.requestBody = Blob.valueof(JsonMsg);
      RestContext.request = req;
      RestContext.response= res;
    
      Test.startTest();
      Test.setMock(HttpcalloutMock.class, new mock_test());
      WebserviceResource.callWS();
      Test.stopTest();
  }*/
  
  global class mock_test implements HttpCalloutMock {
        global httpResponse respond(HTTPrequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            return res;
        }
    }
}