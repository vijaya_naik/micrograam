@isTest(seealldata = false)
public class  PsychometricServiceTest{

  public static testmethod void testData(){
  
     Address__c mgp=new  Address__c();
          mgp.Address_Line_1__c='BDA Complex';
          mgp.Address_Line_2__c='permanentAddressLine2';
          mgp.City__c='permanentAddressCity';
          mgp.Pincode__c=123;
          mgp.State__c='Karnataka';
          mgp.Country__c='India'; 
          mgp.Residing_Since__c=Date.today();
          mgp.House_Ownership__c='Owned';
          insert mgp;
          
               
      Account acc=new Account();
      acc.Name='Microgram';
      acc.Account_Type__c='Borrower'; 
      insert acc;
      
      Income_Detail__c income=new Income_Detail__c();
      income.Designation__c='Manager';
      income.Office_Address__c=mgp.id;
      income.Annual_Income__c=200000;
      insert income;
      
      Loan_Application__c loan=new Loan_Application__c();
      loan.Account_Id__c=acc.id;
      loan.Reference_1_Address__c=mgp.id;
      loan.Reference_2_Address__c=mgp.id;
      loan.Income_Detail_Current_Job__c=income.id;
      loan.Income_Detail_Previous_Job__c=income.id;
      insert loan;   
    
    Application_Psychometric__c psy=new Application_Psychometric__c();
    psy.Source__c='Visual DNA';
    psy.Psychometric_Test_Score__c='Microgram';
    psy.Loan_Application_Id__c=loan.id;
    insert psy;
    
     Application_Psychometric__c psy1=new Application_Psychometric__c();
    psy1.Source__c='Jombay';
    psy1.Psychometric_Test_Score__c='Microgram2';
    psy1.Loan_Application_Id__c=loan.id;
    insert psy1;
    
    
    Map<String, Object> sychometricMap=new Map<String, Object>();
    sychometricMap.put('psychometricTestScore','Microgram');
    sychometricMap.put('source','Visual DNA');
    sychometricMap.put('loanApplicationId',loan.id);
    
     PsychometricService service=new  PsychometricService();
     PsychometricService.getPsychometric(psy.id);
     PsychometricService.getPsychometric(loan.id,'Visual DNA');
     PsychometricService.getPsychometrics(loan.id);
     PsychometricService.createPsychometricInstance(sychometricMap,psy);
     PsychometricService.createPsychometric(sychometricMap);
     PsychometricService.getPsychometricDenormalized(psy);
     PsychometricService.deletePsychometric(psy.id);
     PsychometricService.updatePsychometric(psy1.id,sychometricMap);
     PsychometricService.deletePsychometricData(psy1.id);
     
  }
}