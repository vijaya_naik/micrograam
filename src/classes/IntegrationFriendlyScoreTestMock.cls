global class IntegrationFriendlyScoreTestMock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        HTTPResponse res = new HTTPResponse();
        res.setBody('{"id": "16690","score": "17%","score_points": 545}"');
        res.setStatus('OK');
        return res;
    }
}