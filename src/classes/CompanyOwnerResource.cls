@RestResource(urlMapping='/v1/companyOwner')
global with sharing class CompanyOwnerResource {
    @HttpPost
    global static void cCompanyOwner() {
        RestRequest req = RestContext.request;
        // Deserialize the JSON string into name-value pairs
        List<Object> paramlist=(List<Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        for(Integer i=0;i<paramlist.size();i++){
        Map<String, Object> params = (Map<String, Object>)paramlist.get(i);
        System.debug('Received CompanyOwner Registration request:: ' + params);
        // Invoke service
        System.debug( CompanyOwnerService.getCompanyOwnerDenormalized(CompanyOwnerService.createCompanyOwner(params,null)));
        }
    }
    
    @HttpGet
    global static Map<String,String> getCompanyOwner() {
        // Get id/accountId from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
        		String loanApplicationId = req.params.get('loanApplicationId');
        //String source=req.params.get('source');
        //if ( loanApplicationId != null && source!=null) {
            // Invoke service to get by loanApplicationId 
            System.debug('Received  Get request for loanApplicationId=' + loanApplicationId);
            //id= CompanyOwnerService.getCompanyOwner(loanApplicationId, source).Id;
             //   }
            // Invoke service to get by Loan Application id
            System.debug('Received CompanyOwner Get request for id=' + id);
            return CompanyOwnerService.getCompanyOwnerDenormalized(CompanyOwnerService.getCompanyOwnerById(id));
        
    }
  /*  
    @HttpDelete
    global static void deleteCompanyOwner() {
        // Get id from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
        System.debug('Received CompanyOwner Delete request for id=' + id);
        // Invoke service
        CompanyOwnerService.deleteCompanyOwner(id);
    }
    
    @HttpPatch
    global static Map<String,String> updateCompanyOwner() {
        RestRequest req = RestContext.request;
        // Get id from request parameter
        String id = req.params.get('id');
                		String loanApplicationId = req.params.get('loanApplicationId');
        String source=req.params.get('source');
        if ( loanApplicationId != null && source!=null) {
            // Invoke service to get by loanApplicationId 
            System.debug('Received  Get request for loanApplicationId=' + loanApplicationId);
            id= CompanyOwnerService.getCompanyOwner(loanApplicationId, source).Id;
                }
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received CompanyOwner Update request for id=' + id + '::' + params);
        // Invoke service
        return CompanyOwnerService.updateCompanyOwner(id, params);
    }
*/
}