@RestResource(urlMapping='/v1/ws/*')
global with sharing class WebserviceResource {
    @HttpPost
    global static void callWS() {
        RestRequest req = RestContext.request;
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received LoanApplication Registration request:: ' + params);
        // Invoke service
        // Grab the type from the end of the URL
        List<String> tokens = req.requestURI.split('/');
        String type = tokens[tokens.size()-1];
        System.debug('Received Get Data request for type=' + type);
        RestResponse res = RestContext.response;
        if (res == null) {
            res = new RestResponse();
            RestContext.response = res;
        }
        
        System.debug('re params.............'+req.params);
        System.debug('params.............'+params);
        /* below code is commented by extentor and wrote changed code.
           req.params don't have value only param have value.
        */
        if ( 'pan'.equalsIgnoreCase(type) ) {
              System.debug('in pan'+req.params.get('loanApplicationId'));
              String loanApplicationId = req.params.get('loanApplicationId');
              WebserviceCallSoftcell.panCall(loanApplicationId);
        }else if ( 'aadhar'.equalsIgnoreCase(type) ) {
              System.debug('in aadhar'+req.params.get('loanApplicationId'));
              String loanApplicationId = req.params.get('loanApplicationId');
              WebserviceCallSoftcell.AadharCall(loanApplicationId);
        }else if('cibil'.equalsIgnoreCase(type)){
              System.debug('in pan'+req.params.get('loanApplicationId'));
              String loanApplicationId = req.params.get('loanApplicationId');
              WebserviceCallSoftcell.CIBILCall(loanApplicationId);
        }else if('cibilack'.equalsIgnoreCase(type)){
               System.debug('in pan'+req.params.get('loanApplicationId'));
              String loanApplicationId = req.params.get('loanApplicationId');
              System.debug('in pan'+req.params.get('acknowledgementId'));
              String acknowledgementId = req.params.get('acknowledgementId');
                          WebserviceCallSoftcell.AckCall(acknowledgementId,loanApplicationId);

        }
        
        /*if ( 'pan'.equalsIgnoreCase(type) ) {
              System.debug('in pan'+ params.get('loanApplicationId'));
              String loanApplicationId = String.valueof(params.get('loanApplicationId'));
              WebserviceCallSoftcell.panCall(loanApplicationId);
        }else if ( 'aadhar'.equalsIgnoreCase(type) ) {
              System.debug('in aadhar'+ params.get('loanApplicationId'));
              String loanApplicationId = String.valueof(params.get('loanApplicationId'));
              WebserviceCallSoftcell.AadharCall(loanApplicationId);
        }else if('cibil'.equalsIgnoreCase(type)){
              System.debug('in pan'+ params.get('loanApplicationId'));
              String loanApplicationId = String.valueof(params.get('loanApplicationId'));
              WebserviceCallSoftcell.CIBILCall(loanApplicationId);
        }else if('cibilack'.equalsIgnoreCase(type)){
               System.debug('in pan'+ params.get('loanApplicationId'));
              String loanApplicationId = String.valueof(params.get('loanApplicationId'));
              System.debug('in pan'+ params.get('acknowledgementId'));
              String acknowledgementId = String.valueof(params.get('acknowledgementId'));
                          WebserviceCallSoftcell.AckCall(acknowledgementId,loanApplicationId);

        }*/
        // HTTPResponse res1=WebserviceCallSoftcell.makeCall();
       // System.debug('res.. '+res1);
    }
   
}