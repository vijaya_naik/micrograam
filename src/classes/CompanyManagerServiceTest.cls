@isTest(seealldata = false)
public class CompanyManagerServiceTest{

  public static testmethod void mangerServiceTestmethods(){
  
    Address__c mgp=new  Address__c();
          mgp.Address_Line_1__c='BDA Complex';
          mgp.Address_Line_2__c='permanentAddressLine2';
          mgp.City__c='permanentAddressCity';
          mgp.Pincode__c=123;
          mgp.State__c='Karnataka';
          mgp.Country__c='India'; 
          mgp.Residing_Since__c=Date.today();
          mgp.House_Ownership__c='Owned';
          insert mgp;
          
       Account acc=new Account();
      acc.Name='Microgram';
      acc.Account_Type__c='Borrower'; 
      insert acc;
      
      Income_Detail__c income=new Income_Detail__c();
      income.Designation__c='Manager';
      income.Office_Address__c=mgp.id;
      income.Annual_Income__c=200000;
      insert income;
      
      Loan_Application__c loan=new Loan_Application__c();
      loan.Account_Id__c=acc.id;
      loan.Reference_1_Address__c=mgp.id;
      loan.Reference_2_Address__c=mgp.id;
      loan.Income_Detail_Current_Job__c=income.id;
      loan.Income_Detail_Previous_Job__c=income.id;
      insert loan;
  
    Company_Manager__c cmpmgr=new Company_Manager__c();
      cmpmgr.Company_Manager_Designation__c='Manager';
      cmpmgr.Work_Experience__c=12;
      cmpmgr.Manager_Address__c=mgp.id;
      insert cmpmgr;
      
      Map<String,Object> addressMap=new Map<String,Object>();
          Company_Manager__c mgpAddress=new Company_Manager__c();
          addressMap.put('companyManagerName','xty');
          addressMap.put('loanApplicationId',loan.id);
          addressMap.put('companyManagerDesignation','Manager');
          addressMap.put('workExperience',2);
          addressMap.put('din','mgpAddress');
      List<Map<String,Object>> maplist =new List<Map<String,Object>>();
        maplist.add(addressMap);
         /* maplist.put('loanApplicationId',loan.id);
          maplist.put('companyManagerDesignation','Manager');
          maplist.put('workExperience',2);
          maplist.put('din','mgpAddress');*/
      
  CompanyManagerService mgrService=new CompanyManagerService();
  CompanyManagerService.getCompanyManagerById(cmpmgr.id);
  CompanyManagerService.getCompanyManagerByLoanId(cmpmgr.id);
  CompanyManagerService.createCompanyManager(addressMap,cmpmgr);
  CompanyManagerService.getCompanyManagerDenormalized(cmpmgr);
   CompanyManagerService.getCompanyManagerDenormalized(cmpmgr.id);
    CompanyManagerService.deleteWithLoanApplicationId(cmpmgr.id); 
      try{
          
      
      CompanyManagerService.createCompanyManager(maplist);
      }catch(Exception e){}
  }
}