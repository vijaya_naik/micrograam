public class ApplicantTriggerHandler {
    
    
    public static void onAfterInsert(List<Applicant__c> applicantList){
        ApplicantTriggerHandler.generateOtpAfterInsert(applicantList);
    }
    
    
    public static void onAfterUpdate(List<Applicant__c> applicantList,Map<Id,Applicant__c> applicantMap)
    {
        ApplicantTriggerHandler.generateOtpAfterUpdate(applicantList,applicantMap);
        ApplicantTriggerHandler.fetchUserAadhaarDetails(applicantList,applicantMap);
    }
    
    public static void generateOtpAfterInsert(List<Applicant__c> applicantList)
    {
          List<Integration_Message__c> messageList = new List<Integration_Message__c>();
          IntegrationMessageProvider__c messageProvider = IntegrationMessageProvider__c.getAll().get(Label.AadhaarOtp);
          for(Applicant__c applicant : applicantList)
          {
              if(applicant.KYC_Status__c=='Submitted')
              {
                  messageList.add(IntegrationMessageHandler.getIntegrationMessages(applicant.Id, messageProvider));
              }
              if(messageList.size()>0)
              {
                  insert messageList;
              }
          }
    }
    
    public static void generateOtpAfterUpdate(List<Applicant__c> applicantList,Map<Id,Applicant__c> applicantMap)
    {
         List<Integration_Message__c> messageList = new List<Integration_Message__c>();
         IntegrationMessageProvider__c messageProvider = IntegrationMessageProvider__c.getAll().get(Label.AadhaarOtp);
         for(Applicant__c applicant : applicantList)
         {
             if(applicant.KYC_Status__c <> applicantMap.get(applicant.Id).KYC_Status__c && applicant.KYC_Status__c=='Submitted')
             {
                 messageList.add(IntegrationMessageHandler.getIntegrationMessages(applicant.Id, messageProvider));
             }
         }
        
        
         if(messageList.size() > 0){
            insert messageList;
        }
    }
    
    
    
    
    public static void fetchUserAadhaarDetails(List<Applicant__c> applicantList,Map<Id,Applicant__c> applicantMap)
    {
         List<Integration_Message__c> messageList = new List<Integration_Message__c>();
         IntegrationMessageProvider__c messageProvider = IntegrationMessageProvider__c.getAll().get(Label.FetchAadhaarDetails);
         for(Applicant__c applicant : applicantList)
         {
             if(applicant.Aadhaar_Bridge_Otp__c <> applicantMap.get(applicant.Id).Aadhaar_Bridge_Otp__c)
             {
                 messageList.add(IntegrationMessageHandler.getIntegrationMessages(applicant.Id, messageProvider));
             }
         }
        
        
         if(messageList.size() > 0){
            insert messageList;
        }
    }

}