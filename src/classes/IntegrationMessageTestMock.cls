global class IntegrationMessageTestMock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        HTTPResponse res = new HTTPResponse();
        res.setBody('Test');
        res.setStatus('OK');
        return res;
    }
}