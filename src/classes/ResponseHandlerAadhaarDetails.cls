public class ResponseHandlerAadhaarDetails implements IResponseHandler{
    
    
    public void execute(Object response, Integration_Message__c message){
        
        HttpResponse res=(HttpResponse)response;
        System.debug('respnse---'+res.getBody());
        try{
            String jsonString = res.getBody();
    
            Map<String, Object> obj = (Map<String, Object>) JSON.deserializeUntyped(jsonString);
            
            Map<String, Object> kycMap = (Map<String, Object>) obj.get('kyc');
            Map<String, Object> poaMap = (Map<String, Object>) kycMap.get('poa');
            Map<String, Object> poiMap = (Map<String, Object>) kycMap.get('poi');
            
            String applicantName = (String) poiMap.get('name');
            NameWrapper nameWrap = ResponseHandlerAadhaarDetails.getFirstAndLastName(applicantName);
            
            Applicant__c applicant=[SELECT Account_Id__c, Account_Id__r.Account_Type__c from Applicant__c where Id=:message.Input_String__c];
            
            Address__c address = new Address__c(
                Address_Line_1__c = (String) poaMap.get('co'),
                Address_Line_2__c = (String) poaMap.get('po'),
                City__c = (String) poaMap.get('vtc'),
                State__c = (String) poaMap.get('state'),
                Country__c = Label.India,
                Pincode__c = Decimal.valueOf((String) poaMap.get('pc')),
                Custom_External_Id__c = (String) obj.get('aadhaar-id')
            );
            upsert address Custom_External_Id__c;
            
            Application_Kyc__c kycObj=new Application_Kyc__c(
                Account_Id__c = applicant.Account_Id__c,
                Loan_Application_Id__c = applicant.Account_Id__r.Account_Type__c == 'Borrower' ? LoanApplicationService.getActiveApplicationForAccountId(applicant.Account_Id__c) : null,
                Aadhar_Number__c = Decimal.valueOf((String) obj.get('aadhaar-id')),
                First_Name__c = nameWrap.firstName,
                Gender__c = (String) poiMap.get('gender'),
                Last_Name__c = nameWrap.lastName,
                Source__c = Label.AadhaarBridge,
                Date_Of_Birth__c = ResponseHandlerAadhaarDetails.convertStringToDate((String) poiMap.get('dob')),
                Permanent_Address__c = address.Id,
                Custom_External_Id__c = (String) obj.get('aadhaar-id')
            );
            upsert kycObj Custom_External_Id__c;  
            
            /*Attachment attmnt = new Attachment(
                Name = 'Aadhaar Photo.jpg',
                Body = EncodingUtil.base64Decode((String) kycMap.get('photo')),
                ParentId = kycObj.Id,
                ContentType = 'application/jpg'
            );
            insert attmnt;*/
        }
        catch(exception e){
            ExceptionHandler.saveExceptionLog(e, 'Parsing Exception', 'Adhaar Bridge Integration', 'execute');
            message.Response_Body__c = res.getBody();
            message.Failed_to_Parse__c = true;
        }  
    }
   
    
    public static Date convertStringToDate(String dateString){
        List<String> tempList = new List<String>();
        tempList = dateString.split('-');
        Date tempDate = null;
        if(tempList.size() == 3){
            tempDate = Date.newInstance(Integer.valueOf(tempList[2]), Integer.valueOf(tempList[1]), Integer.valueOf(tempList[0]));
        }
        
        return tempDate;
    }
    
    
    public static NameWrapper getFirstAndLastName(String applicantName ){
        Integer spaceIndex = applicantName.indexOf(' ');
        NameWrapper nameWrap = new NameWrapper();
        
        if(spaceIndex > 0){
            nameWrap.firstName = applicantName.left(spaceIndex);
            nameWrap.lastName = applicantName.right(applicantName.length()-spaceIndex-1);
        }
        else{
            nameWrap.firstName = applicantName;
            nameWrap.lastName = '';
        }

        return nameWrap;
    }
    
    
    public class NameWrapper{
        public String firstName;
        public String lastName;
    }
}