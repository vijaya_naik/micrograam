/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class BorrowerService_test {

    static testMethod void myUnitTest() {
    try{
         Object fname='Shambhavi';
         Object lname='Dixit';
         Object applicantType='Individual';
        
         
       Map<String, Object> paramsMap=new  Map<String, Object>();
        paramsMap.put('firstName',fname);
        paramsMap.put('lastName',lname);
        paramsMap.put('applicantType',applicantType);
        paramsMap.put('submit',true);
        
         Address__c mgpAddress=new Address__c();
         paramsMap.put('permanentAddressLine1',mgpAddress);
         paramsMap.put('permanentAddressLine2',mgpAddress);
         paramsMap.put('permanentAddressCity',mgpAddress);
         paramsMap.put('permanentAddressState',mgpAddress);
         paramsMap.put('permanentAddressCountry',mgpAddress);
        
          paramsMap.put('registeredAddressLine1',mgpAddress);
          paramsMap.put('registeredAddressLine2',mgpAddress);
          paramsMap.put('registeredAddressCity',mgpAddress);
          paramsMap.put('registeredAddressState',mgpAddress);
          paramsMap.put('registeredAddressCountry',mgpAddress);
        
        BorrowerService.registerBorrower(paramsMap);
        
         Account a=UtilityClassTest.insertAccount('true');
         insert a;
         string accId=string.valueof(a.id);
        
         Account a1=UtilityClassTest.insertAccount('true');
         insert a1;
         string accId1=string.valueof(a1.id);
        
         Attachment att=UtilityClassTest.InsertAttachment(a.id);
        insert att;
        
        Applicant__c  Applicant=UtilityClassTest.applicant(a.id);
        insert Applicant;
        
        BorrowerService.updateBorrower(a.id, paramsMap);
        BorrowerService.deleteBorrower(a.id);
        BorrowerService.getBorrower(a.id);
       }Catch(Exception e){}
    }
     static testMethod void myUnitTestTwo() {
     try{
         Object fname='Shambhavi';
         Object lname='Dixit';
         Object applicantType='Non-Individual';
         
      Map<String, Object> paramsMap2=new  Map<String, Object>();
        paramsMap2.put('companyFirmName',fname);
        paramsMap2.put('lastName',lname);
        paramsMap2.put('applicantType',applicantType);
        paramsMap2.put('submit',true);
       
          
          Address__c mgpAddress=new Address__c();
         paramsMap2.put('permanentAddressLine1',mgpAddress);
         paramsMap2.put('permanentAddressLine2',mgpAddress);
         paramsMap2.put('permanentAddressCity',mgpAddress);
         paramsMap2.put('permanentAddressState',mgpAddress);
         paramsMap2.put('permanentAddressCountry',mgpAddress);
         
         paramsMap2.put('registeredAddressLine1',mgpAddress);
         paramsMap2.put('registeredAddressLine2',mgpAddress);
         paramsMap2.put('registeredAddressCity',mgpAddress);
         paramsMap2.put('registeredAddressState',mgpAddress);
         paramsMap2.put('registeredAddressCountry',mgpAddress);
         
         paramsMap2.put('correspondenceAddressLine1',mgpAddress);
         paramsMap2.put('correspondenceAddressLine2',mgpAddress);
         paramsMap2.put('correspondenceAddressCity',mgpAddress);
         paramsMap2.put('correspondenceAddressState',mgpAddress);
         paramsMap2.put('correspondenceAddressCountry',mgpAddress);
         
         paramsMap2.put('currentAddressLine1',mgpAddress);
         paramsMap2.put('currentAddressLine2',mgpAddress);
         paramsMap2.put('currentAddressCity',mgpAddress);
         paramsMap2.put('currentAddressState',mgpAddress);
         paramsMap2.put('currentAddressCountry',mgpAddress);
         BorrowerService.registerBorrower(paramsMap2);
         }Catch(Exception e){}
     }
      static testMethod void myUnitTestThree() {
        Map<String, Object> paramsMap3=new  Map<String, Object>();
        try{
         BorrowerService.registerBorrower(paramsMap3);
        }catch(exception e){
            
        }
        
      }
      
}