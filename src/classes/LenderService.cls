public class LenderService {
    public static Map<String,String> registerLender(Map<String, Object> params) {
        String firstName = String.valueOf(params.get('firstName'));
        String lastName = String.valueOf(params.get('lastName'));
        String applicantType = String.valueOf(params.get('applicantType'));
        
        //1. Create Account instance & set fields
        Account lenderAccount = new Account();
        lenderAccount.Account_Type__c = String.valueOf(params.get('accountType'));
        lenderAccount.Applicant_Type__c=applicantType;
        if('Individual'.equals(applicantType))
            lenderAccount.Name = firstName + ' ' + lastName;
        else if('Non-Individual'.equals(applicantType))
            lenderAccount.Name=String.valueOf(params.get('companyFirmName'));

        Address__c registeredAddress= AddressService.createRegisteredAddress(params, null);
        Address__c correspondenseAddress= AddressService.createCorrespondenceAddress(params, null);
        
        //2. Create Applicant instance & set fields
        Applicant__c applicant = ApplicantService.createApplicant(params, null);
       
        //3. Create Permanant Address instance & set fields
        Address__c permanentAddress = AddressService.createPermanentAddress(params, null);

        //4. Create Current Address instance & set fields
        Address__c currentAddress = AddressService.createCurrentAddress(params, null);
        
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
            insert lenderAccount;
            applicant.Account_Id__c = lenderAccount.Id; //Update parent Account Id
            if (permanentAddress != null) {
                insert permanentAddress;
                applicant.Permanent_Address__c = permanentAddress.Id; //Update related Address Id
            }
            if (currentAddress != null) {
                insert currentAddress;
                applicant.Current_Address__c = currentAddress.Id; //Update related Address Id
            }
            if (registeredAddress != null) {
                insert registeredAddress;
                applicant.Registered_Address__c = registeredAddress.Id; //Update related Address Id
            }
             if (correspondenseAddress != null) {
                insert correspondenseAddress;
                applicant.Correspondence_Address__c = correspondenseAddress.Id; //Update related Address Id
            }
        
            insert applicant;
            
            //In case of Submit - additionally create Application_KYC record for "User Input" source
            String isSubmit = String.valueOf(params.get('submit'));
            if ( isSubmit != null && 'true'.equalsIgnoreCase(isSubmit) ) {
                if('Individual'.equals(applicantType)) {
                    createApplicationKYCOnSubmitForIndividual(lenderAccount.Id, applicant);
                } else if('Non-Individual'.equals(applicantType)) {
                    createApplicationKYCOnSubmitForNonIndividual(lenderAccount.Id, applicant, lenderAccount.Name);
                }
                //Update the KYC_Status to "Submitted"
                applicant.KYC_Status__c = 'Submitted';
                update applicant;
            }
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
        
        return getLenderDenormalized(lenderAccount.Id);
    }

    public static Map<String,String> getLenderDenormalized(String lenderAccountId) {
        Map<String,String> lenderMap = new Map<String,String>();
        // Get account details
        Account account = [
            SELECT Id, Name, Account_Type__c,Applicant_Type__c
            FROM Account 
            WHERE Account.Id = :lenderAccountId
        ];
        
        //Get applicant details
        Applicant__c applicant = ApplicantService.getApplicantForAccountId(lenderAccountId);
        
        //Get current address details
        if (applicant.Current_Address__c != null) {
            Address__c currentAddress = AddressService.getAddressById(applicant.Current_Address__c);
            lenderMap.putAll(AddressService.getCurrentAddressDenormalized(currentAddress));
        }
        
        //Get permanent address details
        if (applicant.Permanent_Address__c != null) {
            Address__c permanentAddress = AddressService.getAddressById(applicant.Permanent_Address__c);
            lenderMap.putAll(AddressService.getPermanentAddressDenormalized(permanentAddress));
        }

        if (applicant.Registered_Address__c != null) {
            Address__c RegisteredAddress = AddressService.getAddressById(applicant.Registered_Address__c);
            lenderMap.putAll(AddressService.getRegisteredAddressDenormalized(RegisteredAddress));
        }
        if (applicant.Correspondence_Address__c != null) {
            Address__c CorrespondenseAddress = AddressService.getAddressById(applicant.Correspondence_Address__c);
            lenderMap.putAll(AddressService.getCorrespondenceAddressDenormalized(CorrespondenseAddress));
        }
        
        lenderMap.putAll(ApplicantService.getApplicantDenormalized(applicant));
        if (account.Account_Type__c != null) lenderMap.put('accountType', account.Account_Type__c);
        if ('Non-Individual'.equals(account.Applicant_Type__c)) lenderMap.put('companyFirmName', account.Name);
        
        lenderMap.put('applicantType', account.Applicant_Type__c);
        lenderMap.put('id', account.Id);
        
        return lenderMap;
    }
    
    public static void deleteLender(String lenderAccountId) {
        //Get applicant record
        Applicant__c applicant = ApplicantService.getApplicantForAccountId(lenderAccountId);
        //Get account record
        Account lenderAccount = [SELECT Id FROM Account WHERE Account.Id = :lenderAccountId];
        //Get current address record
        Address__c currentAddress = null;
        if (applicant.Current_Address__c != null) {
            currentAddress = AddressService.getAddressById(applicant.Current_Address__c);
        }
        //Get permanent address record
        Address__c permanentAddress = null;
        if (applicant.Permanent_Address__c != null) {
            permanentAddress = AddressService.getAddressById(applicant.Permanent_Address__c);
        }
        Address__c RegisteredAddress =null;
       if (applicant.Registered_Address__c != null) 
             RegisteredAddress = AddressService.getAddressById(applicant.Registered_Address__c);
        Address__c CorrespondenseAddress =null;
        if (applicant.Correspondence_Address__c != null) 
             CorrespondenseAddress = AddressService.getAddressById(applicant.Correspondence_Address__c);

        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
            if (currentAddress != null)
                delete currentAddress; //Delete current address record
            if (permanentAddress != null)
                delete permanentAddress; //Delete permanent address record
            if (RegisteredAddress != null)
                delete RegisteredAddress; //Delete registered address record
            if (CorrespondenseAddress != null)
                delete CorrespondenseAddress; //Delete correspondence address record 
            
            delete applicant; //Delete applicant record
            delete lenderAccount; //Delete account record
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
    
    public static Map<String,String> updateLender(String lenderAccountId, Map<String, Object> params) {
        String firstName = params.get('firstName') == null ? null : String.valueOf(params.get('firstName'));
        String lastName = params.get('lastName') == null ? null : String.valueOf(params.get('lastName'));
        String applicantType = params.get('applicantType') == null ? null : String.valueOf(params.get('applicantType'));

        //Get & update applicant record
        Applicant__c applicant = ApplicantService.getApplicantForAccountId(lenderAccountId);
        applicant = ApplicantService.createApplicant(params, applicant);

        //Get & update account record
        Account lenderAccount = [SELECT Id FROM Account WHERE Account.Id = :lenderAccountId];
        if (params.get('accountType') != null)
            lenderAccount.Account_Type__c = String.valueOf(params.get('accountType'));
        if (applicantType != null)
            lenderAccount.Applicant_Type__c=applicantType;
        if(applicantType != null && 'Individual'.equals(applicantType) && firstName != null && lastName != null)
            lenderAccount.Name = firstName + ' ' + lastName;
        else if(applicantType != null && 'Non-Individual'.equals(applicantType))
            lenderAccount.Name=String.valueOf(params.get('companyFirmName'));
        
        //Get & update current address record
        Address__c currentAddress = null;
        if (applicant.Current_Address__c != null) {
            currentAddress = AddressService.getAddressById(applicant.Current_Address__c);
        }
        currentAddress = AddressService.createCurrentAddress(params, currentAddress);
        
        //Get & update permanent address record
        Address__c permanentAddress = null;
        if (applicant.Permanent_Address__c != null) {
            permanentAddress = AddressService.getAddressById(applicant.Permanent_Address__c);
        }
        permanentAddress = AddressService.createPermanentAddress(params, permanentAddress);
        
        Address__c RegisteredAddress =null;
        if (applicant.Registered_Address__c != null) {
             RegisteredAddress = AddressService.getAddressById(applicant.Registered_Address__c);
        }
        RegisteredAddress = AddressService.createRegisteredAddress(params, RegisteredAddress);
        
        Address__c CorrespondenseAddress =null;
        if (applicant.Correspondence_Address__c != null) {
             CorrespondenseAddress = AddressService.getAddressById(applicant.Correspondence_Address__c);
        }
        CorrespondenseAddress = AddressService.createCorrespondenceAddress(params, CorrespondenseAddress);
        
        //Update in database
        Savepoint sp = Database.setSavePoint();
        try {
            if (permanentAddress != null) {
                upsert permanentAddress;
                applicant.Permanent_Address__c = permanentAddress.Id; //Update related Address Id
            }
            if (currentAddress != null) {
                upsert currentAddress;
                applicant.Current_Address__c = currentAddress.Id; //Update related Address Id
            }
            if(CorrespondenseAddress!=null){
                upsert CorrespondenseAddress;
                applicant.Correspondence_Address__c=CorrespondenseAddress.Id;
            }
            if(RegisteredAddress!=null){
                upsert RegisteredAddress;
                applicant.Registered_Address__c=RegisteredAddress.Id;
            }
            
            update applicant;
            update lenderAccount;

/*            //In case of Submit - additionally create Application_KYC record for "User Input" source
            String isSubmit = String.valueOf(params.get('submit'));
            if ( isSubmit != null && 'true'.equalsIgnoreCase(isSubmit) ) {
                if('Individual'.equals(applicantType)) {
                    createApplicationKYCOnSubmit(lenderAccount.Id, applicant);
                }
                //Update the KYC_Status to "Submitted"
                applicant.KYC_Status__c = 'Submitted';
                update applicant;
            } */
        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
        
        return getLenderDenormalized(lenderAccountId);
    }
    
    private static void createApplicationKYCOnSubmitForIndividual(String lenderId, Applicant__c applicant) {
        Map<String, Object> params = new Map<String, Object>();
        //Create "User Input" source record
        params.put('source', 'User Input');
        params.put('lenderId', lenderId);
        params.put('firstName', applicant.First_Name__c);
        params.put('lastName', applicant.Last_Name__c);
        params.put('dateOfBirth', applicant.Date_Of_Birth__c);
        params.put('gender', applicant.Gender__c);
        params.put('fathersFirstName', applicant.Fathers_First_Name__c);
        params.put('fathersLastName', applicant.Fathers_Last_Name__c);
        params.put('aadharNumber', applicant.Aadhar_Number__c);
        params.put('pan', applicant.PAN__c);
        params.put('voterID', applicant.Voter_ID__c);
        
        //Copy Permanent Address to "User Input" source record
        Address__c permanentAddress = AddressService.getAddressById(applicant.Permanent_Address__c);
        params.put('permanentAddressLine1', permanentAddress.Address_Line_1__c);
        params.put('permanentAddressLine2', permanentAddress.Address_Line_2__c);
        params.put('permanentAddressCity', permanentAddress.City__c);
        params.put('permanentAddressPincode', permanentAddress.Pincode__c);
        params.put('permanentAddressState', permanentAddress.State__c);
        params.put('permanentAddressCountry', permanentAddress.Country__c);
        params.put('currentAddressResidingSince', permanentAddress.Residing_Since__c);
        
        ApplicationKYCService.createApplicationKYC(params);
    }
    
    private static void createApplicationKYCOnSubmitForNonIndividual(
        String lenderId, Applicant__c applicant, String companyFirmName) {
        Map<String, Object> params = new Map<String, Object>();
        //Create "User Input" source record
        params.put('source', 'User Input');
        params.put('lenderId', lenderId);
        params.put('companyFirmName', companyFirmName);
        params.put('dateOfIncorporation', applicant.Date_of_Incorporation__c);
        params.put('pan', applicant.PAN__c);
        
        //Copy Registered Address to "User Input" source record
        Address__c registeredAddress = AddressService.getAddressById(applicant.Registered_Address__c);
        params.put('registeredAddressLine1', registeredAddress.Address_Line_1__c);
        params.put('registeredAddressLine2', registeredAddress.Address_Line_2__c);
        params.put('registeredAddressCity', registeredAddress.City__c);
        params.put('registeredAddressPincode', registeredAddress.Pincode__c);
        params.put('registeredAddressState', registeredAddress.State__c);
        params.put('registeredAddressCountry', registeredAddress.Country__c);
        
        ApplicationKYCService.createApplicationKYC(params);
    }
}