@isTest
global class WebserviceCallSoftcellTest{
    static Address__c addres;
    static Account acc;
    static Income_Detail__c incomeDetail;
    static Loan_Application__c loanApp ;
    static Applicant__c aplcnt;
    
    private static final String ORG_URL = URL.getSalesforceBaseUrl().toExternalForm() ;
      
     static void init() {
    // setup test data 
       //create dummy Address Record
       addres =new  Address__c();
       addres.Address_Line_1__c='Street 1';
       addres.Address_Line_2__c='Street 2';
       addres.City__c='Bangalore ';
       addres.Pincode__c= 12345;
       addres.State__c='KA';
       addres.Country__c='India'; 
       addres.Residing_Since__c=System.today();
       addres.House_Ownership__c='Owned';
       insert addres;
          
       //create dummy account record
       acc=new Account();
       acc.Name='Microgram';
       acc.Account_Type__c='Borrower'; 
       insert acc;
       
       //create dummy applicant
       aplcnt = TestUtility.objmgp2pApplicant();
       aplcnt.Account_Id__c = acc.Id;
       insert aplcnt;
       //create dummy Income Detail Record
       incomeDetail = new Income_Detail__c();
       incomeDetail.Designation__c='Manager';
       incomeDetail.Office_Address__c=addres.id;
       incomeDetail.Annual_Income__c=30000;
       insert incomeDetail;
       
       //create dummmy Loan Application Record.
       loanApp = new Loan_Application__c();
       loanApp.Account_Id__c=acc.id;
       loanApp.Reference_1_Address__c=addres.id;
       loanApp.Reference_2_Address__c=addres.id;
       loanApp.Income_Detail_Current_Job__c=incomeDetail.id;
       loanApp.Income_Detail_Previous_Job__c=incomeDetail.id;
       insert loanApp; 
    }
    
    global Static testmethod void testone(){
        init();
        test.startTest();
        Test.setMock(HttpcalloutMock.class, new mock_test());
        try {
            WebserviceCallSoftcell.AckCall('12345',loanApp .Id);
        }
        catch(Exception e){}
        try {
            WebserviceCallSoftcell.CIBILCall(loanApp .Id);
        }
        catch(Exception e){}
        try {
            WebserviceCallSoftcell.panCall(loanApp .Id);
        }
        catch(Exception e){}
        try {
            WebserviceCallSoftcell.AadharCall(loanApp .Id);
        }
        catch(Exception e){}
        test.stopTest();
        
    }
    
    global class mock_test implements HttpCalloutMock {
        global httpResponse respond(HTTPrequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            return res;
        }
    }
}