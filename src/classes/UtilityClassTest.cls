//Utlity class is created by : shambhvi dixit
public class UtilityClassTest {
   public static account insertAccount(string i){
        Account acc = new Account();
        acc.Name='test'+i;
        return acc;
    } 
    public static Attachment InsertAttachment(Id id){
        Attachment attach=new Attachment();    
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=id;
        //insert attach;
        return attach;
    }
   public static Applicant__c applicant(Id accId){
    Applicant__c appli=new Applicant__c();
    appli.First_Name__c='Test';
    appli.Account_Id__c=accid;
    appli.E_Mail__c='test@test.com';
    return appli;
    } 
    public static Applicant_Attachment__c insertAttachement(Id ApplicantId){
    Applicant_Attachment__c mg=new Applicant_Attachment__c();
    mg.Applicant_Id__c=ApplicantId;
    mg.Attachment_Id__c='Photo';
    mg.Attachment_Type__c='Photo';
    return mg;
    }
    public static Loan_Application__c insertloanApplication(Id AccId){
        Loan_Application__c loanApp=new Loan_Application__c();
        loanApp.Account_Id__c=AccId;
        loanApp.Auto_Loan__c=22.0;
        //insert loanApp;
        return loanApp;
      }
   public static Loan_Application_Attachment__c insertloanApplicationAttachement(Id loanAppId,String applicationAttachementID){
        Loan_Application_Attachment__c loanApp=new Loan_Application_Attachment__c();
        loanApp.Loan_Application_Id__c=loanAppId;
        loanApp.Attachment_Comments__c='new';
        loanApp.Attachment_Id__c=applicationAttachementID;
      //  insert loanApp;
       return loanApp;
       }
      public static Applicant__c insertApplicant(id accids){
        Applicant__c app=new Applicant__c();
        app.First_Name__c='Test1';
        app.E_Mail__c='abc@abc.com';
        app.Account_Id__c=accids;
        return app;
       } 
       public static Applicant_Attachment__c appAtt(id applicantId){
        Applicant_Attachment__c appAttache=new Applicant_Attachment__c();
        appAttache.Applicant_Id__c=applicantId;
        appAttache.Attachment_Id__c='new ';
        appAttache.Attachment_Type__c='Photo';
        return appAttache;
       }
        public static Applicant__c createApplicant(id accid){
        Applicant__c appAttache=new Applicant__c();
        appAttache.Applicant_Nature__c='true';
        appAttache.Account_Id__c=accid;
        appAttache.First_Name__c='test';
        appAttache.Middle_Name__c='test';
        appAttache.Last_Name__c='test';
        appAttache.Aadhar_Number__c=123455;
        appAttache.Voter_ID__c='45678';
        appAttache.Bank_Account_Number__c=123456789;
        appAttache.Bank_IFSC_Code__c='7890678';
        appAttache.Bank_Name__c='Axis Bank';
        appAttache.Completion_Year__c=2016;
        appAttache.Date_Of_Birth__c=date.today();
        appAttache.E_Mail__c='abc@test.com';
        appAttache.Fathers_First_Name__c='papa';
        appAttache.Fathers_Middle_Name__c='papa';
        appAttache.Fathers_Last_Name__c='papa';
        appAttache.Gender__c='Female';
        appAttache.Highest_Qualification_Desc__c='Graduate';
        appAttache.Highest_Qualification_Type__c='correspondence';
        appAttache.KYC_Status__c='true';
        appAttache.Landline_Number__c=123456;
        appAttache.Landline_STD_Code__c=080;
        appAttache.Last_Education_Institute__c='test';
        appAttache.Marital_Status__c='single';
        appAttache.Mobile_Number__c='789789789';
        appAttache.No_Of_Dependent_Children__c=5;
        appAttache.No_Of_Dependents__c=5;
        appAttache.Office_Number__c='123456789';
        appAttache.Office_Extension__c=12;
        appAttache.Office_STD_Code__c=080;
        appAttache.PAN__c='cpcpcpc';
        appAttache.Profile_Facebook__c='newName';
        appAttache.Profile_LinkedIn__c='newName';
        appAttache.Profile_Twitter__c='newName';
        appAttache.Primary_Contact_Name__c='1010101010';
        appAttache.Date_of_Incorporation__c=Date.today();
        appAttache.Registration_Number__c='12310101';
       // appAttache.mgp2p__Registered_Address__c='new bangalore';
       // appAttache.mgp2p__Correspondence_Address__c='new address';
        appAttache.Profile_Twitter__c='yes profile';
        insert appAttache;
        return appAttache;
       }
}