@isTest(seeAlldata=false)
private class KYCRulesMasterService_Test {
    
    public static testmethod void crdtMethods(){

        KYC_Rules_Master__c kycObj = TestUtility.objKYCrm();
        insert kycObj;
        KYC_Rules_Master__c kycObj1 = TestUtility.objKYCrm();
            kycObj1.Field_Name__c = 'Namefield';
            kycObj1.Field_Order__c = 45;
            kycObj1.Min_Score__c = 54;
        KYCRulesMasterService kycRMS = new KYCRulesMasterService();
        Map<String, Object> kycMap = new Map<String,Object>();
            kycMap.put('fieldName',kycObj1.Field_Name__c);
            kycMap.put('minScore', kycObj1.Min_Score__c);
            kycMap.put('fieldOrder', kycObj1.Field_Order__c);
        Test.startTest();
        KYCRulesMasterService.getKYCRulesMaster(kycObj.Id);
        KYCRulesMasterService.getKYCRules();
        KYCRulesMasterService.getKYCRulesMasterDenormalized(kycObj);
        KYCRulesMasterService.getKYCRulesMasterMap();
        KYCRulesMasterService.updateKYCRulesMaster(kycObj.Id, kycMap);
        KYCRulesMasterService.deleteKYCRulesMaster(kycObj.Id);
        Test.stopTest();
    }

    public static testmethod void crdtMethods1(){

        KYC_Rules_Master__c kycObj1 = TestUtility.objKYCrm();
            kycObj1.Field_Name__c = 'Namefield';
            kycObj1.Field_Order__c = 45;
            kycObj1.Min_Score__c = 54;
        KYCRulesMasterService kycRMS = new KYCRulesMasterService();
        Map<String, Object> kycMap = new Map<String,Object>();
            kycMap.put('fieldName',kycObj1.Field_Name__c);
            kycMap.put('minScore', kycObj1.Min_Score__c);
            kycMap.put('fieldOrder', kycObj1.Field_Order__c);
        Test.startTest();
        KYCRulesMasterService.createKYCRulesMaster(kycMap);
        Test.stopTest();
    }
    
}