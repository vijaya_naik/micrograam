@isTest(seealldata = false)
public class  IncomeDetailServiceTest{
  
    public static testmethod void  incomeTestdata(){
    
       Address__c mgp=new  Address__c();
          mgp.Address_Line_1__c='BDA Complex';
          mgp.Address_Line_2__c='permanentAddressLine2';
          mgp.City__c='permanentAddressCity';
          mgp.Pincode__c=123;
          mgp.State__c='Karnataka';
          mgp.Country__c='India'; 
          mgp.Residing_Since__c=Date.today();
          mgp.House_Ownership__c='Owned';
          insert mgp;
    
      Account acc=new Account();
      acc.Name='Microgram';
      acc.Account_Type__c='Borrower'; 
      insert acc;
      
      Income_Detail__c income=new Income_Detail__c();
      income.Designation__c='Manager';
      income.Office_Address__c=mgp.id;
      income.Annual_Income__c=200000;
      income.Experience_in_Job__c=2;
      income.Industry_Profression__c='devloper';
      income.Occupation__c='joob';
      income.Organization__c='private';
      insert income;
      
      Map<String, Object> incomeMap=new Map<String, Object>();
      incomeMap.put('currentAnnualIncome',200000);
      incomeMap.put('currentDesignation','Manager');
      incomeMap.put('currentExperienceInJob',3.0);
      incomeMap.put('currentIndustryProfession','devloper');
      incomeMap.put('currentOccupation','joob');
      incomeMap.put('currentOrganization','private');
      
      Map<String, Object> incomeMap1=new Map<String, Object>();
      incomeMap1.put('previousAnnualIncome',200000);
      incomeMap1.put('previousDesignation','Manager');
      incomeMap1.put('previousExperienceInJob',3.0);
      incomeMap1.put('previousIndustryProfession','devloper');
      incomeMap1.put('previousOccupation','joob');
      incomeMap1.put('previousOrganization','private');
      
       IncomeDetailService incomeService=new  IncomeDetailService();
       IncomeDetailService.getIncomeDetailById(income.id);
       IncomeDetailService.getCurrentIncomeDetailDenormalized(income);
       IncomeDetailService.getPreviousIncomeDetailDenormalized(income);
       IncomeDetailService.createCurrentIncomeDetail(incomeMap,income);
       IncomeDetailService.createPreviousIncomeDetail(incomeMap1,income);
    
    }

}