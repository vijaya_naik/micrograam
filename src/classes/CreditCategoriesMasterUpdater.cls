//Utility class to do mass updates for Credit Categories Master records when needed
public class CreditCategoriesMasterUpdater {
    public static void updateCreditScoringRuleSet(String oldValue, String newValue) {
        //Get records by existing value
        List<Credit_Categories_Master__c> creditCategories = [
            SELECT Credit_Scoring_Ruleset__c
            FROM Credit_Categories_Master__c 
            WHERE Credit_Categories_Master__c.Credit_Scoring_Ruleset__c = :oldValue
        ];
        //Update to new value
        for ( Credit_Categories_Master__c creditCategory : creditCategories ) {
            creditCategory.Credit_Scoring_Ruleset__c = newValue;
        }
        update creditCategories;
        System.debug('Updated '+creditCategories.size()+' records.');
    }
}