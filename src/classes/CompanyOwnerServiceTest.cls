@isTest(seealldata = false)
public class CompanyOwnerServiceTest{

    public static testmethod void cmpServiceTestMethods(){
    
        Address__c mgp=new  Address__c();
          mgp.Address_Line_1__c='BDA Complex';
          mgp.Address_Line_2__c='permanentAddressLine2';
          mgp.City__c='permanentAddressCity';
          mgp.Pincode__c=123;
          mgp.State__c='Karnataka';
          mgp.Country__c='India'; 
          mgp.Residing_Since__c=Date.today();
          mgp.House_Ownership__c='Owned';
          insert mgp;
          
               
      Account acc=new Account();
      acc.Name='Microgram';
      acc.Account_Type__c='Borrower'; 
      insert acc;
      
      Income_Detail__c income=new Income_Detail__c();
      income.Designation__c='Manager';
      income.Office_Address__c=mgp.id;
      income.Annual_Income__c=200000;
      insert income;
      
      Loan_Application__c loan=new Loan_Application__c();
      loan.Account_Id__c=acc.id;
      loan.Reference_1_Address__c=mgp.id;
      loan.Reference_2_Address__c=mgp.id;
      loan.Income_Detail_Current_Job__c=income.id;
      loan.Income_Detail_Previous_Job__c=income.id;
      insert loan;   
    
       Company_Owner__c  cmpowner= new Company_Owner__c();
       cmpowner.Company_Owner_PAN__c='ABCDE1234Y';
       cmpowner.Loan_Application_Id__c=loan.id;
       cmpowner.Owner_Address__c=mgp.id;
       cmpowner.Ownership_Percentage__c='3';
       cmpowner.Name='Microgram';
       insert cmpowner;
       
        
      Map<String, Company_Owner__c> addressMap=new Map<String, Company_Owner__c>();
          Company_Owner__c mgpAddress=new Company_Owner__c();
          addressMap.put('companyOwnerPAN',mgpAddress);
          addressMap.put('companyOwnerName',mgpAddress);
          addressMap.put('ownershipPercentage',mgpAddress);
          
       CompanyOwnerService ownrService=new CompanyOwnerService();
       CompanyOwnerService.getCompanyOwnerById(cmpowner.id);
       CompanyOwnerService.getCompanyOwnerByLoanId(cmpowner.id);
       CompanyOwnerService.getCompanyOwnerDenormalized(cmpowner);
       CompanyOwnerService.createCompanyOwner(addressMap,cmpowner);
       CompanyOwnerService.getCompanyOwnerDenormalized(loan.id);
       CompanyOwnerService.deleteWithLoanApplicationId(loan.id); 
    
    }
}