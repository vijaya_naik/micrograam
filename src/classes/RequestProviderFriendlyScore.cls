public class RequestProviderFriendlyScore implements IRequestProvider{    
    
    public Object execute(Integration_Message__c message){
        AccessTokenHandlerFriendlyScore accessTokenHandler = new AccessTokenHandlerFriendlyScore();
        
        System.debug('Token before Callout--->'+(String) Cache.Org.get(Label.FriendlyScore));
    
        HttpResponse res = RequestProviderFriendlyScore.doCallout(message);
        
        String responseBody = res.getBody();
        Map<String, Object> resParsed = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
        
        System.debug('Status Code----'+res.getStatusCode());
        System.debug('Error Message----'+(String) resParsed.get('error_description'));
            
        if(res.getStatus() <> 'OK' && res.getStatusCode() == 401){
            accessTokenHandler.storeAccessToken('', Label.FriendlyScore);
            System.debug('Token after Callout1--->'+(String) Cache.Org.get(Label.FriendlyScore));
            res = RequestProviderFriendlyScore.doCallout(message);
            System.debug('Token after Callout2--->'+(String) Cache.Org.get(Label.FriendlyScore));
        }
        
        return res;
    }
    
    
    public static HttpResponse doCallout(Integration_Message__c message){
        AccessTokenHandlerFriendlyScore accessTokenHandler = new AccessTokenHandlerFriendlyScore();
        
        Loan_Application__c application = [SELECT Id, Partner_Id__c FROM Loan_Application__c WHERE Id = :message.Input_String__c];
        
        String token = '';
        if(accessTokenHandler.getAccessToken(Label.FriendlyScore) == null || accessTokenHandler.getAccessToken(Label.FriendlyScore) == ''){
            token = accessTokenHandler.generateAccessToken();
            accessTokenHandler.storeAccessToken(token, Label.FriendlyScore);
        }
        else{
            token = accessTokenHandler.getAccessToken(Label.FriendlyScore);
        }
        
        
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndPoint('https://friendlyscore.com/api/v2/users/partner-id/'+application.Partner_Id__c+'/show.json');
        req.setHeader('Content-Type','application/json');
        req.setHeader('Authorization','Bearer '+token);
        
        return (new Http()).send(req);
    }
}