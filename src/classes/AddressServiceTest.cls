@isTest(seealldata = false)
public class AddressServiceTest {
    
    public static testmethod void addresservicetestMethods(){
        //inserting test data
         Address__c mgp=new  Address__c();
          mgp.Address_Line_1__c='BDA Complex';
          mgp.Address_Line_2__c='permanentAddressLine2';
          mgp.City__c='permanentAddressCity';
          mgp.Pincode__c=123;
          mgp.State__c='Karnataka';
          mgp.Country__c='India'; 
          mgp.Residing_Since__c=Date.today();
          mgp.House_Ownership__c='Owned';
          insert mgp;
        
        Map<String, Address__c> addressMap=new Map<String, Address__c>();
          Address__c mgpAddress=new Address__c();
          addressMap.put('permanentAddressLine1',mgpAddress);
          addressMap.put('permanentAddressLine2',mgpAddress);
          addressMap.put('permanentAddressCity',mgpAddress);
          addressMap.put('permanentAddressState',mgpAddress);
          addressMap.put('permanentAddressCountry',mgpAddress);
          
           Map<String, Address__c> regstaddressMap=new Map<String, Address__c>();
          Address__c resmgpAddress=new Address__c();
          regstaddressMap.put('registeredAddressLine1',resmgpAddress);
          regstaddressMap.put('registeredAddressLine2',resmgpAddress);
          regstaddressMap.put('registeredAddressCity',resmgpAddress);
          regstaddressMap.put('registeredAddressState',resmgpAddress);
          regstaddressMap.put('registeredAddressCountry',resmgpAddress);
          
           Map<String, Address__c> correspondanceaddressMap=new Map<String, Address__c>();
          Address__c corsmgpAddress=new Address__c();
          correspondanceaddressMap.put('correspondenceAddressLine1',corsmgpAddress);
          correspondanceaddressMap.put('correspondenceAddressLine2',corsmgpAddress);
          correspondanceaddressMap.put('correspondenceAddressCity',corsmgpAddress);
          correspondanceaddressMap.put('correspondenceAddressState',corsmgpAddress);
          correspondanceaddressMap.put('correspondenceAddressCountry',corsmgpAddress);
        
        Map<String, Address__c> mngraddressMap=new Map<String, Address__c>();
          Address__c mgnrmgpAddress=new Address__c();
          mngraddressMap.put('managerAddressLine1',mgnrmgpAddress);
          mngraddressMap.put('managerAddressLine2',mgnrmgpAddress);
          mngraddressMap.put('managerAddressCity',mgnrmgpAddress);
          mngraddressMap.put('managerAddressState',mgnrmgpAddress);
          mngraddressMap.put('managerAddressCountry',mgnrmgpAddress);
        
          Map<String, Address__c> ownraddressMap=new Map<String, Address__c>();
          Address__c ownrmgpAddress=new Address__c();
          ownraddressMap.put('ownerAddressLine1',ownrmgpAddress);
          ownraddressMap.put('ownerAddressLine2',ownrmgpAddress);
          ownraddressMap.put('ownerAddressCity',ownrmgpAddress);
          ownraddressMap.put('ownerAddressState',ownrmgpAddress);
          ownraddressMap.put('ownerAddressCountry',ownrmgpAddress);
          
        Map<String, Address__c> curntaddressMap=new Map<String, Address__c>();
          Address__c curntmgpAddress=new Address__c();
          curntaddressMap.put('currentAddressLine1',curntmgpAddress);
          curntaddressMap.put('currentAddressLine2',curntmgpAddress);
          curntaddressMap.put('currentAddressCity',curntmgpAddress);
          curntaddressMap.put('currentAddressState',curntmgpAddress);
          curntaddressMap.put('currentAddressCountry',curntmgpAddress);
          
          Map<String, Address__c> curntoffaddressMap=new Map<String, Address__c>();
          Address__c curntoffmgpAddress=new Address__c();
          curntoffaddressMap.put('currentOfficeAddressLine1',curntoffmgpAddress);
          curntoffaddressMap.put('currentOfficeAddressLine2',curntoffmgpAddress);
          curntoffaddressMap.put('currentOfficeAddressCity',curntoffmgpAddress);
          curntoffaddressMap.put('currentOfficeAddressState',curntoffmgpAddress);
         curntoffaddressMap.put('currentOfficeAddressCountry',curntoffmgpAddress);
         
          Map<String, Address__c> previousoffaddressMap=new Map<String, Address__c>();
          Address__c prevoffmgpAddress=new Address__c();
          previousoffaddressMap.put('previousOfficeAddressLine1',prevoffmgpAddress);
          previousoffaddressMap.put('previousOfficeAddressLine2',prevoffmgpAddress);
          previousoffaddressMap.put('previousOfficeAddressCity',prevoffmgpAddress);
          previousoffaddressMap.put('previousOfficeAddressState',prevoffmgpAddress);
          previousoffaddressMap.put('previousOfficeAddressCountry',prevoffmgpAddress);
          
          Map<String, Address__c> referaddressMap=new Map<String, Address__c>();
          Address__c refmgpAddress=new Address__c();
          referaddressMap.put('reference1AddressLine1',refmgpAddress);
          referaddressMap.put('reference1AddressLine2',refmgpAddress);
          referaddressMap.put('reference1AddressCity',refmgpAddress);
          referaddressMap.put('reference1AddressState',refmgpAddress);
          referaddressMap.put('reference1AddressCountry',refmgpAddress);
          
           Map<String, Address__c> refer1addressMap=new Map<String, Address__c>();
          Address__c ref1mgpAddress=new Address__c();
          refer1addressMap.put('reference2AddressLine1',ref1mgpAddress);
          refer1addressMap.put('reference2AddressLine2',ref1mgpAddress);
          refer1addressMap.put('reference2AddressCity',ref1mgpAddress);
          refer1addressMap.put('reference2AddressState',ref1mgpAddress);
          refer1addressMap.put('reference2AddressCountry',ref1mgpAddress);
        
        
          AddressService adrsservice=new AddressService();
          AddressService.getAddressById(mgp.id);
          AddressService.deleteAddress(mgp.Id);
          AddressService.getPermanentAddressDenormalized(mgp);
          AddressService.getCurrentOfficeAddressDenormalized(mgp);
          AddressService.getRegisteredAddressDenormalized(mgp);
          AddressService.getCorrespondenceAddressDenormalized(mgp);
          AddressService.getPreviousOfficeAddressDenormalized(mgp);
          AddressService.getCurrentAddressDenormalized(mgp);
          AddressService.getManagerAddressDenormalized(mgp);
          AddressService.getOwnerAddressDenormalized(mgp);
          AddressService.getReference1AddressDenormalized(mgp);
          AddressService.getReference2AddressDenormalized(mgp);
          AddressService.createPermanentAddress(addressMap,mgp);
          AddressService.createRegisteredAddress(regstaddressMap,mgp);
          AddressService.createCorrespondenceAddress(correspondanceaddressMap,mgp);
          AddressService.createManagerAddress(mngraddressMap,mgp);
          AddressService.createOwnerAddress(ownraddressMap,mgp);
          AddressService.createCurrentAddress(curntaddressMap,mgp);
          AddressService.createCurrentOfficeAddress(curntoffaddressMap,mgp);
          AddressService.createPreviousOfficeAddress( previousoffaddressMap,mgp);
          AddressService.createReference1Address(referaddressMap,mgp);
          AddressService.createReference2Address(refer1addressMap,mgp);
        
    }

}