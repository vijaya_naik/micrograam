global class BorrowerDTO {
    public Account borrowerAccount { get; set; }
    public Applicant__c applicant { get; set; }
    public Address__c currentAddress { get; set; }
    public Address__c permanentAddress { get; set; }
        public Address__c registeredAddress { get; set; }
    public Address__c correspondenseAddress { get; set; }
}