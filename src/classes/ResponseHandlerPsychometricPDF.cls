public class ResponseHandlerPsychometricPDF implements IResponseHandler {
    public void execute(Object response, Integration_Message__c message)
    {
         HttpResponse resp = (HttpResponse) response;
         System.debug('Psy Response data'+resp);
         String jsonString = resp.getBody();
         System.debug('PDF Body--'+jsonString);
         
         Map<String, Object> obj = (Map<String, Object>) JSON.deserializeUntyped(jsonString);
         
         Map<String, Object> callbackObj = (Map<String, Object>) obj.get('callbackObj');
         Attachment attmnt = new Attachment();
         attmnt.ParentId = message.Input_String__c;
         attmnt.Name = (String)callbackObj.get('filename');
         attmnt.Description = 'Fetched From Psychometric';
         attmnt.Body = EncodingUtil.base64Decode((String)callbackObj.get('file'));
         attmnt.ContentType = 'application/pdf';
         
         delete [SELECT Id FROM Attachment WHERE ParentId = :message.Input_String__c AND Description = 'Fetched From Psychometric'];
         
         insert attmnt;
         
         Loan_Application_Attachment__c loanAppAttachment = new Loan_Application_Attachment__c(
             Attachment_Id__c = attmnt.Id,
             Loan_Application_Id__c = message.Input_String__c,
             Attachment_Type__c = 'MetrixZen',
             Custom_External_Id__c = message.Input_String__c + 'Psychometric PDF Integration'
         );
         
         upsert loanAppAttachment Custom_External_Id__c;
    }
}