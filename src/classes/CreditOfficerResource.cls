@RestResource(urlMapping='/v1/creditofficer/*')
global with sharing class CreditOfficerResource {
    @HttpGet
    global static void  getData() {
        RestRequest req = RestContext.request;
        // Grab the type from the end of the URL
        List<String> tokens = req.requestURI.split('/');
        String type = tokens[tokens.size()-1];
        System.debug('Received Get Data request for type=' + type);
        RestResponse res = RestContext.response;
    	if (res == null) {
        	res = new RestResponse();
        	RestContext.response = res;
    	}
        
        if ( 'loanapplication'.equalsIgnoreCase(type) ) {
            // Get request parameters
            String kycStatus = req.params.get('kycStatus');
            String creditCheckStatus = req.params.get('creditCheckStatus');
            String applicantType=req.params.get('applicantType');          
              System.debug('Received LoanApplications request for kycStatus=' + kycStatus + '::creditCheckStatus=' + creditCheckStatus);
            // Invoke the service
            Map<String, Map<String,Object>> returnMap =CreditOfficerService.getLoanApplicationList(kycStatus, creditCheckStatus);
            List<String> remlist=new List<String>();
            if(applicantType!=null){
                for (Id key : returnMap.keySet()) {
    				Map<String,Object> row = returnMap.get(key);
                    System.debug('need to check values..'+row.get('applicantType')+'  '+applicantType );
                    if(!applicantType.equalsIgnoreCase(String.valueOf(row.get('applicantType')))) {
                        remlist.add(key);
                    }
                }
            }
            for(integer i=0;i<remlist.size();i++){
                returnMap.remove(remlist.get(i));
            }
            res.responseBody = Blob.valueOf(JSON.serialize(returnMap.values()));
        	res.statusCode = 200;
        }else  if ( 'applicationKYC'.equalsIgnoreCase(type) ) {
            Map<String, Map<String,String>> returnMap = new Map<String, Map<String,String>>();
            // Get request parameters
            String loanApplicationId = req.params.get('loanApplicationId');
            String lenderId = req.params.get('lenderId');
            if ( loanApplicationId != null ) {
                System.debug('Received request for applicationKYCs for loanApplicationId=' + loanApplicationId);
                returnMap = ApplicationKYCService.getApplicationKYCs(loanApplicationId);
            } else if ( lenderId != null ) {
                System.debug('Received request for applicationKYCs for lenderId=' + lenderId);
                returnMap = ApplicationKYCService.getApplicationKYCsForLender(lenderId);
            }
            res.responseBody = Blob.valueOf(JSON.serialize(returnMap.values()));
        	res.statusCode = 200;
        }else  if ( 'creditBureau'.equalsIgnoreCase(type) ) {
            // Get request parameters
            String loanApplicationId = req.params.get('loanApplicationId');
            System.debug('Received  request for CreditBureau=' + loanApplicationId );
            // Invoke the service
            Map<String, Map<String,String>> returnMap = CreditBureauService.getCreditBureaus(loanApplicationId);
            res.responseBody = Blob.valueOf(JSON.serialize(returnMap.values()));
        	res.statusCode = 200;
        }else  if ( 'psychometric'.equalsIgnoreCase(type) ) {
            // Get request parameters
            String loanApplicationId = req.params.get('loanApplicationId');
            System.debug('Received  request for Psychometrics=' + loanApplicationId );
            // Invoke the service
            Map<String, Map<String,String>> returnMap = PsychometricService.getPsychometrics(loanApplicationId);
            res.responseBody = Blob.valueOf(JSON.serialize(returnMap.values()));
        	res.statusCode = 200;
        }else  if ( 'socialMedia'.equalsIgnoreCase(type) ) {
            // Get request parameters
            String loanApplicationId = req.params.get('loanApplicationId');
            System.debug('Received  request for applicationKYCs=' + loanApplicationId );
            // Invoke the service
            Map<String, Map<String,String>> returnMap = SocialMediaService.getSocialMedias(loanApplicationId);
            res.responseBody = Blob.valueOf(JSON.serialize(returnMap.values()));
        	res.statusCode = 200;
        }else if ('approve'.equalsIgnoreCase(type)){
            String loanApplicationId = req.params.get('loanApplicationId');
            String lenderId = req.params.get('lenderId');
            String approvalType=req.params.get('approvalType'); //can be 'kyc' or 'creditcheck'
            String status='Pass';
            String decisionTakenBy = req.params.get('decisionTakenBy');
            if ( loanApplicationId != null ) {
                if('kyc'.equalsIgnoreCase(approvalType)){
                    CreditOfficerService.updateKYCStatus(loanApplicationId, status);
                }else if('creditcheck'.equalsIgnoreCase(approvalType)){
                    CreditOfficerService.updateCreditCheckStatus(loanApplicationId, status, 'Approved', decisionTakenBy);
                }
            } else if ( lenderId != null ) {
                if('kyc'.equalsIgnoreCase(approvalType)) {
                    CreditOfficerService.updateKYCStatusForLender(lenderId, status);
                }
            }
       }else if ('reject'.equalsIgnoreCase(type)){
            String loanApplicationId = req.params.get('loanApplicationId');
           	String lenderId = req.params.get('lenderId');
	        String approvalType=req.params.get('approvalType'); //can be 'kyc' or 'creditcheck'
            String status='Fail';
            String decisionTakenBy = req.params.get('decisionTakenBy');
           	if ( loanApplicationId != null ) {
                if('kyc'.equalsIgnoreCase(approvalType)){
                    CreditOfficerService.updateKYCStatus(loanApplicationId, status);
                }else if('creditcheck'.equalsIgnoreCase(approvalType)){
                    CreditOfficerService.updateCreditCheckStatus(loanApplicationId, status, 'Rejected', decisionTakenBy);
                }
            } else if ( lenderId != null ) {
                if('kyc'.equalsIgnoreCase(approvalType)) {
                    CreditOfficerService.updateKYCStatusForLender(lenderId, status);
                }
            }
       }else if('categoryScore'.equalsIgnoreCase(type)){
            String loanApplicationId = req.params.get('loanApplicationId');
			//get all Categories..
			Map<String,Map<String,String>> allcategories=CreditCategoriesMasterService.getCreditCategories(loanApplicationId);
           System.debug('size of allcaterories map'+allcategories.size());
           for ( String category : allcategories.keyset() ) {
               Integer score=CreditScoreService.getCreditScore(category, 'category', loanApplicationId);
               Map<String,String> categorymap= allcategories.get(category);
               categorymap.put('score',String.valueOf(score));
               allcategories.put(category,categorymap);
           }
            res.responseBody = Blob.valueOf(JSON.serialize(allcategories.values()));
        	res.statusCode = 200;
       }else if('kycScore'.equalsIgnoreCase(type)){
           List<Map<String,String>> retval= new List<Map<String,String>>();
           String loanApplicationId = req.params.get('loanApplicationId');
           String lenderId = req.params.get('lenderId');
           Map<String,KYC_Rules_Master__c> rules=KYCRulesMasterService.getKYCRules();
           List<Application_KYC_Score__c> scores=new List<Application_KYC_Score__c>();
           if ( loanApplicationId != null ) {
           	   scores=KYCScoreService.getKYCScore(loanApplicationId);
           } else if ( lenderId != null ) {
               scores=KYCScoreService.getKYCScoreForLender(lenderId);
           }
           for(Integer i=0;i<scores.size();i++){
               Map<String,String> val=KYCScoreService.getKYCScoreDenormalized(scores.get(i));
               val.putAll(KYCRulesMasterService.getKYCRulesMasterDenormalized(rules.get(val.get('fieldId'))));
               retval.add(val);
           }
            res.responseBody = Blob.valueOf(JSON.serialize(retval));
        	res.statusCode = 200;

    }else if('criteriaScore'.equalsIgnoreCase(type)){
            String loanApplicationId = req.params.get('loanApplicationId');
        	String category=req.params.get('categoryId');
			//get all Categories..
            res.responseBody = Blob.valueOf(JSON.serialize(CreditOfficerService.getCriteriaScore( category, loanApplicationId)));
        	res.statusCode = 200;
    }else if('creditScore'.equalsIgnoreCase(type)){
                    String loanApplicationId = req.params.get('loanApplicationId');
			//get all Categories..
			List<Map<String,Object>> retVal= new List<Map<String,Object>>();
			Map<String,Map<String,String>> allcategories=CreditCategoriesMasterService.getCreditCategories();
           System.debug('size of allcaterories map'+allcategories.size());
           for ( String category : allcategories.keyset() ) {
               Integer score=CreditScoreService.getCreditScore(category, 'category', loanApplicationId);
               Map<String,String> categorymap= allcategories.get(category);
               categorymap.put('score',String.valueOf(score));
               if(score!=null) retVal.add(categorymap);
               List<Map<String,Object>> criteriascore=CreditOfficerService.getCriteriaScore(categorymap.get('categoryId'),loanApplicationId);
               System.debug('size of criteria list got for category'+categorymap.get('categoryId')+criteriascore.size());
               if(criteriascore.size()>0) 
                   retVal.addAll(criteriascore);
               
           }
            res.responseBody = Blob.valueOf(JSON.serialize(retVal));
        	res.statusCode = 200;
    } else if ( 'lender'.equalsIgnoreCase(type) ) {
        // Get request parameters
        String kycStatus = req.params.get('kycStatus');
        System.debug('Received Lenders request for kycStatus=' + kycStatus);
        // Invoke the service
        Map<String, Map<String,Object>> returnMap = CreditOfficerService.getLendersList(kycStatus);
        res.responseBody = Blob.valueOf(JSON.serialize(returnMap.values()));
        res.statusCode = 200;
    }

  }
}