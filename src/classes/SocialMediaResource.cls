@RestResource(urlMapping='/v1/socialMedia')
global with sharing class SocialMediaResource {
    @HttpPost
    global static Map<String,String> cSocialMedia() {
        RestRequest req = RestContext.request;
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received SocialMedia Registration request:: ' + params);
        // Invoke service
        return SocialMediaService.createSocialMedia(params);
    }
    
    @HttpGet
    global static Map<String,String> getSocialMedia() {
        // Get id/accountId from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
        String loanApplicationId = req.params.get('loanApplicationId');
        String source=req.params.get('source');
        if ( loanApplicationId != null && source!=null) {
            // Invoke service to get by loanApplicationId 
            System.debug('Received  Get request for loanApplicationId=' + loanApplicationId);
            id= SocialMediaService.getSocialMedia(loanApplicationId, source).Id;
        }
        // Invoke service to get by Loan Application id
        System.debug('Received SocialMedia Get request for id=' + id);
        return SocialMediaService.getSocialMediaDenormalized(SocialMediaService.getSocialMedia(id));
        
    }
    
    @HttpDelete
    global static void deleteSocialMedia() {
        // Get id from request parameter
        RestRequest req = RestContext.request;
        String id = req.params.get('id');
        System.debug('Received SocialMedia Delete request for id=' + id);
        // Invoke service
        SocialMediaService.deleteSocialMedia(id);
    }
    
    @HttpPatch
    global static Map<String,String> updateSocialMedia() {
        RestRequest req = RestContext.request;
        // Get id from request parameter
        String id = req.params.get('id');
                String loanApplicationId = req.params.get('loanApplicationId');
        String source=req.params.get('source');
        if ( loanApplicationId != null && source!=null) {
            // Invoke service to get by loanApplicationId 
            System.debug('Received  Get request for loanApplicationId=' + loanApplicationId);
            id= SocialMediaService.getSocialMedia(loanApplicationId, source).Id;
                }
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.tostring());
        System.debug('Received SocialMedia Update request for id=' + id + '::' + params);
        // Invoke service
        return SocialMediaService.updateSocialMedia(id, params);
    }

}