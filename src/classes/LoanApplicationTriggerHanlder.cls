public class LoanApplicationTriggerHanlder{
    
    public static void onAfterInsert(List<Loan_Application__c> applicationList){
        LoanApplicationTriggerHanlder.getSocialMediaScoreOnInsert(applicationList);
        LoanApplicationTriggerHanlder.getPsychometricOnInsert(applicationList);

    }
    
    
    
    public static void onAfterUpdate(List<Loan_Application__c> applicationList, Map<Id, Loan_Application__c> applicationMap){
        LoanApplicationTriggerHanlder.getSocialMediaScoreOnUpdate(applicationList, applicationMap);
        LoanApplicationTriggerHanlder.getPsychometricOnUpdate(applicationList, applicationMap);
    }
    
    
    public static void getSocialMediaScoreOnInsert(List<Loan_Application__c> applicationList){
        List<Integration_Message__c> messageList = new List<Integration_Message__c>();
        IntegrationMessageProvider__c messageProvider = IntegrationMessageProvider__c.getAll().get(Label.FriendlyScore);
        
        for(Loan_Application__c application :applicationList){
            if(application.FriendlyScore_Submitted__c == TRUE){
                messageList.add(IntegrationMessageHandler.getIntegrationMessages(application.Id, messageProvider));
            }
        }
        
        if(messageList.size() > 0){
            insert messageList;
        }
    }


     public static void getPsychometricOnInsert(List<Loan_Application__c> applicationList){
        List<Integration_Message__c> messageList = new List<Integration_Message__c>();
        IntegrationMessageProvider__c messageProvider = IntegrationMessageProvider__c.getAll().get(Label.Psychometric);
        IntegrationMessageProvider__c messageProviderPDF = IntegrationMessageProvider__c.getAll().get(Label.PsychometricPDF);
        
        for(Loan_Application__c application :applicationList){
            if(application.Psychometric_Survey_Submitted__c == TRUE){
                messageList.add(IntegrationMessageHandler.getIntegrationMessages(application.Id, messageProvider));
                messageList.add(IntegrationMessageHandler.getIntegrationMessages(application.Id, messageProviderPDF));
            }
        }
        
        if(messageList.size() > 0){
            insert messageList;
        }
    }
    
    
    
    public static void getSocialMediaScoreOnUpdate(List<Loan_Application__c> applicationList, Map<Id, Loan_Application__c> applicationMap){
        List<Integration_Message__c> messageList = new List<Integration_Message__c>();
        IntegrationMessageProvider__c messageProvider = IntegrationMessageProvider__c.getAll().get(Label.FriendlyScore);
        
        for(Loan_Application__c application :applicationList){
            if(
                application.FriendlyScore_Submitted__c <> applicationMap.get(application.Id).FriendlyScore_Submitted__c && 
                application.FriendlyScore_Submitted__c == TRUE
            ){
                messageList.add(IntegrationMessageHandler.getIntegrationMessages(application.Id, messageProvider));
            }
        }
        
        if(messageList.size() > 0){
            insert messageList;
        }
    }


     public static void getPsychometricOnUpdate(List<Loan_Application__c> applicationList, Map<Id, Loan_Application__c> applicationMap){
        List<Integration_Message__c> messageList = new List<Integration_Message__c>();
        IntegrationMessageProvider__c messageProvider = IntegrationMessageProvider__c.getAll().get(Label.Psychometric);
        IntegrationMessageProvider__c messageProviderPDF = IntegrationMessageProvider__c.getAll().get(Label.PsychometricPDF);
        
        for(Loan_Application__c application :applicationList){
            if(
                application.Psychometric_Survey_Submitted__c <> applicationMap.get(application.Id).Psychometric_Survey_Submitted__c && 
                application.Psychometric_Survey_Submitted__c == TRUE
            ){
                messageList.add(IntegrationMessageHandler.getIntegrationMessages(application.Id, messageProvider));
                messageList.add(IntegrationMessageHandler.getIntegrationMessages(application.Id, messageProviderPDF));
            }
        }
        
        if(messageList.size() > 0){
            insert messageList;
        }
    }
}