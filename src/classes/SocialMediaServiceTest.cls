@isTest(seealldata = false)
public class SocialMediaServiceTest{

  public static testmethod void SocialMediaServiceTest1(){
      //create dummy Address Record
       Address__c addres =new  Address__c();
       addres.Address_Line_1__c='Street 1';
       addres.Address_Line_2__c='Street 2';
       addres.City__c='Bangalore ';
       addres.Pincode__c= 12345;
       addres.State__c='KA';
       addres.Country__c='India'; 
       addres.Residing_Since__c=System.today();
       addres.House_Ownership__c='Owned';
       insert addres;
          
       //create dummy account record
       Account acc=new Account();
       acc.Name='Microgram';
       acc.Account_Type__c='Borrower'; 
       insert acc;
       
       //create dummy Income Detail Record
       Income_Detail__c incomeDetail = new Income_Detail__c();
       incomeDetail.Designation__c='Manager';
       incomeDetail.Office_Address__c=addres.id;
       incomeDetail.Annual_Income__c=30000;
       insert incomeDetail;
       
       //create dummmy Loan Application Record.
       Loan_Application__c loanApp = new Loan_Application__c();
       loanApp.Account_Id__c=acc.id;
       loanApp.Reference_1_Address__c=addres.id;
       loanApp.Reference_2_Address__c=addres.id;
       loanApp.Income_Detail_Current_Job__c=incomeDetail.id;
       loanApp.Income_Detail_Previous_Job__c=incomeDetail.id;
       insert loanApp;   
       
       //create dummy Application_Social_Media__c  record.
       Application_Social_Media__c  appSocialMedia = new Application_Social_Media__c ();
       appSocialMedia.Loan_Application_Id__c = loanApp.Id;
       appSocialMedia.Social_Media_Score__c = 'test';
       appSocialMedia.Source__c = 'Vendor';
       insert appSocialMedia;
       
       Map<String, Object> paramMap = new Map<String, Object>();
       paramMap.put('socialMediaScore','socialMediaScore');
       paramMap.put('source','Vendor');
       paramMap.put('loanApplicationId',loanApp.id);
    
       //call class methods
       SocialMediaService mediaService = new  SocialMediaService();
       SocialMediaService.getSocialMedia(appSocialMedia.Id);
       SocialMediaService.getSocialMedia(loanApp.Id,'Vendor');
       SocialMediaService.getSocialMedias(loanApp.Id);
       SocialMediaService.createSocialMedia(paramMap);
       SocialMediaService.createSocialMediaInstance(paramMap,appSocialMedia);
       SocialMediaService.getSocialMediaDenormalized(appSocialMedia);
       SocialMediaService.updateSocialMedia(appSocialMedia.Id,paramMap);
       SocialMediaService.deleteSocialMedia(appSocialMedia.Id);
       SocialMediaService.deleteSocialMediaData(loanApp.Id);
   
  }
  
}