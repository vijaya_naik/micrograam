public class ResponseHandlerFriendlyScore implements IResponseHandler{
    public void execute(Object response, Integration_Message__c message){
        HttpResponse resp = (HttpResponse) response;
        System.debug('respnse---'+resp);
        System.debug('respnse body---'+resp.getBody());
        System.debug('status---'+resp.getStatus());
        
        
        
        String jsonString = resp.getBody();
        Map<String, Object> obj = (Map<String, Object>) JSON.deserializeUntyped(jsonString);
        
        Application_Social_Media__c socialMedia = new Application_Social_Media__c();
        
        socialMedia.Social_Media_Score__c = String.valueOf(obj.get('score_points'));
        String percentage = (String) obj.get('score');
        socialMedia.Social_Media_Score_Percentage__c = Decimal.valueOf(percentage.subString(0,percentage.length()-1));
        socialMedia.Source__c = 'FriendlyScore';
        socialMedia.Loan_Application_Id__c = message.Input_String__c;
        socialMedia.Custom_External_Id__c = message.Input_String__c + 'FriendlyScore Integration';

        upsert socialMedia Custom_External_Id__c;
    }
}