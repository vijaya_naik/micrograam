@isTest (SeeAllData=false)
private class IntegrationMessageTest{
    static testMethod void testOne(){
        Integration_Message__c message = new Integration_Message__c(
            Request_Provider__c = 'RequestProviderTest',
            Response_Handler__c = 'ResponseHandlerTest',
            Input_String__c = ''
        );
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new IntegrationMessageTestMock());
        insert message;
        Test.stopTest();
    }
    
    
    static testMethod void testTwo(){
        Integration_Message__c message = new Integration_Message__c(
            Request_Provider__c = 'RequestProviderTest',
            Response_Handler__c = 'ResponseHandlerTest',
            Input_String__c = ''
        );
        
        Test.startTest();
        insert message;
        Test.stopTest();
    }
    
    
    static testMethod void testThree(){
        IntegrationMessageProvider__c messageProvider = new IntegrationMessageProvider__c(
            Request_Provider__c = 'RequestProviderTest',
            Response_Handler__c = 'ResponseHandlerTest',
            No_of_Retries__c = 5
        );
        
        Test.startTest();
        IntegrationMessageHandler.getIntegrationMessages('', messageProvider);
        Test.stopTest();
    }
    
    
    /*static testMethod void testRetry(){
        Integration_Message__c message = new Integration_Message__c(
            Request_Provider__c = 'RequestProviderTest',
            Response_Handler__c = 'ResponseHandlerTest',
            Max_No_of_Retries__c = 5,
            Input_String__c = ''
        );
        
        Test.setMock(HttpCalloutMock.class, new IntegrationMessageTestMock());
        insert message;
        
        message.Max_No_of_Retries__c = 6;
        message.No_of_Retries__c = 2;
        message.Status__c = 'Failure';
        message.Stop_Retry__c = FALSE;
        update message;
        
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        
        Test.startTest();
        System.schedule('ScheduleApexClassTest', CRON_EXP, new IntegrationMessageReTryScheduler());
        Test.stopTest();
    }*/
    
    
    static testMethod void testBatch(){
        Integration_Message__c message = new Integration_Message__c(
            Request_Provider__c = 'RequestProviderTest',
            Response_Handler__c = 'ResponseHandlerTest',
            Input_String__c = ''
        );   
        insert message;     
        
        Test.startTest();
        String messageCode = [SELECT Message_Code__c FROM Integration_Message__c WHERE Id = :message.Id].Message_Code__c;
        Database.executeBatch(new IntegrationMessageProessBatch(messageCode),1);
        Test.stopTest();
    }
    
    
    static testMethod void testRetry2(){
        Integration_Message__c message = new Integration_Message__c(
            Request_Provider__c = 'RequestProviderTest',
            Response_Handler__c = 'ResponseHandlerTest',
            Input_String__c = ''
        );
        Test.setMock(HttpCalloutMock.class, new IntegrationMessageTestMock());
        insert message;
        
        Test.startTest();
        IntegrationMessageHandler.retry(message);
        Test.stopTest();
    }
}