@isTest
public class AdminService_Test {
    static testmethod void testAdServices() {
        Test.startTest();
            Account tempAccount= new Account();    
            tempAccount.Name = 'XXX';
            insert tempAccount;
            Address__c m1= new Address__c();
            m1.State__c='1';
            m1.Address_Line_1__c='aa';
            m1.Address_Line_2__c='aac';
            m1.City__c='qq';
            m1.Country__c='aa';
            m1.Pincode__c=111111.10;
            m1.House_Ownership__c='rented';
            m1.Residing_Since__c=Date.today();
            insert m1;
            
           Address__c mgp=new  Address__c();
           mgp.Address_Line_1__c='BDA Complex';
           mgp.Address_Line_2__c='permanentAddressLine2';
           mgp.City__c='permanentAddressCity';
           mgp.Pincode__c=123;
           mgp.State__c='Karnataka';
           mgp.Country__c='India'; 
           mgp.Residing_Since__c=Date.today();
           mgp.House_Ownership__c='Owned';
          insert mgp;
          
            Applicant__c m=new Applicant__c();
            m.Current_Address__c=m1.Id;
            m.Account_Id__c=tempAccount.Id;
            m.Registered_Address__c=m1.Id;
            m.Permanent_Address__c=mgp.Id;
            m.Correspondence_Address__c=m1.Id;
            m.E_Mail__c='ajeet.kumar6171@gmail.com';
            insert m;
        
            Loan_Application__c mm = new Loan_Application__c();
            mm.Account_Id__c=tempAccount.Id;
            insert mm;
            //Caliing first Method 
            try{
                AdminService.deleteAllDataByAccountId(tempAccount.Id);
            }
            catch(Exception e){}
        Test.stopTest();
    }
}