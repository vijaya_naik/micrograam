public class AddressService {
    public static Address__c getAddressById(String addressId) {
        Address__c address = [
            SELECT Id, Address_Line_1__c, Address_Line_2__c, City__c,
            	   Pincode__c, State__c, Residing_Since__c,House_Ownership__c,Country__c
            FROM Address__c 
            WHERE Id = :addressId
        ];
        return address;
    }
     public static void deleteAddress(String id) {
        Address__c address=getAddressById(id);
        //Delete from database
        Savepoint sp = Database.setSavePoint();
        try {
            if(address!=null)     delete address;

        } catch (Exception e) {
            Database.rollback(sp); //Rollback in case of error
            throw e;
        }
    }
    public static Map<String,String> getPermanentAddressDenormalized(Address__c permanentAddress) {
        Map<String,String> addressMap = new Map<String,String>();
        
        if (permanentAddress.Address_Line_1__c != null) addressMap.put('permanentAddressLine1', permanentAddress.Address_Line_1__c);
        if (permanentAddress.Address_Line_2__c != null) addressMap.put('permanentAddressLine2', permanentAddress.Address_Line_2__c);
        if (permanentAddress.City__c != null) addressMap.put('permanentAddressCity', permanentAddress.City__c);
        if (permanentAddress.Pincode__c != null) addressMap.put('permanentAddressPincode', String.valueOf(permanentAddress.Pincode__c));
        if (permanentAddress.State__c != null) addressMap.put('permanentAddressState', permanentAddress.State__c);
        if (permanentAddress.Country__c != null) addressMap.put('permanentAddressCountry', permanentAddress.Country__c);

        return addressMap;
    }
   public static Map<String,String> getCurrentOfficeAddressDenormalized(Address__c currentOfficeAddress) {
        Map<String,String> addressMap = new Map<String,String>();
        
        if (currentOfficeAddress.Address_Line_1__c != null) addressMap.put('currentOfficeAddressLine1', currentOfficeAddress.Address_Line_1__c);
        if (currentOfficeAddress.Address_Line_2__c != null) addressMap.put('currentOfficeAddressLine2', currentOfficeAddress.Address_Line_2__c);
        if (currentOfficeAddress.City__c != null) addressMap.put('currentOfficeAddressCity', currentOfficeAddress.City__c);
        if (currentOfficeAddress.Pincode__c != null) addressMap.put('currentOfficeAddressPincode', String.valueOf(currentOfficeAddress.Pincode__c));
        if (currentOfficeAddress.State__c != null) addressMap.put('currentOfficeAddressState', currentOfficeAddress.State__c);
        if (currentOfficeAddress.Country__c != null) addressMap.put('currentOfficeAddressCountry', currentOfficeAddress.Country__c);

        return addressMap;
    }
    public static Map<String,String> getRegisteredAddressDenormalized(Address__c registeredAddress) {
    Map<String,String> addressMap = new Map<String,String>();
    
    if (registeredAddress.Address_Line_1__c != null) addressMap.put('registeredAddressLine1', registeredAddress.Address_Line_1__c);
    if (registeredAddress.Address_Line_2__c != null) addressMap.put('registeredAddressLine2', registeredAddress.Address_Line_2__c);
    if (registeredAddress.City__c != null) addressMap.put('registeredAddressCity', registeredAddress.City__c);
    if (registeredAddress.Pincode__c != null) addressMap.put('registeredAddressPincode', String.valueOf(registeredAddress.Pincode__c));
    if (registeredAddress.State__c != null) addressMap.put('registeredAddressState', registeredAddress.State__c);
    if (registeredAddress.Country__c != null) addressMap.put('registeredAddressCountry', registeredAddress.Country__c);
    return addressMap;
    }
    public static Map<String,String> getCorrespondenceAddressDenormalized(Address__c correspondenceAddress) {
    Map<String,String> addressMap = new Map<String,String>();
    
    if (correspondenceAddress.Address_Line_1__c != null) addressMap.put('correspondenceAddressLine1', correspondenceAddress.Address_Line_1__c);
    if (correspondenceAddress.Address_Line_2__c != null) addressMap.put('correspondenceAddressLine2', correspondenceAddress.Address_Line_2__c);
    if (correspondenceAddress.City__c != null) addressMap.put('correspondenceAddressCity', correspondenceAddress.City__c);
    if (correspondenceAddress.Pincode__c != null) addressMap.put('correspondenceAddressPincode', String.valueOf(correspondenceAddress.Pincode__c));
    if (correspondenceAddress.State__c != null) addressMap.put('correspondenceAddressState', correspondenceAddress.State__c);
    if (correspondenceAddress.Country__c != null) addressMap.put('correspondenceAddressCountry', correspondenceAddress.Country__c);
    return addressMap;
}
       public static Map<String,String> getPreviousOfficeAddressDenormalized(Address__c previousOfficeAddress) {
        Map<String,String> addressMap = new Map<String,String>();
        
        if (previousOfficeAddress.Address_Line_1__c != null) addressMap.put('previousOfficeAddressLine1', previousOfficeAddress.Address_Line_1__c);
        if (previousOfficeAddress.Address_Line_2__c != null) addressMap.put('previousOfficeAddressLine2', previousOfficeAddress.Address_Line_2__c);
        if (previousOfficeAddress.City__c != null) addressMap.put('previousOfficeAddressCity', previousOfficeAddress.City__c);
        if (previousOfficeAddress.Pincode__c != null) addressMap.put('previousOfficeAddressPincode', String.valueOf(previousOfficeAddress.Pincode__c));
        if (previousOfficeAddress.State__c != null) addressMap.put('previousOfficeAddressState', previousOfficeAddress.State__c);
         if (previousOfficeAddress.Country__c != null) addressMap.put('previousOfficeAddressCountry', previousOfficeAddress.Country__c);       
        return addressMap;
    }
    public static Map<String,String> getCurrentAddressDenormalized(Address__c currentAddress) {
        Map<String,String> addressMap = new Map<String,String>();
        
        if (currentAddress.Address_Line_1__c != null) addressMap.put('currentAddressLine1', currentAddress.Address_Line_1__c);
        if (currentAddress.Address_Line_2__c != null) addressMap.put('currentAddressLine2', currentAddress.Address_Line_2__c);
        if (currentAddress.City__c != null) addressMap.put('currentAddressCity', currentAddress.City__c);
        if (currentAddress.Pincode__c != null) addressMap.put('currentAddressPincode', String.valueOf(currentAddress.Pincode__c));
        if (currentAddress.State__c != null) addressMap.put('currentAddressState', currentAddress.State__c);
        if (currentAddress.Residing_Since__c != null) addressMap.put('currentAddressResidingSince', String.valueOf(currentAddress.Residing_Since__c));
        if (currentAddress.House_Ownership__c != null) addressMap.put('currentAddressHouseOwnership', String.valueOf(currentAddress.House_Ownership__C));
        if (currentAddress.Country__c != null) addressMap.put('currentAddressCountry', currentAddress.Country__c);

        return addressMap;
    }
        public static Map<String,String> getManagerAddressDenormalized(Address__c managerAddress) {
        Map<String,String> rmanagerAddress = new Map<String,String>();
        
        if (managerAddress.Address_Line_1__c != null) rmanagerAddress.put('managerAddressLine1', managerAddress.Address_Line_1__c);
        if (managerAddress.Address_Line_2__c != null) rmanagerAddress.put('managerAddressLine2', managerAddress.Address_Line_2__c);
        if (managerAddress.City__c != null) rmanagerAddress.put('managerAddressCity', managerAddress.City__c);
        if (managerAddress.Pincode__c != null) rmanagerAddress.put('managerAddressPincode', String.valueOf(managerAddress.Pincode__c));
        if (managerAddress.State__c != null) rmanagerAddress.put('managerAddressState', managerAddress.State__c);
        if (managerAddress.Country__c != null) rmanagerAddress.put('managerAddressCountry', managerAddress.Country__c);
            
        //if (managerAddress.Residing_Since__c != null) rmanagerAddress.put('managerAddressResidingSince', String.valueOf(managerAddress.Residing_Since__c));
        
        return rmanagerAddress;
    }
    
public static Map<String,String> getOwnerAddressDenormalized(Address__c ownerAddress) {
    Map<String,String> rownerAddress = new Map<String,String>();
    System.debug('in get owner address denormalized'+ownerAddress);
    if (ownerAddress.Address_Line_1__c != null) rownerAddress.put('ownerAddressLine1', ownerAddress.Address_Line_1__c);
    if (ownerAddress.Address_Line_2__c != null) rownerAddress.put('ownerAddressLine2', ownerAddress.Address_Line_2__c);
    if (ownerAddress.City__c != null) rownerAddress.put('ownerAddressCity', ownerAddress.City__c);
    if (ownerAddress.Pincode__c != null) rownerAddress.put('ownerAddressPincode', String.valueOf(ownerAddress.Pincode__c));
    if (ownerAddress.State__c != null) rownerAddress.put('ownerAddressState', ownerAddress.State__c);
    if (ownerAddress.Country__c != null) rownerAddress.put('ownerAddressCountry', ownerAddress.Country__c);

    //if (ownerAddress.Residing_Since__c != null) rownerAddress.put('ownerAddressResidingSince', String.valueOf(ownerAddress.Residing_Since__c));
    
    return rownerAddress;
}
    
    public static Map<String,String> getReference1AddressDenormalized(Address__c reference1Address) {
        Map<String,String> addressMap = new Map<String,String>();
        
        if (reference1Address.Address_Line_1__c != null) addressMap.put('reference1AddressLine1', reference1Address.Address_Line_1__c);
        if (reference1Address.Address_Line_2__c != null) addressMap.put('reference1AddressLine2', reference1Address.Address_Line_2__c);
        if (reference1Address.City__c != null) addressMap.put('reference1AddressCity', reference1Address.City__c);
        if (reference1Address.Pincode__c != null) addressMap.put('reference1AddressPincode', String.valueOf(reference1Address.Pincode__c));
        if (reference1Address.State__c != null) addressMap.put('reference1AddressState', reference1Address.State__c);
        if (reference1Address.Country__c != null) addressMap.put('reference1AddressCountry', reference1Address.Country__c);
        return addressMap;
    }
    public static Map<String,String> getReference2AddressDenormalized(Address__c reference2Address) {
        Map<String,String> addressMap = new Map<String,String>();
        
        if (reference2Address.Address_Line_1__c != null) addressMap.put('reference2AddressLine1', reference2Address.Address_Line_1__c);
        if (reference2Address.Address_Line_2__c != null) addressMap.put('reference2AddressLine2', reference2Address.Address_Line_2__c);
        if (reference2Address.City__c != null) addressMap.put('reference2AddressCity', reference2Address.City__c);
        if (reference2Address.Pincode__c != null) addressMap.put('reference2AddressPincode', String.valueOf(reference2Address.Pincode__c));
        if (reference2Address.State__c != null) addressMap.put('reference2AddressState', reference2Address.State__c);
        if (reference2Address.Country__c != null) addressMap.put('reference2AddressCountry', reference2Address.Country__c);
        return addressMap;
    }
    public static Address__c createPermanentAddress(Map<String, Object> params, Address__c permanentAddress) {
        boolean hasValue =false;
        if (permanentAddress == null)
        	permanentAddress = new Address__c(); //For new record creation
        
        if (params.containsKey('permanentAddressLine1')) {
        	permanentAddress.Address_Line_1__c = String.valueOf(params.get('permanentAddressLine1'));
        	hasValue=true;
        }
        if (params.containsKey('permanentAddressLine2')) {
            permanentAddress.Address_Line_2__c = String.valueOf(params.get('permanentAddressLine2'));
            hasValue=true;
        }
        if (params.containsKey('permanentAddressCity') && !'-1'.equals(String.valueOf(params.get('permanentAddressCity'))) ) {
            permanentAddress.City__c = String.valueOf(params.get('permanentAddressCity'));
            hasValue=true;
        }
        if (params.containsKey('permanentAddressPincode') && params.get('permanentAddressPincode') != null) {
            permanentAddress.Pincode__c = Integer.valueOf(params.get('permanentAddressPincode'));
            hasValue=true;
        }
        if (params.containsKey('permanentAddressState') && !'-1'.equals(String.valueOf(params.get('permanentAddressState'))) ) {
            permanentAddress.State__c = String.valueOf(params.get('permanentAddressState'));
            hasValue=true;
        }
         if (params.containsKey('permanentAddressCountry') && !'-1'.equals(String.valueOf(params.get('permanentAddressCountry'))) ) {
            permanentAddress.Country__c = String.valueOf(params.get('permanentAddressCountry'));
            hasValue=true;
        }           
        if(params.containsKey('permanentAddressResidingSince') && params.get('permanentAddressResidingSince') != null){
            permanentAddress.Residing_Since__c=Date.valueOf(String.valueOf(params.get('permanentAddressResidingSince')));
            hasValue=true;
        }
        if(hasValue)
			return permanentAddress;
        else return null;
    }
    public static Address__c createRegisteredAddress(Map<String, Object> params, Address__c registeredAddress) {
    boolean hasValue=false;
    if (registeredAddress == null)
    	registeredAddress = new Address__c(); //For new record creation
    
    if (params.containsKey('registeredAddressLine1')) {
    	registeredAddress.Address_Line_1__c = String.valueOf(params.get('registeredAddressLine1'));
        hasValue=true;
    }
     	if (params.containsKey('registeredAddressLine2')) {
        	registeredAddress.Address_Line_2__c = String.valueOf(params.get('registeredAddressLine2'));
        	hasValue=true;
    	}
        if (params.containsKey('registeredAddressCity')&& !'-1'.equals(String.valueOf(params.get('registeredAddressCity'))) ) {
        registeredAddress.City__c = String.valueOf(params.get('registeredAddressCity'));
        hasValue=true;
        }
        if (params.containsKey('registeredAddressPincode') && params.get('registeredAddressPincode') != null) {
        	registeredAddress.Pincode__c = Integer.valueOf(params.get('registeredAddressPincode'));
            hasValue=true;
        }
        
        if (params.containsKey('registeredAddressState')&& !'-1'.equals(String.valueOf(params.get('registeredAddressState'))) ) {
        	registeredAddress.State__c = String.valueOf(params.get('registeredAddressState'));
            hasValue=true;
        }
         if (params.containsKey('registeredAddressCountry')&& !'-1'.equals(String.valueOf(params.get('registeredAddressCountry'))) ) {
        	registeredAddress.Country__c = String.valueOf(params.get('registeredAddressCountry'));
            hasValue=true;
        }    
		if(hasValue)
			return registeredAddress;
        else return null;
}
    public static Address__c createCorrespondenceAddress(Map<String, Object> params, Address__c correspondenceAddress) {
	boolean hasValue=false;
    if (correspondenceAddress == null)
    	correspondenceAddress = new Address__c(); //For new record creation
    
        if (params.containsKey('correspondenceAddressLine1')){ 
    	correspondenceAddress.Address_Line_1__c = String.valueOf(params.get('correspondenceAddressLine1'));
            hasValue=true;
        }
        if (params.containsKey('correspondenceAddressLine2')) {
        correspondenceAddress.Address_Line_2__c = String.valueOf(params.get('correspondenceAddressLine2'));
            hasValue=true;
        }
        
        if (params.containsKey('correspondenceAddressCity')&& !'-1'.equals(String.valueOf(params.get('correspondenceAddressCity'))) ) {
        correspondenceAddress.City__c = String.valueOf(params.get('correspondenceAddressCity'));
            hasValue=true;
        }
        
        if (params.containsKey('correspondenceAddressPincode') && params.get('correspondenceAddressPincode') != null) {
        correspondenceAddress.Pincode__c = Integer.valueOf(params.get('correspondenceAddressPincode'));
            hasValue=true;
        }
        
        if (params.containsKey('correspondenceAddressState')&& !'-1'.equals(String.valueOf(params.get('correspondenceAddressState'))) ) {
        correspondenceAddress.State__c = String.valueOf(params.get('correspondenceAddressState'));
            hasValue=true;
        }
         if (params.containsKey('correspondenceAddressCountry')&& !'-1'.equals(String.valueOf(params.get('correspondenceAddressCountry'))) ) {
        correspondenceAddress.Country__c = String.valueOf(params.get('correspondenceAddressCountry'));
            hasValue=true;
        }
        if(params.containsKey('correspondenceAddressResidingSince') && params.get('correspondenceAddressResidingSince') != null){
        correspondenceAddress.Residing_Since__c=Date.valueOf(String.valueOf(params.get('correspondenceAddressResidingSince')));
    hasValue=true;
        }
    if (hasValue)
		return correspondenceAddress;
    else return null;
}
    public static Address__c createManagerAddress(Map<String, Object> params, Address__c managerAddress) {
    boolean hasValue=true;
    if (managerAddress == null)
    	managerAddress = new Address__c(); //For new record creation
    
        if (params.containsKey('managerAddressLine1')) {
        managerAddress.Address_Line_1__c = String.valueOf(params.get('managerAddressLine1'));
        hasValue=true;
        }
        if (params.containsKey('managerAddressLine2')) {
        managerAddress.Address_Line_2__c = String.valueOf(params.get('managerAddressLine2'));
            hasValue=true;
        }
        
        if (params.containsKey('managerAddressCity')&& !'-1'.equals(String.valueOf(params.get('managerAddressCity'))) ) {
        managerAddress.City__c = String.valueOf(params.get('managerAddressCity'));
            hasValue=true;
        }
        
        if (params.containsKey('managerAddressPincode') && params.get('managerAddressPincode') != null) {
        managerAddress.Pincode__c = Integer.valueOf(String.valueOf(params.get('managerAddressPincode')));
hasValue=true;
        }        
        if (params.containsKey('managerAddressState')&& !'-1'.equals(String.valueOf(params.get('managerAddressState'))) ) {
        managerAddress.State__c = String.valueOf(params.get('managerAddressState'));
        hasValue=true;
        }
        if (params.containsKey('managerAddressCountry')&& !'-1'.equals(String.valueOf(params.get('managerAddressCountry'))) ) {
        managerAddress.Country__c = String.valueOf(params.get('managerAddressCountry'));
        hasValue=true;
        }
        if (params.containsKey('managerAddressResidingSince') && params.get('managerAddressResidingSince') != null) {
        managerAddress.Residing_Since__c = Date.valueOf(String.valueOf(params.get('managerAddressResidingSince')));
        hasValue=true;
        }
    
        if (hasValue)
	    	return managerAddress;
        else return null;
    
}
    public static Address__c createOwnerAddress(Map<String, Object> params, Address__c ownerAddress) {
        boolean hasValue=false;
    if (ownerAddress == null)
    	ownerAddress = new Address__c(); //For new record creation
    System.debug('in create owner Address'+ownerAddress);
        if (params.containsKey('ownerAddressLine1')) {
        ownerAddress.Address_Line_1__c = String.valueOf(params.get('ownerAddressLine1'));
        hasValue=true;
        }
        if (params.containsKey('ownerAddressLine2')) {
        ownerAddress.Address_Line_2__c = String.valueOf(params.get('ownerAddressLine2'));
hasValue=true;
        }        
        if (params.containsKey('ownerAddressCity')){ 
        ownerAddress.City__c = String.valueOf(params.get('ownerAddressCity'));
        hasValue=true;
        }
        if (params.containsKey('ownerAddressPincode') && params.get('ownerAddressPincode') != null) {
        ownerAddress.Pincode__c = Integer.valueOf(String.valueOf(params.get('ownerAddressPincode')));
        hasValue=true;
        }
        
        if (params.containsKey('ownerAddressState')&& !'-1'.equals(String.valueOf(params.get('ownerAddressState'))) ) {
        ownerAddress.State__c = String.valueOf(params.get('ownerAddressState'));
        hasValue=true;
        }
        if (params.containsKey('ownerAddressCountry')&& !'-1'.equals(String.valueOf(params.get('ownerAddressCountry'))) ) {
        ownerAddress.Country__c = String.valueOf(params.get('ownerAddressCountry'));
        hasValue=true;
        }
        if (params.containsKey('ownerAddressResidingSince') && params.get('ownerAddressResidingSince') != null) {
        ownerAddress.Residing_Since__c = Date.valueOf(String.valueOf(params.get('ownerAddressResidingSince')));
        hasValue=true;
        }
    
            if (hasValue)
       return ownerAddress;
        else return null;
}
    public static Address__c createCurrentAddress(Map<String, Object> params, Address__c currentAddress) {
        boolean hasValue=false;
        if (currentAddress == null)
        	currentAddress = new Address__c(); //For new record creation
        
        if (params.containsKey('currentAddressLine1')) {
            currentAddress.Address_Line_1__c = String.valueOf(params.get('currentAddressLine1'));
            hasValue=true;
        }
        if (params.containsKey('currentAddressLine2')) {
            currentAddress.Address_Line_2__c = String.valueOf(params.get('currentAddressLine2'));
            hasValue=true;
        }
        if (params.containsKey('currentAddressCity')&& !'-1'.equals(String.valueOf(params.get('currentAddressCity'))) ) {
            currentAddress.City__c = String.valueOf(params.get('currentAddressCity'));
hasValue=true;
        }
        if (params.containsKey('currentAddressPincode') && params.get('currentAddressPincode') != null) {
            currentAddress.Pincode__c = Integer.valueOf(String.valueOf(params.get('currentAddressPincode')));
            hasValue=true;
        }
        if (params.containsKey('currentAddressState')&& !'-1'.equals(String.valueOf(params.get('currentAddressState'))) ) {
            currentAddress.State__c = String.valueOf(params.get('currentAddressState'));
            hasValue=true;
        }
        if (params.containsKey('currentAddressCountry')&& !'-1'.equals(String.valueOf(params.get('currentAddressCountry'))) ) {
            currentAddress.Country__c = String.valueOf(params.get('currentAddressCountry'));
            hasValue=true;
        }
        if (params.containsKey('currentAddressResidingSince') && params.get('currentAddressResidingSince') != null){ 
            currentAddress.Residing_Since__c = Date.valueOf(String.valueOf(params.get('currentAddressResidingSince')));
            hasValue=true;
        }
        if (params.containsKey('currentAddressHouseOwnership') && params.get('currentAddressHouseOwnership') != null){ 
            currentAddress.House_Ownership__c = String.valueOf(params.get('currentAddressHouseOwnership'));
            hasValue=true;
        }
        
            if (hasValue)
	        	return currentAddress;
        else return null;
    }
    
    public static Address__c createCurrentOfficeAddress(Map<String, Object> params, Address__c currentOfficeAddress) {
        boolean hasValue=false;
        if (currentOfficeAddress == null)
        	currentOfficeAddress = new Address__c(); //For new record creation
        
        if (params.containsKey('currentOfficeAddressLine1')){ 
            currentOfficeAddress.Address_Line_1__c = String.valueOf(params.get('currentOfficeAddressLine1'));
            hasValue=true;
        }
        if (params.containsKey('currentOfficeAddressLine2')) {
            currentOfficeAddress.Address_Line_2__c = String.valueOf(params.get('currentOfficeAddressLine2'));
hasValue=true;
        }            
        if (params.containsKey('currentOfficeAddressCity')&& !'-1'.equals(String.valueOf(params.get('currentOfficeAddressCity'))) ) {
            currentOfficeAddress.City__c = String.valueOf(params.get('currentOfficeAddressCity'));
            hasValue=true;
        }
        if (params.containsKey('currentOfficeAddressPincode') && params.get('currentOfficeAddressPincode') != null) {
            currentOfficeAddress.Pincode__c = Integer.valueOf(String.valueOf(params.get('currentOfficeAddressPincode')));
            hasValue=true;
        }
        if (params.containsKey('currentOfficeAddressState')&& !'-1'.equals(String.valueOf(params.get('currentOfficeAddressState'))) ) {
            currentOfficeAddress.State__c = String.valueOf(params.get('currentOfficeAddressState'));
            hasValue=true;
        }
        if (params.containsKey('currentOfficeAddressCountry')&& !'-1'.equals(String.valueOf(params.get('currentOfficeAddressCountry'))) ) {
            currentOfficeAddress.Country__c = String.valueOf(params.get('currentOfficeAddressCountry'));
            hasValue=true;
        }
       
            if (hasValue)
	        	return currentOfficeAddress;
        	else return null;
    }
    
    public static Address__c createPreviousOfficeAddress(Map<String, Object> params, Address__c previousOfficeAddress) {
        boolean hasValue=false;
        if (previousOfficeAddress == null)
        	previousOfficeAddress = new Address__c(); //For new record creation
        
        if (params.containsKey('previousOfficeAddressLine1')) {
            previousOfficeAddress.Address_Line_1__c = String.valueOf(params.get('previousOfficeAddressLine1'));
            hasValue=true;
        }
        if (params.containsKey('previousOfficeAddressLine2')) {
            previousOfficeAddress.Address_Line_2__c = String.valueOf(params.get('previousOfficeAddressLine2'));
            hasValue=true;
        }
        if (params.containsKey('previousOfficeAddressCity')&& !'-1'.equals(String.valueOf(params.get('previousOfficeAddressCity'))) ) {
            previousOfficeAddress.City__c = String.valueOf(params.get('previousOfficeAddressCity'));
            hasValue=true;
        }
        if (params.containsKey('previousOfficeAddressPincode') && params.get('previousOfficeAddressPincode') != null) {
            previousOfficeAddress.Pincode__c = Integer.valueOf(String.valueOf(params.get('previousOfficeAddressPincode')));
            hasValue=true;
        }
        if (params.containsKey('previousOfficeAddressState')&& !'-1'.equals(String.valueOf(params.get('previousOfficeAddressState'))) ) {
            previousOfficeAddress.State__c = String.valueOf(params.get('previousOfficeAddressState'));
            hasValue=true;
        }
       if (params.containsKey('previousOfficeAddressCountry')&& !'-1'.equals(String.valueOf(params.get('previousOfficeAddressCountry'))) ) {
            previousOfficeAddress.Country__c = String.valueOf(params.get('previousOfficeAddressCountry'));
            hasValue=true;
        }
            if (hasValue)
	        	return previousOfficeAddress;
            else return null;
       
    }
    public static Address__c createReference1Address(Map<String, Object> params, Address__c reference1Address) {
        boolean hasValue=false;
        if (reference1Address == null)
        	reference1Address = new Address__c(); //For new record creation
        
        if (params.containsKey('reference1AddressLine1')) {
            reference1Address.Address_Line_1__c = String.valueOf(params.get('reference1AddressLine1'));
            hasValue=true;
        }
        if (params.containsKey('reference1AddressLine2')) {
            reference1Address.Address_Line_2__c = String.valueOf(params.get('reference1AddressLine2'));
            hasValue=true;
        }
        if (params.containsKey('reference1AddressCity')&& !'-1'.equals(String.valueOf(params.get('reference1AddressCity'))) ) {
            reference1Address.City__c = String.valueOf(params.get('reference1AddressCity'));
            hasValue=true;
        }
        if (params.containsKey('reference1AddressPincode') && params.get('reference1AddressPincode') != null) {
            reference1Address.Pincode__c = Integer.valueOf(String.valueOf(params.get('reference1AddressPincode')));
            hasValue=true;
        }
        if (params.containsKey('reference1AddressState')&& !'-1'.equals(String.valueOf(params.get('reference1AddressState'))) ) {
            reference1Address.State__c = String.valueOf(params.get('reference1AddressState'));
            hasValue=true;
        }
       if (params.containsKey('reference1AddressCountry')&& !'-1'.equals(String.valueOf(params.get('reference1AddressCountry'))) ) {
            reference1Address.Country__c = String.valueOf(params.get('reference1AddressCountry'));
            hasValue=true;
        }
        if (hasValue){
                    System.debug('returning the refrence1 address'+reference1Address);

	        	return reference1Address;
        }else return null;
       
    }
    
    
public static Address__c createReference2Address(Map<String, Object> params, Address__c reference2Address) {
        boolean hasValue=false;
        if (reference2Address == null)
        	reference2Address = new Address__c(); //For new record creation
        
        if (params.containsKey('reference2AddressLine1')) {
            reference2Address.Address_Line_1__c = String.valueOf(params.get('reference2AddressLine1'));
            hasValue=true;
        }
        if (params.containsKey('reference2AddressLine2')) {
            reference2Address.Address_Line_2__c = String.valueOf(params.get('reference2AddressLine2'));
            hasValue=true;
        }
        if (params.containsKey('reference2AddressCity')&& !'-1'.equals(String.valueOf(params.get('reference2AddressCity'))) ) {
            reference2Address.City__c = String.valueOf(params.get('reference2AddressCity'));
            hasValue=true;
        }
        if (params.containsKey('reference2AddressPincode') && params.get('reference2AddressPincode') != null) {
            reference2Address.Pincode__c = Integer.valueOf(String.valueOf(params.get('reference2AddressPincode')));
            hasValue=true;
        }
        if (params.containsKey('reference2AddressState')&& !'-1'.equals(String.valueOf(params.get('reference2AddressState'))) ) {
            reference2Address.State__c = String.valueOf(params.get('reference2AddressState'));
            hasValue=true;
        }
       if (params.containsKey('reference2AddressCountry')&& !'-1'.equals(String.valueOf(params.get('reference2AddressCountry'))) ) {
            reference2Address.Country__c = String.valueOf(params.get('reference2AddressCountry'));
            hasValue=true;
        }
    if (hasValue){
               System.debug('returning the refrence2 address'+reference2Address);

	        	return reference2Address;
    }
            else return null;
       
    }
}