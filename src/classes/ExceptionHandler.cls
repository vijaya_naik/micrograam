public with sharing class ExceptionHandler {
    public static void saveExceptionLog(Exception exptn, String message,  String className, String methodName){
        Exception_Log__c log = new Exception_Log__c();
        log.Class_Name__c = className ; 
        log.Custom_Message__c = message;
        log.Method_Name__c = methodName ; 
        log.Stack_Trace__c = exptn.getStackTraceString(); 
        log.Error_Message__c = exptn.getMessage(); 
        log.Error_Type__c = exptn.getTypeName(); 
        log.Line_Number__c = exptn.getLineNumber();                        
        
        insert log;
    }
}