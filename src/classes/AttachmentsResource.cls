@RestResource(urlMapping='/v1/attachments')
global with sharing class AttachmentsResource {
    @HttpGet
    global static List<Map<String,String>> getAttachmentList() {
        List<Map<String,String>> attachmentList = new List<Map<String,String>>();
        RestRequest req = RestContext.request;
        // Get accountId from request parameter
        String accountId = req.params.get('accountId');
        // Get loanApplicationId from request parameter
        String loanApplicationId = req.params.get('loanApplicationId');
        // Invoke service
        if ( accountId != null )
        	attachmentList = AttachmentService.getAttachmentsByAccountId(accountId);
        else if ( loanApplicationId != null )
            attachmentList = AttachmentService.getAttachmentsByLoanApplicationId(loanApplicationId);
        
        return attachmentList;
    }
}