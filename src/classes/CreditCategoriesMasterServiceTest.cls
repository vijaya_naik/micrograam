@isTest(seealldata = false)
public class CreditCategoriesMasterServiceTest {
    
    public static testmethod void creditMasterMethods(){
    
       Credit_Categories_Master__c credMstr= new Credit_Categories_Master__c();
        credMstr.Credit_Scoring_Ruleset__c='Individual - Personal Loan';
        credMstr.Category_Name__c='xyz';
        credMstr.Category_Order__c=4;
        credMstr.Category_Weight__c=3;
        insert credMstr;
        
       Credit_Categories_Master__c credMstr1= new Credit_Categories_Master__c();
        credMstr.Credit_Scoring_Ruleset__c='Individual - Personal Loan';
        credMstr1.Category_Name__c='pqrs';
        credMstr1.Category_Order__c=4;
        credMstr1.Category_Weight__c=3;
        insert credMstr1;
        
       Address__c mgp=new  Address__c();
          mgp.Address_Line_1__c='BDA Complex';
          mgp.Address_Line_2__c='permanentAddressLine2';
          mgp.City__c='permanentAddressCity';
          mgp.Pincode__c=123;
          mgp.State__c='Karnataka';
          mgp.Country__c='India'; 
          mgp.Residing_Since__c=Date.today();
          mgp.House_Ownership__c='Owned';
          insert mgp;
          
               
      Account acc=new Account();
      acc.Name='Microgram';
      acc.Account_Type__c='Borrower'; 
      insert acc;
      
      Income_Detail__c income=new Income_Detail__c();
      income.Designation__c='Manager';
      income.Office_Address__c=mgp.id;
      income.Annual_Income__c=200000;
      insert income;
      
      Loan_Application__c loan=new Loan_Application__c();
      loan.Account_Id__c=acc.id;
      loan.Reference_1_Address__c=mgp.id;
      loan.Reference_2_Address__c=mgp.id;
      loan.Income_Detail_Current_Job__c=income.id;
      loan.Income_Detail_Previous_Job__c=income.id;
      insert loan;  
      
       Map<String, Object> mapaddress=new Map<String, Object>();
        mapaddress.put('creditScoringRuleset','Individual - Personal Loan');
        mapaddress.put('categoryName','123');
        mapaddress.put('categoryWeight','3');
        mapaddress.put('categoryOrder','345');
        
        CreditCategoriesMasterService crdtsrvc=new CreditCategoriesMasterService();
        CreditCategoriesMasterService.getCreditCategoriesMaster(credMstr.id);
        CreditCategoriesMasterService.getCreditCategories(loan.id);
        CreditCategoriesMasterService.getCreditCategories();
        CreditCategoriesMasterService.createCreditCategoriesMaster(mapaddress);
        CreditCategoriesMasterService.deleteCreditCategoriesMaster(credMstr.id);
    
        CreditCategoriesMasterService.updateCreditCategoriesMaster(credMstr1.id,mapaddress);
    }

}